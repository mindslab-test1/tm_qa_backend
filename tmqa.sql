create table COMMON_CODE_TB
(
    CODE    varchar(10) not null
        primary key,
    CODE_NM varchar(10) null
);

create table CS_DTC_DTL_TB
(
    DTCT_DTC_NO        varchar(5)    not null comment '탐지사전번호',
    DTCT_DTC_GRP_NO    varchar(5)    not null comment '탐지사전그룹번호',
    DTCT_DTC_GRP_IN_NO varchar(5)    not null comment '탐지사전그룹내순서',
    DTCT_DTC_ED_NO     varchar(5)    null comment '탐지사전끝번호',
    DTCT_DTC_CON       varchar(4000) null comment '탐지사전내용',
    UPDATOR_ID         varchar(100)  null comment '수정자아이디',
    UPDATED_TM         datetime      null comment '수정일시',
    CREATOR_ID         varchar(100)  null comment '생성자아이디',
    CREATED_TM         datetime      null comment '생성일시',
    primary key (DTCT_DTC_NO, DTCT_DTC_GRP_NO, DTCT_DTC_GRP_IN_NO)
)
    comment 'CS 탐지사전 상세 테이블';

create table CS_KWD_DTCT_RST_TB
(
    REC_KEY     varchar(100)  not null comment '녹취키',
    REC_ID      varchar(100)  not null comment '녹취ID',
    REC_FILE_NM varchar(300)  not null comment '녹취파일명',
    STMT_NO     varchar(5)    not null comment '문장번호',
    CS_LCTG_CD  varchar(10)   not null comment 'CS 대분류 코드',
    DTCT_DTC_NO varchar(5)    not null comment '탐지사전번호',
    CS_KWD      varchar(1000) null comment 'CS 키워드',
    UPDATOR_ID  varchar(100)  null comment '수정자아이디',
    UPDATED_TM  datetime      null comment '수정일시',
    CREATOR_ID  varchar(100)  null comment '생성자아이디',
    CREATED_TM  datetime      null comment '생성일시',
    primary key (REC_KEY, REC_ID, REC_FILE_NM, STMT_NO, CS_LCTG_CD, DTCT_DTC_NO)
)
    comment 'CS 키워드 탐지 결과테이블';

create table CS_KWD_LIST_TB
(
    DTCT_DTC_NO varchar(5)    not null comment '탐지사전번호'
        primary key,
    CS_KWD      varchar(1000) null comment 'CS 키워드',
    UPDATOR_ID  varchar(100)  null comment '수정자아이디',
    UPDATED_TM  datetime      null comment '수정일시',
    CREATOR_ID  varchar(100)  null comment '생성자아이디',
    CREATED_TM  datetime      null comment '생성일시'
)
    comment 'CS 키워드 리스트 테이블';

create table CS_KWD_RNK_TB
(
    REC_KEY     varchar(100)  not null comment '녹취키',
    REC_ID      varchar(100)  not null comment '녹취ID',
    REC_FILE_NM varchar(300)  not null comment '녹취파일명',
    KWD_RNK     varchar(5)    not null comment '키워드순위',
    CS_KWD      varchar(1000) null comment 'CS 키워드',
    UPDATOR_ID  varchar(100)  null comment '수정자아이디',
    UPDATED_TM  datetime      null comment '수정일시',
    CREATOR_ID  varchar(100)  null comment '생성자아이디',
    CREATED_TM  datetime      null comment '생성일시',
    primary key (REC_KEY, REC_ID, REC_FILE_NM, KWD_RNK)
)
    comment 'CS 키워드 순위 테이블';

create table CS_LCTG_CD_TB
(
    CS_LCTG_CD varchar(10)  not null comment 'CS 대분류 코드'
        primary key,
    CS_LCTG_NM varchar(100) null comment 'CS 대분류명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment 'CS 대분류 코드 테이블';

create table CS_LCTG_INFO_TB
(
    CS_LCTG_CD   varchar(10)  not null comment 'CS 대분류 코드',
    CS_APP_ORD   int          not null comment 'CS 적용차수',
    CS_APP_ST_DT varchar(8)   null comment 'CS 적용시작일자',
    CS_APP_ED_DT varchar(8)   null comment 'CS 적용종료일자',
    USED_YN      varchar(1)   null comment '사용여부',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATED_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (CS_LCTG_CD, CS_APP_ORD)
)
    comment 'CS 대분류 정보 테이블';

create table CS_LCTG_KWD_TB
(
    CS_LCTG_CD     varchar(10)  not null comment 'CS 대분류 코드',
    CS_APP_ORD     int          not null comment 'CS 적용차수',
    DTCT_DTC_NO    varchar(5)   not null comment '탐지사전번호',
    CS_DTCT_TRG_CD varchar(5)   null comment 'CS 탐지대상코드',
    CS_CUSL_SCR    varchar(5)   null comment 'CS 상담자점수',
    CS_CUST_SCR    varchar(5)   null comment 'CS 고객점수',
    UPDATOR_ID     varchar(100) null comment '수정자아이디',
    UPDATED_TM     datetime     null comment '수정일시',
    CREATOR_ID     varchar(100) null comment '생성자아이디',
    CREATED_TM     datetime     null comment '생성일시',
    primary key (CS_LCTG_CD, CS_APP_ORD, DTCT_DTC_NO)
)
    comment 'CS 대분류 키워드 테이블';

create table CS_STT_TB
(
    CS_PK              int         not null,
    CNSL_DTM           datetime    null,
    CS_TLNO_CTT        varchar(10) null,
    CNSL_CS_NAM        varchar(10) null,
    CNSR_NAM           varchar(10) null,
    CNSL_EMPNO         varchar(10) null,
    COCL_SCTN_COD      varchar(10) null,
    COCL_SCTN_NAM      varchar(10) null,
    OUB_BZ_SCTN_COD    varchar(10) null,
    OUB_BZ_SCTN_NAM    varchar(10) null,
    CNSL_BZ_LCLS_COD   varchar(10) null,
    CNSL_BZ_LCLS_NAM   varchar(10) null,
    CNSL_BZ_MCLSF_COD  varchar(10) null,
    CNSL_BZ_MCLSF_NAM  varchar(10) null,
    CNSL_BZ_SCLSF_COD  varchar(10) null,
    CNSL_BZ_SCLSF_NAM  varchar(10) null,
    CNSL_KEYWORD       varchar(10) null,
    MAX_SILENCE        varchar(10) null,
    RECG_ID_CTT        varchar(10) null,
    CNSL_PK            varchar(10) null,
    PROG_STAT_CD       varchar(10) null,
    CNSL_CRNC_RST_NAM2 varchar(10) null,
    CMPT_YN            varchar(10) null,
    INS_LON_CTR_NUM    varchar(10) null,
    ROWNUM             int         not null
        primary key
);

create table QA_ANS_DTL_TB
(
    ANS_CD     varchar(10)  not null comment '답변코드',
    ANS_SEQ    int          not null comment '답변순서',
    ANS_CONT   varchar(300) null comment '답변내용',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시',
    primary key (ANS_CD, ANS_SEQ)
)
    comment '답변 상세 테이블';

create table QA_ANS_TB
(
    ANS_CD     varchar(10)  not null comment '답변코드'
        primary key,
    ANS_NM     varchar(100) null comment '답변명',
    ANS_DESC   varchar(300) null comment '답변설명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '답변 테이블';

create table QA_AUDIT_QUESTION_TB
(
    NSPL_NO    varchar(12)                            not null comment '증권번호',
    QNA_NO     int auto_increment comment '질의응답 순번',
    USER_ID    varchar(30)                            not null comment '질의 회원아이디',
    QST_TYPE   varchar(12)                            not null comment '질의코드',
    CONTENT    text                                   not null comment '내용',
    IS_DELETED tinyint(1) default 0                   not null comment '삭제여부',
    CREATOR_ID varchar(100)                           not null comment '등록자 아이디',
    CREATED_TM datetime   default current_timestamp() not null comment '등록일시',
    UPDATOR_ID varchar(100)                           null comment '수정자 아이디',
    UPDATED_TM datetime                               null comment '수정일시',
    primary key (QNA_NO, NSPL_NO)
)
    comment 'QA심사 - 질의응답 - 질의';

create table QA_AUDIT_ANSWER_TB
(
    NSPL_NO    varchar(12)                            null comment '증권번호',
    QNA_NO     int                                    null comment '질의응답 순번',
    ANSWER_NO  int auto_increment comment '답변 순번'
        primary key,
    USER_ID    varchar(30)                            not null comment '답변 회원아이디',
    ANS_TYPE   varchar(12)                            not null comment '답변코드',
    CONTENT    text                                   not null comment '내용',
    IS_DELETED tinyint(1) default 0                   not null comment '삭제여부',
    CREATOR_ID varchar(100)                           not null comment '등록자 아이디',
    CREATED_TM datetime   default current_timestamp() not null comment '등록일시',
    UPDATOR_ID varchar(100)                           null comment '수정자 아이디',
    UPDATED_TM datetime                               null comment '수정일시',
    constraint Q_A_A_fk_Q_A_Q
        foreign key (QNA_NO, NSPL_NO) references QA_AUDIT_QUESTION_TB (QNA_NO, NSPL_NO)
            on update cascade on delete set null
)
    comment 'QA심사 - 질의응답 - 답변';

create index QA_AUDIT_ANSWER_index_01
    on QA_AUDIT_ANSWER_TB (ANS_TYPE);

create index QA_AUDIT_QUESTION_index_01
    on QA_AUDIT_QUESTION_TB (QST_TYPE);

create table QA_AUTO_ALC_CNT_TB
(
    AUTO_ALC_CD  varchar(10)  not null comment '자동할당코드'
        primary key,
    AUTO_ALC_CNT int          null comment '자동 할당 건수',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시'
)
    comment '자동 할당 건수 테이블';

create table QA_AUTO_ALC_DTCT_TB
(
    CHK_CTG_CD varchar(5)   not null comment '평가구분코드',
    CHK_ITM_CD varchar(10)  not null comment '평가항목코드',
    CHK_CTG_NM varchar(100) null comment '평가구분명',
    CHK_ITM_NM varchar(100) null comment '평가항목명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시',
    primary key (CHK_CTG_CD, CHK_ITM_CD)
)
    comment '자동 할당 탐지 테이블';

create table QA_AUTO_ALC_LOG_TB
(
    TRG_LOG_TM varchar(8)   not null comment '''로그생성일자''',
    TRG_CTG    varchar(3)   not null comment '변경 대상',
    CHG_CONT   varchar(200) null comment '변경 상세 내역',
    WORKER_ID  varchar(100) null comment '변경자 ID',
    UPDATOR_ID varchar(100) null comment '수정자 ID',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자 ID',
    CREATED_TM datetime     null comment '생성일시',
    primary key (TRG_LOG_TM, TRG_CTG)
)
    comment '자동 할당 로그 테이블';

create table QA_AUTO_ALC_PROD_TB
(
    PROD_CAT   varchar(10)  not null comment '보종구분',
    PROD_CD    varchar(10)  not null comment '상품코드',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시',
    primary key (PROD_CAT, PROD_CD)
)
    comment '자동 할당 상품 테이블';

create table QA_AUTO_ALC_SCR_TB
(
    TA_SCR_OVER_VAL varchar(5)   not null comment 'TA점수이상값',
    TA_SCR_BEL_VAL  varchar(5)   not null comment 'TA점수이하값',
    UPDATOR_ID      varchar(100) null comment '수정자아이디',
    UPDATED_TM      datetime     null comment '수정일시',
    CREATOR_ID      varchar(100) null comment '생성자아이디',
    CREATED_TM      datetime     null comment '생성일시',
    primary key (TA_SCR_OVER_VAL, TA_SCR_BEL_VAL)
)
    comment '자동 할당 점수 테이블';

create table QA_AUTO_ALC_TMR_TB
(
    SAES_CNTR_CD varchar(10)  not null comment '센터코드',
    SAES_PRT_CD  varchar(10)  not null comment '실코드',
    SAES_EMNO    varchar(8)   not null comment '상담원사번',
    SAES_EMNM    varchar(30)  null comment '상담원명',
    CNTR_NM      varchar(100) null comment '센터명',
    PRT_NME      varchar(100) null comment '실명',
    SEL_YN       varchar(1)   null comment '선택여부',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (SAES_CNTR_CD, SAES_PRT_CD, SAES_EMNO)
)
    comment '자동 할당 상담사 테이블';

create table QA_AUT_MGT_TB
(
    AUT_CD     varchar(20)  not null comment '권한코드'
        primary key,
    AUT_NM     varchar(300) null comment '권한명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '권한 관리 테이블';

create table QA_AUT_PAGE_FUNC_TB
(
    AUT_CD     varchar(20)  not null,
    PAGE_CD    varchar(20)  not null,
    FUNC_CD    varchar(50)  not null,
    UPDATOR_ID varchar(100) null,
    UPDATED_TM datetime     null,
    CREATOR_ID varchar(100) null,
    CREATED_TM datetime     null,
    primary key (AUT_CD, PAGE_CD, FUNC_CD)
);

create table QA_AUT_PAGE_TB
(
    AUT_CD     varchar(20)  not null comment '권한코드',
    PAGE_CD    varchar(20)  not null comment '화면코드',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시',
    primary key (AUT_CD, PAGE_CD)
)
    comment '화면 권한 테이블';

create table QA_CAL_INFO_TB
(
    REC_KEY          varchar(100) not null comment '녹취키',
    REC_ID           varchar(100) not null comment '녹취ID',
    REC_FILE_NM      varchar(300) not null comment '녹취파일명',
    REC_COMP_CD      varchar(2)   null comment '녹취업체코드',
    REC_BIZ_CD       varchar(2)   null comment '녹취업무코드',
    REC_BIZ_TYPE     varchar(2)   null comment '녹취업무유형',
    PROG_STAT_CD     varchar(10)  null comment '진행상태코드',
    PROG_STAT_DTL_CD varchar(10)  null comment '진행상태상세코드',
    REC_ST_DT        varchar(10)  null comment '녹취시작일자',
    REC_ST_TM        varchar(10)  null comment '녹취시작시간',
    REC_ED_TM        varchar(10)  null comment '녹취종료시간',
    REC_DU_TM        varchar(5)   null comment '녹취통화시간',
    CONV_SVR_NM      varchar(30)  null comment '녹취변환서버명',
    CONV_ST_TM       datetime     null comment '녹취변환시작일시',
    CONV_COM_TM      datetime     null comment '녹취변환완료일시',
    CONV_LOC_PATH    varchar(300) null comment '녹취변환위치경로',
    CONV_FILE_NM     varchar(300) null comment '녹취변환파일명',
    REC_TRANS_TM     datetime     null comment '녹취전송일시',
    REC_FILE_EXT     varchar(5)   null comment '녹취파일확장자',
    STEREO_SPLIT_YN  varchar(1)   null comment '스테레오분할여부',
    CHN_CLA_CD       varchar(1)   null comment '채널구분코드',
    STT_PROC_SVR     varchar(30)  null comment 'STT처리서버명',
    STT_PROC_SP      varchar(10)  null comment 'STT처리속도',
    STT_ST_TM        datetime     null comment 'STT시작일시',
    STT_COM_TM       datetime     null comment 'STT완료일시',
    TA_PROC_SVR      varchar(30)  null comment 'TA처리서버명',
    C_VOIC_SP        varchar(10)  null comment '클라이언트음성속도',
    A_VOIC_SP        varchar(10)  null comment '에이전트음성속도',
    PROC_PRI         varchar(2)   null comment '처리우선순위',
    MAX_SILENCE      varchar(3)   null comment '최대무음',
    META_INFO_YN     varchar(1)   null comment '메타정보여부',
    CAL_TYPE         varchar(200) null comment '상담유형
(상담 or 청약)',
    UPDATOR_ID       varchar(100) null comment '수정자아이디',
    UPDATED_TM       datetime     null comment '수정일시',
    CREATOR_ID       varchar(100) null comment '생성자아이디',
    CREATED_TM       datetime     null comment '생성일시',
    primary key (REC_KEY, REC_ID, REC_FILE_NM)
)
    comment '콜 정보 테이블';

create table QA_CHK_ITM_TB
(
    PROD_CAT     varchar(5)   not null comment '보종구분',
    CHK_APP_ORD  int          not null comment '항목적용차수',
    CHK_ITM_CD   varchar(10)  not null comment '평가항목코드',
    CHK_ITM_NM   varchar(100) null comment '평가항목명',
    CHK_ITM_DESC varchar(300) null comment '평가항목설명',
    CHK_NO       int          null comment '항목순서',
    PART_INSP_YN varchar(1)   null comment '부분검수여부',
    USED_YN      varchar(1)   null comment '사용여부',
    AUTO_INSP_YN varchar(1)   null comment '자동검수여부',
    CHK_CTG_CD   varchar(5)   not null comment '평가구분코드',
    CHK_CTG_NM   varchar(100) null comment '평가구분명',
    NOR_SCR      varchar(5)   null comment '정상점수',
    SUP_SCR      varchar(5)   null comment '보완점수',
    RTN_SCR      varchar(5)   null comment '반송점수',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    DTCT_CTG_CD  varchar(1)   null comment '탐지구분코드',
    COND_ITM_CD  varchar(10)  null comment '조건항목코드',
    primary key (PROD_CAT, CHK_APP_ORD, CHK_ITM_CD, CHK_CTG_CD)
)
    comment '평가항목 테이블';

create table QA_CHK_ORD_TB
(
    PROD_CAT      varchar(5)   not null comment '보종구분',
    CHK_APP_ORD   int          not null comment '항목적용차수',
    CHK_APP_ST_DT varchar(8)   null comment '항목적용시작일자',
    CHK_APP_ED_DT varchar(8)   null comment '항목적용종료일자',
    USED_YN       varchar(1)   null comment '사용여부',
    UPDATOR_ID    varchar(100) null comment '수정자아이디',
    UPDATED_TM    datetime     null comment '수정일시',
    CREATOR_ID    varchar(100) null comment '생성자아이디',
    CREATED_TM    datetime     null comment '생성일시',
    primary key (PROD_CAT, CHK_APP_ORD)
)
    comment '평가항목 차수 테이블';

create table QA_CHK_RST_TB
(
    NSPL_NO     varchar(12)  not null comment '증권번호',
    TMS_INFO    int          not null comment '회차정보',
    PROD_CAT    varchar(5)   not null comment '보종구분',
    CHK_APP_ORD int          not null comment '항목적용차수',
    CHK_ITM_CD  varchar(10)  not null comment '평가항목코드',
    REC_KEY     varchar(100) not null comment '녹취키',
    REC_ID      varchar(100) not null comment '녹취ID',
    REC_FILE_NM varchar(300) not null comment '녹취파일명',
    PSN_JUG_CAT varchar(5)   not null comment '인심사구분',
    JUG_RST     varchar(5)   null comment '심사결과',
    SCR_RST     varchar(5)   null comment '점수결과',
    ANS_RST     varchar(5)   null comment '답변결과',
    DTCT_RST    varchar(5)   null comment '탐지결과',
    SEC_ST_TM   time         null comment '구간시작시간',
    SEC_ED_TM   time         null comment '구간종료시간',
    UPDATOR_ID  varchar(100) null comment '수정자아이디',
    UPDATED_TM  datetime     null comment '수정일시',
    CREATOR_ID  varchar(100) null comment '생성자아이디',
    CREATED_TM  datetime     null comment '생성일시',
    CHK_CTG_CD  varchar(5)   not null comment '평가구분코드',
    CHK_CTG_NM  varchar(10)  null,
    CHK_ITM_NM  varchar(10)  null,
    CHK_NO      int          null,
    primary key (NSPL_NO, TMS_INFO, PROD_CAT, CHK_APP_ORD, CHK_CTG_CD, CHK_ITM_CD, PSN_JUG_CAT)
);

create table QA_CODE_DTL_TB
(
    CD_GP      varchar(20)  not null comment '코드그룹',
    CD         varchar(100) not null comment '코드',
    CD_NM      varchar(100) null comment '코드명',
    CD_DESC    varchar(300) null comment '코드설명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시',
    primary key (CD_GP, CD)
)
    comment '코드 상세 테이블';

create table QA_CODE_TB
(
    CD_GP      varchar(20)  not null comment '코드그룹'
        primary key,
    CD_GP_NM   varchar(100) null comment '코드그룹명',
    CD_GP_DESC varchar(300) null comment '코드그룹설명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '코드 테이블';

create table QA_COND_DATA_TB
(
    PROD_CAT      varchar(5)   not null comment '보종구분',
    COND_DATA_CD  varchar(20)  not null comment '조건데이터코드',
    COND_DATA_NM  varchar(100) null comment '조건데이터명',
    COND_DATA_TP  varchar(10)  null comment '조건데이터타입',
    COND_DATA_IDX varchar(10)  null comment '조건데이터인덱스',
    USED_YN       varchar(1)   null comment '사용여부',
    UPDATOR_ID    varchar(100) null comment '수정자아이디',
    UPDATED_TM    datetime     null comment '수정일시',
    CREATOR_ID    varchar(100) null comment '생성자아이디',
    CREATED_TM    datetime     null comment '생성일시',
    primary key (PROD_CAT, COND_DATA_CD)
)
    comment '조건 데이터 테이블';

create table QA_COND_LIST_TB
(
    COND_ITM_CD varchar(10)  not null comment '조건항목코드',
    PROD_CAT    varchar(5)   not null comment '보종구분',
    COND_ITM_NM varchar(300) null comment '조건항목명',
    UPDATOR_ID  varchar(100) null comment '수정자아이디',
    UPDATED_TM  datetime     null comment '수정일시',
    CREATOR_ID  varchar(100) null comment '생성자아이디',
    CREATED_TM  datetime     null comment '생성일시',
    primary key (COND_ITM_CD, PROD_CAT)
)
    comment '조건 리스트 테이블';

create table QA_COND_TB
(
    COND_ITM_CD  varchar(10)  not null comment '조건항목코드',
    PROD_CAT     varchar(5)   not null comment '보종구분',
    COND_NO      int          not null comment '조건순서',
    COND_ITM_NM  varchar(300) null comment '조건항목명',
    COND_TP_FIR  varchar(10)  null comment '조건타입1',
    COND_VAL_FIR varchar(20)  null comment '조건값1',
    COND_OP_FIR  varchar(30)  null comment '조건연산자1',
    COND_TP_SE   varchar(10)  null comment '조건타입2',
    COND_VAL_SE  varchar(20)  null comment '조건값2',
    COND_OP_SE   varchar(30)  null comment '조건연산자2',
    COND_TP_THR  varchar(10)  null comment '조건타입3',
    COND_VAL_THR varchar(20)  null comment '조건값3',
    LOGIC_OP     varchar(5)   null comment '논리연산자',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (COND_ITM_CD, PROD_CAT, COND_NO)
)
    comment '조건 테이블';

create table QA_CTR_CAL_REL_TB
(
    NSPL_NO     varchar(12)  not null comment '증권번호',
    TA_TMS_INFO int          not null comment '회차정보',
    REC_KEY     varchar(100) not null comment '녹취키',
    REC_ID      varchar(100) not null comment '녹취ID',
    REC_FILE_NM varchar(300) not null comment '녹취파일명',
    UPDATOR_ID  varchar(100) null comment '수정자아이디',
    UPDATED_TM  datetime     null comment '수정일시',
    CREATOR_ID  varchar(100) null comment '생성자아이디',
    CREATED_TM  datetime     null comment '생성일시',
    primary key (NSPL_NO, TA_TMS_INFO, REC_KEY, REC_ID, REC_FILE_NM)
)
    comment '계약 콜 관계 테이블';

create table QA_CTR_LIST_TB
(
    NSPL_NO         varchar(12)  not null comment '증권번호'
        primary key,
    TA_TMS_INFO     int          null comment 'TA회차번호',
    QA_TMS_INFO     int          null comment 'QA회차번호',
    WRAP_ID         varchar(20)  null comment '청약서ID',
    REG_DY          varchar(8)   null comment '등록일',
    QA_REQ_TM       varchar(14)  null comment 'QA등록일자',
    INS_PROD_CD     varchar(9)   null comment '상품코드',
    INS_PROD_CTGY   varchar(10)  null comment '상품구분코드',
    PROD_NME        varchar(100) null comment '상품명',
    APTN_STAT       char(2)      null comment '청약상태',
    CSMR_ADMN       varchar(30)  null comment '계약자명',
    CSMR_ADMN_NO    varchar(20)  null comment '고객관리번호',
    PNM             varchar(30)  null comment '피보험자명',
    SAES_EMNM       varchar(30)  null comment '상담원명',
    SAES_EMNO       varchar(8)   null comment '상담원사번',
    CNTR_NM         varchar(100) null comment '센터명',
    SAES_PRT_CD     varchar(10)  null comment '실코드',
    PRT_NME         varchar(100) null comment '실명',
    QA_TRG_CON_CD   varchar(3)   null comment '대상내용코드',
    QA_TRG_TRANS_TM datetime     null comment '대상전송일자',
    QA_ALLC_DTME    varchar(14)  null comment '배정일자',
    OVER_SXTFV_YN   varchar(2)   null comment '계약자65세이상여부',
    SMP_REC_YN      varchar(1)   null comment '산편녹취여부',
    CTR_INS_AG      varchar(3)   null comment '계약자나이',
    PB_INS_AG       varchar(3)   null comment '피보험자나이',
    QA_LIMIT_DT     varchar(8)   null comment 'QA보완기한일자',
    SAES_CNTR_CD    varchar(255) null comment '모집지점코드',
    CMPGN_NM        varchar(100) null comment '캠페인명',
    INS_FEE         int          null comment '보험료',
    UPDATOR_ID      varchar(100) null comment '수정자아이디',
    UPDATED_TM      datetime     null comment '수정일시',
    CREATOR_ID      varchar(100) null comment '생성자아이디',
    CREATED_TM      datetime     null comment '생성일자'
)
    comment '계약 리스트 테이블';

create table QA_CTR_SCRP_TB
(
    NSPL_NO      varchar(12)   not null comment '증권번호',
    TA_TMS_INFO  int           not null comment '회차정보',
    ALL_SEQ      varchar(5)    not null comment '전체순번',
    PROD_CAT     varchar(5)    null comment '보종구분',
    CHK_APP_ORD  int           null comment '항목적용차수',
    CHK_ITM_CD   varchar(10)   null comment '평가항목코드',
    DTCT_DTC_NO  varchar(5)    null comment '탐지사전번호',
    GUD_STMT     varchar(8000) null comment '가이드 문장',
    SCRP         varchar(8000) null comment '스크립트',
    STMT_SEQ     int           null comment '문장 순서',
    ESTY_SCRP_YN varchar(1)    null comment '필수문장여부',
    UPDATOR_ID   varchar(100)  null comment '수정자아이디',
    UPDATED_TM   datetime      null comment '수정일시',
    CREATOR_ID   varchar(100)  null comment '생성자아이디',
    CREATED_TM   datetime      null comment '생성일시',
    CHK_CTG_CD   varchar(5)    not null comment '평가구분코드',
    SCRP_LCTG_CD varchar(10)   null comment '스크립트 대분류코드',
    SCRP_MCTG_CD varchar(10)   null comment '스크립트 중분류코드',
    SCRP_SCTG_CD varchar(10)   null comment '스크립트 소분류코드',
    SCRP_APP_ORD int           null comment '스크립트 적용차수',
    CUST_ANS_YN  varchar(1)    null comment '고객답변여부',
    primary key (NSPL_NO, TA_TMS_INFO, ALL_SEQ, CHK_CTG_CD)
)
    comment '계약 스크립트 테이블';

create table QA_CUSL_KWD_TB
(
    CUSL_CD      varchar(10)  not null comment '상담코드',
    CUSL_APP_ORD int          not null comment '상담 적용차수',
    DTCT_DTC_NO  varchar(5)   not null comment '탐지사전번호',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (CUSL_CD, CUSL_APP_ORD, DTCT_DTC_NO)
)
    comment '상담 키워드 테이블';

create table QA_CUSL_SEC_INFO_TB
(
    PROD_CAT     varchar(5)   not null comment '보종구분',
    PROD_CD      varchar(10)  not null comment '상품코드',
    PLN_CD       varchar(10)  not null comment '플랜코드',
    CHK_ITM_CD   varchar(10)  not null comment '평가항목코드',
    CUSL_CD      varchar(10)  not null comment '상담코드',
    CUSL_APP_ORD int          not null comment '상담 적용차수',
    CHK_CTG_CD   varchar(5)   null comment '평가구분코드',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (PROD_CAT, PROD_CD, PLN_CD, CHK_ITM_CD, CUSL_CD, CUSL_APP_ORD)
)
    comment '상담 구간 정보 테이블';

create table QA_CUSL_SEC_TB
(
    CUSL_CD        varchar(10)  not null comment '상담코드',
    CUSL_APP_ORD   int          not null comment '상담 적용차수',
    CUSL_NM        varchar(100) null comment '상담명',
    CUSL_APP_ST_DT varchar(8)   null comment '상담 적용시작일자',
    CUSL_APP_ED_DT varchar(8)   null comment '상담 적용종료일자',
    USED_YN        varchar(1)   null comment '사용여부',
    UPDATOR_ID     varchar(100) null comment '수정자아이디',
    UPDATED_TM     datetime     null comment '수정일시',
    CREATOR_ID     varchar(100) null comment '생성자아이디',
    CREATED_TM     datetime     null comment '생성일시',
    primary key (CUSL_CD, CUSL_APP_ORD)
)
    comment '상담 구간 테이블';

create table QA_DAMBO_TB
(
    DAMBO_CD   varchar(100) not null comment '담보코드'
        primary key,
    DAMBO_NM   varchar(300) null comment '담보명',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '담보 테이블';

create table QA_DTC_DTL_TB
(
    DTCT_DTC_NO        varchar(5)    not null comment '탐지사전번호',
    DTCT_DTC_GRP_NO    varchar(5)    not null comment '탐지사전그룹번호',
    DTCT_DTC_GRP_IN_NO varchar(5)    not null comment '탐지사전그룹내순서',
    DTCT_DTC_ED_NO     varchar(5)    null comment '탐지사전끝번호',
    DTCT_DTC_CON       varchar(4000) null comment '탐지사전내용',
    UPDATOR_ID         varchar(100)  null comment '수정자아이디',
    UPDATED_TM         datetime      null comment '수정일시',
    CREATOR_ID         varchar(100)  null comment '생성자아이디',
    CREATED_TM         datetime      null comment '생성일시',
    primary key (DTCT_DTC_NO, DTCT_DTC_GRP_NO, DTCT_DTC_GRP_IN_NO)
)
    comment '탐지사전 상세 테이블';

create table QA_FUNC_MGT_TB
(
    PAGE_CD    varchar(20)  not null,
    FUNC_CD    varchar(50)  not null,
    FUNC_NM    varchar(20)  null,
    UPDATOR_ID varchar(100) null,
    UPDATED_TM datetime     null,
    CREATOR_ID varchar(100) null,
    CREATED_TM datetime     null,
    primary key (PAGE_CD, FUNC_CD)
);

create table QA_PAGE_MGT_TB
(
    PAGE_CD    varchar(20)  not null comment '화면코드'
        primary key,
    PAGE_NM    varchar(300) null comment '화면명',
    PAGE_LOC   varchar(300) null,
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '화면 관리 테이블';

create table QA_PAGE_OWN_INFO_TB
(
    INFO_ID    int          not null comment '정보ID'
        primary key,
    PAGE_CD    varchar(20)  null comment '화면코드',
    PAGE_NM    varchar(300) null comment '화면명',
    OWN_INFO   varchar(50)  null comment '보유정보',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '화면 보유 정보 테이블';

create table QA_PAGE_USE_HIS_TB
(
    HIS_ID        int          not null comment '이력ID'
        primary key,
    USER_ID       varchar(12)  null comment '사용자ID',
    USER_IP       varchar(15)  null comment '사용자IP',
    CSMR_ADMIN_NO varchar(20)  null comment '고객관리번호',
    CHK_TM        datetime     null comment '조회일시',
    PAGE_CD       varchar(20)  null comment '화면코드',
    CHK_CTG       varchar(1)   null comment '조회구분',
    PSN_INFO_CNT  int          null comment '개인정보건수',
    UPDATOR_ID    varchar(100) null comment '수정자아이디',
    UPDATED_TM    datetime     null comment '수정일시',
    CREATOR_ID    varchar(100) null comment '생성자아이디',
    CREATED_TM    datetime     null comment '생성일시'
)
    comment '화명 사용 이력 테이블';

create table QA_PROD_CHK_ITM_TB
(
    PROD_CAT    varchar(5)   not null comment '보종구분',
    PROD_CD     varchar(10)  not null comment '상품코드',
    PLN_CD      varchar(10)  null comment '플랜코드',
    CHK_APP_ORD int          not null comment '항목적용차수',
    CHK_CTG_CD  varchar(5)   not null comment '평가구분코드',
    CHK_ITM_CD  varchar(10)  not null comment '평가항목코드',
    UPDATOR_ID  varchar(100) null comment '수정자아이디',
    UPDATED_TM  datetime     null comment '수정일시',
    CREATOR_ID  varchar(100) null comment '생성자아이디',
    CREATED_TM  datetime     null comment '생성일시',
    primary key (PROD_CAT, PROD_CD, CHK_APP_ORD, CHK_CTG_CD, CHK_ITM_CD)
)
    comment '상품별 평가 항목 테이블';

create table QA_PROD_TB
(
    PROD_CAT    varchar(5)   not null comment '보종구분',
    PROD_CD     varchar(10)  not null comment '상품코드',
    PLN_CD      varchar(10)  not null comment '플랜코드',
    CHK_APP_ORD int          null comment '항목적용차수',
    SALE_ST_DT  varchar(8)   null comment '판매시작일',
    SALE_ED_DT  varchar(8)   null comment '판매종료일',
    PROD_NM     varchar(200) null comment '상품명',
    SCPR_NM     varchar(200) null comment '스크립트명',
    USED_YN     varchar(1)   null comment '사용여부',
    BUNDLE_YN   varchar(1)   null comment '묶음여부',
    UPDATOR_ID  varchar(100) null comment '수정자아이디',
    UPDATED_TM  datetime     null comment '수정일시',
    CREATOR_ID  varchar(100) null comment '생성자아이디',
    CREATED_TM  datetime     null comment '생성일시',
    primary key (PROD_CAT, PROD_CD)
)
    comment '상품 테이블';

create table QA_PSN_INFO_TB
(
    CS_PK        varchar(22)  not null comment '고객고유번호',
    PLAR_NUM     varchar(10)  not null comment '사원번호',
    PLAR_NAM     varchar(100) null comment '사원명',
    BLN_OFC_COD  varchar(10)  null comment '소속점포코드',
    BLN_OFC_NAM  varchar(100) null comment '소속점포명',
    CETR_OFC_COD varchar(10)  null comment '센터점포코드',
    CETR_OFC_NAM varchar(100) null comment '센터점포명',
    SOCPOS_SECD  varchar(10)  null comment '신분코드',
    SOCPOS_SENM  varchar(500) null comment '신분명',
    OFCWK_YN     char         null comment '내근여부',
    DSM_YN       char         null comment '해촉여부',
    APO_YMD      varchar(8)   null comment '위촉일자',
    DSM_YMD      varchar(8)   null comment '해촉일자',
    REAPO_YMD    varchar(8)   null comment '재위촉일자',
    DUTY_MTCNT   decimal      null comment '근속월수',
    CRT_DT       date         not null comment '생성일자',
    MDF_DT       date         not null comment '수정일자',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (CS_PK, PLAR_NUM)
)
    comment '인사 정보 테이블';

create table QA_QAM_EMP_TB
(
    USER_ID    varchar(30)  not null comment '사용자아이디'
        primary key,
    USER_NM    varchar(100) null comment '사용자명',
    OFF_MGT    varchar(5)   null comment '휴무관리',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '심사 직원 테이블';

create table QA_RST_STD_INFO_TB
(
    PROD_CAT       varchar(5)   not null comment '보종구분',
    CHK_APP_ORD    int          not null comment '항목적용차수',
    CHK_ITM_CD     varchar(10)  not null comment '평가항목코드',
    QA_RST_ITM_CD  varchar(10)  not null comment 'QA결과항목코드',
    QA_RST_ITM_NM  varchar(100) null comment 'QA결과항목명',
    QA_RST_STD_SCR varchar(5)   null comment 'QA결과기준점수',
    UPDATOR_ID     varchar(100) null comment '수정자아이디',
    UPDATED_TM     datetime     null comment '수정일시',
    CREATOR_ID     varchar(100) null comment '생성자아이디',
    CREATED_TM     datetime     null comment '생성일시',
    primary key (PROD_CAT, CHK_APP_ORD, CHK_ITM_CD, QA_RST_ITM_CD)
)
    comment '결과 기준 정보 테이블';

create table QA_RST_TMS_INFO_TB
(
    NSPL_NO        varchar(12)   not null comment '증권번호',
    QA_TMS_INFO    int           not null comment 'QA 회차정보',
    QA_CNSL_SCR    varchar(3)    null comment 'QA 상담점수',
    QA_SCRP_SCR    varchar(3)    null comment 'QA 스크립트점수',
    CTR_CAL_TOT_TM time          null comment '계약콜 총 시간',
    QA_ED_TM       datetime      null comment 'QA 종료시간',
    QA_PROC_TM     time          null comment 'QA 심사시간',
    TMP_SAV_YN     varchar(1)    null comment '임시 저장 여부',
    QA_ALLC_DTME   varchar(14)   null comment '배정일자',
    QA_USER_NM     varchar(30)   null comment '담당QA',
    QA_NAME_ID     varchar(24)   null comment '담당QA사번',
    EVLT_RSLT_STAT varchar(4)    null comment 'QA 결과',
    SPLM_CTNT      varchar(4000) null comment '심사의견',
    EVLT_COMMENT   blob          null comment '심사의견',
    UPDATOR_ID     varchar(100)  null comment '수정자아이디',
    UPDATED_TM     datetime      null comment '수정일시',
    CREATOR_ID     varchar(100)  null comment '생성자아이디',
    CREATED_TM     datetime      null comment '생성일시',
    primary key (NSPL_NO, QA_TMS_INFO)
)
    comment '결과 회차 정보 테이블';

create table QA_SCRP_DTCT_RST_TB
(
    NSPL_NO       varchar(12)   not null comment '증권번호',
    TA_TMS_INFO   int           not null comment 'TA회차정보',
    REC_KEY       varchar(100)  not null comment '녹취키',
    REC_ID        varchar(100)  not null comment '녹취ID',
    REC_FILE_NM   varchar(300)  not null comment '녹취파일명',
    STMT_NO       varchar(5)    not null comment '문장번호',
    ALL_SEQ       varchar(5)    not null comment '전체순번',
    SPK_DIV_CD    varchar(1)    null comment '화자구분코드',
    PROD_CAT      varchar(5)    null comment '보종구분',
    CHK_APP_ORD   int           null comment '항목적용차수',
    CHK_ITM_CD    varchar(10)   null comment '평가항목코드',
    SCRP_LCTG_CD  varchar(10)   null comment '스크립트 대분류코드',
    SCRP_MCTG_CD  varchar(10)   null comment '스크립트 중분류코드',
    SCRP_SCTG_CD  varchar(10)   null comment '스크립트 소분류코드',
    SCRP_APP_ORD  int           null comment '스크립트 적용차수',
    CUSL_CD       varchar(10)   null comment '상담코드',
    CUSL_APP_ORD  int           null comment '상담 적용차수',
    STMT_ST_TM    time          null comment '문장시작시간',
    STMT_ED_TM    time          null comment '문장종료시간',
    DTCT_STMT     varchar(8000) null comment '탐지문장',
    ANS_YN        varchar(1)    null comment '답변여부',
    ANS_NOR_YN    varchar(1)    null comment '답변 정상여부',
    ANS_ST_TM     time          null,
    ANS_ED_TM     time          null,
    ANS_DTCT_STMT varchar(8000) null comment '답변 탐지문장',
    DTCT_DTC_NO   varchar(5)    null comment '탐지사전번호',
    DTCT_KWD      varchar(4000) null comment '탐지키워드',
    UPDATOR_ID    varchar(100)  null comment '수정자아이디',
    UPDATED_TM    datetime      null comment '수정일시',
    CREATOR_ID    varchar(100)  null comment '생성자아이디',
    CREATED_TM    datetime      null comment '생성일시',
    CHK_CTG_CD    varchar(5)    not null comment '평가구분코드',
    STMT_SEQ      int           null comment '문장 순서',
    REC_ST_DT_TM  varchar(14)   null comment '녹취시작일자',
    SILENCE       varchar(10)   null,
    CHK_ITM_NM    varchar(10)   null,
    primary key (NSPL_NO, TA_TMS_INFO, REC_KEY, REC_ID, REC_FILE_NM, STMT_NO, CHK_CTG_CD, ALL_SEQ)
)
    comment '표준스크립트 탐지결과 테이블';

create table QA_SCRP_SEC_CD_TB
(
    SCRP_LCTG_CD varchar(10)  not null comment '스크립트 대분류코드',
    SCRP_MCTG_CD varchar(10)  not null comment '스크립트 중분류코드',
    SCRP_SCTG_CD varchar(10)  not null comment '스크립트 소분류코드',
    SCRP_LCTG_NM varchar(100) null comment '스크립트 대분류명',
    SCRP_MCTG_NM varchar(100) null comment '스크립트 중분류명',
    SCRP_SCTG_NM varchar(100) null comment '스크립트 소분류명',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (SCRP_LCTG_CD, SCRP_MCTG_CD, SCRP_SCTG_CD)
)
    comment '스크립트 구간 코드 테이블';

create table QA_SCRP_SEC_INFO_TB
(
    PROD_CAT     varchar(5)   not null comment '보종구분',
    PROD_CD      varchar(10)  not null comment '상품코드',
    CHK_ITM_CD   varchar(10)  not null comment '평가항목코드',
    SCRP_LCTG_CD varchar(10)  not null comment '스크립트 대분류코드',
    SCRP_MCTG_CD varchar(10)  not null comment '스크립트 중분류코드',
    SCRP_SCTG_CD varchar(10)  not null comment '스크립트 소분류코드',
    SCRP_APP_ORD int          not null comment '스크립트 적용차수',
    CHK_CTG_CD   varchar(5)   null comment '평가구분코드',
    PLN_CD       varchar(10)  null,
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (PROD_CAT, PROD_CD, CHK_ITM_CD, SCRP_APP_ORD, SCRP_SCTG_CD, SCRP_MCTG_CD, SCRP_LCTG_CD)
)
    comment '스크립트 구간 정보 테이블';

create table QA_SCRP_SEC_TB
(
    SCRP_LCTG_CD   varchar(10)  not null comment '스크립트 대분류코드',
    SCRP_MCTG_CD   varchar(10)  not null comment '스크립트 중분류코드',
    SCRP_SCTG_CD   varchar(10)  not null comment '스크립트 소분류코드',
    SCRP_APP_ORD   int          not null comment '스크립트 적용차수',
    SCRP_LCTG_NM   varchar(100) null comment '스크립트 대분류명',
    SCRP_MCTG_NM   varchar(100) null comment '스크립트 중분류명',
    SCRP_SCTG_NM   varchar(100) null comment '스크립트 소분류명',
    SCRP_APP_ST_DT varchar(8)   null comment '스크립트 적용시작일자',
    SCRP_APP_ED_DT varchar(8)   null comment '스크립트 적용종료일자',
    USED_YN        varchar(1)   null comment '사용여부',
    UPDATOR_ID     varchar(100) null comment '수정자아이디',
    UPDATED_TM     datetime     null comment '수정일시',
    CREATOR_ID     varchar(100) null comment '생성자아이디',
    CREATED_TM     datetime     null comment '생성일시',
    primary key (SCRP_LCTG_CD, SCRP_MCTG_CD, SCRP_SCTG_CD, SCRP_APP_ORD)
)
    comment '스크립트 구간 테이블';

create table QA_SCRP_TB
(
    SCRP_LCTG_CD varchar(10)  not null comment '스크립트 대분류코드',
    SCRP_MCTG_CD varchar(10)  not null comment '스크립트 중분류코드',
    SCRP_SCTG_CD varchar(10)  not null comment '스크립트 소분류코드',
    SCRP_APP_ORD int          not null comment '스크립트 적용차수',
    STMT_SEQ     int          not null comment '문장 순서',
    DTCT_DTC_NO  varchar(5)   not null comment '탐지사전번호',
    DAMBO_CD     varchar(100) null comment '담보코드',
    COND_ITM_CD  varchar(10)  null comment '조건항목코드',
    ST_SCRP_YN   varchar(1)   null comment '시작문장여부',
    ESTY_SCRP_YN varchar(1)   null comment '필수문장여부',
    DAMBO_NM     varchar(300) null comment '담보명',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (SCRP_LCTG_CD, SCRP_MCTG_CD, SCRP_SCTG_CD, SCRP_APP_ORD, STMT_SEQ, DTCT_DTC_NO)
)
    comment '스크립트 테이블';

create table QA_STMT_TB
(
    DTCT_DTC_NO varchar(5)    not null comment '탐지사전번호'
        primary key,
    GUD_STMT    varchar(8000) null comment '가이드 문장',
    SCRP        varchar(8000) null comment '스크립트',
    DAMBO_CD    varchar(10)   null comment '담보코드',
    ANS_CD      varchar(10)   null comment '답변코드',
    CUST_ANS_YN varchar(1)    null comment '고객답변여부',
    UPDATOR_ID  varchar(100)  null comment '수정자아이디',
    UPDATED_TM  datetime      null comment '수정일시',
    CREATOR_ID  varchar(100)  null comment '생성자아이디',
    CREATED_TM  datetime      null comment '생성일시'
)
    comment '문장 테이블';

create table QA_STT_RST_TB
(
    REC_KEY        varchar(100)  not null comment '녹취키',
    REC_ID         varchar(100)  not null comment '녹취ID',
    REC_FILE_NM    varchar(300)  not null comment '녹취파일명',
    STMT_NO        varchar(5)    not null comment '문장번호',
    SPK_DIV_CD     varchar(1)    null comment '화자구분코드',
    STMT_ST_TM     time          null comment '문장시작시간',
    STMT_ED_TM     time          null comment '문장종료시간',
    STMT           varchar(8000) null comment '원문장',
    UPDATE_STMT    varchar(8000) null comment '수정문장',
    UPDATE_STMT_YN varchar(1)    null comment '수정문장여부',
    SILENCE        varchar(3)    null comment '무음',
    UPDATOR_ID     varchar(100)  null comment '수정자아이디',
    UPDATED_TM     datetime      null comment '수정일시',
    CREATOR_ID     varchar(100)  null comment '생성자아이디',
    CREATED_TM     datetime      null comment '생성일시',
    SPK_SPD        decimal(4, 2) null comment '.발화속도',
    primary key (REC_KEY, REC_ID, REC_FILE_NM, STMT_NO)
)
    comment 'STT 결과 테이블';

create table QA_SVC_MOTR_TB
(
    SERV_ID     varchar(50)  not null comment '서버ID',
    SVC_ID      varchar(50)  not null comment '서비스ID',
    SVC_STAT_CD varchar(10)  null comment '서비스상태코드',
    PSS_ID      varchar(10)  null comment '프로세스ID',
    OPRT_DATE   varchar(50)  null comment '가동시간',
    SVC_ST_DATE varchar(50)  null comment '서비스시작시간',
    SVC_ED_DATE varchar(50)  null comment '서비스종료시간',
    UPDATOR_ID  varchar(100) null comment '수정자아이디',
    UPDATED_TM  datetime     null comment '수정일시',
    CREATOR_ID  varchar(100) null comment '생성자아이디',
    CREATED_TM  datetime     null comment '생성일시',
    primary key (SERV_ID, SVC_ID)
)
    comment '서비스 모니터링 테이블';

create table QA_TA_TMS_INFO_TB
(
    NSPL_NO          varchar(12)  not null comment '증권번호',
    TA_TMS_INFO      int          not null comment 'TA 회차정보',
    TA_CNSL_SCR      varchar(3)   null comment 'TA 상담점수',
    TA_SCRP_SCR      varchar(3)   null comment 'TA 스크립트 점수',
    PROG_STAT_CD     varchar(10)  null comment '진행상태코드',
    PROG_STAT_DTL_CD varchar(10)  null comment '진행상태상세코드',
    CTR_CAL_TOT_TM   time         null comment '계약콜 총 시간',
    STT_ED_TM        datetime     null comment 'STT 종료시간',
    TA_ST_TM         datetime     null comment 'TA 시작시간',
    TA_ED_TM         datetime     null comment 'TA 종료시간',
    TA_PROC_SVR      varchar(100) null comment 'TA 처리서버',
    CNSL_SPK_SPD     varchar(6)   null comment '상담발화속도',
    SCRP_SPK_SPD     varchar(6)   null comment '스크립트발화속도',
    IMP_ITM_SPK_SPD  varchar(6)   null comment '중요항목발화속도',
    UPDATOR_ID       varchar(100) null comment '수정자아이디',
    UPDATED_TM       datetime     null comment '수정일시',
    CREATOR_ID       varchar(100) null comment '생성자아이디',
    CREATED_TM       datetime     null comment '생섣일시',
    QA_REQ_TM        datetime     null,
    QA_TRG_CON_ED    varchar(10)  null,
    primary key (NSPL_NO, TA_TMS_INFO)
)
    comment 'TA 회차 정보 테이블';

create table QA_TM_CAL_INFO_TB
(
    CNSL_ID      varchar(30)   not null comment '상담ID'
        primary key,
    CNSL_TM      varchar(14)   null comment '상담일시',
    TLVR_HR      varchar(6)    null comment '통화시간',
    UCID         varchar(100)  null comment 'UCID',
    CNTR_CD      varchar(10)   null comment '지점코드',
    CNTR_NM      varchar(100)  null comment '지점 명',
    PRT_CD       varchar(8)    null comment '실코드',
    PRT_NM       varchar(30)   null comment '실 명',
    CULE_ID      varchar(8)    null comment '상담원ID',
    CULE_NM      varchar(30)   null comment '상담원명',
    CS_PK        varchar(22)   null comment '고객번호',
    CS_NM        varchar(30)   null comment '고객명',
    CNSL_LCLS_CD varchar(10)   null comment '상담대분류코드',
    CNSL_LCLS_NM varchar(30)   null comment '상담대분류명',
    CNSL_SMLC_CD varchar(10)   null comment '상담소분류코드',
    CNSL_SMLC_NM varchar(30)   null comment '상담소분류명',
    TLVR_CTNT    varchar(4000) null comment '통화내용',
    UPDATOR_ID   varchar(100)  null comment '수정자아이디',
    UPDATED_TM   datetime      null comment '수정일시',
    CREATOR_ID   varchar(100)  null comment '생성자아이디',
    CREATED_TM   datetime      null comment '생성일시'
)
    comment 'TM 콜 정보 테이블';

create table QA_TM_PSN_INFO_TB
(
    USER_ID       varchar(8)   not null comment '사용자ID'
        primary key,
    USER_NM       varchar(30)  null comment '사용자명',
    NTCM_DY       char(8)      null comment '입사일',
    EXTR_DY       char(8)      null comment '퇴사일',
    USER_AUTR_GRD varchar(4)   null comment '사용자권한등급(60:QC팀장, 70:QC)',
    UPDATOR_ID    varchar(100) null comment '수정자아이디',
    UPDATED_TM    datetime     null comment '수정일시',
    CREATOR_ID    varchar(100) null comment '생성자아이디',
    CREATED_TM    datetime     null comment '생성일시'
)
    comment 'TM 인사 정보 테이블';

create table QA_USR_LOG_TB
(
    LOG_NO         int auto_increment comment '로그번호'
        primary key,
    USER_ID        varchar(30)  null comment '사용자아이디',
    LOG_CD         varchar(5)   null comment '로그코드',
    LOGIN_DT       datetime     null comment '로그인일시',
    LOGOUT_DT      datetime     null comment '로그아웃일시',
    UPD_BEF_AUT_CD varchar(20)  null comment '변경전권한코드',
    UPD_AFT_AUT_CD varchar(20)  null comment '변경후권한코드',
    UPDATOR_ID     varchar(100) null comment '수정자아이디',
    UPDATED_TM     datetime     null comment '수정일시',
    CREATOR_ID     varchar(100) null comment '생성자아이디',
    CREATED_TM     datetime     null comment '생성일시'
)
    comment '사용자 로그 테이블';

create table QA_USR_MGT_TB
(
    USER_ID    varchar(30)  not null comment '사용자아이디'
        primary key,
    EMP_NO     varchar(30)  null comment '사원번호',
    USER_NM    varchar(100) null comment '사용자명',
    USER_PW    varchar(500) null comment '패스워드',
    ENABLED    int          null comment '사용자상태',
    AUTHORITY  varchar(20)  null comment '시큐리티권한',
    AUT_CD     varchar(20)  null comment '권한코드',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시'
)
    comment '사용자 관리 테이블';

create table ST1_TB
(
    APTN_HR    varchar(14)  not null comment '청약 시간',
    NSPL_NO    varchar(12)  not null comment '증권번호',
    TMS_INFO   int          not null comment '회차정보',
    QA_USER_ID varchar(12)  null comment '담당 QA',
    QA_USER_NM varchar(30)  null comment '담당 QA 명',
    QA_EVLT_TM time         null,
    CTOR_CNT   varchar(5)   null comment '계약자수',
    TA_SCR     varchar(5)   null comment 'TA점수',
    QA_SCR     varchar(5)   null comment 'QA점수',
    UPDATOR_ID varchar(100) null comment '수정자아이디',
    UPDATED_TM datetime     null comment '수정일시',
    CREATOR_ID varchar(100) null comment '생성자아이디',
    CREATED_TM datetime     null comment '생성일시',
    primary key (APTN_HR, NSPL_NO, TMS_INFO)
)
    comment '심사통계1';

create table ST2_TB
(
    APTN_HR      varchar(14)  not null comment '청약 시간',
    NSPL_NO      varchar(12)  not null comment '증권번호',
    TMS_INFO     int          not null comment '회차정보',
    QA_EVLT_TM   time         null comment 'QA 심사시간',
    REC_CAL_TM   time         null comment '녹취콜시간',
    ACP_CNT      varchar(5)   null comment '수용건수',
    NOT_ACP_CNT  varchar(5)   null comment '불수용건수',
    PART_ACP_CNT varchar(5)   null comment '부분수용건수',
    REV_NEC_CNT  varchar(5)   null comment '검토필요건수',
    INQ_ANS_CNT  varchar(5)   null comment '문의답변건수',
    UPDATOR_ID   varchar(100) null comment '수정자아이디',
    UPDATED_TM   datetime     null comment '수정일시',
    CREATOR_ID   varchar(100) null comment '생성자아이디',
    CREATED_TM   datetime     null comment '생성일시',
    primary key (APTN_HR, NSPL_NO, TMS_INFO)
)
    comment '심사통계2';

create table TBL_APTN_NFTN
(
    NSPL_NO      varchar(255) not null comment '증권번호'
        primary key,
    SAES_CNTR_CD varchar(255) null,
    REG_DY       varchar(255) null
)
    comment 'STT 오라클 테이블';

create table TBL_NRCP_ELAG_CD_NFTN
(
    ELAG_CD  varchar(100) not null comment '특약코드',
    PROD_CAT varchar(100) null comment '보종구분',
    ELAG_NME varchar(255) null comment '특약명',
    constraint TBL_NRCP_ELAG_CD_NFTN_ELAG_CD_uindex
        unique (ELAG_CD)
);

alter table TBL_NRCP_ELAG_CD_NFTN
    add primary key (ELAG_CD);

create table TBL_QC_QUESTION_LST
(
    NSPL_NO   varchar(255) not null comment '증권번호'
        primary key,
    RESULT_NM varchar(255) null comment '응답결과',
    TP_NM     varchar(255) null comment '질의응답',
    PRGST_NM  varchar(255) null comment '질의응답'
);

create table TMQA_STT_TB
(
    WRAP_ID           varchar(10) not null comment '청약ID',
    APTN_HR           varchar(10) null comment '청약일자',
    PROD_NME          varchar(10) null comment '상품명',
    NSPL_NO           varchar(10) null comment '증권번호',
    CSMR_ADMN         varchar(10) null,
    PNM               varchar(10) null,
    CNTR_NM           varchar(10) null comment '센터명',
    SAES_EMNM         varchar(10) null,
    SAES_EMNO         varchar(10) null,
    SAES_TYPE         varchar(10) null,
    TA_SCRORE         varchar(10) null comment 'TA점수',
    QC_EVLT_TARGET_YN varchar(10) null comment '대상',
    QC_EVLT_TARGET_NM varchar(10) null comment '대상내용',
    QA_REQ_DY         varchar(10) null comment 'QA 요청일자',
    APTN_STAT_CD      varchar(10) null,
    APTN_STAT         varchar(10) null comment '청약상태',
    BUNDLE_YN         varchar(10) null comment '번들유무',
    PROD_SND_YN       varchar(10) null comment '상품발송안내',
    TMS_INFO          varchar(10) null comment '회차정보',
    ROWNUM            int         not null
        primary key
);

create table TM_STT_TB
(
    CNSL_ID      varchar(10) not null,
    CNSL_TM      datetime    null,
    TLVR_HR      varchar(10) null,
    CNTR_CD      varchar(10) null,
    CNTR_NM      varchar(10) null,
    PRT_CD       varchar(10) null,
    PRT_NM       varchar(10) null,
    CULE_ID      varchar(10) null,
    CULE_NM      varchar(10) null,
    SAES_TYPE    varchar(10) null,
    SAES_TYPE_NM varchar(10) null,
    CS_PK        varchar(10) null,
    CS_NM        varchar(10) null,
    PROG_STAT_CD varchar(10) null,
    UCID         varchar(10) null,
    APTN_STAT_YN varchar(10) null,
    CNSL_LCLS_CD varchar(10) null,
    CNSL_LCLS_NM varchar(10) null,
    CNSL_SMLC_CD varchar(10) null,
    CNSL_SMLC_NM varchar(10) null,
    TLVR_CTNT    varchar(10) null,
    ROWNUM       int         not null
        primary key,
    CSMR_ADMN_NO varchar(10) null,
    NSPL_NO      varchar(10) null
);


