package maum.biz.qa.common.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;
import java.util.Random;

public class AES256 {

    private static final String strKey = "a5c2e1f8152cdc1ca5c2e1f8152cdc1c";
    private static final String AESAlgorithm = "AES/CBC/PKCS5Padding";
    private static final String encCharset = "UTF-8";
    private static final String crypteAlgorithm = "AES";

    public String getCryptStr(String inStr, String cryptDivCd) throws Exception{
        String cryptResult = "";
        if(!inStr.isEmpty()) {
            // endcode
            if("E".equals(cryptDivCd)){
                cryptResult = encryptStr(inStr);
            } else if("D".equals(cryptDivCd)) {		// decode
                cryptResult = decryptStr(inStr);
            }
        }

        return cryptResult;
    }

    public static String encryptStr(String inStr) throws Exception{

        byte initVector[] = new byte[16];
        (new Random()).nextBytes(initVector);

        IvParameterSpec ivSpec = new IvParameterSpec(initVector);

        SecretKeySpec key = new SecretKeySpec(strKey.getBytes(encCharset), crypteAlgorithm);

        Cipher cip = Cipher.getInstance(crypteAlgorithm);
        cip.init(Cipher.ENCRYPT_MODE, key, ivSpec);

        byte[] cipherbytes = cip.doFinal(inStr.getBytes());

        byte[] msgBytes = new byte[initVector.length + cipherbytes.length];

        System.arraycopy(initVector, 0, msgBytes, 0, 16);
        System.arraycopy(cipherbytes, 0, msgBytes, 16, cipherbytes.length);

        String afterCiphered = new String(Base64.encodeBase64(msgBytes));

        return afterCiphered;
    }

    public static String decryptStr(String inStr) throws Exception{
        byte[] base64Decoded = Base64.decodeBase64(inStr.getBytes(encCharset));
        byte[] initVector = Arrays.copyOfRange(base64Decoded, 0, 16);
        byte[] msgByte = Arrays.copyOfRange(base64Decoded, 16, base64Decoded.length);

        SecretKeySpec key = new SecretKeySpec(strKey.getBytes(encCharset), crypteAlgorithm);
        IvParameterSpec ivSpec = new IvParameterSpec(initVector);

        Cipher cip = Cipher.getInstance(AESAlgorithm);
        cip.init(Cipher.DECRYPT_MODE, key, ivSpec);

        byte[] aesDecode = cip.doFinal(msgByte);

        String originStr = new String(aesDecode, encCharset);

        return originStr;
    }

}
