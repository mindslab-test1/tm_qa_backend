package maum.biz.qa.common.utils.seed;

public class SeedCryptUtil {

    /**
     * 데이터를 암호화하여 HEX형식으로 리턴한다.<br>
     * TM측에서 requestParameter 를 이용해 암복호화를 하기 위해 사용한다.
     * @param inDataSrc
     * @return
     */
    public static String encryptDataHex(String inDataSrc, String key)  throws Exception{

        StringBuffer strBuff = new StringBuffer();

        if (!"".equals(inDataSrc)){
            SeedKisaEx seed = new SeedKisaEx();

//            String key = seedkey;	//하드코딩 금지. property, db 등 별도 관리할것

            // SEED 키 설정
            seed.CreateRoundKey(key.getBytes());

            try {
                byte[] bBuff = inDataSrc.getBytes("UTF-8");
                byte[] bEncBuff = seed.EncryptBytes(bBuff);

                for (int i = 0; i < bEncBuff.length; i++) {
                    strBuff.append(ByteBufferHexDumper.getHexDump(bEncBuff[i]));
                }
            } catch (Exception e) {
                e.printStackTrace();	//시스템 로그 사용하지 말것. logger로 구현.
                throw e;
            }
        }

        return strBuff.toString();
    }

    /**
     * HEX형식으로 된 암호화 데이터를 복호화한다.<br>
     * 신정보 시스템 내에서 보안대상필드를 암호화 하는데 사용한다.
     * @param inDataSrc
     * @return
     */
    public static String decryptDataHex(String inDataSrc, String key) throws Exception{
        StringBuffer strBuff = new StringBuffer();

        if (!"".equals(inDataSrc)){
            SeedKisaEx seed = new SeedKisaEx();
//            String key = key;	//하드코딩 금지. property, db 등 별도 관리할것

            // SEED 키 설정
            seed.CreateRoundKey(key.getBytes());

            byte[] bBuff = ByteBufferHexDumper.getBinDump(inDataSrc.getBytes());
            byte[] bDecBuff = seed.DecryptBytes(bBuff);

            try {
                strBuff.append(new String(bDecBuff, "UTF-8"));
            } catch (Exception e) {
                e.printStackTrace();	//시스템 로그 사용하지 말것. logger로 구현.
                throw e;
            }
        }

        return strBuff.toString();
    }


}