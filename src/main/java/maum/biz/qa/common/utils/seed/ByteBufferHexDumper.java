package maum.biz.qa.common.utils.seed;


import java.nio.ByteBuffer;

/**
 * 바이트 버퍼를 16진수로 덤프출력한다.
 * <p/>
 * 다음과 같이 사용할 수 있다.
 * <p/>
 * <pre>
 * String dump = ByteBufferedHexDumper.getHexDump(byte[]);
 * System.out.println(dump);</pre>
 *
 * @author 김병곤
 * @version 1.0
 */
public class ByteBufferHexDumper {

    // 상위 비트
    private static final byte[] highDigits;

    // 하위 비트
    private static final byte[] lowDigits;

    // 테이블
    static {
        final byte[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                '9', 'A', 'B', 'C', 'D', 'E', 'F'};

        int i;
        byte[] high = new byte[256];
        byte[] low = new byte[256];

        for (i = 0; i < 256; i++) {
            high[i] = digits[i >>> 4];
            low[i] = digits[i & 0x0F];
        }

        highDigits = high;
        lowDigits = low;
    }

    /**
     * 바이트 버퍼에서 16진수 덤프내용을 가져온다.
     *
     * @param in 바이트버퍼(<code>ByteBuffer</code>)
     * @return 덤프내용
     */
    public static String getHexDump(ByteBuffer in) {
        int size = in.remaining();

        if (size == 0) {
            return "empty";
        }

        StringBuffer out = new StringBuffer((in.remaining() * 3) - 1);

        int mark = in.position();

        // 제일 처음 것을 채움
        int byteValue = in.get() & 0xFF;
        out.append((char) highDigits[byteValue]);
        out.append((char) lowDigits[byteValue]);
        size--;

        // 나머지 것을 채움
        for (; size > 0; size--) {
            out.append(' ');
            byteValue = in.get() & 0xFF;
            out.append((char) highDigits[byteValue]);
            out.append((char) lowDigits[byteValue]);
        }

        in.position(mark);

        return out.toString();
    }

    /**
     * 바이트 배열을 덤프용 문자열로 변환한다.
     *
     * @param bytes 변환할 바이트 배열
     * @return 덤프내용
     */
    public static String getHexDump(byte[] bytes, boolean space) {
        int size = bytes.length;
        StringBuffer out = new StringBuffer(size);
        for (int i = 0; i < size; i++) {
            // 제일 처음 것을 채움
            int byteValue = bytes[i] & 0xFF;
            out.append((char) highDigits[byteValue]);
            out.append((char) lowDigits[byteValue]);
            if (space == true) {
                if (i < (size - 1))
                    out.append(' ');
            }
        }

        return out.toString();
    }

    public static String getHexDump(byte[] bytes) {
        return getHexDump(bytes, false);
    }

    /**
     * 바이트 배열을 덤프용 문자열로 변환한다.
     *
     * @param one 변환할 바이트 배열
     * @return 덤프내용
     */
    public static String getHexDump(byte one) {
        int byteValue = one & 0xFF;

        char ch[] = new char[2];
        ch[0] = (char) highDigits[byteValue];
        ch[1] = (char) lowDigits[byteValue];

        return new String(ch);
    }

    public static byte[] getBinDump(byte[] bytes) {
        byte[] bTemp = new byte[bytes.length / 2];
        byte[] b = new byte[2];
        int nCount = 0;

        for (int i = 0; i < bytes.length; i += 2) {
            b[0] = bytes[i];
            b[1] = bytes[i + 1];
            bTemp[nCount++] = (byte) (Integer.parseInt(new String(b), 16) &
                    0xFFFF);
        }

        return bTemp;
    }
}