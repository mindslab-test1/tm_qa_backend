package maum.biz.qa.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpClient {

    private Map<String, Object> mapParam;   // json 형태로 타입 변환해야할 param
    private String stringParam; // json 형태 param (화면에서 json 형태를 그대로 받을경우)

    private String protocol;
    private String[] servers;
    private String port;
    private String resource;
    private String method;
    private String task;

    private StringUtil stringUtil = new StringUtil();

    private final Logger LOG = LoggerFactory.getLogger(HttpClient.class);

    // parameter 를 Map 으로 받을경우
    public HttpClient(Map<String,Object> mapParam, String protocol, String[] servers, String port, String resource, String method, String task){
        this.mapParam = mapParam;

        this.protocol = protocol;
        this.servers = servers;
        this.port = port;
        this.resource = resource;
        this.method = method;
        this.task = task;
    }

    /*
    * 1. parameter 를 String (Json 형태) 으로 받을경우
    * 2. parameter 를 String (Query String 형태) 으로 받을경우
    */
    public HttpClient(String stringParam, String protocol, String[] servers, String port, String resource, String method, String task){
        this.stringParam = stringParam;

        this.protocol = protocol;
        this.servers = servers;
        this.port = port;
        this.resource = resource;
        this.method = method;
        this.task = task;
    }

    // POST 요청 (Content Type: application/json; charset=UTF-8)
    public JsonNode requestPostPayload(){
        HttpURLConnection conn = null;
        JsonNode resultJson = null;

        URL[] urls = new URL[servers.length];
        OutputStreamWriter wr = null;

        // content type : application/json 으로 변환
        String param = null;
        if (null != mapParam){
            JSONObject jsonObj = new JSONObject(mapParam);
            param = jsonObj.toString();
        } else if (null != stringParam){
            param = stringParam;
        }
        LOG.info("HttpClient param: " + param);

        // response 정보
        int responseCode = 0;
        String responseMsg;

        for (int i=0; i<servers.length; i++){
            try {
                // Http URL connection
                urls[i] = new URL(protocol + servers[i] + port + resource);

                LOG.info("urls["+i+"]: "+ urls[i]);

                conn = (HttpURLConnection) urls[i].openConnection();
                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setDoOutput(true); // true = post

                // Http Request
                wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(jsonObj.toString());
                wr.write(param);
                wr.flush();
                wr.close();

                if (conn instanceof HttpURLConnection){
                    responseCode = conn.getResponseCode();
                    responseMsg = conn.getResponseMessage();
                    LOG.info("response code: " + responseCode);

                    ObjectMapper objectMapper = new ObjectMapper();
                    switch (responseCode) {
                        case HttpURLConnection.HTTP_OK:
                            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                            String inputLine;
                            String json = "";
                            while((inputLine = reader.readLine()) != null) {    // response 출력
                                json += inputLine;
//                                LOG.debug(inputLine);
                            }
                            reader.close();
                            resultJson = objectMapper.readTree(json);
                            if (resultJson != null){
                                LOG.info("Response size: " + resultJson.size());
                            } else {
                                LOG.info("Response is null");
                            }
                            break;
                        default:
                            LOG.error("response msg: " + responseMsg);
                            resultJson = objectMapper.readTree("{\"response_code\" : \""+responseCode+"\"}");
                            ((ObjectNode) resultJson).put("response_message", responseMsg);
                    }
                }
                else {
                    LOG.info("Variable 'conn' type is not HttpURLConnection");
                }

            } catch (ConnectException e) {
                LOG.warn(e.getMessage());
                LOG.warn("Connection Exception (Find Next Server)");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null){
                    conn.disconnect();
                    conn = null;
                }
                if (wr != null){
                    wr = null;
                }

                // 종료
                if (responseCode == HttpURLConnection.HTTP_OK){
                    LOG.info("Stop the loop and return result in HttpClient");
                    return resultJson;
                }
            }
        }
        return resultJson;
    }

    // POST 요청 (Content Type: application/x-www-form-urlencoded; charset=UTF-8)
    public JsonNode requestPostQueryString(){
        HttpURLConnection conn = null;
        JsonNode resultJson = null;

        URL[] urls = new URL[servers.length];
        OutputStreamWriter wr = null;

        // query string 형태로 변환
        String param = null;
        if (null != stringParam){
            param = stringParam;
        }
        LOG.info("HttpClient param: " + param);

        // response 정보
        int responseCode = 0;

        for (int i=0; i<servers.length; i++){
            try {
                // Http URL connection
                urls[i] = new URL(protocol + servers[i] + port + resource);

                LOG.info("urls["+i+"]: "+ urls[i]);

                conn = (HttpURLConnection) urls[i].openConnection();
                conn.setDoInput(true);  // 서버로 부터 메세지를 InputStream 으로 받을 수 있도록 한다. default: true
                conn.setRequestMethod(method);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("charset", "UTF-8");
                conn.setDoOutput(true); // true = post

                // Http Request
                wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonObj.toString());
                wr.write(param);
                wr.flush();
                wr.close();

                if (conn instanceof HttpURLConnection){
                    responseCode = conn.getResponseCode();
                    LOG.info("response code: " + responseCode);

                    ObjectMapper objectMapper = new ObjectMapper();
                    switch (responseCode) {
                        case HttpURLConnection.HTTP_OK:
                            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                            String inputLine;
                            String json = "";
                            while((inputLine = reader.readLine()) != null) {    // response 출력
                                if (!task.isEmpty() && task.equals("getNtmScriptInfo")){
                                    inputLine = stringUtil.removeElement(inputLine);
                                }
                                json += inputLine;
//                                LOG.debug(inputLine);
                            }
                            reader.close();
                            resultJson = objectMapper.readTree(json);
                            if (resultJson != null) {
                                LOG.info("Response size: " + resultJson.size());
                            } else {
                                LOG.info("Response is null");
                            }
                            break;
                        default:
                            resultJson = objectMapper.readTree("{\"response_code\" : \""+responseCode+"\"}");
                    }
                }
                else {
                    LOG.info("Variable 'conn' type is not HttpURLConnection");
                }
            } catch (ConnectException e) {
                LOG.warn(e.getMessage());
                LOG.warn("Connection Exception (Find Next Server)");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (conn != null){
                    conn.disconnect();
                    conn = null;
                }
                if (wr != null){
                    wr = null;
                }

                // 종료
                if (responseCode == HttpURLConnection.HTTP_OK){
                    LOG.info("Stop the loop and return result in HttpClient");
                    return resultJson;
                }
            }
        }
        return resultJson;
    }


}
