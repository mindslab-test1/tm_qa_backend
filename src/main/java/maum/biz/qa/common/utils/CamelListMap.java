
package maum.biz.qa.common.utils;

import org.apache.commons.collections4.map.ListOrderedMap;

/**
 *  쿼리문 return Field Camelcase 변환 시킴
 *  
 * @author kim
 *
 */
@SuppressWarnings("serial")
public class CamelListMap extends ListOrderedMap<Object, Object> {

    private String toProperCase(String s, boolean isCapital) {

        String rtnValue = "";

        if(isCapital){
            rtnValue = s.substring(0, 1).toUpperCase() +
                    s.substring(1).toLowerCase();
        }else{
            rtnValue = s.toLowerCase();
        }
        return rtnValue;
    }

    private String toCamelCase(String s){
        String[] parts = s.split("_");
        StringBuilder camelCaseString = new StringBuilder();

        for (int i = 0; i < parts.length ; i++) {
            String part = parts[i];
            camelCaseString.append(toProperCase(part, (i != 0 ? true : false))) ;
        }

        return camelCaseString.toString();
    }

    @Override
    public Object put(Object key, Object value) {
        String dbValue = null;
        if (value != null) {
            dbValue = value.toString();
        }
        return super.put(toCamelCase((String)key), dbValue);
    }    
}
