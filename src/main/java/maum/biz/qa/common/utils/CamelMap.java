package maum.biz.qa.common.utils;

import java.util.LinkedHashMap;

public class CamelMap extends LinkedHashMap<String, Object> {

  private static final long serialVersionUID = 6723434363565852261L;

  @Override
  public Object put(String key, Object value) {
    return super.put(CamelUtil.convert2CamelCase(key), value);
  }

}