package maum.biz.qa.common.utils.seed;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author hpalman
 */
public class SeedKisaEx {

    static protected final int BLK_SIZE = SEED_KISA.SeedBlockSize; // 사용 간편성 때문에 재 정의함

    private int[] m_arIntRoundKey = null; // 암.복호화를 위해 비밀키로부터 생성된 라운드키 값 저장용.

    public SeedKisaEx() {
        // TODO 자동 생성된 생성자 스텁
    }

    /**
     * srcBytes : 전체(srcBytes.length 참고)를 암호화하여 반환한다.
     *            srcBytes는 PADDING이 적용되지 않은 상태임
     */
    public byte[] EncryptBytes(byte[] srcBytes) {
        byte[] srcPaddsPlain = SeedPadding.insertPadding(srcBytes, BLK_SIZE); // 패딩을 적용한다.
        byte[] retBytes = new byte[srcPaddsPlain.length]; // 반환할 버퍼의 크기(패딩이 적용된 크기)

        byte[] txtTmp = new byte[BLK_SIZE];
        byte[] encTmp = new byte[BLK_SIZE];

//System.out.println("this.m_arIntRoundKey >>>"+this.m_arIntRoundKey);

        for (int i = 0; i < srcPaddsPlain.length; i += BLK_SIZE) {
            // System.arraycopy(src, srcPos, dest, destPos, length)
            System.arraycopy(srcPaddsPlain, i, txtTmp, 0, BLK_SIZE); // 암호화 대상 버퍼로 복사
            SEED_KISA.SeedEncrypt(txtTmp /*pbData*/, this.m_arIntRoundKey, encTmp /*outData*/); // SEED_KISA.SeedBlockSize 만큼 암호화
            System.arraycopy(encTmp, 0, retBytes, i, BLK_SIZE); // 암호화된 것을 반환버퍼에 복사함.
        }

        return retBytes;
    }

    /**
     * srcBytes : 전체를 디코딩하여 PADDING을 제거한 후 반환한다.
     */
    public byte[] DecryptBytes(byte[] srcBytes) {
        byte[] outBytesPlain = new byte[srcBytes.length];

        // ASSERT ( (srcBytes.length % BLK_SIZE) == 0 );

        byte[] encBytes = new byte[BLK_SIZE];
        byte[] txtBytes = new byte[BLK_SIZE];

        for (int i = 0; i < srcBytes.length; i += BLK_SIZE) {
            // System.arraycopy(src, srcPos, dest, destPos, length);
            System.arraycopy(srcBytes, i, encBytes, 0, BLK_SIZE);

            SEED_KISA.SeedDecrypt(encBytes, this.m_arIntRoundKey, txtBytes);

            System.arraycopy(txtBytes, 0, outBytesPlain, i, BLK_SIZE);
        }

        byte[] retBytes = SeedPadding.removePadding(outBytesPlain, BLK_SIZE); // Unpadding or RemovePadding

        return retBytes;
    }

    /**
     *  기능 : sfilename을 암호화한 후 dfilename에 저장한다.
     */
    public void EncryptFile(String sfilename, String dfilename) throws
            IOException {
        FileInputStream fis = null;
        FileOutputStream fos = null;

        File ifile = new File(sfilename);

        byte[] srcData = new byte[BLK_SIZE];
        byte[] outData = new byte[BLK_SIZE];

        int len; // 읽은 길이

        try {
            fis = new FileInputStream(ifile);
            fos = new FileOutputStream(dfilename);

            int fsize = (int) ifile.length(); // 2G 이상은 당연히 안되겠죠?
            int rem = fsize; // 남아있는 바이트 수

            while (rem > 0) {
                if (rem >= BLK_SIZE) {
                    len = fis.read(srcData); // no Padding
                } else {
                    // for (int i=0;i<srcData.length;i++) srcData[i] = (byte) (7+i);
                    len = fis.read(srcData, 0, rem);
                    SeedPadding.setPadding(srcData, rem, BLK_SIZE); // Padding
                }
                rem -= len; // 남아있는 바이트 수를 읽은 길이만큼 감소시킴

                SEED_KISA.SeedEncrypt(srcData, this.m_arIntRoundKey, outData); // 암호화
                fos.write(outData); // 무조건 16바이트 씩 저장
            }
            fos.flush();
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (fos != null)
                try{fos.close();} catch (IOException e) {}
            if (fis != null)
                try{fis.close();} catch (IOException e) {}
        }
    }

    /**
     * 기능 : sfilename을 복호화한 후 dfilename에 저장한다.
     * @param sfilename
     * @param dfilename
     * @throws IOException
     */
    public void DecryptFile(String sfilename, String dfilename) throws
            IOException {
        FileInputStream fis = null;
        FileOutputStream fos = null;

        File ifile = new File(sfilename);
        File ofile = new File(dfilename);

        byte[] srcData = new byte[BLK_SIZE];
        byte[] outData = new byte[BLK_SIZE];

        int len; // 읽은 길이

        try {
            fis = new FileInputStream(ifile);
            fos = new FileOutputStream(ofile);

            int fsize = (int) ifile.length(); // 2G 이상은 당연히 안되겠죠?
            int rem = fsize; // 남아있는 바이트 수
            int nPadLen = 0;
            while (rem > 0) {
                if (rem > BLK_SIZE) { // 마지막이 아니면 no Padding 임
                    len = fis.read(srcData); // no Padding
                    SEED_KISA.SeedDecrypt(srcData, this.m_arIntRoundKey,
                            outData); // 암호화
                } else {
                    len = fis.read(srcData, 0, rem);
                    SEED_KISA.SeedDecrypt(srcData, this.m_arIntRoundKey,
                            outData); // 암호화
                    nPadLen = SeedPadding.delPadding(outData, rem, BLK_SIZE); // Padding
                    fsize -= nPadLen;
                }
                rem -= len; // 남아있는 바이트 수를 읽은 길이만큼 감소시킴

                fos.write(outData, 0, BLK_SIZE - nPadLen); // 저장
            }
            fos.flush();
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (fos != null)
                try{fos.close();} catch (IOException e) {}
            if (fis != null)
                try{fis.close();} catch (IOException e) {}
        }
    }

    /**
     * 비밀키로부터 ROUNDKEY를 구하여 설정한다.
     * @param byteUserKey 16바이트의 비밀키 BYTE 배열
     */
    public void CreateRoundKey(byte[] byteUserKey) {
        m_arIntRoundKey = new int[SEED_KISA.NoRoundKeys];
        SEED_KISA.SeedRoundKey(m_arIntRoundKey, byteUserKey); // 비밀키로부터 ROUNDKEY를 구하여 설정한다.
    }

}