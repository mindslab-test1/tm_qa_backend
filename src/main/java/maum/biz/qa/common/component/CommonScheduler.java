package maum.biz.qa.common.component;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.client.WebSocketConnectionManager;

@Log4j2
@Component
@RequiredArgsConstructor
public class CommonScheduler {

    private final SimpUserRegistry simpUserRegistry;
    private final WebSocketConnectionManager webSocketConnectionManager;

    @Scheduled(cron = "0 */5 * * * *", zone = "Asia/Seoul")
    public void testAttach(){
        if(webSocketConnectionManager.isRunning() && simpUserRegistry.getUserCount() > 0){
            webSocketConnectionManager.stop();
            log.info("WEBSOCKET CONNECTION CLOSE");
        }
    }

}
