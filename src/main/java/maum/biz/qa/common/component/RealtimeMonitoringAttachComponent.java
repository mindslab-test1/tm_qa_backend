package maum.biz.qa.common.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.math.BigDecimal;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * maum.biz.qa.common.component
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-22
 */
@Component
@Log4j2
public class RealtimeMonitoringAttachComponent {

/*

    private final SimpMessagingTemplate template;

    */
/**
     * attach
     *//*

    @SneakyThrows
    public void subscribe() {
        new StandardWebSocketClient().doHandshake(new TextWebSocketHandler() {
            @Override
            public void afterConnectionEstablished(WebSocketSession session) {
                log.info("STT REALTIME MONITORING SOCKET CONNECTION");
            }

            @SneakyThrows
            @Override
            public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
                log.info("STT REALTIME MONITORING SOCKET CLOSE");
                session.close();
            }

            @SneakyThrows
            @Override
            protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
                String msg = new String(message.getPayload().array(), StandardCharsets.UTF_8);
                log.info("MESSAGE : {}", msg);
                template.convertAndSend("/ws/subscribe/realtime/monitoring", msg);
            }
        }, new WebSocketHttpHeaders(), URI.create(wsUrl)).get();
    }

    */
/**
     * test attach
     *//*

    public void testAttach(){
        Long time = System.currentTimeMillis();
        log.info("time - {}", time);
        template.convertAndSend("/ws/subscribe/realtime/monitoring", ImmutableMap.of("time", time));
    }
*/

}