package maum.biz.qa.common.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component("servletInterceptor")
public class ServletInterceptor extends HandlerInterceptorAdapter {

//    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    private Log logger = LogFactory.getLog(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("ServletInterceptor - preHandle");
        HttpSession session = request.getSession(false);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.info("ServletInterceptor :: preHandle ::: authentication===========>{}"+authentication);
        String uri = request.getRequestURI();

        if (session != null && authentication != null && uri != null){
            Object obj = authentication.getPrincipal();
            logger.info("Request URI: " + uri);

            if (obj instanceof UserDetails){ // 로그인 O
                /*logger.info("It's authenticated user");
                Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
                if (authorities.size() < 1){    // 권한이 없는경우
                    return true;
                }*/
                String targetUrl = "/";
                /*for (GrantedAuthority grantedAuthority : authorities) {
                    if (grantedAuthority.getAuthority().equals("ROLE_1")) {
                        targetUrl = "/script";
                        break;
                    } else if (grantedAuthority.getAuthority().equals("ROLE_2")) {
                        targetUrl = "/sampleSimulation";
                        break;
                    } else if (grantedAuthority.getAuthority().equals("ROLE_3")) {
                        targetUrl = "/suptech";
                        break;
                    } else if (grantedAuthority.getAuthority().equals("ROLE_4")) {
                        targetUrl = "/taReprocess";
                        break;
                    } else if (grantedAuthority.getAuthority().equals("ROLE_5")) {
                        targetUrl = "/serviceMonitoring";
                        break;
                    } else if (grantedAuthority.getAuthority().equals("ROLE_6")) {
                        targetUrl = "/accountInfo";
                        break;
                    }
                }*/
                logger.info("Redirect URI: " + targetUrl);
                response.sendRedirect(request.getContextPath() + targetUrl);
                return false;
            } else if (obj instanceof String) {    // 로그인 X or 세션 만료
                logger.info("It's unauthenticated user");
                String ajaxCall = request.getHeader("AJAX");
                if ( ajaxCall != null && ajaxCall.equals("true") ){
                    logger.info("Session time out");
                    logger.info("Response.sendError 302 for AJAX");
                    response.sendError(302);
                }
            }
        }

//        return super.preHandle(request, response, handler);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }
}
