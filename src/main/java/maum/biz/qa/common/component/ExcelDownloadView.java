package maum.biz.qa.common.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

@Component
public class ExcelDownloadView extends AbstractView {

    private Log logger = LogFactory.getLog(this.getClass());

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model,
                                           HttpServletRequest req,
                                           HttpServletResponse res) throws Exception {

        logger.info("Process Excel View");

        Locale locale = (Locale) model.get("locale");
        String workbookName = (String) model.get("workbookName");

        // 파일명 생성
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", locale);
        SimpleDateFormat timeFormat = new SimpleDateFormat("hhmmss", locale);
        String date = dateFormat.format(currentDate);
        String time = timeFormat.format(currentDate);
        String fileName = workbookName + "_" + date + "_" + time + ".xlsx";

        // 브라우저에 따른 파일명 인코딩
        String browser = req.getHeader("User-Agent");
        if (browser.indexOf("MSIE") > -1){
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } else if (browser.indexOf("Chrome") > -1){
            StringBuffer sb = new StringBuffer();
            for (int i=0; i<fileName.length(); i++){
                char c = fileName.charAt(i);
                if (c > '~'){
                    sb.append(URLEncoder.encode(" " + c, "UTF-8"));
                } else {
                    sb.append(c);
                }
            }
            fileName = sb.toString();
        } else {
            fileName = "\"" + new String(fileName.getBytes("UTF-8"), "8859_1") + "\"";
        }

        res.setContentType("application/download; charset=utf-8");
        res.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
        res.setHeader("Content-Transfer-Encoding", "binary");

        OutputStream os;
        SXSSFWorkbook workbook;

        // 파일 생성
        workbook = (SXSSFWorkbook) model.get("workbook");
        os = res.getOutputStream();
        workbook.write(os);

        if (workbook != null){
            workbook.close();
        }
        if (os != null){
            os.close();
        }
    }
}
