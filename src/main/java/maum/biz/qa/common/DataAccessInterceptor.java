package maum.biz.qa.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * DataAccessInterceptor
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-15  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-15
 */
@Slf4j
@Component
@Intercepts({
        @Signature(
                type = Executor.class, method = "update", args = {MappedStatement.class, Object.class}
        )
})
public class DataAccessInterceptor implements Interceptor {

    final DataSource dataSource;

    public DataAccessInterceptor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        int result = (int) invocation.proceed();
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        if(!mappedStatement.getId().contains("setDataAccessLog")){
            Map<String, Object> dataAccessMap = new LinkedHashMap<>();
            String command = mappedStatement.getSqlCommandType().name();
            Map<String, Object> sqlParameterMap = getSqlAndParameter(mappedStatement, invocation.getArgs()[1]);
            dataAccessMap.put("command", command);
            dataAccessMap.put("tableNm", sqlParameterMap.get("tableNm"));
            dataAccessMap.put("sqlQuery", sqlParameterMap.get("sql"));
            dataAccessMap.put("parameter", new ObjectMapper().writeValueAsString(sqlParameterMap.get("parameter")));
            dataAccessMap.put("executeResultCnt", result);
            if(SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails){
                dataAccessMap.put("executeUserId", ((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
            }else{
                dataAccessMap.put("executeUserId", "");
            }
            setLog(dataAccessMap);
        }
        return result;
    }

    /**
     * sql, parameter 가공 및 추출
     * @param mappedStatement
     * @param param
     * @return
     */
    private Map<String, Object> getSqlAndParameter(MappedStatement mappedStatement, Object param) {
        Map<String, Object> rtnMap = new HashMap<>();
        BoundSql boundSql = mappedStatement.getBoundSql(param);
        return Optional.ofNullable(param)
                .map(v -> {
                    Map<String, Object> parameter = new HashMap<>();
                    String sql = boundSql.getSql();
                    if (param instanceof Integer || param instanceof Long || param instanceof Float || param instanceof Double || param instanceof String) {
                        sql = sql.replaceFirst("\\?", "#{" + boundSql.getParameterMappings().get(0).getProperty() + "}");
                        parameter.put(boundSql.getParameterMappings().get(0).getProperty(), param);
                    }
                    else if (param instanceof Map) {
                        List<ParameterMapping> paramMapping = boundSql.getParameterMappings();
                        for (ParameterMapping mapping : paramMapping) {
                            String propValue = mapping.getProperty();
                            // FIXME param으로 넘어온 인자를 제대로 담지 못하는 문제가 있음. 추후수정 211104 최재민
                            if(!propValue.contains("frch_") && !propValue.contains(".")){
                                Object value = ((Map) param).get(propValue);
                                if (value == null) {
                                    continue;
                                }
                                sql = sql.replaceFirst("\\?", "#{" + propValue + "}");
                                parameter.put(propValue, value);
                            }
                        }
                    }
                    else {
                        List<ParameterMapping> paramMapping = boundSql.getParameterMappings();
                        Class<? extends Object> paramClass = param.getClass();
                        for (ParameterMapping mapping : paramMapping) {
                            String propValue = mapping.getProperty();
                            // FIXME list를 제대로 담지 못하는 문제가 있음. 추후수정 211104 최재민
                            if(!propValue.contains("frch_") && !propValue.contains(".")){
                                Field field;
                                try {
                                    field = paramClass.getDeclaredField(propValue);
                                    field.setAccessible(true);
                                    sql = sql.replaceFirst("\\?", "#{" + propValue + "}");
                                    Class<?> javaType = mapping.getJavaType();
                                    if (String.class == javaType) {
                                        parameter.put(propValue, field.get(param));
                                    } else {
                                        parameter.put(propValue, field.get(param).toString());
                                    }
                                } catch (NoSuchFieldException e) {
                                    e.printStackTrace();
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                } catch (Exception e){
                                    log.error("DATA ACCESS INTERCEPTOR PARSING EXCEPTION");
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    rtnMap.put("sql", sql);
                    rtnMap.put("parameter", parameter);
                    rtnMap.put("tableNm", getTableNm(sql));
                    return rtnMap;
                }).orElseGet(() -> {
                    String sql = boundSql.getSql();
                    sql = sql.replaceFirst("\\?", "''");
                    rtnMap.put("sql", sql);
                    rtnMap.put("parameter", new HashMap<>());
                    rtnMap.put("tableNm", getTableNm(sql));
                    return rtnMap;
                });
    }

    /**
     * 테이블 명 추출
     * @param sql
     * @return
     */
    public String getTableNm(String sql){
        Pattern pattern = Pattern.compile("(?<=update|into|from)(\\s+\\w+\\b)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(sql);
        String tableNm;
        if(matcher.find()) tableNm = matcher.group(0).replaceAll(" ", "");
        else tableNm = "NOT FOUND";
        return tableNm;
    }

    /**
     * insert
     * @param param
     */
    private void setLog(Map<String, Object> param){
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = dataSource.getConnection();
            pstmt = conn.prepareStatement(
                    "INSERT INTO DATA_ACCESS_LOG (\n" +
                    "            COMMAND,\n" +
                    "            TABLE_NM,\n" +
                    "            SQL_QUERY,\n" +
                    "            PARAMETER,\n" +
                    "            EXECUTE_RESULT_CNT,\n" +
                    "            EXECUTE_USER_ID\n" +
                    "        ) VALUES (\n" +
                    "            ?,\n" +
                    "            ?,\n" +
                    "            ?,\n" +
                    "            ?,\n" +
                    "            ?,\n" +
                    "            ?\n" +
                    "        )"
            );
            int idx = 1;
            for(String key : param.keySet()){
                pstmt.setString(idx, param.get(key).toString());
                idx++;
            }
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            Optional.ofNullable(pstmt)
                    .ifPresent(v -> {
                        try {
                            v.close();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    });
            Optional.ofNullable(conn)
                    .ifPresent(v -> {
                        try {
                            v.close();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    });
        }
    }

}
