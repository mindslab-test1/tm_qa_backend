package maum.biz.qa.common.handler;

import com.google.gson.Gson;
import maum.biz.qa.common.config.JwtTokenProvider;
import maum.biz.qa.service.common.AdminUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 스프링시큐리티 로그인 성공 처리 핸들러
 *
 * @author unongko
 * @version 1.0
 */

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);

	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private JwtTokenProvider tokenProvider;

	///////////////////////////////
//	@Autowired
//	private final AuthenticationManager authManager;
//
//	@Autowired
//	private final PasswordEncoder passwordEncoder;
//
//	@Autowired
//	private final JwtTokenProvider jwtTokenProvider;
//
//	@Autowired
//	private final SSOInterface ssoIf;
	///////////////////////////////

	@Value("${env.sessiontime}")
	String sessiontime;

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {

		Gson gson = new Gson();
		logger.info("LoginSuccessHandler gson.toJson(auth) : {} ", gson.toJson(auth));
		logger.info("LoginSuccessHandler request : {} ", request);

		String userId = auth.getName();
		logger.info("LoginSuccessHandler userId : {} ", userId);

//		String userPassword = (String) auth.getPrincipal();
		logger.info("LoginSuccessHandler userPassword : {} ", auth.getPrincipal().getClass());

		/*AdminUser adminUser = new AdminUser();
		try{
			adminUser = adminUserService.getLoginAdminUserDetail(userId);
			logger.info("LoginSuccessHandler adminUser : {} ", adminUser);

			logger.info("LoginSuccessHandler auth : {} ", auth);

			//인증
//			auth = authSvc.authenticate(userId, dto.getPassword());
//			jwt = authSvc.generateJwtToken(auth);

			//로그인 성공시 jwt 토큰 생성
			String token = tokenProvider.generateToken(auth);
			logger.info("LoginSuccessHandler token : {} ", token);
			response.setHeader("Authorization", token);
		}catch (Exception e){
			e.printStackTrace();
		}

		logger.info("LoginSuccessHandler user : {} " ,gson.toJson(adminUser));*/

//		if(adminUser != null){
//			HttpSession session = request.getSession();
//			session.setAttribute("accessUser", adminUser);
//			session.setMaxInactiveInterval(Integer.parseInt(sessiontime));   // 30분
//		}

		//response.sendRedirect(request.getContextPath() + "/common/home/dashboard");
	}

//	public String createToken(String userId) {
//		User user = userMapper.userSignIn(userId);
//		if(!passwordEncoder.matches(userInfo.getU_password(), user.getU_password())) {
//			return null;
//		}else {
//			String token = jwtTokenProvider.createToken(user.getU_email(), user.getAuthorities());
//			user.setU_token(token);
//			return user;
//		}
//	}

//	public Authentication authenticate(String id, String password)
//	{
//		Authentication auth = authManager.authenticate(new UsernamePasswordAuthenticationToken(id, password));
//		SecurityContextHolder.getContext().setAuthentication(auth);
//		return auth;
//	}
	
}