package maum.biz.qa.common.config;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.nio.charset.StandardCharsets;

/**
 * maum.biz.qa.common.config
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-11-22
 */
@Log4j2
@Configuration
@EnableWebSocketMessageBroker
public class StompWebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private final SimpMessagingTemplate template;

    @Value("${ta.realtime.ws.url}")
    private String wsUrl;

    public StompWebSocketConfig(@Lazy SimpMessagingTemplate template) {
        this.template = template;
    }

    @Bean
    public WebSocketClient webSocketClient(){
        return new StandardWebSocketClient();
    }

    @Bean
    public WebSocketHandler webSocketHandler(){
        return new TextWebSocketHandler() {

            @Override
            public void afterConnectionEstablished(WebSocketSession session) throws InterruptedException {
                log.info("STT REALTIME MONITORING SOCKET CONNECTION");
                while (true){
                    template.convertAndSend("/ws/subscribe/realtime/monitoring", "test");
                    Thread.sleep(1000);
                }
            }

            @SneakyThrows
            @Override
            public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
                log.info("STT REALTIME MONITORING SOCKET CLOSE");
                session.close();
            }

            @Override
            protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
                String msg = new String(message.getPayload().array(), StandardCharsets.UTF_8);
                template.convertAndSend("/ws/subscribe/realtime/monitoring", msg);
            }

        };
    }

    @Bean
    public WebSocketConnectionManager webSocketConnectionManager(WebSocketClient webSocketClient, WebSocketHandler webSocketHandler){
        return new WebSocketConnectionManager(webSocketClient, webSocketHandler, wsUrl);
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws")
                .setAllowedOrigins("http://localhost:8001")
                .withSockJS();
    }

}