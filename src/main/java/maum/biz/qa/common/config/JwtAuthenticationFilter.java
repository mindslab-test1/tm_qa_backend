package maum.biz.qa.common.config;


import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.service.common.AdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Jwt가 유효한 토큰인지 인증하기 위한 Filter
 */

@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private AdminUserService adminUserService;


    // Request로 들어오는 Jwt Token의 유효성을 검증하는 filter를 filterChain에 등록합니다.
    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain filterChain) throws ServletException, IOException {
        if(!req.getServletPath().contains("/ws")){
            try {
                String jwt = getJwtFromRequest(req);

                log.info("2. JwtAuthenticationFilter :: doFilterInternal ::: jwt ===> {}", jwt);

                if(StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                    String userId = tokenProvider.getUserNoFromJWT(jwt);
                    UserDetails userDetails = adminUserService.loadUserByUsername(userId);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
            catch (Exception e) {
                log.error("Could not set user authentication in security context", e);
            }
        }
        filterChain.doFilter(req, res);
    }

    // 헤더에서 토큰정보 반환
    private String getJwtFromRequest(HttpServletRequest req) {
        log.info("1. JwtAuthenticationFilter :: getJwtFromRequest ::: req.getHeader(Authorization) ===> {}", req.getHeader("Authorization"));

        String bearerToken = req.getHeader("Authorization");//Authorization
        log.info("11. JwtAuthenticationFilter :: getJwtFromRequest ::: bearerToken===> "+bearerToken);

        if(StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            log.info("12. JwtAuthenticationFilter :: hasText & startsWith===bearerToken.length==> "+bearerToken.substring(7, bearerToken.length()) );
            return bearerToken.substring(7);
        }else {
            return null;
        }
    }
}
