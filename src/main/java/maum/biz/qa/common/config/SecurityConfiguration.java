package maum.biz.qa.common.config;

import maum.biz.qa.common.handler.LoginFailureHandler;
import maum.biz.qa.common.handler.LoginSuccessHandler;
import maum.biz.qa.service.common.AdminUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
/**
 * 스프링부트 스프링시큐리티 Config
 *
 * @author unongko
 * @version 1.0
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AdminUserService adminUserService;

    @Autowired
    LoginSuccessHandler loginSuccessHandler;

    @Autowired
    LoginFailureHandler loginFailureHandler;

    @Autowired
    private final JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    public SecurityConfiguration(AdminUserService adminUserService, /*UserSecurityDetailService userDetailSvc,*/
                          JwtAuthenticationEntryPoint unauthorizedHandler)
    {
//        this.userDetailSvc = userDetailSvc;
        this.adminUserService = adminUserService;
        this.unauthorizedHandler = unauthorizedHandler;
    }

    //사용여부 체크
    /*@Bean
    public UserAuthenticationProvider authenticationProvider(AdminUserService adminUserService) {
        System.out.println("test authenticationProvider in bCryptPasswordEncoder ==>"+bCryptPasswordEncoder);
        UserAuthenticationProvider authenticationProvider = new UserAuthenticationProvider();
        authenticationProvider.setUserDetailsService(adminUserService);
        authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder);
        return authenticationProvider;
    }*/


    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(authenticationProvider(adminUserService));
        auth.userDetailsService(adminUserService)
            .passwordEncoder(new MessageDigestPasswordEncoder("SHA-256"));
    }

    /*@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }*/

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * 스프링시큐리티의 설정을 할 수 있다.
     * WebSecurity에 접근 허용 설정을 해버리면 이 설정이 적용되지 않는다.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();
        http
            .csrf().disable()	// csrf 사용 안 함 == REST API 사용하기 때문에
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)	// JWT인증사용하므로 세션 사용 안함
            .and()
            .authorizeRequests() //요청에대한 권한을 처리
            // 토큰을 활용하는 경우 모든 요청에 대해 접근이 가능하도록 함
            .antMatchers("/user/sign-in", "/user/sign-out", "/user/change-password", "/user/auth/refresh-token", "/eai/**", "/ws/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .formLogin()
                .disable() //form기반의 로그인에 대해 비활성화
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .invalidateHttpSession(true)
        ;

        //Add our custom JWT security filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * 스프링시큐리티 앞단 설정들을 할 수 있다.
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/resources/css/**")
                .antMatchers("/resources/js/**")
                .antMatchers("/resources/font/**")
                .antMatchers("/resources/audio/**")
                .antMatchers("/resources/images/**")
                .antMatchers("/favicon**")
                .antMatchers("/ws/**")
                .antMatchers("/v3/api-docs", "/configuration/ui", "/swagger-resources/**", "/swagger-ui/**", "/configuration/security", "/webjars/**", "/swagger/**")
        ;
    }

}