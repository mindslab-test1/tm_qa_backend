package maum.biz.qa.common.config;

import maum.biz.qa.common.DataAccessInterceptor;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@MapperScan(value="maum.biz.qa.mapper.db", sqlSessionFactoryRef="sqlSessionFactory")
public class DataSourceConfig {

    String MYBATIS_CONFIG = "classpath:mybatis/mybatis-config.xml";
    String MYBATIS_MAPPER = "classpath:mybatis/mapper/@db/**/*.xml";
    String dbType;
    Environment env;

    public DataSourceConfig(Environment env) {
        this.env = env;
        this.dbType = env.getProperty("spring.dbType");
        this.MYBATIS_MAPPER = MYBATIS_MAPPER.replaceAll("@", dbType);
    }

    @Bean
    @Primary
    public DataSource DataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring."+ dbType +"db.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring."+ dbType +"db.datasource.url"));
        dataSource.setUsername(env.getProperty("spring."+ dbType +"db.datasource.username"));
        dataSource.setPassword(env.getProperty("spring."+ dbType +"db.datasource.password"));
        return dataSource;
    }


    @Bean
    public SqlSessionFactory sqlSessionFactory(@Autowired @Qualifier("DataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPlugins(new DataAccessInterceptor(dataSource));
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        factoryBean.setConfigLocation(resolver.getResource(MYBATIS_CONFIG));
        factoryBean.setMapperLocations(resolver.getResources(MYBATIS_MAPPER));
        factoryBean.setVfs(SpringBootVFS.class);
        factoryBean.setTypeAliasesPackage("maum.biz.qa");
        return factoryBean.getObject();
    }

    @Bean
    public SqlSession sqlSession(@Autowired @Qualifier("sqlSessionFactory") SqlSessionFactory factory) {
        return new SqlSessionTemplate(factory);
    }

}








