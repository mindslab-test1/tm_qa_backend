package maum.biz.qa.common.config;

import com.google.common.collect.ImmutableMap;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.common.entity.UserPrincipal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@Component
public class JwtTokenProvider { // JWT토큰 생성 및 유효성을 검증하는 컴포넌트

    public final static long TOKEN_VALIDATION_SECOND = 1000L * 10;
    public final static long REFRESH_TOKEN_VALIDATION_SECOND = 1000L * 60 * 24 * 2;

    @Value("${spring.jwt.secret}")
    private String jwtSecret;

    @Value("${spring.jwt.accessExpiredTime}")
    private long accessExpiredTime;

    @Value("${spring.jwt.refreshExpiredTime}")
    private long refreshExpiredTime;

    @PostConstruct
    protected void init() {
        jwtSecret = Base64.getEncoder().encodeToString(jwtSecret.getBytes());
    }

    // Jwt 토큰 생성
    public String generateToken(Authentication authentication, String flag, String uid) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + (flag.equals("access") ? accessExpiredTime : refreshExpiredTime));
        Map<String, Object> map = new HashMap<>();
        map.put("uid", uid);
        String jwt =  Jwts.builder()
                .setSubject(userPrincipal.getId())
                .setHeader(map)
                .setIssuedAt(new Date()) // 토큰 발행일자
                .setExpiration(expiryDate) // 토큰 만료일자
                .signWith(SignatureAlgorithm.HS256, jwtSecret)  // 암호화 알고리즘 HS256, secret값 세팅
                .compact();

        return jwt;
    }

    public String getUserNoFromJWT(String token)
    {
        log.info("JwtTokenProvider ::: getUserNoFromJWT :: token====>"+token);
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        log.info("claims===>"+claims);
        return claims.getSubject();
    }

    public String getTokenParseUuid(String token){
        String uid = (String) Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getHeader()
                .get("uid");
        return uid;
    }

    public boolean validateToken(String authToken) throws JwtException
    {
        log.info("JwtTokenProvider ::: validateToken :: authToken===> "+authToken);
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            log.info("validateToken====claims.getBody().getExpiration()===========> {}", claims.getBody().getExpiration());
            boolean isNotExpire = claims.getBody().getExpiration().after(new Date());
            log.info("validateToken====isNotExpire===========> {}", isNotExpire);
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;

        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        } catch (Exception e){
            log.error("exception error!!");
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public Boolean getExpToken(String jwt) {
        try {
            Date expiration = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt).getBody().getExpiration();
            log.info("getExpToken ========= expiration========>"+expiration);
            Date now = new Date();
            if (expiration.after(now)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getUserName(String jwt) {
        try {
            String getName = String.valueOf(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt).getBody().get("name"));
            String getId = String.valueOf(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt).getBody().getId());
            log.info("getName=>"+getName);
            log.info("getId=>"+getId);
            return String.valueOf(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt).getBody().get("name"));
        } catch (Exception e) {
            return null;
        }
    }

}
