package maum.biz.qa.common.config;

import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException
    {
//        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
        System.out.println("JwtAuthenticationEntryPoint ========== e.getMessage()=======>"+e.getMessage());
        if(e instanceof LockedException){
            httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "==========Access Denied=======");
        }else{
            httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "==========Access Denied=======");
        }
    }
}
