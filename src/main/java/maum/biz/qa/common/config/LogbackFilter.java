package maum.biz.qa.common.config;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

public class LogbackFilter extends Filter<ILoggingEvent> {
    @Override
    public FilterReply decide(ILoggingEvent event) {
        if (event.getMessage().contains("INSERT INTO DATA_ACCESS_LOG")) {
            return FilterReply.DENY;
        }else{
            return FilterReply.ACCEPT;
        }
    }
}
