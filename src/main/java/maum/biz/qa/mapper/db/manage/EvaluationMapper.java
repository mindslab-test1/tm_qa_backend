package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.EvaluationVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface EvaluationMapper {
    // 상품코드 조회
    List<CamelListMap> getProductCodeList(EvaluationVO evaluationVO);
    // 평가 카테고리 조회
    List<CamelListMap> getChkCtgCodeList();
    // 상품 회차 조회
    List<CamelListMap> getProdOrd(EvaluationVO evaluationVO);
    // 최초 평가표 조회
    List<CamelListMap> getFirstMainList(EvaluationVO evaluationVO);
    // 최초 평가표 갯수
    int getFirstMainListCnt();
    // 평가표 메인 조회
    List<CamelListMap> getMainList(EvaluationVO evaluationVO);
    // 평가표 메인 리스트 갯수
    int getMainListCnt(EvaluationVO evaluationVO);
    // 평가표 서브 조회
    List<CamelListMap> getSubList(EvaluationVO evaluationVO);
    // 상품 회차 저장
    int insertApplyOrd(EvaluationVO evaluationVO);
    // 상품 회차 삭제
    int deleteApplyOrd(EvaluationVO evaluationVO);
    // 상품코드 저장
    int insertProductCode(EvaluationVO evaluationVO);
    // 상품코드 삭제
    int deleteProductCode(EvaluationVO evaluationVO);
    // 점검 아이템 저장
    int insertChkItmData(EvaluationVO evaluationVO);
    // 점검 아이템 삭제
    int deleteChkItmData(EvaluationVO evaluationVO);
    // 조건 항목 조회
    List<CamelListMap> getCondList(EvaluationVO evaluationVO);
    // 평가구분 리스트 조회
    List<CamelListMap> getEvalCategoryList();
}
