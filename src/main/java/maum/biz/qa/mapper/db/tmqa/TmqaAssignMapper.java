package maum.biz.qa.mapper.db.tmqa;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tmqa.vo.TmqaVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TmqaAssignApiController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-04-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-04-20
 */
@Mapper
@Repository
public interface TmqaAssignMapper {


    /**
     * 심사자별 상세 현황 조회
     * @param tmqaVO
     * @return
     */
    List<CamelListMap> selectPerJudgeDetail(TmqaVO tmqaVO);

    /**
     * 심사자별 상세 현황 조회 CNT
     * @param tmqaVO
     * @return
     */
    int selectPerJudgeDetailCNT(TmqaVO tmqaVO);

    /**
     * 배정현황 조회
     * @param tmqaVO
     * @return
     */
    List<CamelListMap> selectAssignCntList(TmqaVO tmqaVO);

    /**
     * QA심사자 목록 조회
     * @return
     */
    List<CamelListMap> selectQaUserList();

    /**
     * QA심사자 휴무정보 등록/수정
     * @return
     */
    int insertHolidayInfo(TmqaVO tmqaVO);

    /**
     * 배정일자 수정 (배정 프로세스 수행)
     * @param tmqaVO
     * @return
     */
    int updateAssignDate(TmqaVO tmqaVO);

    /**
     * 배정등록 (배정 프로세스 수행)
     * @param tmqaVO
     * @return
     */
    int insertAssignInfo(TmqaVO tmqaVO);

}
