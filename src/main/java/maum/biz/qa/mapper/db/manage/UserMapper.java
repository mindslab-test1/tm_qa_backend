package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelMap;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * UserMapper
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-10  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-10
 */
@Repository
@Mapper
public interface UserMapper {

    /**
     * 사용자 목록 조회 CNT
     * @param param
     * @return
     */
    int getUsersCNT(@Param("param") Map<String, Object> param);

    /**
     * 사용자 조회
     * @param param
     * @return
     */
    CamelMap getUser(@Param("param") Map<String, Object> param);

    /**
     * 사용자 목록 조회
     * @param param
     * @return
     */
    List<CamelMap> getUsers(@Param("param") Map<String, Object> param);

    /**
     * 사용자 중복 검증
     * @param param
     * @return
     */
    int chkUserId(Map<String, Object> param);

    /**
     * 사용자 추가
     * @param param
     * @return
     */
    int setUser(Map<String, Object> param);

    /**
     * 사용자 수정
     * @param param
     * @return
     */
    int updUser(Map<String, Object> param);

    /**
     * 사용자 삭제
     * @param userId
     * @return
     */
    int delUser(@Param("userId") String userId);

    /**
     * 권한 목록 조회
     * @return
     */
    List<CamelMap> getAuths();

    /**
     * 로그 등록
     * @param param
     * @return
     */
    int setLog(Map<String, Object> param);

    /**
     * 직전 비밀번호 변경이력 3건 확인
     * @param param
     * @return
     */
    List<String> getPasswordPrevLog(Map<String, Object> param);

    /**
     * 기간으로 비밀번호 변경 이력 확인
     * @param param
     * @return
     */
    int getPasswordPrevLogByDate(Map<String, Object> param);

    /**
     * 로그인 실패횟수 수정
     * @param userId 회원 아이디
     * @return result record count
     */
    int updateLoginFailCount(String userId);

}
