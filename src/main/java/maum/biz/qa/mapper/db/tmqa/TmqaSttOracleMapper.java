package maum.biz.qa.mapper.db.tmqa;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TmqaSttOracleMapper {
    List<CamelListMap> selectQaResultQuestion(TmqaSttForm tmqaSttForm);
}
