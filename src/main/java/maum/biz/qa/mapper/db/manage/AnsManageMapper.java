package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.manage.vo.QaAnsMnVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AnsManageMapper {
    // 고객답변 메인 리스트 조회
    List<CamelListMap> getMainList(QaAnsMnVO qaAnsMnVO);
    // 고객담변 메인 리스트 갯수 조회
    int getMainListCnt(QaAnsMnVO qaAnsMnVO);
    // 마지막 답변 코드 조회
    String getMaxAnsCd();
    // 답변 추가 팝업 저장(답변명, 답변설명)
    int insertAnsMnItem(QaAnsMnVO qaAnsMnVO);
    // 답변 추가 팝업 답변내용 저장
    int insertAnsCtItem(QaAnsMnVO qaAnsMnVO);
    // 클릭한 항목 팝업 리스트
    CamelMap getEditAnsList(QaAnsMnVO qaAnsMnVO);
    // 답변 설정 답변 내용 추가
    int insertAnsContItem(QaAnsMnVO qaAnsMnVO);
    // 설정 팝업을 통한 메인리스트(답변명, 답변설명) 삭제
    int delAnsMainList(QaAnsMnVO qaAnsMnVO);
    // 설정 팝업을 통한 메인리스트(답변설명} 삭제
    int delAnsContMainList(QaAnsMnVO qaAnsMnVO);
    // 설정 팝업을 통한 메인리스트(답변내용) 삭제
    int delEditContAnsList(QaAnsMnVO qaAnsMnVO);
    // 하나의 ansCd(답변코드)에 대한 모든 리스트
    List<CamelListMap> getContAnsList(QaAnsMnVO qaAnsMnVO);
    // seq기준에 따른 seq증가
    int editContAns(QaAnsMnVO qaAnsMnVO);
    // 삭제에 따른 seq감소
    int editDellContAns(QaAnsMnVO qaAnsMnVO);
}
