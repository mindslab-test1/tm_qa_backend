package maum.biz.qa.mapper.db.tm;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tm.form.TmSttForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface TmSttMapper {

    /**
     * TM STT 대상 목록 조회
     * @param tmSttForm
     * @return
     */
    List<CamelListMap> getTmSttList(TmSttForm tmSttForm);

    /**
     * TM STT 대상 목록 CNT
     * @param tmSttForm
     * @return
     */
    int getTmSttListCnt(TmSttForm tmSttForm);

    /**
     * STT 상태 코드 목록 조회
     * @return
     */
    List<CamelListMap> getProgStatCd();


    /*List<CamelListMap> getTmSttList_old(TmSttForm tmSttForm);
    public int getTmSttListTotalCnt(TmSttForm requestData);
    public HashMap<String, Object> getTmSttInfo(TmSttPopupForm requestData);*/
}
