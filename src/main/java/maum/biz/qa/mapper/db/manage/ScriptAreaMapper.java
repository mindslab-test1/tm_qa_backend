package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.dto.QaScrpDTO;
import maum.biz.qa.model.manage.entity.*;
import maum.biz.qa.model.manage.vo.QaScrpSecVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ScriptAreaMapper {

    List<CamelListMap> getLctgCd();

    List<CamelListMap> getMctgCd();

    List<CamelListMap> getSctgCd();

    List<CamelListMap> getScriptCodeList(QaScrpSecVO qaScrpSecVO);

    List<CamelListMap> getScriptAreaMainList(QaScrpSecVO qaScrpSecVO);

    int getScripAreaMainListCnt(QaScrpSecVO qaScrpSecVO);

    List<CamelListMap> getScriptArticleList(QaScrpSecVO qaScrpSecVO);


    /*구간 코드 관리*/
    // 스크립트 구간 관리 코드 갯수 조회
    int getCodeCnt(QaScrpSecCd qaScrpSecCd);
    // 스크립트 구간 관리 코드 등록
    int setCode(QaScrpSecCd qaScrpSecCd);
    // 스크립트 구간 관리 대분류 조회
    List<QaScrpSecCd> getLctgList();
    // 스크립트 구간 관리 코드 목록 조회
    List<QaScrpSecCd> getCodeList(QaScrpSecCd qaScrpSecCd);
    // 스크립트 구간 관리 코드 수정
    int updCode(QaScrpSecCd qaScrpSecCd);
    // 스크립트 구간 관리 코드 삭제
    int delCode(QaScrpSecCd qaScrpSecCd);

    /*구간 등록*/
    // 구간 목록 조회
    List<QaScrpSec> getScriptAreaList(QaScrpSec qaScrpSec);
    // 구간 갯수 조회
    int getScriptAreaCnt(QaScrpSec qaScrpSec);
    // 구간 등록
    int setScriptArea(QaScrpSec qaScrpSec);
    // 구간 수정
    int updScriptArea(QaScrpSec qaScrpSec);
    // 구간 삭제
    int delScriptArea(QaScrpSec qaScrpSec);

    /*구간 관리 메인*/
    // 구간 관리 메인 목록 조회
    List<QaScrpSec> getScriptAreaMainList2(@Param("qaScrpSec") QaScrpSec qaScrpSec, @Param("commonDBCursorVO") CommonDBCursorVO commonDBCursorVO);
    // 구간 관리 메인 목록 갯수 조회
    int getScriptAreaMainListCnt(QaScrpSec qaScrpSec);

    /*문장 관리*/
    // 문장 관리 목록 조회
    List<QaScrpDTO> getScriptSentenceList(QaScrp qaScrp);
    // 문장 검색 목록 조회
    List<QaStmt> getSentenceList(@Param("searchKeyword") String str, @Param("commonDBCursorVO") CommonDBCursorVO commonDBCursorVO);
    // 문장 검색 목록 갯수 조회
    int getSentenceListCnt(String str);
    // 동일 문장 존재 여부 확인
    int getSentenceCnt(QaScrp qaScrp);
    // 문장 관리 등록
    int setScriptSentence(QaScrp qaScrp);
    // 문장 관리 삭제
    int delScriptSentence(QaScrp qaScrp);

    // 점검기준 상세 설정 조회
    QaScrpDTO getArticleDetail(QaScrp qaScrp);
    // 문장 발화 조건 항목 조회
    List<QaCondList> getCondList();
    // 문장 발화 조건 상세 항목 조회
    List<QaCond> getCondDetail(QaCond qaCond);
    // 점검기준 상세 설정 수정
    int updArticle(QaScrp qaScrp);
}
