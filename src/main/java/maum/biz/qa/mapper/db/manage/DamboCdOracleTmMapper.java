package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.PuncStcVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DamboCdOracleTmMapper {
    List<CamelListMap> getDamboList(PuncStcVO puncStcVO);
}
