package maum.biz.qa.mapper.db.common;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.vo.LoginUserVO;
import maum.biz.qa.model.manage.vo.AccessibleMenuVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 *
 *
 * @author
 * @version 1.0
 * @see /LoginUser.xml
 */

@Repository
@Mapper
public interface LoginUserMapper {

    /** 로그인 계정 상세 조회 */
    LoginUserVO getLoginUserDetail(@Param("userId") String userId);

    /**
     * 마지막 로그인 일자 갱신
     * @param userId 회원 아이디
     * @return result record count
     */
    int updLastLoginDate(String userId);

    /**
     * 메뉴 권한체크
     * @param auth
     * @param path
     * @return
     */
    int accessibleMenuCheck(@Param("auth") String auth, @Param("path") String path);

    /**
     * 최초 로그인 검증
     * @param userId 회원 아이디
     * @return row count
     */
    int checkFirstLogin(String userId);

    /**
     * 토큰 파싱 UUID 조회
     * @param userId 회원 아이디
     * @return uuid
     */
    String getTokenParseUUID(String userId);

    /**
     * 토큰 파싱 UUID 업데이트
     * @param userId 회원 아이디
     * @param uuid uuid
     * @return result record count
     */
    int updateTokenParseUUID(String userId, String uuid);

    /**
     * 로그인 실패횟수 수정
     * @param userId 회원 아이디
     * @return result record count
     */
    int updateLoginFailCount(String userId);

    /**
     * 로그인 실패횟수 확인
     * @param userId 회원 아이디
     * @return loginFailCount
     */
    int getLoginFailCount(String userId);

}