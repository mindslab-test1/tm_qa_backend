package maum.biz.qa.mapper.db.monitoring;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.monitoring.MonitoringVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProcStatusMapper {
    List<CamelListMap> getSttMainList(MonitoringVO monitoringVO);
    List<CamelListMap> getTaMainList(MonitoringVO monitoringVO);
}
