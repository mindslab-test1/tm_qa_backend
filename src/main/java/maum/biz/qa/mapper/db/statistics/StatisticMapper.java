package maum.biz.qa.mapper.db.statistics;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.StatVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StatisticMapper {
    List<CamelListMap> getQaSelectBoxList();

    List<CamelListMap> getQaStatisticList(StatVO statVO);

    List<CamelListMap> selectTmrStatisticList(StatVO statVO);
}
