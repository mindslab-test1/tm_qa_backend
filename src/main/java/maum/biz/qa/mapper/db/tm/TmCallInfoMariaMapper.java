package maum.biz.qa.mapper.db.tm;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tm.form.TmSttPopupForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;


@Repository
@Mapper
public interface TmCallInfoMariaMapper {

    /* call info 조회 */
    public HashMap<String, Object> getTmCallInfo(TmSttPopupForm requestData);
    /* tm cusl sec 상담키워드 조회 */
    public HashMap<String, Object> getTmCuslSec(String recgIdCtt);
    /* tm stt 솔루션 결과 목록 조회 */
    public List<CamelListMap> getTmSttRstList(TmSttPopupForm requestData);
    /* tm stt 솔루션 탐지문장 수정 */
    public int updateTmSttStmt(TmSttPopupForm requestData);
}
