package maum.biz.qa.mapper.db.monitoring;

import maum.biz.qa.common.utils.CamelListMap;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ServiceMoniMapper {
    List<CamelListMap> getMainList();
}
