package maum.biz.qa.mapper.db.tmqa;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.tmqa.vo.TmqaAuditVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * TmqaAuditMapper
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-06  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-06
 */
@Mapper
@Repository
public interface TmqaAuditMapper {

    /**
     * 심사페이지 상단 MASTER 정보
     * @param nsplNo
     * @return
     */
    public CamelMap getQaAuditMasterInfo(@Param("userId")String userId, @Param("nsplNo")String nsplNo);

    /**
     * 상담 콜 목록
     * @param nsplNo
     * @return
     */
    public List<CamelListMap> getQaAuditCallList(String nsplNo);

    /**
     * 심사 회차별 이력 목록
     * @param nsplno
     * @return
     */
    public List<CamelListMap> getQaAuditRoundList(String nsplno);

    /**
     * 심사시간 총계
     * @param nsplno
     * @return
     */
    public String getQaAuditRoundSumQaTime(String nsplno);

    /**
     * 표준 계약 스크립트 목록
     * @param nsplNo
     * @param tmsInfo
     * @param prodCat
     * @param chkItmCd
     * @return
     */
    public List<CamelListMap> getContractScriptList(@Param("nsplNo") String nsplNo, @Param("tmsInfo") String tmsInfo, @Param("prodCat") String prodCat, @Param("chkItmCd") String chkItmCd);

    /**
     * 표준스크립트 탐지결과 목록 조회
     * @param nsplNo
     * @param tmsInfo
     * @param prodCat
     * @param chkItmCd
     * @return
     */
    public List<CamelListMap> getContractScriptDetectionList(@Param("nsplNo") String nsplNo, @Param("tmsInfo") String tmsInfo, @Param("prodCat") String prodCat, @Param("chkItmCd") String chkItmCd);

    /**
     * 평가항목결과 목록 조회
     * @param nsplNo
     * @param tmsInfo
     * @param psnJugCat
     * @param chkCtgCd
     * @return
     */
    public List<CamelListMap> getEvaluationItemList(@Param("nsplNo") String nsplNo, @Param("tmsInfo") String tmsInfo, @Param("psnJugCat") String psnJugCat, @Param("chkCtgCd") String chkCtgCd);

    /**
     * 심사정보 수정 (임시저장, 심사완료)
     * @param tmqaAuditVO
     * @return
     */
    public int updAudit(TmqaAuditVO tmqaAuditVO);

    /**
     * 평가항목결과 수정 (임시저장, 심사완료)
     * @param tmqaAuditVO
     * @return
     */
    public int updQaCheckResult(TmqaAuditVO tmqaAuditVO);

    /**
     * 질의 / 응답 조회
     * @param nsplNo
     * @return
     */
    public List<CamelListMap> getQuestionsAndAnswers(@Param("nsplNo") String nsplNo);

    /**
     * 응답 등록
     * @param param
     * @return
     */
    public int setAnswer(Map<String, Object> param);

}
