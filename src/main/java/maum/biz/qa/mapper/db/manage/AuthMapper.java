package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.vo.AccessibleMenuVO;
import maum.biz.qa.model.manage.vo.AuthVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AuthMapper
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-12  / 전성진	 / 최초 생성
 * </pre>
 * @since 2021-07-12
 */
@Repository
@Mapper
public interface AuthMapper {

    /**
     * 권한 목록 조회
     * @param authVO
     * @param commonDBCursorVO
     * @return
     */
    List<AuthVO> getAuthList(@Param("authVO") AuthVO authVO, @Param("paging") CommonDBCursorVO commonDBCursorVO);

    /**
     * 권한 목록 조회 CNT
     * @param authVO
     * @return
     */
    int getAuthCnt(@Param("authVO") AuthVO authVO);

    /**
     * 권한 등록
     * @param authVO
     * @return
     */
    int setAuth(AuthVO authVO);

    /**
     * 권한 조회
     * @param authVO
     * @return
     */
    AuthVO getAuth(AuthVO authVO);

    /**
     * 권한 수정
     * @param authVO
     * @return
     */
    int updAuth(AuthVO authVO);

    /**
     * 권한 삭제
     * @param authVO
     * @return
     */
    int delAuth(AuthVO authVO);

    /**
     * 접근가능 메뉴 목록 조회
     * @param accessibleMenuVO
     * @return
     */
    List<AccessibleMenuVO> getAccessibleMenuList(AccessibleMenuVO accessibleMenuVO);


    /**
     * 전체 메뉴 목록 조회
     * @return
     */
    List<AccessibleMenuVO> getMenuList();

    /**
     * 이미 등록된 URL인지 판별
     * @param accessibleMenuVO
     * @return
     */
    int getAccessibleMenu(AccessibleMenuVO accessibleMenuVO);

    /**
     * 접근가능 메뉴 등록
     * @param accessibleMenuVO
     * @return
     */
    Integer setAccessibleMenu(@Param("loginUser")LoginUser loginUser, @Param("list")List<AccessibleMenuVO> accessibleMenuVO);

    /**
     * 접근가능 메뉴 삭제
     * @param accessibleMenuVO
     * @return
     */
    int delAccessibleMenu(String auth);

}
