package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.TaReProcVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TaReProcessMapper {
    List<CamelListMap> selectTaReProcessData(TaReProcVO taReProcVO);

    int getTareProcessDataCnt(TaReProcVO taReProcVO);

    int selectTaReProcessTotalCnt(TaReProcVO taReProcVO);

    List<CamelListMap> selectTaReProcessGroupCnt(TaReProcVO taReProcVO);

    int updateReProcess(TaReProcVO taReProcVO);

    int updateReProcessSelect(TaReProcVO taReProcVO);

    List<CamelListMap> selectSttReProcessData(TaReProcVO taReProcVO);

    int updateSttReProcess(TaReProcVO taReProcVO);
}
