package maum.biz.qa.mapper.db.tmqa;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.model.tmqa.form.TmqaSttPopupForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@Mapper
public interface TmqaCallInfoMariaMapper {


    /**
     * QA대상, QA배정, QA결과 목록 조회
     * @param tmqaSttForm
     * @return
     */
    List<CamelListMap> selectQaTargetList(TmqaSttForm tmqaSttForm);

    /**
     * QA대상, QA배정, QA결과 목록 CNT
     * @param tmqaSttForm
     * @return
     */
    int selectQaTargetListCNT(TmqaSttForm tmqaSttForm);

    /**
     * TMQA STT 전수 솔루션 결과 목록 조회
     * @param tmqaSttForm
     * @return
     */
    List<CamelListMap> getTmqaSttRstList(TmqaSttPopupForm tmqaSttForm);

    /**
     * TMQA STT 전수 솔루션 탐지문장 수정
     * @param tmqaSttPopupForm
     * @return
     */
    int updTmSttStmt(TmqaSttPopupForm tmqaSttPopupForm);


    /* 평가항목결과 목록 조회 */
    List<CamelListMap> getTmqaChkRstList(TmqaSttPopupForm requestData);
    /* 표준 계약 스크립트 목록 조회 */
    List<CamelListMap> getTmqaCtrScrpList(TmqaSttPopupForm requestData);
    /* 표준스크립트 탐지결과 목록 조회  */
    List<CamelListMap> getTmqaScrpDtctRstList(TmqaSttPopupForm requestData);

}