package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.entity.QaCuslKwd;
import maum.biz.qa.model.manage.entity.QaCuslSec;
import maum.biz.qa.model.manage.entity.QaDtcDtl;
import maum.biz.qa.model.manage.entity.QaStmt;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ConsultAreaMapper
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-27  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-27
 */
@Repository
@Mapper
public interface ConsultAreaMapper {

    /**
     * 상담구간 목록 CNT 조회
     * @param vo
     * @return
     */
    int getConsultAreaMainListCNT(QaCuslSec vo);

    /**
     * 상담구간 목록 조회
     * @param vo
     * @return
     */
    List<QaCuslSec> getConsultAreaMainList(@Param("vo") QaCuslSec vo, @Param("paging") CommonDBCursorVO commonDBCursorVO);

    /**
     * 문장 목록 조회
     * @param vo
     * @return
     */
    List<QaCuslKwd> getConsultAreaArticleList(QaCuslKwd vo);

    /**
     * 기 등록 상담코드 조회 (사용 등록)
     * @return
     */
    List<QaCuslSec> getNewConsultTarget();

    /**
     * 상담코드 사용등록
     * @param qaCuslSec
     * @return
     */
    int setConsultSection(QaCuslSec qaCuslSec);

    /**
     * 상담코드 정보수정
     * @param qaCuslSec
     * @return
     */
    int updConsultSection(QaCuslSec qaCuslSec);

    /**
     * 상담코드 정보삭제
     * @param cuslCd
     * @param cuslAppOrd
     * @return
     */
    int delConsultSection(@Param("cuslCd") String cuslCd, @Param("cuslAppOrd") int cuslAppOrd);

    /**
     * 문장 목록 CNT
     * @param val
     * @param commonDBCursorVO
     * @return
     */
    int getConsultAreaStmtListCNT(@Param("value") String val, @Param("paging") CommonDBCursorVO commonDBCursorVO);

    /**
     * 문장 목록
     * @param val
     * @param commonDBCursorVO
     * @return
     */
    List<QaStmt> getConsultAreaStmtList(@Param("value") String val, @Param("paging") CommonDBCursorVO commonDBCursorVO);


    /**
     * 키워드 등록
     * @param vo
     * @return
     */
    int setConsultSentence(QaCuslKwd vo);

    /**
     * 키워드 삭제
     * @param vo
     * @return
     */
    int delConsultSentence(QaCuslKwd vo);


    // 탐지 사전 조회
    List<QaDtcDtl> getDtcDtlInfo(String val);

    // 탐지 사전 등록
    int insertDtcDtlData(QaDtcDtl vo);

    // 탐지 사전 삭제
    int deleteDtcDtl(QaDtcDtl vo);
}
