package maum.biz.qa.mapper.db.cs;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.cs.form.CsSttForm;
import maum.biz.qa.model.cs.form.CsSttPopupForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface CsSttMapper {

    List<CamelListMap> getCsSttList(CsSttForm requestData);
    public int getCsSttListTotalCnt(CsSttForm requestData);
    public HashMap<String, Object> getCsSttInfo(CsSttPopupForm requestData);
}
