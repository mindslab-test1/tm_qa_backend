package maum.biz.qa.mapper.db.common;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * CodeMapper
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-11  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-11
 */
@Repository
@Mapper
public interface CodeMapper {

    /**
     * 그룹 명으로 공통코드 목록 조회
     * @param cdGp
     * @param cdNm
     * @param cd
     * @param sortingColumn
     * @param sorting
     * @return
     */
    List<CamelListMap> getCodeListByGroupCode(@Param("cdGp") String cdGp, @Param("cdNm") String cdNm, @Param("cd") String cd, @Param("sortingColumn") String sortingColumn, @Param("sorting") String sorting);

    /**
     * 코드 중복검사
     * @param cdGp
     * @param cd
     * @return
     */
    int chkCode(@Param("cdGp") String cdGp, @Param("cd") String cd);

    /**
     * 코드 등록
     * @param loginUser
     * @param map
     * @return
     */
    int setCodeDetail(@Param("user") LoginUser loginUser, @Param("param") Map<String, Object> map);

    /**
     * 코드 수정
     * @param loginUser
     * @param map
     * @return
     */
    int updCodeDetail(@Param("user") LoginUser loginUser, @Param("param") Map<String, Object> map);

    /**
     * 코드 삭제
     * @param cd
     * @return
     */
    int delCodeDetail(@Param("cd") String cd);

}
