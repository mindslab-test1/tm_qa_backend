package maum.biz.qa.mapper.db.tmqa;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.model.tmqa.form.TmqaSttPopupForm;
import maum.biz.qa.model.tmqa.form.TmqaTargetPopupForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Mapper
public interface TmqaSttMapper {

    // 대상으로 수동 등록, 삭제
    int setTargetY(TmqaSttForm tmqaSttForm);
    int setTargetN(TmqaSttForm tmqaSttForm);

    // 대상설정 팝업 TMR, TA점수, DTCT, 상품명 목록 조회
    List<CamelListMap> selectTmrUserList();
    List<CamelListMap> selectAutoScrList();
    List<CamelListMap> selectDtctList();
    List<CamelListMap> selectProdList();

    // 대상설정 팝업 건수 조회, 등록, 삭제
    List<CamelListMap> getAutoCntList();
    int setAutoCntTmr(TmqaTargetPopupForm tmqaTargetPopupForm);
    int setAutoCntScr(TmqaTargetPopupForm tmqaTargetPopupForm);
    int setAutoCntDtct(TmqaTargetPopupForm tmqaTargetPopupForm);
    int setAutoCntProd(TmqaTargetPopupForm tmqaTargetPopupForm);
    int delAutoCnt();

    // 대상설정 팝업 TMR, TA점수, DTCT, 상품명 등록
    int insertTmrTarget(TmqaTargetPopupForm tmqaTargetPopupForm);
    int insertTaScrTarget(TmqaTargetPopupForm tmqaTargetPopupForm);
    int insertEvalTarget(TmqaTargetPopupForm tmqaTargetPopupForm);
    int insertProdTarget(TmqaTargetPopupForm tmqaTargetPopupForm);

    // 대상설정 팝업 TMR, TA점수, DTCT, 상품명 삭제
    int delAutoTmr();
    int delAutoScr();
    int delAutoDtct();
    int delAutoProd();
}
