package maum.biz.qa.mapper.db.common;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.common.vo.DashboardVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface DashboardMapper {

    CamelMap selectCsSttChartInfo();

    CamelMap selectCsTaChartInfo();

    CamelMap selectTmSttChartInfo();

    CamelMap selectTmQaChartInfo();

    List<CamelListMap> selectTaScoreBarChart(DashboardVO dashboardVO);

    List<CamelListMap> selectCtrCntrList(DashboardVO dashboardVO);

    List<CamelListMap> selectTmrCntrList(DashboardVO dashboardVO);

    List<CamelListMap> selectMonthTaScoreList(DashboardVO dashboardVO);
}
