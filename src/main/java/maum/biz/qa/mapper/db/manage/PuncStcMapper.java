package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.manage.vo.PuncStcVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface PuncStcMapper {
    List<CamelListMap> getMainList(PuncStcVO puncStcVO);

    int getMainListCnt(PuncStcVO puncStcVO);

    List<CamelListMap> getProdCat();

    int insertCondItem(PuncStcVO puncStcVO);

    CamelMap getEditCondItemNm(PuncStcVO puncStcVO);

    List<CamelListMap> getPuncStcCode(PuncStcVO puncStcVO);

    int delPuncStcdata(PuncStcVO puncStcVO);

    CamelMap getCondItmCd(PuncStcVO puncStcVO);

    CamelMap getPuncMainList(PuncStcVO puncStcVO);

    int insertPuncStc(PuncStcVO puncStcVO);

    List<CamelListMap> getPuncList(PuncStcVO puncStcVO);

    List<CamelListMap> getCompareCode();

    List<CamelListMap> getOperatorCode();

    List<CamelListMap> getDamboList(PuncStcVO puncStcVO);

    List<CamelListMap> getConValCode();

    List<CamelListMap> getLogicCode();

    int delSpItmData(PuncStcVO puncStcVO);

    int insertSpItmData(PuncStcVO puncStcVO);

    int putSpecialAgCd(PuncStcVO puncStcVO);

    int putCondTbItem(PuncStcVO puncStcVO);
}
