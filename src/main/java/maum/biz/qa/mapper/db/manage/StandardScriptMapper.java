package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.StandardScriptVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StandardScriptMapper {

    // 보험 상품 조회
    List<CamelListMap> getProductList(StandardScriptVO vo);
    // 평가항목 조회
    List<CamelListMap> getEvaluationItemList(StandardScriptVO vo);
    // 구간 선택 조회
    List<CamelListMap> getIntervalChoiceList(StandardScriptVO vo);
    // 스크립트 가이드문구 목록 조회
    List<CamelListMap> getScriptSubList(StandardScriptVO vo);
    // 상품 등록
    int insertProductData(StandardScriptVO vo);
    // 상품 삭제
    int deleteProductData(StandardScriptVO vo);
    // 상품 평가 항목 복제
    int copyQaProdChkItm(StandardScriptVO vo);
    // 상품 상담 복제
    int copyQaCuslSecInfo(StandardScriptVO vo);
    // 상품 스크립트 복제
    int copyQaScrpSecInfo(StandardScriptVO vo);


    List<CamelListMap> getScrpSecInfo(StandardScriptVO vo);

    List<CamelListMap> getStmtData(StandardScriptVO vo);

    List<CamelListMap> getCuslSecInfo(StandardScriptVO vo);

    List<CamelListMap> getKeywordData(StandardScriptVO vo);

    List<CamelListMap> getChoosedChkItm(StandardScriptVO vo);

    int insertCheckItem(StandardScriptVO vo);

    int deleteCheckItem(StandardScriptVO vo);

    int insertCuslSecInfo(StandardScriptVO vo);

    int deleteCuslSecInfo(StandardScriptVO vo);

    int insertScrpSecInfo(StandardScriptVO vo);

    int deleteScrpSecInfo(StandardScriptVO vo);

    // 적용차수 리스트 조회
    List<CamelListMap> getApplyOrdList(StandardScriptVO vo);
}
