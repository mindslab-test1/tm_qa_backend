package maum.biz.qa.mapper.db.cs;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.cs.form.CsSttPopupForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;


@Repository
@Mapper
public interface CsCallInfoMariaMapper {

    /* call info 조회 */
    public HashMap<String, Object> getCsCallInfo(CsSttPopupForm requestData);
    /* cs cusl sec 상담키워드 조회 */
    public HashMap<String, Object> getCsCuslSec(String recgIdCtt);
    /* cs stt 솔루션 결과 목록 조회 */
    public List<CamelListMap> getCsSttRstList(CsSttPopupForm requestData);
    /* cs stt 솔루션 탐지문장 수정 */
    public int updateCsSttStmt(CsSttPopupForm requestData);
}
