package maum.biz.qa.mapper.db.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.manage.vo.QaStcMngVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SentenceManageMapper {
    List<CamelListMap> getSentenceManageList(QaStcMngVO qaStcMngVO);

    int getSentenceListCnt(QaStcMngVO qaStcMngVO);

    List<CamelListMap> getApplyAns();

    CamelMap getDetailManage(QaStcMngVO qaStcMngVO);

    List<CamelListMap> sentencePopList(QaStcMngVO qaStcMngVO);

    int getsentencePopListCnt(QaStcMngVO qaStcMngVO);

    List<CamelListMap> getDtcDtlInfo(QaStcMngVO qaStcMngVO);

    int putSentenceData(QaStcMngVO qaStcMngVO);

    int putAnsData(QaStcMngVO qaStcMngVO);

    int insertSentenceRegi(QaStcMngVO qaStcMngVO);

    int delSentenceData(QaStcMngVO qaStcMngVO);

    int insertDtcDtlInfo(QaStcMngVO qaStcMngVO);

    int deleteDtcDtlInfo(QaStcMngVO qaStcMngVO);

    int deleteDetectionDic(QaStcMngVO qaStcMngVO);

    int getMaxDtctDtcGrpNo(QaStcMngVO qaStcMngVO);
}
