package maum.biz.qa.mapper.db.statistics;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.StatVO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StatisticOracleMapper {
    List<CamelListMap> getQaSelectOracleList(StatVO statVO);

    List<CamelListMap> selectTmrOracleList(StatVO statVO);
}
