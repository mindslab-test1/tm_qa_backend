package maum.biz.qa.controller.manage;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.manage.entity.QaCodeDtl;
import maum.biz.qa.model.manage.entity.QaCuslKwd;
import maum.biz.qa.model.manage.entity.QaCuslSec;
import maum.biz.qa.service.manage.ConsultAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage")
public class ManageConsultAreaController {

    @Autowired
    ConsultAreaService service;

    //상담 구간 관리
    @RequestMapping("/consultAreaView")
    public String viewMain(HttpServletRequest request, Model model) {
        log.info("ManageConsultController.{}", "viewMain");

        HttpSession httpSession = request.getSession(true);
        // 탭메뉴 세션 설정
        String tabMenu = "상담 구간 관리";
        String tabMenuUrl = "/manage/consultAreaView";

        String tabMenuAll = (String)httpSession.getAttribute("tabMenuAll");
        String tabMenuUrlAll = (String)httpSession.getAttribute("tabMenuUrlAll");

        if("".equals(tabMenuAll) || tabMenuAll == null){
            tabMenuAll = tabMenu;
            tabMenuUrlAll = tabMenuUrl;
        }else if(!tabMenuAll.contains(tabMenu)){
            tabMenuAll = tabMenuAll+","+tabMenu;
            tabMenuUrlAll = tabMenuUrlAll+","+tabMenuUrl;
        }else {}

        httpSession.setAttribute("tabMenu", tabMenu);
        httpSession.setAttribute("tabMenuAll", tabMenuAll);
        httpSession.setAttribute("tabMenuUrlAll", tabMenuUrlAll);
        // 탭메뉴 세션 설정

        //상담코드 조회
        /*model.addAttribute("codeList", service.getQaCodeDtlList(new QaCodeDtl()));*/

        Gson gson = new Gson();
        log.info("viewMain() httpSession : {}", gson.toJson(httpSession));

        return tabMenuUrl;
    }

    // 상담 목록 테이블 로드
    @RequestMapping("/consultAreaView/consultList")
    public String loadScriptTable(@RequestParam("jsonStr") String jsonStr, Model model) {
        Gson gson = new Gson();
        QaCuslSec vo = gson.fromJson(jsonStr, QaCuslSec.class);
        
        /*model.addAttribute("consultList", service.getQaCuslSecList(vo));*/
        return "empty/manage/subparts/consultAreaSubForm";
    }

    // 문장 목록 테이블 로드
    @RequestMapping(value = "/consultAreaView/articleList")
    public String loadArticleTable(@RequestParam("jsonStr") String jsonStr,
                                   @RequestParam("kwdMode") String mode, Model model) {
        Gson gson = new Gson();
        QaCuslKwd vo = gson.fromJson(jsonStr, QaCuslKwd.class);

        model.addAttribute("jsonCondition", jsonStr);
        model.addAttribute("mode", mode);



        return "empty/manage/subparts/consultAreaSubForm";
    }

    // 상담관리 팝업
    @RequestMapping("/consultAreaView/consultPop")
    public String consultPopup(@RequestParam(value="jsonStr", required=false, defaultValue = "") String jsonStr,
                               @RequestParam(value="kwdMode", required=false, defaultValue = "") String mode, Model model) {

        Gson gson = new Gson();
        if (mode != null && mode.equals("U")) {
            QaCuslSec vo = gson.fromJson(jsonStr, QaCuslSec.class);
            /*model.addAttribute("codeList", service.getQaCuslSecList(vo));*/
        } else {
            //상담코드 조회
            /*List<QaCuslSec> tList = service.getNewConsultTarget();*/
            /*model.addAttribute("codeList", tList);
            model.addAttribute("jsonObj", gson.toJson(tList));*/
        }
        model.addAttribute("mode", mode);

        return "popup/manage/popup/consultAreaSetPop";
    }

    // 문장 관리 팝업
    @RequestMapping("/consultAreaView/articlePop")
    public String articleManagePopup() {
        return "popup/manage/popup/consultAreaManageArticlePop";
    }

    // 문장 추가 팝업
    @RequestMapping("/consultAreaView/addArticlePop")
    public String addArticlePopup(@RequestParam(value="cd", required=false, defaultValue = "") String cd,
                                  @RequestParam(value="ord", required=false, defaultValue = "") String ord, Model model) {
        model.addAttribute("cuslCd", cd);
        model.addAttribute("cuslOrd", ord);
        return "popup/manage/popup/consultAreaAddArticlePop";
    }

    // 문장 추가 조회 목록
    @RequestMapping("/consultAreaView/addArticlePopList")
    public String addArticlePopList(@RequestParam("condStr") String condStr, Model model) {
        /*List<QaStmt> resList = service.getQaStmtList(condStr);
        Gson gson = new Gson();
        model.addAttribute("jsonObj", gson.toJson(resList));
        model.addAttribute("stmtList", resList);
*/
        return "empty/manage/subparts/consultAreaSubForm";
    }

    // 탐지사전 조회 목록
    @RequestMapping("/consultAreaView/manageArticlePopList")
    public String manageArticlePopList(@RequestParam("condStr") String condStr, Model model) {
        /*List<QaStmt> resList = service.getQaStmtList(condStr);
        Gson gson = new Gson();
        model.addAttribute("jsonObj", gson.toJson(resList));
        model.addAttribute("dctStmtList", resList);
*/
        return "empty/manage/subparts/consultAreaSubForm";
    }

    // 상담코드 관리 팝업
    @RequestMapping("/consultAreaView/consultCodePop")
    public String consultCodePopup() {
        return "popup/manage/popup/consultAreaCodePop";
    }

    // 상담코드 팝업 목록
    @RequestMapping("/consultAreaView/consultCodePopList")
    public String consultCodePopList(@RequestParam("jsonStr") String jsonStr,
                                     @RequestParam("mode") String mode, Model model) {
        Gson gson = new Gson();
        QaCodeDtl vo = gson.fromJson(jsonStr, QaCodeDtl.class);
        /*List<QaCodeDtl> dataList = service.getQaCodeDtlList(vo);*/
        /*model.addAttribute("popCodeList", dataList);
        model.addAttribute("jsonData", gson.toJson(dataList));*/
        model.addAttribute("mode", mode);
        model.addAttribute("jsonCondition", jsonStr);

        //return "noLayout/manage/subparts/popConsultCodeTable";
        return "empty/manage/subparts/consultAreaSubForm";
    }
}
