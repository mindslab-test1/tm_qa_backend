package maum.biz.qa.controller.manage;

import io.swagger.annotations.ApiOperation;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.service.manage.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * UserApiController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-10  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-10
 */
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage/user")
public class UserApiController {

    final UserService userService;

    public UserApiController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 사용자 목록 조회
     * @param param
     * @return
     */
    @ApiOperation(value = "관리설정 - 권한 설정 - 사용자 목록", notes = "사용자 목록 조회")
    @GetMapping("/users")
    public Map<String, Object> getUsers(@RequestParam Map<String, Object> param){
        return userService.getUsers(param);
    }

    /**
     * 권한 목록 조회
     * @return
     */
    @ApiOperation(value = "관리설정 - 권한 설정 - 권한 목록", notes = "권한 목록 조회")
    @GetMapping("/auths")
    public Map<String, Object> getAuths(){
        return userService.getAuths();
    }

    /**
     * 사용자 추가
     * @param loginUser
     * @param param
     * @return
     */
    @ApiOperation(value = "관리설정 - 권한 설정 - 사용자 추가", notes = "사용자 추가")
    @PostMapping
    public Map<String, Object> setUser(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                       @RequestBody Map<String, Object> param){
        return userService.setUser(loginUser, param);
    }

    /**
     * 사용자 수정
     * @param loginUser
     * @param param
     * @return
     */
    @ApiOperation(value = "관리설정 - 권한 설정 - 사용자 수정", notes = "사용자 수정")
    @PatchMapping
    public Map<String, Object> updUser(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                       @RequestBody Map<String, Object> param){
        return userService.updUser(loginUser, param);
    }

    /**
     * 사용자 잠금 해제
     * @param loginUser
     * @param userId
     * @return
     */
    @ApiOperation(value = "관리설정 - 권한 설정 - 사용자 잠금 해제", notes = "사용자 잠금 해제")
    @PatchMapping("/{userId}")
    public Map<String, Object> updUnlockUser(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                             @PathVariable("userId") String userId){
        return userService.updateLoginFailCount(userId);
    }



    /**
     * 사용자 삭제
     * @param loginUser
     * @param userId
     * @return
     */
    @ApiOperation(value = "관리설정 - 권한 설정 - 사용자 삭제", notes = "사용자 삭제")
    @DeleteMapping("/{userId}")
    public Map<String, Object> delUser(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                       @PathVariable("userId") String userId){
        return userService.delUser(loginUser, userId);
    }
}
