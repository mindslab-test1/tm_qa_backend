package maum.biz.qa.controller.manage;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.manage.vo.QaStcMngVO;
import maum.biz.qa.service.manage.SentenceManageService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/manage/sentenceManageApi", produces = MediaType.APPLICATION_JSON_VALUE)
public class SentenceManageApiController {
    private SentenceManageService sentenceManageService;

    @Autowired
    public SentenceManageApiController(SentenceManageService sentenceManageService) {
        this.sentenceManageService = sentenceManageService;
    }

    /**
     * [문장 관리] 메인 목록 테이블 로그
     * @param qaStcMngVO
     * @return
     */
    @ApiOperation(value = "문장 관리 - 메인 테이블 리스트 조회", notes = "메인 테이블 리스트 조회")
    @PostMapping("/loadSentenceManageList")
    public Map<String, Object> loadSentenceManageList(@RequestBody QaStcMngVO qaStcMngVO){
        String title = "loadSentenceManageList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = sentenceManageService.getSentenceManageList(qaStcMngVO);
            int totalCnt = sentenceManageService.getSentenceListCnt(qaStcMngVO);
            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
            log.info("dataList >>>>>> ", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장 추가 팝업]답변 내용 목록 리스트 조회
     * @return
     * ANS_CD : 답변코드
     * ANS_NM : 답변명
     * ANS_DESC : 답변설명
     */
    @ApiOperation(value = "문장 관리 - 문장 등롭 팝업 - 답변 내용 리스트 조회", notes = "문장 추가시 답변 내용 리스트 조회")
    @PostMapping("/getApplyAnswer")
    public Map<String, Object> getApplyAnswer(){
        String title = "getApplyAnswer";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = sentenceManageService.getApplyAns();
            resultMap.put("dataList", dataList);
            log.info("dataList >>>>>> ", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장 관리 - 문장 등록 팝업] 문장 추가
     * @param loginUser
     * @param qaStcMngVOList
     * gudStmt : 가이드 문장(문장 내용)
     * scrp : 스크립트(문장 내용)
     * damboCd : 담보코드(특약코드)
     * custAnsYn : 고객답변 유무
     * @return
     */
    @ApiOperation(value = "문장 관리 - 문장 등록 팝업 - 문장 추가", notes = "문장 등록 팝업에서 문장 추가")
    @PostMapping("/addSentenceList")
    public Map<String, Object> addSentenceList(@LoginUserParam LoginUser loginUser,
                                               @RequestBody List<QaStcMngVO> qaStcMngVOList){
        String title = "addSentenceList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(QaStcMngVO perItem : qaStcMngVOList){
                QaStcMngVO vo = new QaStcMngVO();
                BeanUtils.copyProperties(perItem, vo);
                vo.setCreatorId(loginUser.getUserId());
                int ret = sentenceManageService.insertSentenceRegi(vo);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += perItem.getDtctDtcNo();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장관리 상세설정 팝업] 문장 관리 내용 조회
     * @param qaStcMngVO
     * dtctDtcNo : 탐지사전번호
     * @return
     * GUD_STMT : 가이드 문장
     * SCRP : 스크립트
     * DAMBO_CD : 담보코드
     * DTCT_DTC_NO : 탐지사전번호
     * DAMBO_NM : 담보명
     * CUST_ANS_YN : 고객답변여부
     * ANS_NM : 답변명
     * ANS_CD : 답변코드
     */
    @ApiOperation(value = "문장 관리 - 문장관리 상세설정 팝업 - 문장 관리 내용 조회", notes = "문장 관리 내용 조회")
    @PostMapping("/getSentenceDetail")
    public Map<String, Object> getSentenceDetail(@RequestBody QaStcMngVO qaStcMngVO){
        String title = "getSentenceDetail";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            CamelMap detail = sentenceManageService.getSentenceManageDmSetList(qaStcMngVO);
            resultMap.put("detail", detail);
            log.info("detail >>>>>", detail.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장관리 상세설정 팝업] 등록문장 검색
     * @param qaStcMngVO
     * scrp : 스크립트 문장
     * @return
     * SCRP : 스크립트
     * DTCT_DTC_NO : 탐지사전번호
     */
    @ApiOperation(value = "문장 관리 - 문장관리 상세설정 팝업 - 등록문장 검색", notes = "등록문장 검색")
    @PostMapping("/getSentencePopList")
    public Map<String, Object> getSentencePopList(@RequestBody QaStcMngVO qaStcMngVO){
        String title = "getSentencePopList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = sentenceManageService.sentencePopList(qaStcMngVO);
            int totalCnt = sentenceManageService.getSentenceListCnt(qaStcMngVO);
            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
            log.info("dataList", dataList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장관리 상세설정 팝업] 문장 삭제
     * @param qaStcMngVO
     * dtctDtcNo : 탐지사전번호
     * @return
     */
    @ApiOperation(value = "문장 관리 - 문장 삭제", notes = "등록된 문장과 탐지 사전 리스트 삭제")
    @PostMapping("/delSentenceDetail")
    public Map<String, Object> delSentenceDetail(@RequestBody QaStcMngVO qaStcMngVO){
        String title = "getSentencePopList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int ret = sentenceManageService.delSentenceData(qaStcMngVO);
            int ret2 = sentenceManageService.delDetectionDic(qaStcMngVO);

            if(ret > 0 && ret2 > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);

                String failInfo = "";
                if(ret < 1){
                    failInfo += "fail_delSentenceData";
                }
                if(ret2 < 1){
                    failInfo += "fail_delDetectionDic";
                }
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 탐지사전 목록 조회
     * @param qaStcMngVO
     * dtctDtcNo : 탐지사전번호
     * @return
     * DTCT_DTC_NO : 탐지사전번호
     * DTCT_DTC_GRP_NO : 탐지사전그룹번호
     * DTCT_DTC_GRP_IN_NO : 탐지사전그룹내순서
     * DTCT_DTC_ED_NO : 탐지사전끝번호
     * DTCT_DTC_CON : 탐지사전내용
     */
    @ApiOperation(value = "문장 관리 - 문장관리 상세설정 팝업 - 탐지사전 리스트", notes = "탐지사전 리스트 조회")
    @PostMapping("/getSentenceDtcList")
    public Map<String, Object> getSentenceDtcList(@RequestBody QaStcMngVO qaStcMngVO){
        String title = "getSentencePopList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = sentenceManageService.getDtcDtlInfo(qaStcMngVO);
            resultMap.put("dataList", dataList);
            log.info("dataList", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장관리 상세설정 팝업] 탐지사전 내용 삭제
     * @param qaStcMngVO
     * dtctDtcNo : 탐지사전번호
     * dtctDtcGrpNo : 탐지사전그룹번호
     * @return
     */
    @ApiOperation(value = "문장관리 - 문장관리 상세설정 팝업 - 탐지사전 내용 삭제", notes = "탐지사전 리스트 그룹순번 기준으로 삭제")
    @PostMapping("/delDtcDtlInfo")
    public Map<String, Object> delDtcDtlInfo(@RequestBody QaStcMngVO qaStcMngVO){
        String title = "delDtcData";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            String failInfo = "";
            int ret = sentenceManageService.deleteDtcDtlInfo(qaStcMngVO);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                failInfo = qaStcMngVO.getDtctDtcNo() + ":" + qaStcMngVO.getDtctDtcGrpNo()
                        + ":" + qaStcMngVO.getDtctDtcGrpInNo();
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장관리 상세설정 팝업] 탐지사전 내용 추가
     * @param loginUser
     * @param qaStcMngVOList
     * dtctDtcNo : 탐지사전번호
     * dtctDtcGrpNo : 탐지사전그룹번호
     * dtctDtcGrpInNo : 탐지사전그룹내순서
     * dtctDtcEdNo : 탐지사전끝번호
     * @return
     */
    @ApiOperation(value = "문장관리 - 문장관리 상세설정 팝업 - 탐지사전 리스트 추가", notes = "탐지사전 리스트 추가")
    @PostMapping("/insertDtcDtlInfo")
    public Map<String, Object> insertDtcDtlInfo(@LoginUserParam LoginUser loginUser,
                                                @RequestBody List<QaStcMngVO> qaStcMngVOList){
        String title = "delDtcData";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";

            for(QaStcMngVO perItem : qaStcMngVOList){
                perItem.setCreatorId(loginUser.getUserId());
                perItem.setUpdatorId(loginUser.getUserId());
                int ret = sentenceManageService.insertDtcDtlInfo(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += perItem.getDtctDtcNo() + ":" + perItem.getDtctDtcGrpNo()
                            + ":" + perItem.getDtctDtcGrpInNo();
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [문장관리 상세설정 팝업] 문장 관리 업데이트
     * @param loginUser
     * @param qaStcMngVO
     * dtctDtcNo : 탐지사전번호
     * gudDtmt : 가이드 문장
     * scrp : 스크립트
     * damboCd : 담보코드
     * @return
     */
    @PostMapping("/updateSentenceData")
    public Map<String, Object> updateSentenceData(@LoginUserParam LoginUser loginUser,
                                                  @RequestBody QaStcMngVO qaStcMngVO){
        String title = "updateSentenceData";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            log.info("updateSentenceData: ", qaStcMngVO.toString());
            qaStcMngVO.setUpdatorId(loginUser.getUserId());
            int ret = sentenceManageService.putSentenceData(qaStcMngVO);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", qaStcMngVO.getDtctDtcNo());
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    @ApiOperation(value = "문장관리 - 문장관리 상세설정 팝업 - 탐지사전 복사", notes = "탐지사전 복사")
    @PostMapping("/updDtcDtl")
    public Map<String, Object> updDtcDtl(@RequestBody Map<String, Object> paramMap) {
        return sentenceManageService.updDtcDtl(paramMap);
    }
}
