package maum.biz.qa.controller.manage;

import maum.biz.qa.model.common.entity.EmpInfo;
import maum.biz.qa.model.manage.entity.QaScriptInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage")
public class ScriptApiController {

//    @Autowired
//    ScriptService scriptService;

//    @Autowired
//    EmpInfoService empInfoService;

    // 샘플
    @RequestMapping(value="/script/list",  method = {RequestMethod.POST})
    public QaScriptInfo getSampleData() throws Exception {
        QaScriptInfo dto = new QaScriptInfo();
        dto.setStringData1("data1");
//        script.setStringData2("data2");
        dto.setIntegerData1(5);

        System.out.println("getEmpInfoDetail test !!! ");
        EmpInfo empInfo = new EmpInfo();
        int empNo = 7839;
//        empInfo = empInfoService.getEmpInfoDetail(empNo);
        return dto;
    }

}
