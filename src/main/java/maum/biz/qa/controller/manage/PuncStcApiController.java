package maum.biz.qa.controller.manage;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.manage.vo.PuncStcVO;
import maum.biz.qa.service.manage.EvaluationService;
import maum.biz.qa.service.manage.PuncStcOracleService;
import maum.biz.qa.service.manage.PuncStcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/manage/puncStc", produces = MediaType.APPLICATION_JSON_VALUE)
public class PuncStcApiController {
    private PuncStcService puncStcService;
    private PuncStcOracleService puncStcOracleService;
    private EvaluationService evaluationService;

    @Autowired
    public PuncStcApiController(PuncStcService puncStcService, PuncStcOracleService puncStcOracleService,
                                EvaluationService evaluationService) {
        this.puncStcService = puncStcService;
        this.puncStcOracleService = puncStcOracleService;
        this.evaluationService = evaluationService;
    }

    /**
     * 상품구분 리스트 조회
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 상품 구분 리스트", notes = "상품 구분 리스트 조회")
    @GetMapping("/getProdCat")
    public Map<String, Object> getProdCat(){
        String title = "getProdCat";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = puncStcService.getProdCat();
            resultMap.put("dataList" ,dataList);
            log.info("dataList" ,dataList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 문장 발화 메인 리스트 조회
     * @param puncStcVO
     * prodCat : 상품구분
     * condItmNm : 조건항목
     * @return
     * COND_ITM_CD :  조건항목코드
     * PROD_CAT : 보종구분
     * COND_ITM_NM : 조건항목명
     */
    @ApiOperation(value = "문장 발화 조건 - 문장 발화 조건 리스트", notes = "문장 발화 조건 리스트 조회")
    @PostMapping("/getMainList")
    public Map<String, Object> getMainList(@RequestBody PuncStcVO puncStcVO){
        String title = "getMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = puncStcService.getMainList(puncStcVO);
            int totalCnt = puncStcService.getMainListCnt(puncStcVO);
            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
            log.info("dataList", dataList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 조건항목 등록 저장
     * @param loginUser
     * @param puncStcVO
     * condItmCd : 조건항목코드
     * prodCat : 보종구분
     * condItmNm : 조건항목명
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 조건항목 등록", notes = "상품구분, 조건항목명 저장")
    @PostMapping("/insertCondItem")
    public Map<String, Object> insertCondItem(@LoginUserParam LoginUser loginUser,
                                              @RequestBody PuncStcVO puncStcVO){
        String title = "insertCondItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            puncStcVO.setCreatorId(loginUser.getUserId());
            int ret = puncStcService.insertCondItem(puncStcVO);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", "insertCondItem");
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 조건항목 삭제
     * @param loginUser
     * @param puncStcVO
     * prodCat : 보종구분
     * condItmNm : 조건항목명
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 조건항목 삭제", notes = "조건항목 설정 팝업의 조건항목 삭제")
    @PostMapping("/deleteCondItem")
    public Map<String, Object> deleteCondItem(@LoginUserParam LoginUser loginUser,
                                              @RequestBody PuncStcVO puncStcVO){
        String title = "deleteCondItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            puncStcVO.setCreatorId(loginUser.getUserId());
            int ret = puncStcService.delPuncStcData(puncStcVO);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", "deleteCondItem");
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 조건항목 수정
     * @param loginUser
     * @param puncStcVO
     * condItmCd : 조건항목코드
     * prodCat : 보종구분
     * condItmNm : 조건항목명
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 조건항목 수정", notes = "주건항목 설정 팝업창의 상품구분, 조건항목명 수정")
    @PostMapping("/editCondItem")
    public Map<String, Object> editCondItem(@LoginUserParam LoginUser loginUser,
                                              @RequestBody PuncStcVO puncStcVO){
        String title = "editCondItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            puncStcVO.setCreatorId(loginUser.getUserId());
            int ret = puncStcService.insertPuncStc(puncStcVO);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", "insertPuncStc");
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 특약코드 조회
     * @param puncStcVO
     * elagNme : 특약명
     * @return
     * ELAG_CD : 특약코드
     * PROD_CAT : 보종구분
     * ELAG_NME : 특약명
     */
    @ApiOperation(value = "문장 발화 조건 - 특약코드 조회", notes = "기간계 DB에서 특약코드 리스트 조회")
    @PostMapping("/getSpecialAgList")
    public Map<String, Object> getSpecialAgList(@RequestBody PuncStcVO puncStcVO){
        String title = "getSpecialAgList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = puncStcOracleService.getDamBoList(puncStcVO);
            resultMap.put("dataList", dataList);
            log.info("dataList >>>>> " , dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 특약코드 추가
     * @param loginUser
     * @param puncStcVO
     * damboCd : 담보코드
     * damboNm : 담보명
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 특약코드 추가", notes = "기간계 DB에서 특약코드 가져와서 QA_DAMBO_TB에 저장")
    @PostMapping("/addSpecialAgList")
    public Map<String, Object> addSpecialAgList(@LoginUserParam LoginUser loginUser,
                                                @RequestBody PuncStcVO puncStcVO){
        String title = "addSpecialAgList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            puncStcVO.setCreatorId(loginUser.getUserId());
            int ret = puncStcService.putSpecialAgCd(puncStcVO);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", "addSpecialAgList");
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 문장 발화 조건 설정 리스트 조회
     * @param loginUser
     * @param puncStcVO
     * condItmCd : 조건항목코드
     * @return
     * (puncStcList)
     * COND_ITM_CD : 조건항목코드
     * PROD_CAT : 보종구분
     * COND_NO : 조건순서
     * COND_ITM_NM : 조건항목명
     * COND_TP_FIR : 조건타입1
     * COND_VAL_FIR : 조건값1
     * COND_OP_FIR : 조건연산자1
     * COND_TP_SE : 조건타입2
     * COND_OP_SE : 조건연산자2
     * COND_TP_THR : 조건타입3
     * COND_VAL_THR : 조건값3
     * LOGIC_OP : 논리연산자
     * (compareList, operatorList, condValList, logicList)
     * CD_GP : 코드그룹
     * CD : 코드
     * CD_NM : 코드명
     * CD_DESC : 코드설명
     */
    @ApiOperation(value = "문장 발화 조건 - 문장 발화 조건 리스트 조회", notes = "문장 발화 조건 리스트 조회")
    @PostMapping("getPuncList")
    public Map<String, Object> getPuncList(@LoginUserParam LoginUser loginUser,
                                           @RequestBody PuncStcVO puncStcVO){
        String title = "getPuncList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> puncStcList = puncStcService.getPuncList(puncStcVO);
            List<CamelListMap> compareList = puncStcService.getCompareCode();
            List<CamelListMap> operatorList = puncStcService.getOperatorCode();
            List<CamelListMap> condValList = puncStcService.getConValCode();
            List<CamelListMap> logicList = puncStcService.getLogicCode();

            resultMap.put("puncStcList", puncStcList);
            resultMap.put("compareList", compareList);
            resultMap.put("operatorList", operatorList);
            resultMap.put("condValList", condValList);
            resultMap.put("logicList", logicList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 문장 발화 조건 추가
     * @param loginUser
     * @param puncStcVOList
     * condItmCd : 조건항목코드
     * prodCat : 보종구분
     * condNo : 조건순서
     * condItmNm : 조건항목명
     * condTpFir : 조건타입1
     * condValFir : 조건값1
     * condOpFir : 조건연산자1
     * condTpSe : 조건타입2
     * condValSe : 조건값2
     * condOpSe : 조건연산자2
     * condTpThr : 조건타입3
     * condValThr : 조건값3
     * logicOp : 논리연산자
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 문장 발화 조건 추가", notes = "문장 발화 조건 추가")
    @PostMapping("/addSpChkItem")
    public Map<String, Object> addSpChkItem(@LoginUserParam LoginUser loginUser,
                                            @RequestBody List<PuncStcVO> puncStcVOList){
        String title = "addSpChkItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(PuncStcVO perItem : puncStcVOList){
                perItem.setCreatorId(loginUser.getUserId());
                int ret = puncStcService.insertSpItmData(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += (perItem.getCondItmCd() + "_" + perItem.getProdCat() + "_" + perItem.getCondNo());
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 문장 발화 조건 삭제
     * @param puncStcVOList
     * condItmCd : 조건항목코드
     * prodCat : 보종구분
     * condNo : 조건순서
     * @return
     */
    @ApiOperation(value = "문장 발화 조건 - 문장 발화 조건 삭제", notes = "문장 발화 조건 삭제")
    @PostMapping("/delSpChkItem")
    public Map<String, Object> delSpChkItem(@RequestBody List<PuncStcVO> puncStcVOList){
        String title = "addSpChkItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(PuncStcVO perItem : puncStcVOList){
                int ret = puncStcService.delSpItmData(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += (perItem.getCondItmCd() + "_" + perItem.getProdCat() + "_" + perItem.getCondNo());
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
