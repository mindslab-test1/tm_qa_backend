package maum.biz.qa.controller.manage;

import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.vo.AccessibleMenuVO;
import maum.biz.qa.model.manage.vo.AuthVO;
import maum.biz.qa.service.manage.AuthService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * AuthApiController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-12  / 전성진	 / 최초 생성
 * </pre>
 * @since 2021-07-12
 */
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage")
@RequiredArgsConstructor
public class AuthApiController {

    private final AuthService authService;


    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 권한 목록 조회")
    @GetMapping("/authList")
    public Map<Object, Object> getAuthList(AuthVO authVo, CommonDBCursorVO commonDBCursorVO) {
        return authService.getAuthList(authVo, commonDBCursorVO);
    }

    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 권한 등록")
    @PostMapping("/auth")
    public Map<Object, Object> setAuth(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody AuthVO authVo) {
        return authService.setAuth(loginUser, authVo);
    }

    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 권한 조회")
    @GetMapping("/auth")
    public Map<Object, Object> getAuth(AuthVO authVO) {
        return authService.getAuth(authVO);
    }

    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 권한 수정")
    @PutMapping("/auth")
    public Map<Object, Object> updAuth(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody AuthVO authVo) {
        return authService.updAuth(loginUser, authVo);
    }

    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 권한 삭제")
    @DeleteMapping("/auth")
    public Map<Object, Object> delAuth(@RequestBody AuthVO authVo) {
        return authService.delAuth(authVo);
    }

    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 접근가능 메뉴 목록 조회")
    @GetMapping("/auth/menuList")
    public Map<Object, Object> getAccessibleMenuList(AccessibleMenuVO accessibleMenuVO) {
        return authService.getAccessibleMenuList(accessibleMenuVO);
    }

    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 전체 메뉴 목록")
    @GetMapping("/auth/menu")
    public Map<Object, Object> getMenuList() {
        return authService.getMenuList();
    }


    @ApiOperation(value = "관리설정 - 권한 관리 페이지의 접근가능 메뉴 등록")
    @PostMapping("/auth/menu")
    public Map<Object, Object> setAccessibleMenu(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody List<AccessibleMenuVO> list) {
        try {
            return authService.setAccessibleMenu(loginUser, list);
        } catch (SQLException e) {
            e.printStackTrace();
            return ImmutableMap.builder().put("status", DataAccessStatus.FAIL).put("message", e.getMessage()).build();
        }
    }

}
