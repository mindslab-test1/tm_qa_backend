package maum.biz.qa.controller.manage;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.manage.vo.TaReProcVO;
import maum.biz.qa.service.common.CodeService;
import maum.biz.qa.service.manage.TaReProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage/taReprocess", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaReProcessApiController {
    private TaReProcessService taReProcessService;
    private CodeService codeService;

    @Autowired
    public TaReProcessApiController(TaReProcessService taReProcessService,
                                    CodeService codeService) {
        this.taReProcessService = taReProcessService;
        this.codeService = codeService;
    }



    /**
     * TA 재처리 리스트 조회
     * @param taReProcVO
     * nsplNo : 증권번호
     * fromDate : 조회요청 시작일자
     * toDate : 조회요청 끝일자
     * @return
     * NSPL_NO : 증권번호
     * TA_TMS_INFO :  TA 회차번호
     * QA_REQ_TM : QA 등록일자
     * PROG_STAT_CD : 진행상태코드
     * CD_NM : 코드명
     */
    @ApiOperation(value = "Ta 재처리 관리 - 재처리 조회", notes = "TA 재처리 리스트 조회")
    @PostMapping("/taReMainList")
    public Map<String, Object> getTaReMainList(@RequestBody TaReProcVO taReProcVO){
        String title = "getTaReMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = taReProcessService.selectTaReProcessData(taReProcVO);
            int totalCnt = taReProcessService.getTareProcessDataCnt(taReProcVO);
            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
            log.info("dataList >>>>> " + dataList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * TA 전체 건수 조회
     * @param taReProcVO
     * fromDate : 조회일자 시작
     * toDate : 조회일자 끝
     * nsplNo : 증권번호
     * @return
     */
    @ApiOperation(value = "Ta 재처리 관리 - TA 건수", notes = "TA 전체 건수 조회")
    @PostMapping("/taReProcessTotalCnt")
    public Map<String, Object> getTaReProcessTotalCnt(@RequestBody TaReProcVO taReProcVO){
        String title = "getTaReProcessTotalCnt";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int taReTotalCnt = taReProcessService.selectTaReProcessTotalCnt(taReProcVO);
            resultMap.put("taReTotalCnt", taReTotalCnt);
            log.info("taReTotalCnt >>>>> " + taReTotalCnt);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * TA 건수 별 리스트 조회
     * @param taReProcVO
     * nsplNo : 증권번호
     * fromDate : 조회요청 시작일자
     * toDate : 조회요청 끝일자
     * @return
     * PROG_STAT_CD : 진행상태코드
     * CNT : 건수
     */
    @ApiOperation(value = "TA 재처리 관리 - TA 건수 리스트", notes = "TA 건수 별 리스트 조회")
    @PostMapping("/taReGroupCnt")
    public Map<String, Object> getTaReGroupCnt(@RequestBody TaReProcVO taReProcVO){
        String title = "getTaReGroupCnt";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = taReProcessService.selectTaReProcessGroupCnt(taReProcVO);
            resultMap.put("dataList", dataList);
            log.info("dataList >>>>> " + dataList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 진행상태코드 리스트 조회
     * @return
     */
    @ApiOperation(value = "TA 재처리 관리 - 진행상태코드 조회", notes = "진행상태코드 리스트 조회")
    @GetMapping("/progStatCd")
    public Map<String, Object> getProgStatCd(){
        String title = "getProgStatCd";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            resultMap = codeService.getCodeListByGroupCode("PROG_STAT_CD", "","","","");
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * TA 재처리 요청
     * @param loginUser
     * @param taReProcVOList
     * fromDate : 조회일자 시작
     * toDate : 조회일자 끝
     * nsplNo : 증권번호
     * taTmsInfo : TA 회차정보
     * @return
     */
    @ApiOperation(value = "TA 재처리 관리 - TA 재처리", notes = "TA 재처리 요청")
    @PostMapping("/updateReProcess")
    public Map<String, Object> updateReProcess(@LoginUserParam LoginUser loginUser,
                                               @RequestBody List<TaReProcVO> taReProcVOList){
        String title = "updateReProcess";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            taReProcVOList.forEach(elem -> {
                elem.setCreatorId(loginUser.getUserId());
                int ret = 0;
                ret += taReProcessService.updateReProcess(elem);
                if(ret > 0){
                    resultMap.put("isSuccess", true);
                }else{
                    resultMap.put("isSuccess", false);
                }
            });
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
