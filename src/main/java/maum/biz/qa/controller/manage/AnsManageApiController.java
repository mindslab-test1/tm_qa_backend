package maum.biz.qa.controller.manage;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.manage.vo.QaAnsMnVO;
import maum.biz.qa.service.manage.AnsManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/manage/ansManageApi")
public class AnsManageApiController {
    private AnsManageService ansManageService;

    @Autowired
    public AnsManageApiController(AnsManageService ansManageService) {
        this.ansManageService = ansManageService;
    }

    /**
     * 고객답변 메인 리스트 조회
     * @param qaAnsMnVO
     * ansNm : 답변명
     * ansCont : 답변내용
     * @return
     * ANS_CD : 답변코드
     * ANS_NM : 답변명
     * ANS_DESC : 답변설명
     * ANS_SEQ : 답변순서
     * ANS_CONT : 답변내용
     * CREATED_TM : 등록일자
     * UPDATED_TM : 수정일자
     */
    @PostMapping("/mainList")
    public Map<String, Object> getMainList(@RequestBody QaAnsMnVO qaAnsMnVO){
        String title = "getMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = ansManageService.getMainList(qaAnsMnVO);
            int length = ansManageService.getMainListCnt(qaAnsMnVO);
            resultMap.put("dataList", dataList);
            resultMap.put("length", length);
            log.info("result: ", resultMap.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 등록 팝업 - 답변명, 답변설명 추가
     * @param loginUser
     * @param qaAnsMnVO
     * ANS_CD : 답변코드
     * ANS_NM : 답변명
     * ANS_DESC : 답변설명
     * CREATOR_ID : 생성자 아이디
     * @return
     * isSuccess : 추가 성공 여부
     */
    @PostMapping("/addAns")
    public Map<String, Object> ansNmAddPop(@LoginUserParam LoginUser loginUser,
                                               @RequestBody QaAnsMnVO qaAnsMnVO){
        String title = "ansNmAddPop";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            // 답변 코드 숫자 증가
            String oldCd = ansManageService.getMaxAnsCd(); // 기존 답변 코드 가져옴
            String prefix = oldCd.substring(0,1); // A
            String num = oldCd.substring(1); // 00001
            String newAnsCd = prefix + String.format("%05d", (Integer.parseInt(num) + 1));
            QaAnsMnVO vo = QaAnsMnVO.builder()
                                .ansCd(newAnsCd)
                                .creatorId(loginUser.getUserId())
                                .ansNm(qaAnsMnVO.getAnsNm())
                                .ansDesc(qaAnsMnVO.getAnsDesc())
                                .build();

            int ret = ansManageService.insertAnsMnList(vo);
            if(ret > 0){
                resultMap.put("isSuccess", true);
                resultMap.put("newAnsCd", newAnsCd);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", qaAnsMnVO.getAnsNm());
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 등록 팝업 - 답변 내용 추가
     * @param loginUser
     * @param qaAnsMnVOList
     * ANS_CD : 답변코드
     * ANS_SEQ : 답변 순서
     * ANS_CONT : 답변 내용
     * CREATOR_ID : 생성자 아이디
     * @return
     * isSuccess : 추가 성공 여부
     */
    @PostMapping("/addAnsList")
    public Map<String, Object> ansContAddPop(@LoginUserParam LoginUser loginUser,
                                             @RequestBody List<QaAnsMnVO> qaAnsMnVOList){
        String title = "ansNmAddPop";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            // 답변 코드 숫자 증가
            /*String oldCd = ansManageService.getMaxAnsCd(); // 기존 답변 코드 가져옴
            String prefix = oldCd.substring(0,1); // A
            String num = oldCd.substring(1); // 00001
            String newAnsCd = prefix + String.format("%05d", (Integer.parseInt(num) + 1));*/

            for(QaAnsMnVO perItem : qaAnsMnVOList){
                QaAnsMnVO vo = QaAnsMnVO.builder()
                                        .ansCd(perItem.getAnsCd())
                                        .ansSeq(perItem.getAnsSeq())
                                        .ansCont(perItem.getAnsCont())
                                        .creatorId(loginUser.getUserId())
                                        .build();
                int ret = ansManageService.insertAnsCtItem(vo);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += (perItem.getAnsCd()) + "_" + perItem.getAnsSeq() + "_" + perItem.getAnsCont();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 설정 팝업 - 데이터 리스트 조회
     * @param qaAnsMnVO
     * ansNm : 답변명
     * ansDesc : 답변설명
     * ansCd : 답변코드
     * @return
     * ANS_CD : 답변코드
     * ANS_NM : 답변명
     * ANS_DESC : 답변내용
     * UPDATOR_ID : 엡데이트 아이디
     * UPDATED_TM : 업데이트 일자
     * CREATOR_ID : 생성 아이디
     * CREATED_TM : 생성 일자
     * ANS_SEQ : 답변 순서
     */
    @PostMapping("/getAnsEditList")
    public Map<String, Object>  getAnsEditList(@RequestBody QaAnsMnVO qaAnsMnVO){
        String title = "getAnsEditList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            CamelMap ansMainData = ansManageService.getEditAnsList(qaAnsMnVO);
            List<CamelListMap> ansContList = ansManageService.getContAnsList(qaAnsMnVO);

            resultMap.put("ansMainData", ansMainData);
            resultMap.put("ansContList", ansContList);
            log.info("result >>>>>>>>>>", resultMap.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 설정 찹업창 - 답변명, 답변설명 수정
     * @param loginUser
     * @param qaAnsMnVO
     * ansCd : 답변코드
     * ansNm : 답변명
     * ansDesc : 답변설명
     * @return
     */
    @PostMapping("/saveEditAnsNmItem")
    public Map<String, Object> saveEditAnsNmItem(@LoginUserParam LoginUser loginUser,
                                                   @RequestBody QaAnsMnVO qaAnsMnVO){
        String title = "ansEditNmDescAddPop";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            // [답변 설정 팝업 저장]
            // 답변 설정 팝업 저장(답변명, 답변설명)
            QaAnsMnVO vo = QaAnsMnVO.builder()
                                    .ansCd(qaAnsMnVO.getAnsCd())
                                    .ansNm(qaAnsMnVO.getAnsNm())
                                    .ansDesc(qaAnsMnVO.getAnsDesc())
                                    .updatorId(loginUser.getUserId())
                                    .build();
            int ret = ansManageService.insertAnsMnList(vo);

            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", qaAnsMnVO.getAnsNm());
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 설정 찹업창 - 답변내용 리스트 수정
     * @param loginUser
     * @param qaAnsMnVOList
     * ansCd : 답변코드
     * ansSeq : 답변순서
     * ansCont : 답변내용
     * @return
     */
    @PostMapping("/saveEditAnsContItem")
    public Map<String, Object> saveEditAnsContItem(@LoginUserParam LoginUser loginUser,
                                                   @RequestBody List<QaAnsMnVO> qaAnsMnVOList){
        String title = "saveEditAnsContItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";

            for(QaAnsMnVO perItem : qaAnsMnVOList){
                QaAnsMnVO vo = QaAnsMnVO.builder()
                                        .ansCd(perItem.getAnsCd())
                                        .ansSeq(perItem.getAnsSeq())
                                        .ansCont(perItem.getAnsCont())
                                        .updatorId(loginUser.getUserId())
                                        .build();
                int ret = ansManageService.insertAnsContItem(vo);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += (perItem.getAnsCd()) + "_" + perItem.getAnsSeq() + "_" + perItem.getAnsCont();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 설정 찹업창 - 답변명, 답변설명 삭제
     * @param qaAnsMnVO
     * ansCd : 답변코드
     * @return
     */
    @PostMapping("/delEditAnsMnConItem")
    public Map<String, Object> delEditAnsMnConItem(@RequestBody QaAnsMnVO qaAnsMnVO){
        String title = "delEditAnsMnConItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int ret = ansManageService.delAnsMainList(qaAnsMnVO);
            int ret2 = ansManageService.delAnsContMainList(qaAnsMnVO);

            if(ret > 0 && ret2 > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                String failInfo = "";
                if(ret < 1){
                    failInfo += "failAnsMainList";
                }
                if(ret2 < 1){
                    failInfo += "failAnsContList";
                }
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 답변 설정 찹업창 - 답변 리스트 삭제
     * @param qaAnsMnVOList
     * ansCd : 답변 코드
     * ansSeq : 답변 순서
     * @return
     */
    @PostMapping("/delContList")
    public Map<String, Object> delContList(@RequestBody List<QaAnsMnVO> qaAnsMnVOList){
        String title = "delEditAnsMnConItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int ret = 0;
            for(QaAnsMnVO perItem : qaAnsMnVOList){
                ret += ansManageService.delEditContAnsList(perItem);
            }
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", "deleteAnswerContentList");
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
