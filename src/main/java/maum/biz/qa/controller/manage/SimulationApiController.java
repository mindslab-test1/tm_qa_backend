package maum.biz.qa.controller.manage;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.service.manage.SimulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/manage/simulation",  produces = MediaType.APPLICATION_JSON_VALUE)
public class SimulationApiController {
    private SimulationService simulationService;

    @Value("${env.nlpUrl}")
    private String nlpUrl;      // NLP 연동 url

    @Value("${env.hmdUrl}")
    private String hmdUrl;      // HMD 연동 url

    @Autowired
    public SimulationApiController(SimulationService simulationService) {
        this.simulationService = simulationService;
    }

    /**
     * NLP 연동
     * @param jsonStr
     * jsonStr : 분석할 문장
     * @return
     */
    @PostMapping("/callNlp")
    public String callNlp(@RequestBody String jsonStr){
        String title = "callNlp";
        log.info(">>>>>>>>>> [" + title + "] Start");
        String ret = "";
        try{
            ret = simulationService.callSimulationNlp(jsonStr, nlpUrl);
            log.info("nlp result: " + ret);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return ret;
    }

    /**
     * HMD 연동
     * @param jsonStr
     * @return
     */
    @PostMapping("/callHmd")
    public String callHmd(@RequestBody String jsonStr){
        String title = "callHmd";
        log.info(">>>>>>>>>> [" + title + "] Start");
        String ret = "";
        try{
            ret = simulationService.callSimulationHmd(jsonStr, hmdUrl);
            log.info("hmd result: " + ret);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return ret;
    }

}
