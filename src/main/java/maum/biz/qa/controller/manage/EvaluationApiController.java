package maum.biz.qa.controller.manage;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.manage.vo.EvaluationVO;
import maum.biz.qa.service.manage.EvaluationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage/evaluationApi", produces = MediaType.APPLICATION_JSON_VALUE)
public class EvaluationApiController {

    private EvaluationService evaluationService;

    @Autowired
    public EvaluationApiController(EvaluationService evaluationService) {
        this.evaluationService = evaluationService;
    }

    /**
     * 상품구분 리스트 조회
     * @return
     * CD_GP : 코드그룹
     * CD : 코드
     * CD_NM : 코드명
     * CD_DESC : 코드설명
     * (TM-QA DB의 QA_CODE_DTL_TB 설명 참조)
     */
    @ApiOperation(value = "점검 항목 관리 - 상품구분 리스트 조회", notes = "평가표 관리 검색 메뉴 상품 구분 리스트 조회")
    @PostMapping("/getProductCode")
    public Map<String, Object> getProductCode(){
        String title = "getProductCode";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> codeList = evaluationService.getProductCode(EvaluationVO.builder().build());
            resultMap.put("codeList", codeList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 적용차수 리스트 조회
     * @param evaluationVO
     * prodCat : 보종구분(상품구분)
     * @return
     * CHK_APP_ORD : 항목적용차수
     */
    @ApiOperation(value = "점검 항목 관리 - 적용차수 리스트 조회", notes = "평가표 관리 검색 메뉴 적용차수 리스트 조회")
    @PostMapping("/applyOrdList")
    public Map<String, Object> getApplyOrdList(@RequestBody EvaluationVO evaluationVO){
        String title = "getApplyOrdList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = evaluationService.getApplyOrd(evaluationVO);
            resultMap.put("dataList", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 조건 항목 조회
     * @param evaluationVO
     * prodCat : 상품구분
     * @return
     * COND_ITM_CD : 조건항목코드
     * COND_ITM_NM : 조건항목명
     */
    @ApiOperation(value = "점검 항목 관리 - 조건 항목 조회", notes = "조건 항목 조회")
    @PostMapping("/getCondList")
    public Map<String, Object> getCondList(@RequestBody EvaluationVO evaluationVO){
        String title = "getApplyOrdList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = evaluationService.getCondList(evaluationVO);
            resultMap.put("dataList", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가표 관리 조회
     * @param evaluationVO
     * mode : 초기진입인지 아닌지 구분 -> 초기 진입시 상품구분 별 최고 차수 죄히됨
     * prodCat : 보종구분(상품구분)
     * chkAppOrd : 항목적용차수
     * @return
     * PROD_CAT : 상품구분
     * CHK_APP_ORD : 항목적용차구
     * CHK_APP_ST_DT : 항목적용 시작일자
     * CHK_APP_ED_DT : 항목적용 종료일자
     * USED_YN : 사용여부
     * CD_NM : 코드네임
     */
    @ApiOperation(value = "점검 항목 관리 - 평가표 관리 리스트 조회", notes = "평가표 관리 리스트 조회")
    @PostMapping("/getMainList")
    public Map<String, Object> getMainList(@RequestBody EvaluationVO evaluationVO){
        String title = "getEvaluationMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = new ArrayList<>();
            int totalCnt = 0;
            if(evaluationVO.getMode().equals("FIR")){
                dataList = evaluationService.getFirstMainList(evaluationVO);
                totalCnt = evaluationService.getFirstMainListCnt();
            }else{
                dataList = evaluationService.getMainList(evaluationVO);
                totalCnt = evaluationService.getMainListCnt(evaluationVO);
            }

            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가항목 리스트 조회
     * @param evaluationVO
     * prodCat : 상품구분
     * chkAppOrd : 항목적용차수
     * @return
     * PROD_CAT : 상품구분
     * CHK_APP_ORD : 항목적용차수
     * CHK_ITM_CD : 평가항목코드
     * CHK_ITM_NM : 평가항목명
     * CHK_ITM_DESC : 평가항목설명
     * CHK_NO : 항목순서
     * PART_INSP_YN : 부분r검수여부
     * USED_YN : 사용여부
     * AUTO_INSP_YN : 자동검수여부
     * CHK_CTG_CD : 평가구분코드
     * CHK_CTG_NM : 평가구분명
     * NOR_SCR : 정상점수
     * SUP_SCR : 보완점수
     * RTN_SCR : 반송점수
     * DTCT_CTG_CD : 탐지구분코드
     * COND_ITM_CD : 조건항목코드
     */
    @ApiOperation(value = "점검 항목 관리 - 평가항목 리스트 조회", notes = "평가항목 리스트 조회")
    @PostMapping("/getSubList")
    public Map<String, Object> getSubList(@RequestBody EvaluationVO evaluationVO){
        String title = "getEvaluationSubList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = evaluationService.getSubList(evaluationVO);
            resultMap.put("dataList", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [상품구분 등록 팝업] 상품구분 리스트 조회
     * @param evaluationVO
     * cd : 상품구분 코드
     * cdNm : 상품구분 이름
     * @return
     * CD_GP : 코드그룹
     * CD : 코드
     * CD_NM : 코드명
     * CD_DESC : 코드설명
     */
    @ApiOperation(value = "점검 항목 관리 - 상품구분 등록 팝업 - 상품구분 리스트 조회", notes = "상품구분 리스트 데이터 조회")
    @PostMapping("/popProductList")
    public Map<String, Object> popProductList(@RequestBody EvaluationVO evaluationVO){
        String title = "getPopProductList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = evaluationService.getProductCode(evaluationVO);
            resultMap.put("dataList", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 적용차수 저장/삭제
     * @param loginUser
     * @param evaluationVO
     * (삭제)
     * prodCat : 상품구분
     * chkAppOrd : 적용차수
     * (추가)
     * prodCat : 상품구분
     * chkAppOrd : 적용차수
     * chkAppStDt : 적용시작일
     * chkAppEdDt : 적용종료일
     * @return
     */
    @ApiOperation(value = "점검 항목 관리 - 평가표 설정 팝업/평가표 등록 팝업 - 적용차수 삭제 / 저장",
            notes = "적용차수 삭제 / 저장하는 api 제공")
    @PostMapping("/procApplyOrd")
    public Map<String, Object> procApplyOrd(@LoginUserParam LoginUser loginUser,
                                            @RequestBody EvaluationVO evaluationVO){
        String title = "procApplyOrd";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int ret = 0;
            if(evaluationVO.getMode().equals("D")){
                ret = evaluationService.delApplyOrd(evaluationVO);
            }else{
                EvaluationVO vo = EvaluationVO.builder()
                                    .prodCat(evaluationVO.getProdCat())
                                    .chkAppOrd(evaluationVO.getChkAppOrd())
                                    .chkAppStDt(evaluationVO.getChkAppStDt())
                                    .chkAppEdDt(evaluationVO.getChkAppEdDt())
                                    .creatorId(loginUser.getUserId())
                                    .build();

                ret = evaluationService.putApplyOrd(vo);
            }

            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [상품구분 등록 팝업] 상품코드 등록
     * @param loginUser
     * @param evaluationVO
     * cd : 상품코드
     * cdNm : 상품명
     * cdDesc : 상품설명
     * @return
     * true : 성공
     * false : 살패
     */
    @ApiOperation(value = "점검 항목 관리 - 상품구분 등록 팝업 - 상품코드 등록", notes = "상품구분 코드를 저장한다")
    @PostMapping("/addProductCode")
    public Map<String, Object> addProductCode(@LoginUserParam LoginUser loginUser,
                                              @RequestBody EvaluationVO evaluationVO){
        String title = "addProductCode";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            EvaluationVO vo = EvaluationVO.builder()
                            .creatorId(loginUser.getUserId())
                            .cd(evaluationVO.getCd())
                            .cdNm(evaluationVO.getCdNm())
                            .cdDesc(evaluationVO.getCdDesc())
                            .build();
            int ret = evaluationService.putProductCode(vo);
            if(ret > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [상품구분 등록 팝업] 상품코드 삭제
     * @param loginUser
     * @param evaluationVOList
     * (삭제)
     * cd : 상품코드
     * @return
     * true : 성공
     * false : 살패
     */
    @ApiOperation(value = "점검 항목 관리 - 상품구분 등록 팝업 - 상품구분 리스트 삭제", notes = "상품코드 기준으로 상품구분 리스트 삭제")
    @PostMapping("/delProductCode")
    public Map<String, Object> delProductCode(@LoginUserParam LoginUser loginUser,
                                                  @RequestBody List<EvaluationVO> evaluationVOList){
        String title = "delProductCode";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(EvaluationVO perItem : evaluationVOList){
                int ret = 0;
                EvaluationVO vo = EvaluationVO.builder()
                                    .creatorId(loginUser.getUserId())
                                    .cd(perItem.getCd())
                                    .build();
                ret = evaluationService.delProductCode(vo);

                if(ret < 1){
                    failInfo += (perItem.getCd());
                    failInfo += ", ";
                }else{
                    retCnt++;
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
            }

            if(failInfo.length() > 0){
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }


    /**
     * 평가항목 추가
     * @param loginUser
     * @param evaluationVOList
     * prodCat : 상품구분
     * chkAppOrd : 항목적용차수
     * chkItmCd : 평가항목코드
     * chkItmNm : 평가항목명
     * chkItmDesc : 평가항목설명
     * chkNo : 항목순서
     * partInspYn : 부분검수여부
     * chkCtgCd : 평가구분코드
     * chkCtgNm : 평가구분명
     * norScr : 정상점수
     * supScr : 보완점수
     * rtnScr : 반송점수
     * dtctCtgCd : 탐지구분코드
     * condItmCd : 조건항목코드
     * @return
     * true : 추가 성공
     * false : 추가 실패
     */
    @ApiOperation(value = "점검 항목 관리 - 평가학목 리스트 추가", notes = "평가항목 리스트를 추가")
    @PostMapping("/addCheckItem")
    public Map<String, Object> addCheckItem(@LoginUserParam LoginUser loginUser,
                                            @RequestBody List<EvaluationVO> evaluationVOList){
        String title = "addCheckItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";

            for(EvaluationVO perItem : evaluationVOList){
                EvaluationVO vo = new EvaluationVO();
                BeanUtils.copyProperties(perItem, vo);
                vo.setCreatorId(loginUser.getUserId());
                int ret = evaluationService.putChkItmData(vo);

                if(ret < 1){
                    failInfo += (perItem.getProdCat()) + "_" + perItem.getChkAppOrd() + "_" + perItem.getChkItmCd();
                    failInfo += ", ";
                } else{
                    retCnt++;
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
            }
            if(failInfo.length() > 0){
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가항목 리스트 삭제
     * @param loginUser
     * @param evaluationVOList
     * prodCat : 상품구분
     * chkAppOrd : 항목적용차수
     * chkItmCd : 평가항목코드
     * @return
     * true : 삭제 성공
     * false : 삭제 실패
     */
    @ApiOperation(value = "점검 항목 관리 - 평가항목 리스트 삭제", notes = "평가항목 리스트 삭제")
    @PostMapping("/delCheckItem")
    public Map<String, Object> delCheckItem(@LoginUserParam LoginUser loginUser,
                                            @RequestBody List<EvaluationVO> evaluationVOList){
        String title = "addCheckItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";

            for(EvaluationVO perItem : evaluationVOList){
                int ret = evaluationService.delChkItmData(perItem);

                if(ret < 1){
                    failInfo += (perItem.getProdCat()) + "_" + perItem.getChkAppOrd() + "_" + perItem.getChkItmCd();
                    failInfo += ", ";
                } else{
                    retCnt++;
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
            }
            if(failInfo.length() > 0){
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }


    /**
     * 평가구분 리스트 조회
     * @return
     * CD_GP : 코드그룹
     * CD : 코드
     * CD_NM : 코드명
     * CD_DESC : 코드설명
     */
    @ApiOperation(value = "점검 항목 관리 - 평가항목 리스트 - 평가구분 리스트 조회", notes = "평가항목 리스트 수정시 평가구분 목록 조회")
    @PostMapping("/getEvalCategoryList")
    public Map<String, Object> getEvalCategoryList(){
        String title = "getEvalCategoryList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = evaluationService.getEvalCategoryList();
            resultMap.put("dataList", dataList);
            log.info("dataList", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
