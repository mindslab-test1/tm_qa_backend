package maum.biz.qa.controller.manage;

import io.swagger.annotations.ApiOperation;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.entity.QaCond;
import maum.biz.qa.model.manage.entity.QaScrp;
import maum.biz.qa.model.manage.entity.QaScrpSec;
import maum.biz.qa.model.manage.entity.QaScrpSecCd;
import maum.biz.qa.service.manage.ScriptAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage/scriptAreaApi")
public class ManageScriptAreaApiController {

    @Autowired
     ScriptAreaService service;


    @ApiOperation(value = "스크립트 구간 관리 페이지의 코드 등록")
    @PostMapping("/code")
    public Map<String, Object> setCode(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody QaScrpSecCd qaScrpSecCd) {
        return service.setCode(loginUser, qaScrpSecCd);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 대분류 조회")
    @GetMapping("/lctgList")
    public Map<String, Object> getLctgList() {
        return service.getLctgList();
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 코드 목록 조회")
    @GetMapping("/codeList")
    public Map<String, Object> getCodeList(QaScrpSecCd qaScrpSecCd) {
        return service.getCodeList(qaScrpSecCd);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 코드 수정")
    @PutMapping("/code")
    public Map<String, Object> updCode(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody List<QaScrpSecCd> qaScrpSecCdList) {
        return service.updCode(loginUser, qaScrpSecCdList);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 코드 삭제")
    @DeleteMapping("/code")
    public Map<String, Object> delCode(@RequestBody List<QaScrpSecCd> qaScrpSecCdList) {
        return service.delCode(qaScrpSecCdList);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 구간 목록 조회")
    @GetMapping("/scriptAreaList")
    public Map<String, Object> getScriptAreaList(QaScrpSec qaScrpSec) {
        return service.getScriptAreaList(qaScrpSec);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 구간 등록")
    @PostMapping("/scriptArea")
    public Map<String, Object> setScriptArea(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody QaScrpSec qaScrpSec) {
        return service.setScriptArea(loginUser, qaScrpSec);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 구간 수정")
    @PutMapping("/scriptArea")
    public Map<String, Object> updScriptArea(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody QaScrpSec qaScrpSec) {
        return service.updScriptArea(loginUser, qaScrpSec);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 구간 삭제")
    @DeleteMapping("/scriptArea")
    public Map<String, Object> delScriptArea(QaScrpSec qaScrpSec) {
        return service.delScriptArea(qaScrpSec);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 메인 목록 조회")
    @GetMapping("/scriptAreaMainList")
    public Map<String, Object> getScriptAreaMainList(QaScrpSec qaScrpSec, CommonDBCursorVO commonDBCursorVO) {
        return service.getScriptAreaMainList(qaScrpSec, commonDBCursorVO);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 문장 관리 목록 조회")
    @GetMapping("/scriptSentenceList")
    public Map<String, Object> getScriptSentenceList(QaScrp qaScrp) {
        return service.getScriptSentenceList(qaScrp);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 문장 검색 목록 조회")
    @GetMapping("/sentenceList")
    public Map<String, Object> getSentenceList(@RequestParam(value="searchKeyword", required = false) String searchKeyword, CommonDBCursorVO commonDBCursorVO) {
        return service.getSentenceList(searchKeyword, commonDBCursorVO);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 문장 등록")
    @PostMapping("/scriptSentence")
    public Map<String, Object> setSentence(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody List<QaScrp> qaScrpList) {
        return service.setScriptSentence(loginUser, qaScrpList);
    }

    @ApiOperation(value = "스크립트 구간 관리 페이지의 문장 삭제")
    @DeleteMapping("/scriptSentence")
    public Map<String, Object> setSentence(@RequestBody List<QaScrp> qaScrpList) {
        return service.delScriptSentence(qaScrpList);
    }

    @ApiOperation(value = "스크립트 구간 관리 - 문장 설정 - 점검기준 상세 조회")
    @GetMapping("/articleDetail")
    public Map<String, Object> getArticleDetail(QaScrp qaScrp) {
        return service.getArticleDetail(qaScrp);
    }

    @ApiOperation(value = "스크립트 구간 관리 - 문장 설정 - 조건항목 목록 조회")
    @GetMapping("/condList")
    public Map<String, Object> getCondList() {
        return service.getCondList();
    }

    @ApiOperation(value = "스크립트 구간 관리 - 문장 설정 - 조건항목 상세 조회")
    @GetMapping("/condDetail")
    public Map<String, Object> getCondDetail(QaCond qaCond) {
        return service.getCondDetail(qaCond);
    }

    @ApiOperation(value = "스크립트 구간 관리 - 문장 설정 - 조건항목 상세 수정")
    @PostMapping("/article")
    public Map<String, Object> updArticle(@ApiIgnore @LoginUserParam LoginUser loginUser, @RequestBody QaScrp qaScrp) {
        return service.updArticle(loginUser, qaScrp);
    }
}
