package maum.biz.qa.controller.manage;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.manage.vo.EvaluationVO;
import maum.biz.qa.model.manage.vo.QaScrpSecVO;
import maum.biz.qa.model.manage.vo.StandardScriptVO;
import maum.biz.qa.service.manage.EvaluationService;
import maum.biz.qa.service.manage.ScriptAreaService;
import maum.biz.qa.service.manage.StandardScriptService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage/standardScript", produces = MediaType.APPLICATION_JSON_VALUE)
public class StandardScriptApiController {
    private StandardScriptService service;
    private EvaluationService evaluationService;
    private ScriptAreaService scriptAreaService;

    @Autowired
    public StandardScriptApiController(StandardScriptService service,
                                       EvaluationService evaluationService,
                                       ScriptAreaService scriptAreaService) {
        this.service = service;
        this.evaluationService = evaluationService;
        this.scriptAreaService = scriptAreaService;
    }

    /**
     * 표준스크립트 관리 보험상품 목록
     * @param jsonStr
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "표준스크립트 관리 - 보험상품 목록", notes = "보험상품 목록 조회")
    @PostMapping("/prodlist")
    public String getProductList(@RequestBody String jsonStr) {
        log.info("getProductList :: start");
        Gson gson = new Gson();
        StandardScriptVO vo = gson.fromJson(jsonStr,StandardScriptVO.class);
        HashMap<String, Object> resultMap = new HashMap<>();
        try {
            List<CamelListMap> dataList = service.getProductList(vo);
            log.info("prodlist dataList======>",dataList);
            resultMap.put("list", dataList);
        }catch (Exception e){
            log.error("error===================>"+e.getMessage());
            e.printStackTrace();
        }

        return gson.toJson(resultMap);
    }

    /**
     * 표준스크립트 관리 평가항목 목록
     * @param jsonStr
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 평가항목 목록", notes = "평가항목 목록 조회")
    @PostMapping("/evaluationItem")
    public String getEvaluationItemList(@RequestBody String jsonStr) {
        log.info("getEvaluationItemList :: start");
        Gson gson = new Gson();
        StandardScriptVO vo = gson.fromJson(jsonStr,StandardScriptVO.class);
        HashMap<String, Object> resultMap = new HashMap<>();
        try {
            List<CamelListMap> dataList = service.getEvaluationItemList(vo);
            log.info("evaluationItem dataList======>",dataList);
            resultMap.put("list", dataList);
        }catch (Exception e){
            log.error("error===================>"+e.getMessage());
            e.printStackTrace();
        }
        return gson.toJson(resultMap);
    }

    /**
     * 표준스크립트 관리 구간선택 목록
     * @param jsonStr
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 구간 선택 목록", notes = "구간 성택 목록 조회")
    @PostMapping("/intervalChoice")
    public String getIntervalChoiceList(@RequestBody String jsonStr) {
        log.info("getIntervalChoiceList :: start");
        Gson gson = new Gson();
        StandardScriptVO vo = gson.fromJson(jsonStr,StandardScriptVO.class);
        HashMap<String, Object> resultMap = new HashMap<>();
        try {
            List<CamelListMap> dataList = service.getIntervalChoiceList(vo);
            log.info("intervalChoice dataList======>",dataList);
            resultMap.put("list", dataList);
        }catch (Exception e){
            log.error("error===================>"+e.getMessage());
            e.printStackTrace();
        }
        return gson.toJson(resultMap);
    }


    /**
     * 구간 선택 row 선택 조회 > 스크립트 서브 가이드문구 목록
     * @param jsonStr
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 가이드 문록 목록", notes = "가이드 문구 및 몬장내용 목록 조회")
    @PostMapping("/scriptSubList")
    public String getScriptSubList(@RequestBody String jsonStr) {

        Gson gson = new Gson();
        StandardScriptVO vo = gson.fromJson(jsonStr,StandardScriptVO.class);
        HashMap<String, Object> resultMap = new HashMap<>();

        try {
            List<CamelListMap> dataList = service.getScriptSubList(vo);
            log.info("getScriptSubList dataList======>",dataList);
            resultMap.put("list", dataList);
        }catch (Exception e){
            log.error("error===================>"+e.getMessage());
            e.printStackTrace();
        }
        return gson.toJson(resultMap);
    }

    /**
     * 상품 등록 & 삭제(팝업)
     * @param jsonStr
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 상품 등록 및 상품 설정 팝업 - 상품 등록 및 삭제", notes = "상품 아이템 등록 / 삭제")
    @PostMapping("/proc-product")
    public String productInsertDelete(@RequestBody String jsonStr) {
        Gson gson = new Gson();
        StandardScriptVO vo = gson.fromJson(jsonStr, StandardScriptVO.class);
        HashMap<String, Object> resultMap = new HashMap<>();
        if (vo.getChkAppOrd() < 1) {
            vo.setChkAppOrd(1);
        }
        int ret = 0;
        if (vo.getMode().equals("D")) {
            ret = service.delProductData(vo);
        } else {
            ret = service.putProductData(vo);
        }

        if (ret > 0) {
            resultMap.put("isSuccess", true);
        } else {
            resultMap.put("isSuccess", false);
        }

        return gson.toJson(resultMap);
    }

    /**
     * 상품 복사(팝업)
     * @param jsonStr
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 상품 복사 팝업 - 상품 복사", notes = "상품 복사")
    @PostMapping("/proc-copy")
    public String copyProductData(@RequestBody String jsonStr) {
        Gson gson = new Gson();

        StandardScriptVO vo = gson.fromJson(jsonStr, StandardScriptVO.class);
        log.info("vo >>>>>" + vo.toString());
        int pRet = service.copyProdChkItm(vo);
        int cRet = service.copyCuslInfo(vo);
        int sRet = service.copyScrpInfo(vo);

        JsonObject retObj = new JsonObject();
        if (pRet > 0 && cRet > 0 && sRet > 0) {
            retObj.addProperty("isSuccess", true);
        } else {
            retObj.addProperty("isSuccess", false);

            String failInfo = new String();
            if (pRet < 1) {
                failInfo += "productChkItm copy fail! \n";
            }
            if (cRet < 1) {
                failInfo += "cuslInfo copy fail! \n";
            }
            if (sRet < 1) {
                failInfo += "scrpInfo copy fail! \n";
            }
            retObj.addProperty("failInfo", failInfo);
        }

        return retObj.toString();
    }

    /**
     * 표준 스크립트 관리 - 평가항목 선택 팝업 - 상품구분 리스트 조회
     * @param standardScriptVO
     * cd : 코드
     * cdNm : 코드명
     * @return
     * CD_GP : 코드그룹
     * CD : 코드
     * CD_NM : 코드명
     * CD_DESC : 코드섦명
     */
    @ApiOperation(value = "표준 스크립트 관리 - 평가항목 선택 팝업 - 상품구분 리스트", notes = "팡겨항목 선택 팝업의 상품구분 리스트 조회")
    @PostMapping("/productcode")
    public Map<String, Object> getProductCode(@RequestBody StandardScriptVO standardScriptVO){
        String title = "getProductCode";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            EvaluationVO evaluationVO = EvaluationVO.builder()
                                                    .cd(standardScriptVO.getCd())
                                                    .cdNm(standardScriptVO.getCdNm())
                                                    .build();
            List<CamelListMap> codeList = evaluationService.getProductCode(evaluationVO);
            resultMap.put("codeList", codeList);
            log.info("codeList >>>>> " + codeList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * [평갓항목 성택 팝업] 적용차수 리스트 조회
     * @param standardScriptVO
     * CD : 보종구분
     * @return
     * PROD_CAT : 보종구분
     * CHK_APP_ORD : 항목적용차수
     * CHK_APP_ST_DT : 항목적용 시작일자
     * CHK_APP_ED_DT : 항목적용 종료일자
     * USED_YN : 사용여부
     */
    @ApiOperation(value = "표준 스크립트 관리 - 평가항목 선택 팝업 - 적용차수 리스트", notes = "평가항목 선태 팝업의 적용차수 리스트 조회")
    @PostMapping("/applyordlist")
    public Map<String, Object> getApplyOrdList(@RequestBody StandardScriptVO standardScriptVO){
        String title = "getApplyOrdList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> appOrdList = service.getApplyOrdList(standardScriptVO);
            resultMap.put("appOrdList", appOrdList);
            log.info("appOrdList" + appOrdList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가항목 선택 팝업의 평가항목 리스트 조회
     * @param standardScriptVO
     * prodCat : 상품구분
     * chkAppOrd : 적용차수
     * @return
     * PROD_CAT : 보종구분
     * CHK_APP_ORD : 항목적용차수
     * CHK_APP_ST_DT : 항목적용 시작일자
     * CHK_APP_ED_DT : 항목적용 종료일자
     * USED_YN : 사용여부
     * CD_NM : 코드명
     */
    @ApiOperation(value = "표준스크립트 관리 - 평가항목 선택 팝업 - 평가항목 리스트 조회")
    @PostMapping("prodcatlist")
    public Map<String, Object> getProdcatlist(@RequestBody StandardScriptVO standardScriptVO){
        String title = "getProdcatlist";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{

            EvaluationVO evaluationVO = EvaluationVO.builder()
                                            .prodCat(standardScriptVO.getProdCat())
                                            .chkAppOrd(standardScriptVO.getChkAppOrd())
                                            .startRow(standardScriptVO.getStartRow())
                                            .rowsPerPage(standardScriptVO.getRowsPerPage())
                                            .build();
            List<CamelListMap> dataList = evaluationService.getMainList(evaluationVO);
            int totalCnt = evaluationService.getMainListCnt(evaluationVO);
            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
            log.info("dataList >>>>>" + dataList.toString());
            log.info("totalCnt >>>>>" + totalCnt);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가학못 선택 팝업 창의 평가항목 상세 리스트 조회
     * @param standardScriptVO
     * prodCat : 보종구분
     * chkAppOrd : 적용차수
     * @return
     * PROD_CAT : 보종구분
     * CHK_APP_ORD : 항목적용차수
     * CHK_ITM_CD : 평가항목코드
     * CHK_ITM_NM : 형가항목명
     * CHK_ITM_DESC : 형가항목설명
     * CHK_NO : 항목순서
     * PART_INSP_YN : 부분검수여부
     * USED_YN : 사용여부
     * CHK_CTG_CD : 평가구분코드
     * CHK_CTG_NM : 평가구분명
     * NOR_SCR : 정상점수
     * SUP_SCR : 보완점수
     * RTN_SCR : 반송점수
     * DTCT_CTG_CD : 탐지구분코드
     * COND_ITM_CD : 조건항목코두
     */
    @ApiOperation(value = "표준스크립트 관리 - 평가항목 선택 팝업 - 평가항목 상세 리스트", notes = "평가항목 상세 리스트 조회")
    @PostMapping("/prodcat-detail-list")
    public Map<String, Object> getProdCatDetailList(@RequestBody StandardScriptVO standardScriptVO){
        String title = "getProdCatDetailList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            EvaluationVO evaluationVO = EvaluationVO.builder()
                                        .prodCat(standardScriptVO.getProdCat())
                                        .chkAppOrd(standardScriptVO.getChkAppOrd())
                                        .build();
            List<CamelListMap> dataList = evaluationService.getSubList(evaluationVO);
            resultMap.put("dataList", dataList);
            log.info("dataList" + dataList.toString());
            if(!StringUtils.isEmpty(standardScriptVO.getProdCd()) && !StringUtils.isEmpty(standardScriptVO.getPlnCd())){
                List<CamelListMap> choosedItemList = service.getChoosedChkItm(standardScriptVO);
                resultMap.put("choosedItemList", choosedItemList);
                log.info("choosedItemList" + choosedItemList.toString());
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가항목 리스트 추가
     * @param standardScriptVOList
     * prodCat : 보종구분
     * prodCd : 상품코드
     * plnCd : 플랜코드
     * chkAppOrd : 항목적용 차수
     * chkCtgCd : 평가구분 코드
     * chkItmCd : 평가항목 코드
     * @return
     */
    @ApiOperation(value = "표준스크립트 - 평가항목 추가", notes = "평가항목 추가")
    @PostMapping("insert-checkitem")
    public Map<String, Object> insertCheckItem(@LoginUserParam LoginUser loginUser,
                                               @RequestBody List<StandardScriptVO> standardScriptVOList){
        String title = "insertCheckItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();
        log.info("request >>>>>>>>>> " + standardScriptVOList);

        try{
            int retCnt = 0;
            String failInfo = "";
            for(StandardScriptVO perItem : standardScriptVOList){
                perItem.setCreatorId(loginUser.getUserId());
                int ret = service.insertCheckItem(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += perItem.getProdCat() + "_" + perItem.getProdCd();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 평가항목 삭제
     * @param standardScriptVOList
     * prodCat : 보종구분
     * prodCd : 상품코드
     * plnCd : 플랜코드
     * chkAppOrd : 항목적용차수
     * chkItmCd : 평가항목코드
     * chkCtgCd : 평가구분코드
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 평가항목 삭제", notes = "평가항목 삭제")
    @PostMapping("/del-checkitem")
    public Map<String, Object> delCheckItem(@RequestBody List<StandardScriptVO> standardScriptVOList){
        String title = "delCheckItem";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(StandardScriptVO perItem : standardScriptVOList){
                int ret = service.deleteCheckItem(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += perItem.getProdCat() + "_" + perItem.getProdCd();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 표준스크립트 선택 팝업 대분류 리스트 조회
     * @return
     * scrp_lctg_cd : 스크립트 대분류코드
     * scrp_lctg_nm : 스크립트 대분류명
     */
    @ApiOperation(value = "표준스크립트 관리 - 표즌스크립트 선택 - 대분류 리스트", notes = "스크립트 대분류 리스트 조회")
    @GetMapping("/script-lctg")
    public Map<String, Object> getScriptLctgList(){
        String title = "getScriptLctgList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> lctgList = scriptAreaService.getLctgCd();
            resultMap.put("lctgList", lctgList);
            log.info("lctgList" + lctgList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 스크립트 중분류 리스트 조회
     * @return
     * scrp_mctg_cd : 스크립트 중분류코드
     * scrp_mctg_nm : 스크립트 중분류명
     */
    @ApiOperation(value = "표준스크립트 관리 - 표준스크립트 선택 - 중분류 리스트", notes = "스크립트 중분류 리스트 조회")
    @GetMapping("/script-mctg")
    public Map<String, Object> getScriptMctgList(){
        String title = "getScriptLctgList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> mctgList = scriptAreaService.getMctgCd();
            resultMap.put("mctgList", mctgList);
            log.info("mctgList" + mctgList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 스크립트 소분류 리스트 조회
     * @return
     * scrp_sctg_cd : 스크립트 소분류코드
     * scrp_sctg_nm : 스크립트 소분류명
     */
    @ApiOperation(value = "표준스크립트 관리 - 표준스크립트 선택 - 소분류 리스트", notes = "스크립트 소분류 리스트 조회")
    @GetMapping("/script-sctg")
    public Map<String, Object> getScriptSctgList(){
        String title = "getScriptLctgList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> sctgList = scriptAreaService.getSctgCd();
            resultMap.put("sctgList", sctgList);
            log.info("sctgList" + sctgList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 표준스크립트 구간 리스트 조회
     * @param qaScrpSecVO
     * scrpLctgCd : 표준스크립트 대분류코드
     * scrpMctgNm : 표준스크립트 중분류쿄드
     * scrpSctgNm : 표준스크립트 소분류코드
     * scrpAppOrd : 표준스크립트 적용차수
     * scrpMctgNm : 표준스크립트 중분류명
     * scrpSctgNm : 표준스크립트 소분류명
     * scrpAppStDt : 표준스크립트 적용시작일자
     * scrpAppEdDt : 표준스크립트 적용종료일자
     * startRow
     * rowsPerPage
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 표준스크립트 선택 - 구간 리스트 조회", notes = "구간 리스트 조회")
    @PostMapping("/script-mainlist")
    public Map<String, Object> getScriptMainList(@RequestBody QaScrpSecVO qaScrpSecVO){
        String title = "getScriptMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = scriptAreaService.getScriptAreaMainList(qaScrpSecVO);
            int totalCnt = scriptAreaService.getScripAreaMainListCnt(qaScrpSecVO);
            resultMap.put("dataList", dataList);
            resultMap.put("totalCnt", totalCnt);
            log.info("dataList" + dataList.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 표준스크립트 선택 팝업의 구간 상세 리스트 조회
     * @param qaScrpSecVO
     * scrpLctgCd : 표준스크립트 대분류 코드
     * scrpMctgCd : 표준스크립트 중분류 코드
     * scrpSctgCd : 표준스크립트 소분류 코드
     * scrpAppOrd : 표준스크립트 적용차수
     * @return
     * SCRP_LCTG_CD : 표준스크립트 대분류 코드
     * SCRP_MCTG_CD : 표준스크립트 중분류 코드
     * SCRP_SCTG_CD : 표준스크립트 소분류 코드
     * SCRP_APP_ORD : 표준스크립트 적용차수
     * STMT_SEQ : 문장 순서
     * DTCT_DTC_NO : 탐지사전번호
     * DAMBO_CD : 담보코드
     * COND_ITM_CD : 조건항목코드
     * ST_SCRP_YN : 시작문장여부
     * ESTY_SCRP_YN : 필수문장여부
     * DAMBO_NM : 담보명
     * GUD_STMT : 가이드 문장
     * SCRP : 스크립트
     * ANS_CD : 답변코드
     * CUST_ANS_YN : 고객답변여부
     * COND_ITM_NM : 조건항목명
     * DTCT_DTC_YN : (dtct_dtc_cnt > 0, 'Y', 'N') 탐지사전 갯수
     */
    @ApiOperation(value = "표준스크립트관리 - 표준스크립트 선택 팝업 - 구간 상세 리스트 조회", notes = "구간 상세 리스트 조회")
    @PostMapping("/script-detaillist")
    public Map<String, Object> getScriptDetailList(@RequestBody QaScrpSecVO qaScrpSecVO){
        String title = "getScriptMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = scriptAreaService.getScriptArticleList(qaScrpSecVO);
            resultMap.put("dataList", dataList);
            log.info("dataList" + dataList.toString());
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 구간 선택 테이블의 아이템 추가
     * @param loginUser
     * @param standardScriptVOList
     * prodCat : 보종구분
     * prodCd : 상품코드
     * plnCd : 플랜코드
     * chkItmCd : 평가항목코드
     * chkCtgCd : 평가구분코드
     * scrpLctgCd : 스크립트 대분류코드
     * scrpMctgCd : 스크립트 중분류코드
     * scrpSctgCd : 스크립트 소분류코드
     * scrpAppOrd : 스크립트 적용차수
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 구간 선택 테이블 - 스크립트 정보 등록", notes = "구간 선택 테이블의 아이템 추가")
    @PostMapping("/add-scrpsecinfo")
    public Map<String, Object> addScrpSecInfo(@LoginUserParam LoginUser loginUser,
                                              @RequestBody List<StandardScriptVO> standardScriptVOList){
        String title = "addScrpSecInfo";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(StandardScriptVO perItem : standardScriptVOList){
                perItem.setCreatorId(loginUser.getUserId());
                int ret = service.insertScrpSecInfo(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += perItem.getScrpSctgCd() + "_" + perItem.getState();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 구간 서택 테이블에서 스크립트 삭제
     * @param standardScriptVOList
     * prodCat : 보종구분
     * prodCd : 상품코드
     * plnCd : 플랜코드
     * chkItmCd : 평가항목코드
     * scrpLctgCd : 스크립트 대분류코드
     * scrpMctgCd : 스크립트 중분류코드
     * scrpSctgCd : 스크립트 소분류코드
     * scrpAppOrd : 스크립트 적용차수
     * @return
     */
    @ApiOperation(value = "표준스크립트 관리 - 구간 선택 테이블 - 스크립트 삭제", notes = "구간 서택 테이블에서 스크립트 삭제")
    @PostMapping("/del-scrpsecinfo")
    public Map<String, Object> delScrpSecInfo(@RequestBody List<StandardScriptVO> standardScriptVOList){
        String title = "delScrpSecInfo";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            int retCnt = 0;
            String failInfo = "";
            for(StandardScriptVO perItem : standardScriptVOList){
                int ret = service.deleteScrpSecInfo(perItem);
                if(ret > 0){
                    retCnt++;
                }else{
                    failInfo += perItem.getScrpSctgCd() + "_" + perItem.getState();
                    failInfo += ", ";
                }
            }

            if(retCnt > 0){
                resultMap.put("isSuccess", true);
            }else{
                resultMap.put("isSuccess", false);
                resultMap.put("failInfo", failInfo);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
