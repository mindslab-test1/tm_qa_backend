package maum.biz.qa.controller.manage;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.dto.QaCuslSecDTO;
import maum.biz.qa.model.manage.dto.QaDtcDtlDTO;
import maum.biz.qa.model.manage.entity.QaCodeDtl;
import maum.biz.qa.model.manage.entity.QaCuslKwd;
import maum.biz.qa.model.manage.entity.QaCuslSec;
import maum.biz.qa.model.manage.entity.QaDtcDtl;
import maum.biz.qa.service.manage.ConsultAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ConsultApiController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-27  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-27
 */
@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value="/manage/consult", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConsultApiController {

    @Autowired
    ConsultAreaService service;

    /**
     * 상담구간 목록 조회
     * @param qaCuslSec
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 상담구간 목록", notes = "상담구간 목록 조회")
    @GetMapping(value="/sections")
    public Map<String, Object> getSections(QaCuslSec qaCuslSec,
                                           CommonDBCursorVO commonDBCursorVO) {
        return service.getQaCuslSecList(qaCuslSec, commonDBCursorVO);
    }

    /**
     * 문장 목록 조회
     * @param cuslCd
     * @param cuslAppOrd
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 문장 목록", notes = "문장 목록 조회")
    @GetMapping(value="/section/{cuslCd}/{cuslAppOrd}/sentences")
    public Map<String, Object> getSentences(@PathVariable String cuslCd,
                                            @PathVariable int cuslAppOrd) {
        return service.getQaCuslKwdList(QaCuslKwd.builder().cuslCd(cuslCd).cuslAppOrd(cuslAppOrd).build());
    }

    /**
     * 기 등록 상담코드 조회 (사용 등록)
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 사용등록 - 상담코드 목록", notes = "기 등록 상담코드 조회 (사용 등록)")
    @GetMapping(value="/codes")
    public Map<String, Object> getConsultCodes() {
        return service.getNewConsultTarget();
    }

    /**
     * 상담코드 사용등록
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 사용등록", notes = "상담코드 사용등록")
    @PostMapping(value="/section")
    public Map<String, Object> setConsultSection(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                                 @RequestBody QaCuslSec qaCuslSec) {
        return service.setConsultSection(loginUser, qaCuslSec);
    }

    /**
     * 상담코드 수정
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 사용등록", notes = "상담코드 수정")
    @PatchMapping(value="/section")
    public Map<String, Object> updConsultSection(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                                 @RequestBody QaCuslSec qaCuslSec) {
        return service.updConsultSection(loginUser, qaCuslSec);
    }

    /**
     * 상담코드 삭제
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 사용등록", notes = "상담코드 삭제")
    @DeleteMapping(value="/section/{cuslCd}-{cuslAppOrd}")
    public Map<String, Object> delConsultSection(@PathVariable("cuslCd") String cuslCd,
                                                 @PathVariable("cuslAppOrd") int cuslAppOrd) {
        return service.delConsultSection(cuslCd, cuslAppOrd);
    }

    /**
     * 문장목록
     * @param scrp
     * @param commonDBCursorVO
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 문장관리 - 문장추가 - 문장목록", notes = "문장 추가를 위한 문장목록 조회")
    @GetMapping(value="/sentences")
    public Map<String, Object> getSentences(@RequestParam(name = "scrp", required = false) String scrp,
                                            CommonDBCursorVO commonDBCursorVO) {
        return service.getStmts(scrp, commonDBCursorVO);
    }

    /**
     * 상담구간 문장 등록
     * @param loginUser
     * @param cuslCd
     * @param cuslAppOrd
     * @param qaCuslKwd
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 문장관리 - 문장추가 - 문장등록", notes = "문장등록")
    @PostMapping(value="/section/{cuslCd}-{cuslAppOrd}/sentences")
    public Map<String, Object> setConsultSectionSentences(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                            @PathVariable("cuslCd") String cuslCd,
                                            @PathVariable("cuslAppOrd") int cuslAppOrd,
                                            @RequestBody List<QaCuslKwd> qaCuslKwd){
        return service.setConsultSectionSentences(loginUser, qaCuslKwd, cuslCd, cuslAppOrd);
    }

    /**
     * 상담구간 문장 삭제
     * @param cuslCd
     * @param cuslAppOrd
     * @param list
     * @return
     */
    @ApiOperation(value = "관리설정 - 상담구간 관리 - 문장관리 - 문장추가 - 문장삭제", notes = "문장삭제")
    @DeleteMapping(value="/section/{cuslCd}-{cuslAppOrd}/sentences")
    public Map<String, Object> delConsultSectionSentences(@PathVariable("cuslCd") String cuslCd,
                                                          @PathVariable("cuslAppOrd") int cuslAppOrd,
                                                          @RequestBody List<QaCuslKwd> list){
        return service.delConsultSectionSentences(list, cuslCd, cuslAppOrd);
    }

    @PostMapping("/code-proc")
    public String consultCodeDML(@RequestBody String jsonStr) {
        log.debug("@@@@@@@jsonStr : {}", jsonStr);
        Gson gson = new Gson();
        Map<String, Object> map = gson.fromJson(jsonStr, Map.class);
        String getJsonStr = (String)map.get("jsonStr");
        String mode = (String)map.get("mode");

        List<QaCodeDtl> tList = null;
        try {
            tList = gson.fromJson(getJsonStr, new TypeToken< List<QaCodeDtl>>(){}.getType());
        } catch (Exception ex) { // 입력 형태 배열이 아닐시 처리 호출
            QaCodeDtl vo = gson.fromJson(getJsonStr, QaCodeDtl.class);
            tList = new ArrayList<QaCodeDtl>();
            tList.add(vo);
        }
        int result = 0;
        String failInfo = new String();
        for(QaCodeDtl perItem : tList) {
            int ret = 0;
            if (mode != null && mode.equals("D")) {
                /*ret = service.deleteConsultCode(perItem);*/
            } else {
                perItem.setCdGp("SDCODE");
                /*ret = service.insertConsultCode(perItem);*/
            }
            if (ret < 1) {
                failInfo += (perItem.getCd());
                failInfo += ", ";
            } else {
                result++;
            }
        }

        JsonObject retObj = new JsonObject();
        if (result > 0) {
            retObj.addProperty("isSuccess", true);
        } else {
            retObj.addProperty("isSuccess", false);
        }
        if (failInfo.length() > 0) {
            retObj.addProperty("failInfo", failInfo);
        }

        return retObj.toString();
    }
    
    //상담 등록 수정
    @PostMapping("/consult-proc")
    public String scriptDML(@RequestBody QaCuslSecDTO vo) {
        int result = 0;
        if (vo.getMode() != null && vo.getMode().equals("D")) {
            /*result = service.deleteConsultData(vo);*/
        } else {
            /*result = service.insertConsultInfo(vo);*/
        }
        JsonObject retObj = new JsonObject();
        if (result > 0) {
            retObj.addProperty("isSuccess", true);
        } else {
            retObj.addProperty("isSuccess", false);
        }

        return retObj.toString();
    }

    // 문장 등록 삭제
    @PostMapping("/keyword-proc")
    public String articleDML(@RequestBody String jsonStr) {
        Gson gson = new Gson();

        Map<String, Object> map = gson.fromJson(jsonStr, Map.class);
        String getJsonStr = (String)map.get("jsonStr");
        String kwdMode = (String)map.get("kwdMode");

        List<QaCuslKwd> tList = gson.fromJson(getJsonStr, new TypeToken< List<QaCuslKwd>>(){}.getType());
        int result = 0;
        String failInfo = new String();
        System.out.println("@@@@@@@@array Size : " + tList.size() + ", " + kwdMode);
        for(QaCuslKwd perItem : tList) {
            int ret = 0;
            if (kwdMode != null && kwdMode.equals("D")) {
                /*ret = service.deleteKeywordInfo(perItem);*/
            } else {
                /*ret = service.insertKeywordInfo(perItem);*/
            }
            if (ret < 1) {
                failInfo += (perItem.getDtctDtcNo() + ":" + perItem.getCuslCd() + ":" + perItem.getCuslAppOrd());
                failInfo += ", ";
            } else {
                result++;
            }
        }

        JsonObject retObj = new JsonObject();
        if (result > 0) {
            retObj.addProperty("isSuccess", true);
        } else {
            retObj.addProperty("isSuccess", false);
        }
        if (failInfo.length() > 0) {
            retObj.addProperty("failInfo", failInfo);
        }

        return retObj.toString();
    }

    // 탐지 사전 처리
    @PostMapping("/dtc-proc")
    public String dtcDML(@RequestBody String jsonStr) {
        Gson gson = new Gson();

        System.out.println("@@@@@@@@" + jsonStr);
        Map<String, Object> map = gson.fromJson(jsonStr, Map.class);
        String getJsonStr = (String)map.get("jsonStr");
        System.out.println("@@@@@@@@" + getJsonStr);

        List<QaDtcDtlDTO> tList = gson.fromJson(getJsonStr, new TypeToken< List<QaDtcDtlDTO>>(){}.getType());
        int result = 0;
        String failInfo = new String();
        System.out.println("@@@@@@@@array Size : " + tList.size());
        for(QaDtcDtlDTO perItem : tList) {
            int ret = 0;
            if (perItem.getState().equals("D")) {
                ret = service.deleteDtcDtlInfo(perItem);
            } else {
                ret = service.insertDtcDtlInfo(perItem);
            }
            if (ret < 1) {
                failInfo += (perItem.getDtctDtcNo() + ":" + perItem.getDtctDtcGrpNo() + ":" + perItem.getDtctDtcGrpInNo());
                failInfo += ", ";
            } else {
                result++;
            }
        }

        JsonObject retObj = new JsonObject();
        if (result > 0) {
            retObj.addProperty("isSuccess", true);
        } else {
            retObj.addProperty("isSuccess", false);
        }
        if (failInfo.length() > 0) {
            retObj.addProperty("failInfo", failInfo);
        }

        return retObj.toString();
    }

    // 탐지사전 목록 조회
    @RequestMapping(value="/dtcDtlList", produces = "text/plain;charset=UTF-8")
    public String searchDtcDtlInfo(@RequestParam String dtcNo) {
        Gson gson = new Gson();

        List<QaDtcDtl> dataList = service.getDtcDtlInfo(dtcNo);
        return gson.toJson(dataList);
    }

    // 상담 코드 조회
    @RequestMapping(value="/getCode", produces = "text/plain;charset=UTF-8")
    public String getCodeList() {
        Gson gson = new Gson();
        /*List<QaCodeDtl> dataList = service.getQaCodeDtlList(new QaCodeDtl());*/

        return gson.toJson(new ArrayList<>());
    }
}
