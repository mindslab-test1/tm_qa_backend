package maum.biz.qa.controller.tm;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.tm.form.TmSttForm;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.service.tm.TmSttService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/tm/stt")
public class TmSttApiController {

    private final TmSttService tmSttService;


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "TM STT 대상 페이지의 목록 조회")
    @GetMapping(value="/target/list")
    public Map<Object, Object> getTmqaSttList(TmSttForm tmSttForm) {
        return tmSttService.getTmSttList(tmSttForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "TM STT 대상 페이지의 STT 상태 코드 목록 조회")
    @GetMapping(value="/target/code")
    public Map<Object, Object> getProgStatCd() {
        return tmSttService.getProgStatCd();
    }




    /*@Autowired
    TmSttService tmSttService;

    // tm stt 목록 조회
    @RequestMapping(value="/tm/tmStt/list",  method = {RequestMethod.GET})
    public ResponseEntity<?> getTmSttList(TmSttForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("getTmSttList() request data :::: {} " , gson.toJson(requestData));
        CommonPageVO tmSttList = tmSttService.getTmSttList(requestData);


        logger.info("getTmSttList() : {}", gson.toJson(tmSttList));
        return new ResponseEntity<CommonPageVO>(tmSttList, HttpStatus.OK);
    }

    // tm stt 정보 조회
    @RequestMapping(value="/tm/tmStt/info",  method = {RequestMethod.POST})
    public ResponseEntity<?> getTmSttInfo(TmSttPopupForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("request data :::: " + gson.toJson(requestData));
        HashMap<String, Object> tmSttInfo = tmSttService.getTmSttInfo(requestData);

        logger.info("getTmSttInfo() : {}", gson.toJson(tmSttInfo));
        return new ResponseEntity<>(tmSttInfo, HttpStatus.OK);
    }

    // tm stt 솔루션 결과 목록 조회
    @RequestMapping(value="/tm/tmSttRst/list",  method = {RequestMethod.POST})
    public ResponseEntity<?> getTmSttRstList(TmSttPopupForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("getTmSttRstList() request data :::: {} " , gson.toJson(requestData));

        List<CamelListMap> tmSttRstList = tmSttService.getTmSttRstList(requestData);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("data", tmSttRstList);
        logger.info("getTmSttRstList() resultMap data :::: {} " , gson.toJson(resultMap));

        return new ResponseEntity<>(resultMap, HttpStatus.OK);
    }

    // tm stt 솔루션 탐지문장 수정
    @RequestMapping(value="/tm/tmSttStmt/update",  method = {RequestMethod.POST})
    public ResponseEntity<?> updateTmSttStmt(TmSttPopupForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("updateTmSttStmt() request data :::: {} " , gson.toJson(requestData));
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        resultMap = tmSttService.updateTmSttStmt(requestData);
        logger.info("updateTmSttStmt() resultMap data :::: {} " , gson.toJson(resultMap));

        return new ResponseEntity<>(resultMap, HttpStatus.OK);
    }*/

}
