package maum.biz.qa.controller.cs;

import com.google.gson.Gson;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.vo.CommonPageVO;
import maum.biz.qa.model.cs.form.CsSttForm;
import maum.biz.qa.model.cs.form.CsSttPopupForm;
import maum.biz.qa.service.cs.CsSttService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;


@RestController
public class CsSttApiController {

    private static final Logger logger = LoggerFactory.getLogger(CsSttApiController.class);

    @Autowired
    CsSttService csSttService;

    // cs stt 목록 조회
    @RequestMapping(value="/cs/csStt/list",  method = {RequestMethod.GET})
    public ResponseEntity<?> getCsSttList(CsSttForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("request data :::: {} " , gson.toJson(requestData));
        CommonPageVO csSttList = csSttService.getCsSttList(requestData);

        logger.info("getCsSttList() : {}", gson.toJson(csSttList));
        return new ResponseEntity<CommonPageVO>(csSttList, HttpStatus.OK);
    }

    // cs stt 정보 조회
    @RequestMapping(value="/cs/csStt/info",  method = {RequestMethod.POST})
    public ResponseEntity<?> getCsSttInfo(CsSttPopupForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("request data :::: " + gson.toJson(requestData));
        HashMap<String, Object> csSttInfo = csSttService.getCsSttInfo(requestData);

        logger.info("getCsSttInfo() : {}", gson.toJson(csSttInfo));
        return new ResponseEntity<>(csSttInfo, HttpStatus.OK);
    }

    // cs stt 솔루션 결과 목록 조회
    @RequestMapping(value="/cs/csSttRst/list",  method = {RequestMethod.POST})
    public ResponseEntity<?> getCsSttRstList(CsSttPopupForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("getCsSttRstList() request data :::: {} " , gson.toJson(requestData));

        List<CamelListMap> csSttRstList = csSttService.getCsSttRstList(requestData);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("data", csSttRstList);
        logger.info("getCsSttRstList() resultMap data :::: {} " , gson.toJson(resultMap));

        return new ResponseEntity<>(resultMap, HttpStatus.OK);
    }

    // cs stt 솔루션 탐지문장 수정
    @RequestMapping(value="/cs/csSttStmt/update",  method = {RequestMethod.POST})
    public ResponseEntity<?> updateCsSttStmt(CsSttPopupForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("updateCsSttStmt() request data :::: {} " , gson.toJson(requestData));
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        resultMap = csSttService.updateCsSttStmt(requestData);
        logger.info("updateCsSttStmt() resultMap data :::: {} " , gson.toJson(resultMap));

        return new ResponseEntity<>(resultMap, HttpStatus.OK);
    }

}
