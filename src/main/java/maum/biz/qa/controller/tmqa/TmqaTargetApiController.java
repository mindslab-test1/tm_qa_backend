package maum.biz.qa.controller.tmqa;

import com.google.gson.Gson;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.model.tmqa.form.TmqaSttPopupForm;
import maum.biz.qa.model.tmqa.form.TmqaTargetPopupForm;
import maum.biz.qa.service.tmqa.TmqaSttService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/tmqa")
public class TmqaTargetApiController {

    private final TmqaSttService tmqaSttService;


    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(
            value = "TMQA QA 대상 페이지의 목록 조회",
            notes = "TMQA QA 대상 이거나 대상으로 지정할 수 있는 모든 리스트를 보여줍니다"
    )
    @PostMapping(value="/target/list")
    public Map<String, Object> getTmqaSttList(@RequestBody TmqaSttForm requestData) {
        return tmqaSttService.getTmqaSttList(requestData);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상 페이지의 Target 등록")
    @PostMapping(value = "/target/targetY")
    public Map<String, Object> setTargetY(@RequestBody List<TmqaSttForm> tmqaSttForms) {
        return tmqaSttService.setTargetY(tmqaSttForms);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상 페이지의 Target 삭제")
    @PostMapping(value = "/target/targetN")
    public Map<String, Object> setTargetN(@RequestBody List<TmqaSttForm> tmqaSttForms) {
        return tmqaSttService.setTargetN(tmqaSttForms);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 TMR 조회")
    @GetMapping(value="/target/tmr")
    public Map<String, Object> getTmrUserList() throws Exception {
        return tmqaSttService.getTmrUserList();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 TA점수 조회")
    @GetMapping(value="/target/scr")
    public Map<String, Object> getAutoScrList() throws Exception {
        return tmqaSttService.getAutoScrList();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 DTCT 조회")
    @GetMapping(value="/target/dtct")
    public Map<String, Object> getDtctList() throws Exception {
        return tmqaSttService.getDtctList();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 상품명 조회")
    @GetMapping(value="/target/prod")
    public Map<String, Object> getProdList() throws Exception {
        return tmqaSttService.getProdList();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 각 항목별 건수")
    @GetMapping(value="/target/count")
    public Map<String, Object> getCount() throws Exception {
        return tmqaSttService.getTargetPopupCount();
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 TMR 저장")
    @PostMapping(value = "/target/tmr")
    public Map<String, Object> setAutoTmrUser(@RequestBody List<TmqaTargetPopupForm> tmqaTargetPopupForm) throws Exception {
        return tmqaSttService.setAutoTmrUser(tmqaTargetPopupForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 TA점수 저장")
    @PostMapping(value = "/target/scr")
    public Map<String, Object> setAutoScr(@RequestBody TmqaTargetPopupForm tmqaTargetPopupForm) throws Exception {
        return tmqaSttService.setAutoScr(tmqaTargetPopupForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 TA 부분/미탐지 구간 저장")
    @PostMapping(value = "/target/dtct")
    public Map<String, Object> setAutoDtct(@RequestBody List<TmqaTargetPopupForm> tmqaTargetPopupForm) throws Exception {
        return tmqaSttService.setAutoDtct(tmqaTargetPopupForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 상품명 저장")
    @PostMapping(value = "/target/prod")
    public Map<String, Object> setAutoProd(@RequestBody List<TmqaTargetPopupForm> tmqaTargetPopupForm) throws Exception {
        return tmqaSttService.setAutoProd(tmqaTargetPopupForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "Tmqa QA 대상설정 팝업의 건수 저장")
    @PostMapping(value = "/target/count")
    public Map<String, Object> setAutoCnt(@RequestBody TmqaTargetPopupForm tmqaTargetPopupForm) throws Exception {
        return tmqaSttService.setAutoCnt(tmqaTargetPopupForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_QA_REVIEWER', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "TMQA STT 전수 팝업의 대화(스크립트) 목록 조회")
    @GetMapping(value="/allSttRst/list")
    public Map<String, Object> getTmqaAllSttRstList(TmqaSttPopupForm tmqaTargetPopupForm) throws Exception {
        return tmqaSttService.getTmqaAllSttRstList(tmqaTargetPopupForm);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_QA_REVIEWER', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "TMQA STT 전수 팝업의 탐지문장 수정")
    @PutMapping(value = "/allSttRst/stmt")
    public Map<String, Object> updTmSttStmt(@LoginUserParam LoginUser loginUser, @RequestBody TmqaSttPopupForm tmqaSttPopupForm) throws Exception {
        return tmqaSttService.updTmSttStmt(tmqaSttPopupForm, loginUser);
    }

}
