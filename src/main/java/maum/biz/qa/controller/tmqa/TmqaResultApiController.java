package maum.biz.qa.controller.tmqa;


import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.service.tmqa.TmqaResultService;
import maum.biz.qa.service.tmqa.TmqaSttService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/tmqaResult", produces = MediaType.APPLICATION_JSON_VALUE)
public class TmqaResultApiController {

    private final TmqaSttService tmqaSttService;
    private final TmqaResultService tmqaResultService;

    /**
     * QA결과 조회
     *
     * [param]
     * 상품명 : prodNmeSrch[PROD_NME]
     * 증권번호 : nsplNoSrch[NSPL_NO]
     * 담당QA : qaUserNm[QA_USER_NM]
     * 대상 : targetContSrch[QA_TRG_CON_CD]
     * 센터 : cntrNmSrch[SAES_CNTR_CD]
     *
     * [return]
     * 청약일자 : REG_DY
     * 상품명 : PROD_NME
     * 증권번호 : NSPL_NO
     * 계약자 : CSMR_ADMN
     * 피보험자 : PNM
     * 상담원 : SAES_EMNM
     * 센터 : CNTR_NM
     * 실 : PRT_NME
     * 청약상태 : APTN_STAT
     * TA점수 : TA_SCR
     * QA상태 : PRGST_NM(oracle)
     * QA결과 : EVLT_RSLT_STAT
     * QA점수 : QA_SCR
     * 담당QA : QA_USER_NM
     * QA실행일자 : QA_PROC_TM
     * 콜타임 : CTR_CAL_TOT_TM
     * 업무처리시간 : QA_ED_TM - QA_REQ_TM
     * 질의요청 : TP_NM(oracle)
     * 응답결과 : RESULT_NM(oracle)
     * QA요청일자 : QA_REQ_TM
     * 대상 : QA_TRG_CON_CD
     */
    @ApiOperation(value = "QA 결과 - QA결과 조회", notes = "QA결과 리스트 조회")
    @PostMapping("/getQaResultList")
    public Map<String, Object> getQaResultList(@RequestBody TmqaSttForm tmqaSttForm){
        String title = "getQaResultList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> resultList = tmqaSttService.getTmqaTargetList(tmqaSttForm);
            List<CamelListMap> qaList = tmqaResultService.getResultQuestion(tmqaSttForm);
            int resultTotalCnt = tmqaSttService.selectQaTargetListCNT(tmqaSttForm);

            // List merge
            for(CamelListMap perMap : resultList) {
                String val1 = (String) perMap.get("nsplNo");
                boolean isMapped = false;
                for(CamelListMap qaMap : qaList) {
                    String val2 = (String) qaMap.get("nsplNo");
                    if(val1.equals(val2)){
                        perMap.put("result_nm", qaMap.get("resultNm"));
                        perMap.put("tp_nm", qaMap.get("tpNm"));
                        perMap.put("prgst_nm", qaMap.get("qprgstNm"));

                        isMapped = true;
                        qaList.remove(qaMap);
                        break;
                    }
                }

                //  매칭 데이터 없을 시 빈 값 생성
                if(!isMapped){
                    perMap.put("result_nm", new String());
                    perMap.put("tp_nm", new String());
                    perMap.put("prgst_nm", new String());
                }
            }

            resultMap.put("data", resultList);
            resultMap.put("length", resultTotalCnt);
            log.info("getTmqaSttList resultMap data======>", resultMap.toString());

        } catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
