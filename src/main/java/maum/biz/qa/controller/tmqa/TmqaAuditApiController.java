package maum.biz.qa.controller.tmqa;


import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.service.tmqa.TmqaAuditService;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Map;

/**
 * TmqaAuditApiController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-06  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-06
 */
@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/tmqa/audit", produces = MediaType.APPLICATION_JSON_VALUE)
public class TmqaAuditApiController {

    final TmqaAuditService tmqaAuditService;

    public TmqaAuditApiController(TmqaAuditService tmqaAuditService) {
        this.tmqaAuditService = tmqaAuditService;
    }

    /**
     * 청약 MASTER 정보
     * @param loginUser
     * @param nsplNo
     * @return
     */
    @ApiOperation(value = "QA심사 - 청약 MASTER 정보", notes = "청약 MASTER 정보 조회")
    @GetMapping("/offer/{nsplNo}")
    public Map<String, Object> getQaAuditMasterInfo(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                                    @PathVariable String nsplNo){
        return tmqaAuditService.getQaAuditMasterInfo(loginUser, nsplNo);
    }

    /**
     * 상담 콜 목록
     * @param nsplNo
     * @return
     */
    @ApiOperation(value = "QA심사 - 상담 콜 목록", notes = "상담 콜 목록 조회")
    @GetMapping("/stts/{nsplNo}")
    public Map<String, Object> getQaAuditCallList(@PathVariable String nsplNo){
        return tmqaAuditService.getQaAuditCallList(nsplNo);
    }

    /**
     * 심사 회차별 이력 목록
     * @param nsplNo
     * @return
     */
    @ApiOperation(value = "QA심사 - 회차별 이력 목록", notes = "회차별 이력 목록 조회")
    @GetMapping("/rounds/{nsplNo}")
    public Map<String, Object> getQaAuditRoundList(@PathVariable String nsplNo){
        return tmqaAuditService.getQaAuditRoundList(nsplNo);
    }

    /**
     * 평가항목결과 목록 조회
     * @param nsplNo
     * @param tmsInfo
     * @param psnJugCat
     * @param chkCtgCd
     * @return
     */
    @ApiOperation(value = "QA심사 - 평가항목결과 목록", notes = "평가항목결과 목록 조회")
    @GetMapping("/contract/{nsplNo}/evaluation-item/{tmsInfo}-{psnJugCat}-{chkCtgCd}")
    public Map<String, Object> getEvaluationItemList(@PathVariable String nsplNo,
                                                     @PathVariable String tmsInfo,
                                                     @PathVariable String psnJugCat,
                                                     @PathVariable String chkCtgCd){
        return tmqaAuditService.getEvaluationItemList(nsplNo, tmsInfo, psnJugCat, chkCtgCd);
    }

    /**
     * 표준 계약 스크립트 목록
     * @param nsplNo
     * @param tmsInfo
     * @param prodCat
     * @param chkItmCd
     * @return
     */
    @ApiOperation(value = "QA심사 - 표준 계약 스크립트", notes = "표준 계약 스크립트 목록 조회")
    @GetMapping("/contract/{nsplNo}/script/{tmsInfo}-{prodCat}-{chkItmCd}")
    public Map<String, Object> getContractScriptList(@PathVariable String nsplNo,
                                                     @PathVariable String tmsInfo,
                                                     @PathVariable String prodCat,
                                                     @PathVariable String chkItmCd){
        return tmqaAuditService.getContractScriptList(nsplNo, tmsInfo, prodCat, chkItmCd);
    }

    /**
     * 표준스크립트 탐지결과 목록 조회
     * @param nsplNo
     * @param tmsInfo
     * @param prodCat
     * @param chkItmCd
     * @return
     */
    @ApiOperation(value = "QA심사 - 표준스크립트 탐지결과", notes = "표준스크립트 탐지결과 목록 조회")
    @GetMapping("/contract/{nsplNo}/script/detection/{tmsInfo}-{prodCat}-{chkItmCd}")
    public Map<String, Object> getContractScriptDetectionList(@PathVariable String nsplNo,
                                                              @PathVariable String tmsInfo,
                                                              @PathVariable String prodCat,
                                                              @PathVariable String chkItmCd){
        return tmqaAuditService.getContractScriptDetectionList(nsplNo, tmsInfo, prodCat, chkItmCd);
    }

    /**
     * 심사정보 임시저장 / 심사완료
     * @param loginUser
     * @param round
     * @return
     */
    @ApiOperation(value = "QA심사 - 심사정보 임시저장 / 심사완료", notes = "심사정보 임시저장 / 심사완료 처리")
    @PutMapping("/round")
    public Map<String, Object> setAudit(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                        @RequestBody Map<String, Object> round){
        return tmqaAuditService.setAudit(loginUser, round);
    }

    /**
     * 질의응답 조회
     * @param nsplNo
     * @return
     */
    @ApiOperation(value = "QA심사 - 질의응답", notes = "질의응답 목록 조회")
    @GetMapping("/qna/{nsplNo}")
    public Map<String, Object> getQuestionsAndAnswers(@PathVariable String nsplNo){
        return tmqaAuditService.getQuestionsAndAnswers(nsplNo);
    }

    /**
     * 응답 저장
     * @param loginUser
     * @param param
     * @return
     */
    @ApiOperation(value = "QA심사 - 질의응답 - 응답 등록", notes = "응답 등록")
    @PostMapping("/qna/answer")
    public Map<String, Object> setAnswer(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                         @RequestBody Map<String, Object> param){
        return tmqaAuditService.setAnswer(loginUser, param);
    }

}
