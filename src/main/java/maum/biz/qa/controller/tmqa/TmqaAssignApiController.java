package maum.biz.qa.controller.tmqa;


import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.model.tmqa.vo.TmqaVO;
import maum.biz.qa.service.tmqa.TmqaAssignService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * TmqaAssignApiController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-04-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-04-20
 */
@Slf4j
@RestController
@RequestMapping(value = "/tmqa/assign")
public class TmqaAssignApiController {

    final TmqaAssignService tmqaAssignService;

    public TmqaAssignApiController(TmqaAssignService tmqaAssignService) {
        this.tmqaAssignService = tmqaAssignService;
    }

    /**
     * QA대상, QA배정 목록 조회
     * @param tmqaSttForm
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_QA_REVIEWER', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "QA배정 - QA대상, QA배정 목록", notes = "QA대상, QA배정 목록 조회")
    @PostMapping("/target/list")
    public Map<String, Object> getAssignTargetList(@ApiIgnore @RequestBody TmqaSttForm tmqaSttForm){
        return tmqaAssignService.getQaAssignTargetList(tmqaSttForm);
    }

    /**
     * 배정현황 조회
     * @param tmqaVO
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "QA배정 - 배정현황", notes = "배정현황 조회")
    @PostMapping("/user/reviewer/assign/list")
    public Map<String, Object> getAssignCntList(@ApiIgnore @RequestBody TmqaVO tmqaVO){
        return tmqaAssignService.getAssignCntList(tmqaVO);
    }

    /**
     * QA심사자 목록 조회
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "QA배정 - QA심사자 목록", notes = "QA심사자 목록 조회")
    @PostMapping("/user/reviewer/list")
    public Map<String, Object> getQaUserList(@ApiIgnore @LoginUserParam LoginUser loginUser){
        return tmqaAssignService.getQaUserList();
    }

    /**
     * QA심사자 휴무정보 등록/수정
     * @param loginUser
     * @param tmqaVOList
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "QA배정 - QA심사자 휴무정보 등록/수정", notes = "QA심사자 휴무정보 등록/수정")
    @PostMapping("/holiday")
    public Map<String, Object> setOrUpdHolidayInfo(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                                   @RequestBody List<TmqaVO> tmqaVOList){
        return tmqaAssignService.setOrUpdHolidayInfo(loginUser, tmqaVOList);
    }

    /**
     * 배정등록
     * @param loginUser
     * @param tmqaVOList
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "QA배정 - 배정등록", notes = "배정등록")
    @PostMapping("/info")
    public Map<String, Object> setAssignInfo(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                             @RequestBody List<TmqaVO> tmqaVOList){
        return tmqaAssignService.setAssignInfo(loginUser, tmqaVOList);
    }

    /**
     * 심사자별 상세 현황 조회
     * @param tmqaVO
     * @return
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_QA_REVIEWER', 'ROLE_UW_ADMIN')")
    @ApiOperation(value = "QA배정 - 심사자별 상세 현황", notes = "심사자별 상세 현황 조회")
    @PostMapping("/judge/detail")
    public Map<String, Object> getPerJudgeDetail(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                                 @RequestBody TmqaVO tmqaVO){
        return tmqaAssignService.getPerJudgeDetail(loginUser, tmqaVO);
    }

}
