package maum.biz.qa.controller.tmqa;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class TmqaSttViewController {

    private static final Logger logger = LoggerFactory.getLogger(TmqaSttViewController.class);

    // TM - STT 대상 목록
    @RequestMapping(value="/tmqa/tmqaStt", method = RequestMethod.GET)
    public String goTmqaSttList(HttpServletRequest request){
        logger.info("TmqaSttViewController.{}", "goTmqaSttList()");

        HttpSession httpSession = request.getSession(true);
        // 탭메뉴 세션 설정
        String tabMenu = "QA 대상";
        String tabMenuUrl = "/tmqa/tmqaStt";

        String tabMenuAll = (String)httpSession.getAttribute("tabMenuAll");
        String tabMenuUrlAll = (String)httpSession.getAttribute("tabMenuUrlAll");

        if("".equals(tabMenuAll) || tabMenuAll == null){
            tabMenuAll = tabMenu;
            tabMenuUrlAll = tabMenuUrl;
        }else if(!tabMenuAll.contains(tabMenu)){
            tabMenuAll = tabMenuAll+","+tabMenu;
            tabMenuUrlAll = tabMenuUrlAll+","+tabMenuUrl;
        }else {}

        httpSession.setAttribute("tabMenu", tabMenu);
        httpSession.setAttribute("tabMenuAll", tabMenuAll);
        httpSession.setAttribute("tabMenuUrlAll", tabMenuUrlAll);
        // 탭메뉴 세션 설정

        Gson gson = new Gson();
        logger.info("goTmqaSttList() httpSession : {}", gson.toJson(httpSession));

        return tabMenuUrl;
    }

    // tmqa - STT 심사 팝업 화면
    @RequestMapping(value="/tmqa/tmqaSttAuditPopup")
    public String goTmqaSttAuditPopupView(HttpServletRequest request, Model model){
        logger.info("TmqaSttViewController.{}", "goTmqaSttAuditPopupView()");
        logger.info("goTmqaSttAuditPopupView. tmsInfo {} ", request.getParameter("tmsInfo"));
        logger.info("goTmqaSttAuditPopupView. nsplNo {} ", request.getParameter("nsplNo"));
        logger.info("goTmqaSttAuditPopupView. ucid {} ", request.getParameter("ucid"));

        String tmsInfo  = request.getParameter("tmsInfo");
        String nsplNo  = request.getParameter("nsplNo");
        String ucid  = request.getParameter("ucid");

        model.addAttribute("tmsInfo", tmsInfo);
        model.addAttribute("nsplNo", nsplNo);
        model.addAttribute("ucid", ucid);

        return "popup/tmqa/tmqaSttAuditPopup";
    }

    // tmqa - STT 심사 팝업 화면
    @RequestMapping(value="/tmqa/tmqaAllSttPopup")
    public String goTmqaAllSttPopupView(HttpServletRequest request, Model model){
        logger.info("TmqaSttViewController.{}", "goTmqaAllSttPopupView()");
        logger.info("goTmqaAllSttPopupView. tmsInfo {} ", request.getParameter("tmsInfo"));
        logger.info("goTmqaAllSttPopupView. nsplNo {} ", request.getParameter("nsplNo"));
        logger.info("goTmqaAllSttPopupView. ucid {} ", request.getParameter("ucid"));

        String tmsInfo  = request.getParameter("tmsInfo");
        String nsplNo  = request.getParameter("nsplNo");
        String ucid  = request.getParameter("ucid");

        model.addAttribute("tmsInfo", tmsInfo);
        model.addAttribute("nsplNo", nsplNo);
        model.addAttribute("ucid", ucid);

        return "popup/tmqa/tmqaAllSttPopup";
    }
}
