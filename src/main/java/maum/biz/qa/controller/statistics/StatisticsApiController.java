package maum.biz.qa.controller.statistics;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.manage.vo.StatVO;
import maum.biz.qa.service.statistics.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
public class StatisticsApiController {
    private StatisticsService statisticsService;

    @Autowired
    public StatisticsApiController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    /**
     * [심사자 통계] 담당 QA 리스트 조회
     * @return
     */
    @ApiOperation(value = "심사자 통계 - 담당 QA 리스트 조회", notes = "담당 QA 리스트 조회")
    @GetMapping("/getQaList")
    public Map<String, Object> getQaList(){
        String title = "getQaList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = statisticsService.getQaSelectBoxList();
            resultMap.put("dataList", dataList);
            log.info("dataList >>>>>> ", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }


    /**
     * [심사자 통계] 심사자 통계 메인리스트 조회
     * @param statVO
     * qaId : 담당 QA 사번
     * fromDate: 청약일자 시작
     * toDate : 청약일자 끝
     * @return
     * accessTime : 처리시간
     * callTime: 계약콜 총 시간
     * finalAverage: QA평균점수
     * finalTotal: QA총점수
     * nsplNoCnt: 계약건수
     * cmsrAdmnNo: 계약자수
     * oneAverage: TA평균점수
     * oneQaCousel: QA상담점수
     * oneQaScript: QA스크립트점수
     * oneTotal: TA총점수
     * qaNameId: 담당QA Id
     * qaUserNm: 담당QA
     * regDy: 청약일자
     * objectContentManual: 수동지정
     * objectContentProduct: 상품
     * objectContentTaScore: TA점수
     * objectContentTaSection: TA구간
     * objectContentTmr: TMR
     */
    @ApiOperation(value = "심사자 통계 - 메인리스트 조회", notes = "심사자 통계 메인리스트 조회")
    @PostMapping("/getQaStatMainList")
    public Map<String, Object> getQaStatisticList(@RequestBody StatVO statVO){
        String title = "getQaStatMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = statisticsService.getQaStatisticList(statVO);
//            int totalCnt = sentenceManageService.getSentenceListCnt(qaStcMngVO);
            resultMap.put("dataList", dataList);
//            resultMap.put("totalCnt", totalCnt);
            log.info("dataList >>>>>> ", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }


    /**
     * [상담원 통계] 상담원 통계 메인리스트 조회
     * @param statVO
     * fromDate: 청약일자 시작
     * toDate: 청약일자 끝
     * prodNm : 상품명
     * aptnCd: 청약상태
     * cntrCd: 모집지점코드
     * partCd: 실코드
     * empNo: 상담원사번
     * @return
     * cntrNm: 센터명
     * dutyMtcnt: 근속월수
     * nsplCnt: 청약건수
     * prtNme: 실명
     * qaCnt: QA건수
     * qaFinAvg: QA평균점수
     * qaFinCuslAvg: QA상담평균점수
     * qaFinScr: QA전체점수
     * qaFinScrpAvg: QA스크립트평균점수
     * qaRate: 배정률?(대상내용건수/계약건수)
     * qaRtnCnt: QC반송 수
     * qaSuppCnt: QC보완 수
     * qaSuppFinCnt: QC완판 수 => QA완료건수
     * saesEmnm: 상담원
     * saesEmno: 사번
     * ta1CuslAvg: TA상담평균점수
     * ta1FinAvg: TA평균점수
     * ta1FinScr: TA전체점수
     * ta1ScrpAvg: TA스크립트평균점수
     */
    @ApiOperation(value = "상담원 통계 - 메인리스트 조회", notes = "상담원 통계 메인리스트 조회")
    @PostMapping("/getTmrStatMainList")
    public Map<String, Object> getTmrStatisticList(@RequestBody StatVO statVO){
        String title = "getTmrStatMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = statisticsService.selectTmrStatisticList(statVO);
//            int totalCnt = sentenceManageService.getSentenceListCnt(qaStcMngVO);
            resultMap.put("dataList", dataList);
//            resultMap.put("totalCnt", totalCnt);
            log.info("dataList >>>>>> ", dataList);
        }catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

}
