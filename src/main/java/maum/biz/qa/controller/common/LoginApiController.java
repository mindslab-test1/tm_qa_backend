package maum.biz.qa.controller.common;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.config.JwtTokenProvider;
import maum.biz.qa.model.common.entity.AdminUser;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.model.common.vo.LoginUserVO;
import maum.biz.qa.model.common.vo.TokenVO;
import maum.biz.qa.service.common.AdminUserService;
import maum.biz.qa.service.common.LoginUserService;
import maum.biz.qa.service.manage.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 로그인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Slf4j
@RestController
@RequestMapping(value="/user")
public class LoginApiController {

    @Autowired
    private LoginUserService loginUserService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Value("${env.seedkey}")
    String seedkey;

    @Value("${env.sessionTime}")
    String sessionTime;

    @Value("${spring.jwt.accessExpiredTime}")
    private long accessExpiredTime;

    @Value("${spring.jwt.refreshExpiredTime}")
    private long refreshExpiredTime;

    @Autowired
    private AdminUserService adminUserService;


    //Rest API 로그인
    @PostMapping("/sign-in")
    public Map<String, Object> login(@RequestBody String jsonStr, HttpServletResponse response, HttpServletRequest request) throws Exception{
        Gson gson = new Gson();
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        AdminUser adminUser = new AdminUser();
        String accessToken = "";
        String refreshToken = "";

        try{
            adminUser = gson.fromJson(jsonStr, AdminUser.class);

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(adminUser.getUserId(), adminUser.getUserPw()));
            SecurityContextHolder.getContext().setAuthentication(authentication);

            if(!loginUserService.checkLoginFailCount(adminUser.getUserId())){
                throw new LockedException("bad credentials");
            }

            if(loginUserService.checkFirstLogin(adminUser.getUserId()) || !loginUserService.getPasswordPrevLogByDate(adminUser.getUserId())){
                resultMap.put("firstLogin", true);
                resultMap.put("user", userService.getUser(ImmutableMap.of("userId", adminUser.getUserId())));
            }else{
                //로그인 성공시 jwt 토큰 생성
                accessToken = tokenProvider.generateToken(authentication, "access", loginUserService.setTokenParseUUID(adminUser.getUserId()));
                refreshToken = tokenProvider.generateToken(authentication, "refresh", null);

                loginUserService.updLastLoginDate(adminUser.getUserId());

                // Redis에 회원정보 저장
                String key = "loginUser:" + adminUser.getUserId();
                LoginUserVO loginUserVO = loginUserService.getLoginUserDetail(adminUser.getUserId());
                redisTemplate.opsForValue().set(key, LoginUser.of(loginUserVO));
                redisTemplate.expireAt(key, new Date(new Date().getTime() + accessExpiredTime));

                // Redis에 refresh_token 저장
                redisTemplate.opsForValue().set(refreshToken, adminUser.getUserId() + ":" + refreshToken);
                redisTemplate.expireAt(refreshToken, new Date(new Date().getTime() + refreshExpiredTime));

                resultMap.put("access_token", accessToken);
                resultMap.put("refresh_token", refreshToken);
                resultMap.put("user_info", LoginUser.of(loginUserVO));
            }
        } catch (LockedException e){
            throw new LockedException(e.getMessage());
        } catch (BadCredentialsException e){
            loginUserService.updateLoginFailCount(adminUser.getUserId());
            throw new BadCredentialsException("bad credentials");
        } catch (Exception e){
            e.printStackTrace();
            throw new BadCredentialsException("bad credentials");
        }

        //생성한 토큰 전달
        return resultMap;
    }

    /**
     * 사용자 수정
     * @param param
     * @return
     */
    @PatchMapping("/change-password")
    public Map<String, Object> updUser(@RequestBody Map<String, Object> param){
        return userService.updUser(null, param);
    }

    //Rest API 로그아웃
    @PostMapping("/sign-out")
    public Map<String, Object> logOut(@LoginUserParam LoginUser loginUser, HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication != null){
            new SecurityContextLogoutHandler().logout(request,response,authentication);
        }
        HashMap<String, Object> resultMap = new HashMap<>();
        try{
            if(loginUser != null){
                loginUserService.setLogoutLog(loginUser.getUserId());
            }
            resultMap.put("processMessage", "logout suceess");
        }catch (Exception e){
            e.printStackTrace();
            resultMap.put("processMessage", "logout fail");
        }
        return resultMap;
    }

    // 현재 로그인한 유저 정보
    @GetMapping("/me")
    public ResponseEntity me(@LoginUserParam LoginUser loginUser) throws Exception {
        return Optional.ofNullable(loginUser)
                .map(v -> {
                    String key = "loginUser:" + v.getUserId();
                    redisTemplate.opsForValue().set(key, v);
                    redisTemplate.expireAt(key, new Date(new Date().getTime() + accessExpiredTime));
                    v.setAccessToken(tokenProvider.generateToken(SecurityContextHolder.getContext().getAuthentication(), "access", loginUserService.getTokenParseUUID(v.getUserId())));
                    v.setExpiredTime(accessExpiredTime);
                    return ResponseEntity.ok(v);
                }).orElseGet(() -> ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    /**
     * 메뉴 권한체크
     * @param loginUser
     * @param path
     * @return
     */
    @GetMapping("/page/auth/check")
    public boolean accessibleMenuCheck(@LoginUserParam LoginUser loginUser, @RequestParam String path) {
        return loginUserService.accessibleMenuCheck(loginUser.getUserRole().name(), path);
    }

    // 리프레시 토큰 발급
    @PostMapping("/auth/refresh-token")
    public TokenVO generatorRefreshToken(@RequestBody Map<String, Object> paramMap) {
        TokenVO tokenVO = new TokenVO();

        if(paramMap != null && paramMap.get("refresh_token") != null) {
            String data = (String) redisTemplate.opsForValue().get(paramMap.get("refresh_token"));

            if(data != null && data.length() != 0) {
                try {
                    // authentication 생성
                    UserDetails userDetails = adminUserService.loadUserByUsername(data.split(":")[0]);
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                    // access_token 생성성
                   String accessToken = tokenProvider.generateToken(authentication, "access", loginUserService.setTokenParseUUID(authentication.getName()));
                    tokenVO.setAccess_token(accessToken);
                    tokenVO.setProcessStatus(true);
                }catch (Exception e) {
                    e.printStackTrace();
                    tokenVO.setProcessStatus(false);
                }
            }else {
                tokenVO.setProcessStatus(false);
            }
        }else {
            tokenVO.setProcessStatus(false);
        }

        return tokenVO;
    }

}
