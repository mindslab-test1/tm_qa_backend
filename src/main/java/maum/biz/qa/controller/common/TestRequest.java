package maum.biz.qa.controller.common;

import lombok.Data;

/**
 * maum.biz.qa.controller.common
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-05
 */
@Data
public class TestRequest {

    private long nspl;
    private long msg;
    private long ctnt;

}
