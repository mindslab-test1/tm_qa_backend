package maum.biz.qa.controller.common;

import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.resolvers.LoginUserParam;
import maum.biz.qa.service.common.CodeService;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Map;

/**
 * 공통코드 관리 컨트롤러
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-11  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-11
 */
@Slf4j
@RestController
public class CodeApiController {

    final CodeService codeService;

    public CodeApiController(CodeService codeService) {
        this.codeService = codeService;
    }


    /**
     * 그룹 명으로 공통코드 목록 조회
     * @return
     */
    @ApiOperation(value = "공통 - 공통코드 목록(그룹 명)", notes = "그룹 명으로 공통코드 목록 조회")
    @GetMapping("/api/eai/statistics/audit")
    public Map<String, Object> getTest(){
        return ImmutableMap.of("nspl", 1, "msg", 2, "ctnt", 3);
    }

    /**
     * 그룹 명으로 공통코드 목록 조회
     * @return
     */
    @ApiOperation(value = "공통 - 공통코드 목록(그룹 명)", notes = "그룹 명으로 공통코드 목록 조회")
    @PostMapping("/api/eai/statistics/audit")
    public Map<String, Object> postTest(@RequestBody TestRequest testRequest){
        return ImmutableMap.of("nspl", testRequest.getNspl(), "msg", testRequest.getMsg(), "ctnt", testRequest.getCtnt());
    }

    /**
     * 그룹 명으로 공통코드 목록 조회
     * @param cdGp
     * @param cdNm
     * @param sortingColumn
     * @param sorting
     * @return
     */
    @ApiOperation(value = "공통 - 공통코드 목록(그룹 명)", notes = "그룹 명으로 공통코드 목록 조회")
    @GetMapping("/codes/{cdGp}")
    public Map<String, Object> getCodeListByGroupCode(@PathVariable String cdGp,
                                                      @RequestParam(value="cdNm", required = false) String cdNm,
                                                      @RequestParam(value="cd", required = false) String cd,
                                                      @RequestParam(value="sortingColumn", required = false) String sortingColumn,
                                                      @RequestParam(value="sorting", required = false) String sorting){
        return codeService.getCodeListByGroupCode(cdGp, cdNm, cd, sortingColumn, sorting);
    }

    /**
     * 공통코드 등록
     * @param loginUser
     * @param param
     * @return
     */
    @ApiOperation(value = "공통 - 공통코드 등록", notes = "공통코드 등록")
    @PostMapping("/code")
    public Map<String, Object> setCodeDetail(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                             @RequestBody Map<String, Object> param){
        return codeService.setCodeDetail(loginUser, param);
    }

    /**
     * 공통코드 수정
     * @param loginUser
     * @param param
     * @return
     */
    @ApiOperation(value = "공통 - 공통코드 수정", notes = "공통코드 수정")
    @PatchMapping("/code")
    public Map<String, Object> updCodeDetail(@ApiIgnore @LoginUserParam LoginUser loginUser,
                                             @RequestBody List<Map<String, Object>> param){
        return codeService.updCodes(loginUser, param);
    }

    /**
     * 공통코드 삭제
     * @param param
     * @return
     */
    @ApiOperation(value = "공통 - 공통코드 삭제", notes = "공통코드 삭제")
    @DeleteMapping("/code")
    public Map<String, Object> delCodeDetail(@RequestBody List<String> param){
        return codeService.delCodes(param);
    }

}
