package maum.biz.qa.controller.common;

import com.google.gson.Gson;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.model.common.vo.DashboardVO;
import maum.biz.qa.service.common.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_QA_ADMIN', 'ROLE_UW_ADMIN')")
@RequestMapping(value = "/dashboard", produces = "text/plain;charset=UTF-8")
public class DashboardApiController {

    private DashboardService dashboardService;

    @Autowired
    public DashboardApiController(DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }

    /**
     * CS / TM 대상건수 pie chart 데이터 조회
     * @return
     */
    @ApiOperation(value = "대쉬보드 - CS / TM 대상건수 차트 데이터 조회", notes = "CS / TM 대상건수 pie chart 데이터 조회")
    @PostMapping(value = "/getPieChartData")
    public String getPieChartData(){
        String title = "getPieChartData";
        log.info(">>>>>>>>>>>>>>>[" + title + "] Start");

        Gson gson = new Gson();
        Map<String, Object> resultMap = new HashMap<>();

        try{
            CamelMap csStt = dashboardService.getCsSttChartInfo();
            CamelMap csTa = dashboardService.getCsTaChartInfo();
            CamelMap tmStt = dashboardService.getTmSttChartInfo();
            CamelMap tmQa = dashboardService.getTmQaChartInfo();

            resultMap.put("csStt", csStt);
            resultMap.put("csTa", csTa);
            resultMap.put("tmStt", tmStt);
            resultMap.put("tmQa", tmQa);
            log.info(resultMap.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return gson.toJson(resultMap);
    }

    @ApiOperation(value = "대쉬보드 - 센터 리스트 조회", notes = "전체 센터 리스트 조회")
    @PostMapping(value = "/getCenterList")
    public String getCenterList(@RequestBody String jsonStr){
        String title = "getCenterList";
        log.info(">>>>>>>>>>>>>>>[" + title + "] Start");

        Gson gson = new Gson();
        DashboardVO dashboardVO = gson.fromJson(jsonStr, DashboardVO.class);
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> centerList = dashboardService.getCtrCntrList(dashboardVO);
            resultMap.put("centerList", centerList);
            log.info(resultMap.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return gson.toJson(resultMap);
    }

    @ApiOperation(value = "대쉬보드 - 센터별 TA 점수붙포 데이터 조회", notes = "센터별 TA 점수붙포 차트 데이터 조회")
    @PostMapping(value = "/getPerCenterTaChart")
    public String getPerCenterTaChart(@RequestBody String jsonStr){
        String title = "getPerCenterTaChart";
        log.info(">>>>>>>>>>>>>>>[" + title + "] Start");

        Gson gson = new Gson();
        DashboardVO dashboardVO = gson.fromJson(jsonStr, DashboardVO.class);
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> centerTaList = dashboardService.getCenterTaScoreInfo(dashboardVO);
            resultMap.put("centerTaList", centerTaList);
            log.info(resultMap.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return gson.toJson(resultMap);
    }

    @ApiOperation(value = "대쉬보드 - 센터벌 TMR 배점 데이터 조회", notes = "센터별 TMR 차트 데이터 조회")
    @PostMapping("/getPerCenterTmrChart")
    public String getPerCenterTmrChart(@RequestBody String jsonStr){
        String title = "getPerCenterTaChart";
        log.info(">>>>>>>>>>>>>>>[" + title + "] Start");

        Gson gson = new Gson();
        DashboardVO dashboardVO = gson.fromJson(jsonStr, DashboardVO.class);
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> centerTmrList = dashboardService.getTmrCntrList(dashboardVO);
            resultMap.put("centerTmrList", centerTmrList);
            log.info(resultMap.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return gson.toJson(resultMap);
    }

    @ApiOperation(value = "대쉬보드 - 월 누적 TA 점수 데이터 조회", notes = "월 누적 TA 점수 데이터 조회")
    @PostMapping("/getMonthTaScoreChart")
    public String getMonthTaScoreChart(@RequestBody String jsonStr){
        String title = "getMonthTaScoreChart";
        log.info(">>>>>>>>>>>>>>>[" + title + "] Start");

        Gson gson = new Gson();
        DashboardVO dashboardVO = gson.fromJson(jsonStr, DashboardVO.class);
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> monthTaScoreList = dashboardService.getMonthTaScoreList(dashboardVO);
            resultMap.put("monthTaScoreList", monthTaScoreList);
            log.info(resultMap.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return gson.toJson(resultMap);
    }
}
