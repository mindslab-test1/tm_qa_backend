package maum.biz.qa.controller.common;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.socket.client.WebSocketConnectionManager;

import java.util.Map;

/**
 * maum.biz.qa.controller.common
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-12-03
 */
@RestController
@RequestMapping("/real-time")
@RequiredArgsConstructor
public class RealtimeApiController {

    private final WebSocketConnectionManager webSocketConnectionManager;
    private final SimpMessagingTemplate template;

    @Value("${ta.realtime.ws.url}")
    private String wsUrl;

    @GetMapping
    public boolean startUpWebSocketStream(){
        if(!webSocketConnectionManager.isRunning()){
            webSocketConnectionManager.start();
        }
        return true;
    }

    @PostMapping("/stt")
    public boolean connectStt(@RequestBody Map<String, Object> param){
        try{
            template.convertAndSend(wsUrl, param);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

}
