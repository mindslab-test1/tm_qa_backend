package maum.biz.qa.controller.monitoring;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.monitoring.MonitoringVO;
import maum.biz.qa.service.monitoring.MonitoringProcService;
import maum.biz.qa.service.monitoring.ServiceMoniService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = "/monitoring", produces = MediaType.APPLICATION_JSON_VALUE)
public class ServiceMonitorApiController {

    private ServiceMoniService serviceMoniService;
    private MonitoringProcService monitoringProcService;

    @Autowired
    public ServiceMonitorApiController(ServiceMoniService serviceMoniService,
                                       MonitoringProcService monitoringProcService) {
        this.serviceMoniService = serviceMoniService;
        this.monitoringProcService = monitoringProcService;
    }

    /**
     * 서비스 모니터링 데이터 조회
     * [param]
     * [return]
     * SERV_ID : 서버ID
     * SVC_ID : 서비스ID
     * SVC_STAT_CD : 서비스상태코드
     * OPRT_DATE : 서비스 가동시간
     * SVC_ST_DATE : 서비스 시작 시간
     * SVC_ED_DATE : 서비스 종료 시간
     */
    @ApiOperation(value = "서비스 모니터링 - 서비스 상태 조회", notes = "서버의 서비스 상태 조회")
    @PostMapping("/getServiceMonitData")
    public Map<String, Object> getServiceMonitData(){
        String title = "getServiceMonitData";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> dataList = serviceMoniService.getMainList();
            resultMap.put("dataList", dataList);
            log.info("dataList>>>>>>>>>>" + resultMap.toString());
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }

    /**
     * 처리 상태 모니터링 - 녹취 / STT 처리상태 조회
     * @param monitoringVO recStDt : 녹취시작일자
     * @return
     * CD_NM_STT : 녹취 업체
     * STT_PROC_SVR : STT처리서버명
     * processing : 녹취 진행
     * recError : 녹취 에러
     * recContNone : 녹취 내용 없음
     * sttOnStandBy : STT 대기중
     * sttProcessing : STT 처리중
     * sttProcessEnd : STT 처리완료
     * sttError : STT 오류
     */
    @ApiOperation(value = "처리 상태 모니터링 - 녹취/STT 처리상태 조회", notes = "녹치/STT 처리상태 조회")
    @PostMapping("/getSttMainList")
    public Map<String, Object> getSttMainList(@RequestBody MonitoringVO monitoringVO){
        String title = "getSttMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> sttDataList = monitoringProcService.getSttMainList(monitoringVO);
            resultMap.put("sttDataList", sttDataList);
            log.info("sttDataList>>>>>>>>>>>>" + sttDataList);
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return  resultMap;
    }

    /**
     * 처리 상태 모니터링 - TA 처리상태 조회
     * @param monitoringVO recStDt : 녹취시작일자
     * @return
     * numberOfEndtries : 인입건수
     * qaStandBy : QA 대기
     * qaProcessing : QA 처리중
     * qaConversionError : QA 변환오류
     * qaProcessingCompleted : QA 처리완료
     * noDetection : 녹취파일 미탐지
     */
    @ApiOperation(value = "처리 상태 모니터링 - TA 처리상태 조회", notes = "TA 처리상태 조회")
    @PostMapping("/getTaMainList")
    public Map<String, Object> getTaMainList(@RequestBody MonitoringVO monitoringVO){
        String title = "getTaMainList";
        log.info(">>>>>>>>>> [" + title + "] Start");
        Map<String, Object> resultMap = new HashMap<>();

        try{
            List<CamelListMap> taDataList = monitoringProcService.getTaMainList(monitoringVO);
            resultMap.put("taDataList", taDataList);
            log.info("taDataList>>>>>>>>>>>>>" + taDataList);
        } catch (Exception e){
            log.error(e.getMessage());
        }

        return resultMap;
    }
}
