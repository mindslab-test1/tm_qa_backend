package maum.biz.qa.service.tm;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.mapper.db.tm.TmSttMapper;
import maum.biz.qa.model.tm.form.TmSttForm;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class TmSttService {

    private final TmSttMapper tmSttMapper;

    /**
     * TM STT 대상 목록 조회
     * @param tmSttForm
     * @return
     */
    public Map<Object, Object> getTmSttList(TmSttForm tmSttForm) {
        int totalCnt = tmSttMapper.getTmSttListCnt(tmSttForm);

        return totalCnt > 0 ?
                ImmutableMap.builder()
                        .put("total_cnt", totalCnt)
                        .put("data", tmSttMapper.getTmSttList(tmSttForm))
                        .build()
                :
                ImmutableMap.builder()
                        .put("total_cnt", 0)
                        .put("data", new ArrayList())
                        .build();
    }

    /**
     * STT 상태 코드 목록 조회
     * @return
     */
    public Map<Object, Object> getProgStatCd() {
        return Optional.of(tmSttMapper.getProgStatCd())
                .map(v -> ImmutableMap.builder().put("data", v).build())
                .orElseGet(() -> ImmutableMap.builder().put("data", null).build());
    }





    // 기존 소스
    /*private static final Logger logger = LoggerFactory.getLogger(TmSttService.class);

    @Autowired
    private TmSttMapper tmSttMapper;

    @Autowired
    private TmCallInfoMariaMapper tmCallInfoMapper;

    public CommonPageVO getTmSttList(TmSttForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("request data getOrder :::: {}" , gson.toJson(requestData.getOrder().get(0)));
        logger.info("request data getOrder column :::: {}" , gson.toJson(requestData.getOrder().get(0).get(CommonDBCursorVO.OrderCriterias.column)));

        CommonPageVO paging = new CommonPageVO();

        int start = requestData.getStart(); //시작페이지
        int length = requestData.getLength();  //한페이지당크기
        int totRowCnt = tmSttMapper.getTmSttListTotalCnt(requestData);   //전체게시물수
        int totPageCnt = (totRowCnt/length)+1;    //전체페이지수

        // 전체 출력
        if(length == -1){
            length = totRowCnt;
        }
        
        int page = 0 ;  // 현재페이지 번호
        if(start == 0){
            page = 1;
        } else {
            page = ((start/length)+1);
        }
        int limit = length;

        String sorting = requestData.getOrder().get(0).get(CommonDBCursorVO.OrderCriterias.dir);

        String sortingColumn = "";

        switch(requestData.getOrder().get(0).get(CommonDBCursorVO.OrderCriterias.column)) {
            case "1": sortingColumn = "CNSL_TM";      // 상담일시
                break;
            case "2": sortingColumn = "CNTR_NM";   // 센터
                break;
            case "3": sortingColumn = "PRT_NM";        // 실
                break;
            case "6": sortingColumn = "CS_NM";   // 고객명
                break;
            case "8": sortingColumn = "PROG_STAT_CD";   // STT상태
                break;
            default: sortingColumn = "RNUM";
                break;
        }

        paging.handlePaging(page, limit, sortingColumn, sorting);

        logger.info("request start :::: {}",start);
        logger.info("request page :::: {}",page);
        logger.info("request totRowCnt :::: {}",totRowCnt);
        logger.info("request totPageCnt :::: {}",totPageCnt);
        logger.info("request limit :::: {}",limit);

        logger.info("request getStartRow :::: {}",paging.getStartRow());
        logger.info("request getEndRow :::: {}",paging.getEndRow());

        // tm stt 목록 요청
        requestData.setStartRow(paging.getStartRow());
        requestData.setEndRow(paging.getEndRow());
        requestData.setSorting(sorting);
        requestData.setSortingColumn(sortingColumn);

        // 기간계 tm stt 목록 조회
        List<CamelListMap> resultListMap = new ArrayList<CamelListMap>();
        resultListMap = tmSttMapper.getTmSttList(requestData);

        logger.info("camelListMap :::: {}" , gson.toJson(resultListMap));

        paging.handlePagingList(resultListMap);
        // datagrid 컬럼 설정
        paging.setDraw(requestData.getDraw());
        paging.setRecordsTotal(totRowCnt);
        paging.setRecordsFiltered(totRowCnt);

        return paging;
    }

    public HashMap<String, Object> getTmSttInfo(TmSttPopupForm requestData)  throws Exception {
        Gson gson = new Gson();

        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        HashMap<String, Object> callInfoMap = new HashMap<String, Object>();
        // 오라클 tm stt info 조회
        resultMap = tmSttMapper.getTmSttInfo(requestData);
        // 솔루션 콜정보 조회
        callInfoMap = tmCallInfoMapper.getTmCallInfo(requestData);
        logger.info("tmCallInfoMapper data :::: " + gson.toJson(callInfoMap));
        if(callInfoMap != null){
            resultMap.put("recStTm", Util.chkNull((String) callInfoMap.get("recStTm"),""));
            resultMap.put("recEdTm",Util.chkNull((String) callInfoMap.get("recEdTm"),""));
        }

        return resultMap;
    }

    *//* tm stt 솔루션 결과 목록 조회 *//*
    public List<CamelListMap> getTmSttRstList(TmSttPopupForm requestData) {
        return tmCallInfoMapper.getTmSttRstList(requestData);
    }

    *//** tm stt 솔루션 탐지문장 수정 *//*
    public HashMap<String, Object> updateTmSttStmt(TmSttPopupForm requestData) throws Exception {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        int checkCnt = 0;
        checkCnt = tmCallInfoMapper.updateTmSttStmt(requestData);

        if (checkCnt > 0) {
            resultMap.put("result","SUCCESS");
        } else {
            resultMap.put("result","FAIL");
        }

        return resultMap;
    }*/
}

