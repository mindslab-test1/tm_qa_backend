package maum.biz.qa.service.cs;

import com.google.gson.Gson;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.Util;
import maum.biz.qa.mapper.db.cs.CsCallInfoMariaMapper;
import maum.biz.qa.mapper.db.cs.CsSttMapper;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.common.vo.CommonPageVO;
import maum.biz.qa.model.cs.form.CsSttForm;
import maum.biz.qa.model.cs.form.CsSttPopupForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class CsSttService {

    private static final Logger logger = LoggerFactory.getLogger(CsSttService.class);

    @Autowired
    private CsSttMapper csSttMapper;

    @Autowired
    private CsCallInfoMariaMapper csCallInfoMapper;

    public CommonPageVO getCsSttList(CsSttForm requestData) throws Exception {
        Gson gson = new Gson();
        logger.info("request data getOrder :::: {}" , gson.toJson(requestData.getOrder().get(0)));
        logger.info("request data getOrder column :::: {}" , gson.toJson(requestData.getOrder().get(0).get(CommonDBCursorVO.OrderCriterias.column)));

        CommonPageVO paging = new CommonPageVO();

        int start = requestData.getStart(); //시작페이지
        int length = requestData.getLength();  //한페이지당크기
        int totRowCnt = csSttMapper.getCsSttListTotalCnt(requestData);   //전체게시물수
        int totPageCnt = (totRowCnt/length)+1;    //전체페이지수

        // 전체 출력
        if(length == -1){
            length = totRowCnt;
        }
        
        int page = 0 ;  // 현재페이지 번호
        if(start == 0){
            page = 1;
        } else {
            page = ((start/length)+1);
        }
        int limit = length;
        String sorting = requestData.getOrder().get(0).get(CommonDBCursorVO.OrderCriterias.dir);

        String sortingColumn = "";

        switch(requestData.getOrder().get(0).get(CommonDBCursorVO.OrderCriterias.column)) {
            case "1": sortingColumn = "CNSL_DTM";      // 상담일시
                break;
            case "2": sortingColumn = "CS_TLNO_CTT";   // 고객전화번호
                break;
            case "4": sortingColumn = "CS_PK";        // 고객번호
                break;
            case "7": sortingColumn = "CNSL_EMPNO";   // 상담사사번
                break;
            case "9": sortingColumn = "OUB_BZ_SCTN_NAM";   // 업무구분
                break;
            case "14": sortingColumn = "MAX_SILENCE";   // 무음길이
                break;
            case "17": sortingColumn = "PROG_STAT_CD";   // STT상태
                break;
            default: sortingColumn = "RNUM";
                break;
        }

        paging.handlePaging(page, limit, sortingColumn, sorting);

        // cs stt 목록 요청
        requestData.setStartRow(paging.getStartRow());
        requestData.setEndRow(paging.getEndRow());
        requestData.setSorting(sorting);
        requestData.setSortingColumn(sortingColumn);

        // 기간계 cs stt 목록 조회
        List<CamelListMap> resultListMap = new ArrayList<CamelListMap>();
        resultListMap = csSttMapper.getCsSttList(requestData);

//        logger.info("camelListMap :::: {}" , gson.toJson(resultListMap));

//         솔루션 상담키워드 조회   START
        HashMap<String, Object> cuslSecMap = new HashMap<String, Object>();

        for(CamelListMap camelListMap : resultListMap) {
            // cs stt 녹취번호로 솔루션 cs 상담키워드명 조회
//            logger.info("recgIdCtt :::: {}" , camelListMap.get("recgIdCtt"));
            cuslSecMap = csCallInfoMapper.getCsCuslSec((String) camelListMap.get("recgIdCtt"));
            if(cuslSecMap == null){
                camelListMap.put("CUSL_NM","");
            }else{
                camelListMap.put("CUSL_NM",cuslSecMap.get("cuslNm"));
            }
//            logger.info("cuslNm :::: {}" , camelListMap.get("cuslNm"));
        }
//            솔루션 상담키워드 조회   END


        paging.handlePagingList(resultListMap);
        // datagrid 컬럼 설정
        paging.setDraw(requestData.getDraw());
        paging.setRecordsTotal(totRowCnt);
        paging.setRecordsFiltered(totRowCnt);

        return paging;
    }

    public HashMap<String, Object> getCsSttInfo(CsSttPopupForm requestData)  throws Exception {
        Gson gson = new Gson();

        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        HashMap<String, Object> callInfoMap = new HashMap<String, Object>();
        // 오라클 cs stt info 조회
        resultMap = csSttMapper.getCsSttInfo(requestData);
        // 솔루션 콜정보 조회
        callInfoMap = csCallInfoMapper.getCsCallInfo(requestData);
        logger.info("callInfoMap data :::: " + gson.toJson(callInfoMap));
        if(callInfoMap != null){
            resultMap.put("recStTm", Util.chkNull((String) callInfoMap.get("recStTm"),""));
            resultMap.put("recEdTm",Util.chkNull((String) callInfoMap.get("recEdTm"),""));
        }

        return resultMap;
    }

    /* cs stt 솔루션 결과 목록 조회 */
    public List<CamelListMap> getCsSttRstList(CsSttPopupForm requestData) {
        return csCallInfoMapper.getCsSttRstList(requestData);
    }

    /** cs stt 솔루션 탐지문장 수정 */
    public HashMap<String, Object> updateCsSttStmt(CsSttPopupForm requestData) throws Exception {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        int checkCnt = 0;
        checkCnt = csCallInfoMapper.updateCsSttStmt(requestData);

        if (checkCnt > 0) {
            resultMap.put("result","SUCCESS");
        } else {
            resultMap.put("result","FAIL");
        }

        return resultMap;
    }
}

