package maum.biz.qa.service.statistics;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.statistics.StatisticMapper;
import maum.biz.qa.model.manage.vo.StatVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class StatisticsService {
    private StatisticMapper statisticMapper;

    @Autowired
    public StatisticsService(StatisticMapper statisticMapper) {
        this.statisticMapper = statisticMapper;
    }

    // QA 담당 목록 조회
    public List<CamelListMap> getQaSelectBoxList(){
        return statisticMapper.getQaSelectBoxList();
    }

    // QA 통계 목록 조회
    public List<CamelListMap> getQaStatisticList(StatVO statVO){
        return statisticMapper.getQaStatisticList(statVO);
    }


    // TMR 통계 목록 조회
    public List<CamelListMap> selectTmrStatisticList(StatVO statVO){
        return statisticMapper.selectTmrStatisticList(statVO);
    }
}
