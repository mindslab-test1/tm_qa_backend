package maum.biz.qa.service.tmqa;

import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.enums.tmqa.AssignStatus;
import maum.biz.qa.mapper.db.tmqa.TmqaAssignMapper;
import maum.biz.qa.mapper.db.tmqa.TmqaCallInfoMariaMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.model.tmqa.vo.TmqaVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * maum.biz.qa.service.tmqa
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-04-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-04-20
 */
@Service
public class TmqaAssignService {

    final TmqaCallInfoMariaMapper tmqaCallInfoMariaMapper;

    final TmqaAssignMapper tmqaAssignMapper;

    public TmqaAssignService(TmqaCallInfoMariaMapper tmqaCallInfoMariaMapper, TmqaAssignMapper tmqaAssignMapper) {
        this.tmqaCallInfoMariaMapper = tmqaCallInfoMariaMapper;
        this.tmqaAssignMapper = tmqaAssignMapper;
    }

    /**
     * 심사자별 상세 현황 조회
     * @param tmqaVO
     * @return
     */
    public Map<String, Object> getPerJudgeDetail(LoginUser loginUser, TmqaVO tmqaVO){
        Map<String, Object> rtnMap = new HashMap<>();
        if(!Optional.ofNullable(tmqaVO.getQaNameId()).isPresent()){
            tmqaVO.setQaNameId(loginUser.getUserId());
            tmqaVO.setUserRole(loginUser.getUserRole().name());
        }
        int cnt = tmqaAssignMapper.selectPerJudgeDetailCNT(tmqaVO);
        rtnMap.put("length", cnt);
        if(cnt > 0){
            rtnMap.put("data", tmqaAssignMapper.selectPerJudgeDetail(tmqaVO));
        }else{
            rtnMap.put("data", new ArrayList<>());
        }
        return rtnMap;
    }

    /**
     * QA대상, QA배정 목록 조회
     * @param tmqaSttForm
     * @return
     */
    public Map<String, Object> getQaAssignTargetList(TmqaSttForm tmqaSttForm){
        Map<String, Object> rtnMap = new HashMap<>();
        int cnt = tmqaCallInfoMariaMapper.selectQaTargetListCNT(tmqaSttForm);
        rtnMap.put("length", cnt);
        if(cnt > 0){
            rtnMap.put("data", tmqaCallInfoMariaMapper.selectQaTargetList(tmqaSttForm));
        }else{
            rtnMap.put("data", new ArrayList<>());
        }
        return rtnMap;
    }

    /**
     * 배정현황 조회
     * @param tmqaVO
     * @return
     */
    public Map<String, Object> getAssignCntList(TmqaVO tmqaVO){
        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("data", tmqaAssignMapper.selectAssignCntList(tmqaVO));
        return rtnMap;
    }

    /**
     * QA심사자 목록 조회
     * @return
     */
    public Map<String, Object> getQaUserList(){
        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("data", tmqaAssignMapper.selectQaUserList());
        return rtnMap;
    }

    /**
     * QA심사자 휴무정보 등록/수정
     * @param loginUser
     * @param tmqaVOList
     * @return
     */
    @Transactional
    public Map<String, Object> setOrUpdHolidayInfo(LoginUser loginUser, List<TmqaVO> tmqaVOList){
        Map<String, Object> rtnMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        tmqaVOList.stream().forEach(tmqaVO -> {
            tmqaVO.setCreatorId(loginUser.getUserId());
            if(tmqaAssignMapper.insertHolidayInfo(tmqaVO) == 0){
                Map<String, Object> userInfo = new HashMap<>();
                userInfo.put("userId", tmqaVO.getUserId());
                userInfo.put("userNm", tmqaVO.getUserNm());
                list.add(userInfo);
            }
        });
        if(list.size() > 0){
            rtnMap.put("result", list.size() == tmqaVOList.size() ? DataAccessStatus.FAIL : DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("data", list);
        }else{
            rtnMap.put("result", DataAccessStatus.SUCCESS);
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get("result"))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

    /**
     * 배정등록
     * @param loginUser
     * @param tmqaVOList
     * @return
     */
    @Transactional
    public Map<String, Object> setAssignInfo(LoginUser loginUser, List<TmqaVO> tmqaVOList){
        Map<String, Object> rtnMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        tmqaVOList.stream().forEach(tmqaVO -> {
            Map<String, Object> assignInfo;
            LocalDate now = LocalDate.now();
            tmqaVO.setCreatorId(loginUser.getUserId());
            tmqaVO.setQaAllcDtme(now.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
            tmqaVO.setQaLimitDt(now.plusDays(7).format(DateTimeFormatter.ofPattern("yyyyMMdd")));
            if(tmqaAssignMapper.updateAssignDate(tmqaVO) == 0){
                assignInfo = new HashMap<>();
                assignInfo.put("nsplNo", tmqaVO.getNsplNo());
                assignInfo.put("userId", tmqaVO.getUserId());
                assignInfo.put("userNm", tmqaVO.getUserNm());
                assignInfo.put("status", AssignStatus.UPDATE_DATE_FAIL);
                list.add(assignInfo);
            }else{
                if(tmqaAssignMapper.insertAssignInfo(tmqaVO) == 0){
                    assignInfo = new HashMap<>();
                    assignInfo.put("nsplNo", tmqaVO.getNsplNo());
                    assignInfo.put("userId", tmqaVO.getUserId());
                    assignInfo.put("userNm", tmqaVO.getUserNm());
                    assignInfo.put("status", AssignStatus.ASSIGN_INSERT_FAIL);
                    list.add(assignInfo);
                }
            }
        });
        if(list.size() > 0){
            rtnMap.put("result", list.size() == tmqaVOList.size() ? DataAccessStatus.FAIL : DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("data", list);
        }else{
            rtnMap.put("result", DataAccessStatus.SUCCESS);
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get("result"))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

}
