package maum.biz.qa.service.tmqa;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.tmqa.TmqaAssignMapper;
import maum.biz.qa.mapper.db.tmqa.TmqaSttOracleMapper;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TmqaResultService {
    private TmqaSttOracleMapper tmqaSttOracleMapper;
    private TmqaAssignMapper tmqaAssignMapper;

    @Autowired
    public TmqaResultService(TmqaSttOracleMapper tmqaSttOracleMapper, TmqaAssignMapper tmqaAssignMapper) {
        this.tmqaSttOracleMapper = tmqaSttOracleMapper;
        this.tmqaAssignMapper = tmqaAssignMapper;
    }

    /**
     * QA 결과 질의응답 값 조회
     */
    public List<CamelListMap> getResultQuestion(TmqaSttForm tmqaSttForm){
        return tmqaSttOracleMapper.selectQaResultQuestion(tmqaSttForm);
    }

}
