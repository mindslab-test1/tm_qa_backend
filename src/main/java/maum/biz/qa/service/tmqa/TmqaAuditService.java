package maum.biz.qa.service.tmqa;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.enums.tmqa.AuditStatus;
import maum.biz.qa.mapper.db.tmqa.TmqaAuditMapper;
import maum.biz.qa.model.common.enumm.UserRoleEnum;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.tmqa.vo.TmqaAuditVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * TmqaAuditService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-06  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-06
 */
@Service
public class TmqaAuditService {

    final TmqaAuditMapper tmqaAuditMapper;
    private String status = "status";

    public TmqaAuditService(TmqaAuditMapper tmqaAuditMapper) {
        this.tmqaAuditMapper = tmqaAuditMapper;
    }

    /**
     * 청약 MASTER 정보
     * @param nsplNo
     * @return
     */
    public Map<String, Object> getQaAuditMasterInfo(LoginUser loginUser, String nsplNo){
        Map<String, Object> rtnMap = new HashMap<>();
        // ADMIN일 경우 모든 데이터 확인가능, 아닐 경우 자기 자신의 데이터만 확인가능
        return Optional.ofNullable(tmqaAuditMapper.getQaAuditMasterInfo(loginUser.getUserRole() == UserRoleEnum.ROLE_ADMIN || loginUser.getUserRole() == UserRoleEnum.ROLE_QA_ADMIN || loginUser.getUserRole() == UserRoleEnum.ROLE_UW_ADMIN ? null : loginUser.getUserId(), nsplNo))
                .map(v -> {
                    rtnMap.put("data", v);
                    rtnMap.put(status, DataAccessStatus.SUCCESS);
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    return rtnMap;
                });
    }

    /**
     * 상담 콜 목록
     * @param nsplNo
     * @return
     */
    public Map<String, Object> getQaAuditCallList(String nsplNo){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(tmqaAuditMapper.getQaAuditCallList(nsplNo))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(!v.isEmpty()){
                        rtnMap.put(status, DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 심사 회차별 이력 목록
     * @param nsplNo
     * @return
     */
    public Map<String, Object> getQaAuditRoundList(String nsplNo){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(tmqaAuditMapper.getQaAuditRoundList(nsplNo))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(!v.isEmpty()){
                        rtnMap.put("totalQaTime", tmqaAuditMapper.getQaAuditRoundSumQaTime(nsplNo));
                        rtnMap.put(status, DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 평가항목결과 목록 조회
     * @param nsplNo
     * @param tmsInfo
     * @param psnJugCat
     * @param chkCtgCd
     * @return
     */
    public Map<String, Object> getEvaluationItemList(String nsplNo, String tmsInfo, String psnJugCat, String chkCtgCd){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(tmqaAuditMapper.getEvaluationItemList(nsplNo, tmsInfo, psnJugCat, chkCtgCd))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(!v.isEmpty()){
                        rtnMap.put(status, DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 표준 계약 스크립트 목록
     * @param nsplNo
     * @param tmsInfo
     * @return
     */
    public Map<String, Object> getContractScriptList(String nsplNo, String tmsInfo, String prodCat, String chkItmCd){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(tmqaAuditMapper.getContractScriptList(nsplNo, tmsInfo, prodCat, chkItmCd))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(!v.isEmpty()){
                        rtnMap.put(status, DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 표준스크립트 탐지결과 목록 조회
     * @param nsplNo
     * @param tmsInfo
     * @return
     */
    public Map<String, Object> getContractScriptDetectionList(String nsplNo, String tmsInfo, String prodCat, String chkItmCd){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(tmqaAuditMapper.getContractScriptDetectionList(nsplNo, tmsInfo, prodCat, chkItmCd))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(!v.isEmpty()){
                        rtnMap.put(status, DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 심사정보 임시저장 / 심사완료
     * @param loginUser
     * @param round
     * @return
     */
    @Transactional
    @SuppressWarnings("all") // 분기처리 기준 초과로 인한 lint disable
    public Map<String, Object> setAudit(LoginUser loginUser, Map<String, Object> round){
        Map<String, Object> rtnMap = new HashMap<>();
        TmqaAuditVO vo = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).convertValue(round, TmqaAuditVO.class);
        if(!"Y".equals(vo.getTmpSavYn()) && ("1".equals(vo.getEvltRsltStat()) || "3".equals(vo.getEvltRsltStat()))){
            rtnMap.put(status, AuditStatus.TEMP_SAVE_WRONG_STATE);
        }else{
            List<Map<String, Object>> list = vo.getEvaluationItems().stream()
                    .map(v -> {
                        Map<String, Object> itemMap = new HashMap<>();
                        v.setUpdatorId(loginUser.getUserId());
                        if(tmqaAuditMapper.updQaCheckResult(v) > 0){
                            itemMap.put(status, AuditStatus.QA_CHECK_RESULT_UPDATE_SUCCEED);
                        }else{
                            itemMap.put(status, AuditStatus.QA_CHECK_RESULT_UPDATE_FAILED);
                            itemMap.put("code", v.getChkItmCd());
                        }
                        return itemMap;
                    }).collect(Collectors.toList());
            long failCount = list.stream().filter(v -> v.get(status).equals(AuditStatus.QA_CHECK_RESULT_UPDATE_FAILED)).count();
            if(list.size() == failCount){
                rtnMap.put(status, AuditStatus.QA_CHECK_RESULT_UPDATE_FAILED);
            }else if(failCount > 0){
                rtnMap.put(status, AuditStatus.QA_CHECK_RESULT_UPDATE_PARTIAL_SUCCEEDED);
                rtnMap.put("codes", list.stream().filter(v -> v.get("code") != null).map(v -> v.get("code")).collect(Collectors.toList()));
            }else{
                vo.setUpdatorId(loginUser.getUserId());
                AtomicInteger cnslScr = new AtomicInteger();
                AtomicInteger scrpScr = new AtomicInteger();
                vo.getEvaluationItems().stream()
                        .forEach(v -> {
                            if("S".equals(v.getChkCtgCd())){
                                scrpScr.set(scrpScr.get()+Integer.parseInt(v.getScrRst()));
                            }else if("C".equals(v.getChkCtgCd())){
                                cnslScr.set(cnslScr.get()+Integer.parseInt(v.getScrRst()));
                            }
                        });
                vo.setQaScrpScr(scrpScr.get());
                vo.setQaCnslScr(cnslScr.get());
                if(tmqaAuditMapper.updAudit(vo) > 0){
                    rtnMap.put(status, DataAccessStatus.SUCCESS);
                }else{
                    rtnMap.put(status, AuditStatus.AUDIT_UPDATE_FAILED);
                }
            }
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get(status))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

    /**
     * 질의 / 응답 조회
     * @param nsplNo
     * @return
     */
    public Map<String, Object> getQuestionsAndAnswers(String nsplNo){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(tmqaAuditMapper.getQuestionsAndAnswers(nsplNo))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(!v.isEmpty()){
                        rtnMap.put(status, DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put(status, DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put(status, DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 응답 등록
     * @param loginUser
     * @param param
     * @return
     */
    public Map<String, Object> setAnswer(LoginUser loginUser, Map<String, Object> param){
        Map<String, Object> rtnMap = new HashMap<>();
        param.put("creatorId", loginUser.getUserId());
        // TODO 추후 기간계 연동
        rtnMap.put(status, tmqaAuditMapper.setAnswer(param) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
        return rtnMap;
    }

}
