package maum.biz.qa.service.tmqa;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.mapper.db.tmqa.TmqaCallInfoMariaMapper;
import maum.biz.qa.mapper.db.tmqa.TmqaSttMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.tmqa.form.TmqaSttForm;
import maum.biz.qa.model.tmqa.form.TmqaSttPopupForm;
import maum.biz.qa.model.tmqa.form.TmqaTargetPopupForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
@Service
public class TmqaSttService {

    private final TmqaSttMapper tmqaSttMapper;
    private final TmqaCallInfoMariaMapper tmqaCallInfoMariaMapper;

    public List<CamelListMap> getTmqaTargetList(TmqaSttForm tmqaSttForm) {
        return tmqaCallInfoMariaMapper.selectQaTargetList(tmqaSttForm);
    }

    public int selectQaTargetListCNT(TmqaSttForm tmqaSttForm){
        return tmqaCallInfoMariaMapper.selectQaTargetListCNT(tmqaSttForm);
    }

    // Tmqa QA 대상 목록 조회
    public Map<String, Object> getTmqaSttList(TmqaSttForm tmqaSttForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        int cnt = tmqaCallInfoMariaMapper.selectQaTargetListCNT(tmqaSttForm);
        rtnMap.put("length", cnt);
        if(cnt > 0){
            rtnMap.put("data", tmqaCallInfoMariaMapper.selectQaTargetList(tmqaSttForm));
        }else{
            rtnMap.put("data", null);
        }
        return rtnMap;
    }

    // Tmqa QA 대상 대상으로 수동 등록
    @Transactional
    public Map<String, Object> setTargetY(List<TmqaSttForm> tmqaSttForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();

        tmqaSttForm.forEach(v -> {
            try {
                result.addAndGet(tmqaSttMapper.setTargetY(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("data", false);
            }
        });

        if(result.intValue() == (tmqaSttForm.size())) {
            rtnMap.put("data", true);
        }else {
            rtnMap.put("data", false);
        }

        return rtnMap;
    }

    // Tmqa QA 대상 대상으로 수동 삭제
    @Transactional
    public Map<String, Object> setTargetN(List<TmqaSttForm> tmqaSttForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();

        tmqaSttForm.forEach(v -> {
            try {
                result.addAndGet(tmqaSttMapper.setTargetN(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("data", false);
            }
        });

        if(result.intValue() == (tmqaSttForm.size())) {
            rtnMap.put("data", true);
        }else {
            rtnMap.put("data", false);
        }

        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 TMR 조회
    public Map<String, Object> getTmrUserList() {
        Map<String, Object> rtnMap = new HashMap<>();
        List<CamelListMap> result = tmqaSttMapper.selectTmrUserList();
        if(result != null && result.size() > 0){
            rtnMap.put("data", result);
        }else{
            rtnMap.put("data", null);
        }
        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 TA점수 조회
    public Map<String, Object> getAutoScrList() {
        Map<String, Object> rtnMap = new HashMap<>();
        List<CamelListMap> result = tmqaSttMapper.selectAutoScrList();
        if(result != null && result.size() > 0){
            rtnMap.put("data", result);
        }else{
            rtnMap.put("data", null);
        }
        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 DTCT 조회
    public Map<String, Object> getDtctList() {
        Map<String, Object> rtnMap = new HashMap<>();
        List<CamelListMap> result = tmqaSttMapper.selectDtctList();
        if(result != null && result.size() > 0){
            rtnMap.put("data", result);
        }else{
            rtnMap.put("data", null);
        }
        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 상품명 조회
    public Map<String, Object> getProdList() {
        Map<String, Object> rtnMap = new HashMap<>();
        List<CamelListMap> result = tmqaSttMapper.selectProdList();
        if(result != null && result.size() > 0){
            rtnMap.put("data", result);
        }else{
            rtnMap.put("data", null);
        }
        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 각 항목별 건수 조회
    public Map<String, Object> getTargetPopupCount() {
        Map<String, Object> rtnMap = new HashMap<>();
        List<CamelListMap> result = tmqaSttMapper.getAutoCntList();
        if(result != null && result.size() > 0){
            rtnMap.put("data", result);
        }else{
            rtnMap.put("data", null);
        }
        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 TMR 저장
    @Transactional
    public Map<String, Object> setAutoTmrUser(List<TmqaTargetPopupForm> tmqaTargetPopupForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();
        tmqaSttMapper.delAutoTmr();

        tmqaTargetPopupForm.forEach(v -> {
            try {
                result.addAndGet(tmqaSttMapper.insertTmrTarget(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("result", false);
            }
        });

        if(result.intValue() == (tmqaTargetPopupForm.size())) {
            rtnMap.put("result", true);
        }else {
            rtnMap.put("result", false);
        }

        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 TA점수 저장
    @Transactional
    public Map<String, Object> setAutoScr(TmqaTargetPopupForm tmqaTargetPopupForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        tmqaSttMapper.delAutoScr();
        int result = tmqaSttMapper.insertTaScrTarget(tmqaTargetPopupForm);

        if (result > 0) {
            rtnMap.put("result", true);
        } else {
            rtnMap.put("result", false);
        }
        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 TA 부분/미탐지 구간 저장
    @Transactional
    public Map<String, Object> setAutoDtct(List<TmqaTargetPopupForm> tmqaTargetPopupForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();
        tmqaSttMapper.delAutoDtct();

        tmqaTargetPopupForm.forEach(v -> {
            try {
                result.addAndGet(tmqaSttMapper.insertEvalTarget(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("result", false);
            }
        });

        if(result.intValue() == (tmqaTargetPopupForm.size())) {
            rtnMap.put("result", true);
        }else {
            rtnMap.put("result", false);
        }

        return rtnMap;
    }

    // Tmqa QA 대상설정 팝업 상품명 저장
    @Transactional
    public Map<String, Object> setAutoProd(List<TmqaTargetPopupForm> tmqaTargetPopupForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();
        tmqaSttMapper.delAutoProd();

        tmqaTargetPopupForm.forEach(v -> {
            try {
                result.addAndGet(tmqaSttMapper.insertProdTarget(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("result", false);
            }
        });

        if(result.intValue() == (tmqaTargetPopupForm.size())) {
            rtnMap.put("result", true);
        }else {
            rtnMap.put("result", false);
        }

        return rtnMap;
    }

    /**
     * Tmqa QA 대상설정 팝업 건수 저장
     * @param tmqaTargetPopupForm
     * @return
     * @throws Exception
     */
    @Transactional
    public Map<String, Object> setAutoCnt(TmqaTargetPopupForm tmqaTargetPopupForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        int result = 0;
        tmqaSttMapper.delAutoCnt();

        result += tmqaSttMapper.setAutoCntTmr(tmqaTargetPopupForm);
        result += tmqaSttMapper.setAutoCntScr(tmqaTargetPopupForm);
        result += tmqaSttMapper.setAutoCntDtct(tmqaTargetPopupForm);
        result += tmqaSttMapper.setAutoCntProd(tmqaTargetPopupForm);

        if (result == 4) {
            rtnMap.put("result", true);
        } else {
            rtnMap.put("result", false);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

    /**
     * TMQA STT 전수 솔루션 결과 목록 조회
     * @param tmqaSttPopupForm
     * @return
     */
    public Map<String, Object> getTmqaAllSttRstList(TmqaSttPopupForm tmqaSttPopupForm) {
        Map<String, Object> rtnMap = new HashMap<>();
        List<CamelListMap> result = tmqaCallInfoMariaMapper.getTmqaSttRstList(tmqaSttPopupForm);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else{
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    /**
     * TMQA STT 전수 솔루션 탐지문장 수정
     * @param tmqaSttPopupForm
     * @param loginUser
     * @return
     */
    @Transactional
    public Map<String, Object> updTmSttStmt(TmqaSttPopupForm tmqaSttPopupForm, LoginUser loginUser) {
        Map<String, Object> rtnMap = new HashMap<>();
        tmqaSttPopupForm.setUpdatorId(loginUser.getUserId());
        int result = tmqaCallInfoMariaMapper.updTmSttStmt(tmqaSttPopupForm);
        if(result > 0) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else{
            rtnMap.put("status", DataAccessStatus.FAIL);
        }

        return rtnMap;
    }

}
