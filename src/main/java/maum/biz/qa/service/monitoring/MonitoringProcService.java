package maum.biz.qa.service.monitoring;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.monitoring.ProcStatusMapper;
import maum.biz.qa.model.monitoring.MonitoringVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MonitoringProcService {

    private ProcStatusMapper procStatusMapper;

    @Autowired
    public MonitoringProcService(ProcStatusMapper procStatusMapper) {
        this.procStatusMapper = procStatusMapper;
    }

    public List<CamelListMap> getSttMainList(MonitoringVO monitoringVO){
        return procStatusMapper.getSttMainList(monitoringVO);
    }

    public List<CamelListMap> getTaMainList(MonitoringVO monitoringVO){
        return procStatusMapper.getTaMainList(monitoringVO);
    }
}
