package maum.biz.qa.service.monitoring;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.monitoring.ServiceMoniMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ServiceMoniService {

    private ServiceMoniMapper serviceMoniMapper;

    @Autowired
    public ServiceMoniService(ServiceMoniMapper serviceMoniMapper) {
        this.serviceMoniMapper = serviceMoniMapper;
    }

    public List<CamelListMap> getMainList(){
        return serviceMoniMapper.getMainList();
    }
}
