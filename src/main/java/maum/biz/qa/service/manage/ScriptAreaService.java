package maum.biz.qa.service.manage;

import lombok.RequiredArgsConstructor;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.mapper.db.manage.ScriptAreaMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.dto.QaScrpDTO;
import maum.biz.qa.model.manage.entity.*;
import maum.biz.qa.model.manage.vo.QaScrpSecVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

;

@Service
@RequiredArgsConstructor
public class ScriptAreaService {

    private final ScriptAreaMapper mapper;


    public List<CamelListMap> getLctgCd(){
        return mapper.getLctgCd();
    }

    public List<CamelListMap> getMctgCd(){
        return mapper.getMctgCd();
    }

    public List<CamelListMap> getSctgCd(){
        return mapper.getSctgCd();
    }

    public List<CamelListMap> getScriptCodeList(QaScrpSecVO qaScrpSecVO){
        return mapper.getScriptCodeList(qaScrpSecVO);
    }

    public List<CamelListMap> getScriptAreaMainList(QaScrpSecVO qaScrpSecVO){
        return mapper.getScriptAreaMainList(qaScrpSecVO);
    }

    public int getScripAreaMainListCnt(QaScrpSecVO qaScrpSecVO){
        return mapper.getScripAreaMainListCnt(qaScrpSecVO);
    }

    public List<CamelListMap> getScriptArticleList(QaScrpSecVO qaScrpSecVO){
        return mapper.getScriptArticleList(qaScrpSecVO);
    }





    // 스크립트 구간 관리 코드 등록
    public Map<String, Object> setCode(LoginUser loginUser, QaScrpSecCd qaScrpSecCd) {
        Map<String, Object> rtnMap = new HashMap<>();
        int result;

        result = mapper.getCodeCnt(qaScrpSecCd);
        if(result > 0) {
            rtnMap.put("status", DataAccessStatus.CONFLICT);
        }else {
            qaScrpSecCd.setUpdatorId(loginUser.getUserId());
            result = mapper.setCode(qaScrpSecCd);
            if(result > 0) {
                rtnMap.put("status", DataAccessStatus.SUCCESS);
            }else {
                rtnMap.put("status", DataAccessStatus.FAIL);
            }
        }

        return rtnMap;
    }

    // 스크립트 구간 관리 대분류 조회
    public Map<String, Object> getLctgList() {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaScrpSecCd> result = mapper.getLctgList();
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 스크립트 구간 관리 코드 목록 조회
    public Map<String, Object> getCodeList(QaScrpSecCd qaScrpSecCd) {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaScrpSecCd> result = mapper.getCodeList(qaScrpSecCd);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 스크립트 구간 관리 코드 수정
    @Transactional
    public Map<String, Object> updCode(LoginUser loginUser, List<QaScrpSecCd> qaScrpSecCdList) {
        Map<String, Object> rtnMap = new HashMap<>();

        try {
            qaScrpSecCdList.forEach(v -> {
                v.setUpdatorId(loginUser.getUserId());
                mapper.updCode(v);
            });
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }catch (Exception e) {
            rtnMap.put("status", DataAccessStatus.FAIL);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }

        return rtnMap;
    }

    // 스크립트 구간 관리 코드 삭제
    @Transactional
    public Map<String, Object> delCode(List<QaScrpSecCd> qaScrpSecCd) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();

        qaScrpSecCd.forEach(v -> {
            try {
                result.addAndGet(mapper.delCode(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("status", DataAccessStatus.FAIL);
            }
        });

        if(rtnMap.get("status") != null) {
            return rtnMap;
        }else if(result.intValue() == qaScrpSecCd.size()) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else {
            rtnMap.put("status", DataAccessStatus.PARTIAL_SUCCESS);
        }

        return rtnMap;
    }

    // 구간 목록 조회
    public Map<String, Object> getScriptAreaList(QaScrpSec qaScrpSec) {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaScrpSec> result = mapper.getScriptAreaList(qaScrpSec);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 구간 등록
    public Map<String, Object> setScriptArea(LoginUser loginUser, QaScrpSec qaScrpSec) {
        Map<String, Object> rtnMap = new HashMap<>();
        int result;

        result = mapper.getScriptAreaCnt(qaScrpSec);
        if(result > 0) {
            rtnMap.put("status", DataAccessStatus.CONFLICT);
        }else {
            qaScrpSec.setUpdatorId(loginUser.getUserId());
            result = mapper.setScriptArea(qaScrpSec);
            if(result > 0) {
                rtnMap.put("status", DataAccessStatus.SUCCESS);
            }else {
                rtnMap.put("status", DataAccessStatus.FAIL);
            }
        }

        return rtnMap;
    }

    // 구간 수정
    public Map<String, Object> updScriptArea(LoginUser loginUser, QaScrpSec qaScrpSec) {
        Map<String, Object> rtnMap = new HashMap<>();
        qaScrpSec.setUpdatorId(loginUser.getUserId());

        int result = mapper.updScriptArea(qaScrpSec);
        if(result > 0) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else {
            rtnMap.put("status", DataAccessStatus.FAIL);
        }

        return rtnMap;
    }

    // 구간 삭제
    public Map<String, Object> delScriptArea(QaScrpSec qaScrpSec) {
        Map<String, Object> rtnMap = new HashMap<>();

        int result = mapper.delScriptArea(qaScrpSec);
        if(result > 0) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else {
            rtnMap.put("status", DataAccessStatus.FAIL);
        }

        return rtnMap;
    }

    // 구간 관리 메인 목록 조회
    public Map<String, Object> getScriptAreaMainList(QaScrpSec qaScrpSec, CommonDBCursorVO commonDBCursorVO) {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaScrpSec> result = mapper.getScriptAreaMainList2(qaScrpSec, commonDBCursorVO);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
            rtnMap.put("cnt", mapper.getScriptAreaMainListCnt(qaScrpSec));
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 문장 관리 목록 조회
    public Map<String, Object> getScriptSentenceList(QaScrp qaScrp) {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaScrpDTO> result = mapper.getScriptSentenceList(qaScrp);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 문장 검색 목록 조회
    public Map<String, Object> getSentenceList(String searchKeyword, CommonDBCursorVO commonDBCursorVO) {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaStmt> result = mapper.getSentenceList(searchKeyword, commonDBCursorVO);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
            rtnMap.put("cnt", mapper.getSentenceListCnt(searchKeyword));
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 문장 관리 등록
    public Map<String, Object> setScriptSentence(LoginUser loginUser, List<QaScrp> qaScrpList) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger duplicateCnt = new AtomicInteger();

        qaScrpList.forEach(v -> {
            int result = mapper.getSentenceCnt(v);
            if(result > 0) {
                duplicateCnt.addAndGet(result);
            }else {
                try {
                    v.setCreatorId(loginUser.getUserId());
                    mapper.setScriptSentence(v);
                }catch (Exception e) {
                    e.printStackTrace();
                    rtnMap.put("status", DataAccessStatus.FAIL);
                }
            }
        });

        if(rtnMap.get("status") != null) {
            return rtnMap;
        }else if(duplicateCnt.intValue() == 0) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else {
            rtnMap.put("status", DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("data", duplicateCnt.intValue());
        }

        return rtnMap;
    }

    // 문장 관리 삭제
    public Map<String, Object> delScriptSentence(List<QaScrp> qaScrpList) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();

        qaScrpList.forEach(v -> {
            try {
                result.addAndGet(mapper.delScriptSentence(v));
            }catch (Exception e) {
                e.printStackTrace();
                rtnMap.put("status", DataAccessStatus.FAIL);
            }
        });

        if(rtnMap.get("status") != null) {
            return rtnMap;
        }else if(result.intValue() == qaScrpList.size()) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else {
            rtnMap.put("status", DataAccessStatus.PARTIAL_SUCCESS);
        }

        return rtnMap;
    }

    // 점검기준 상세 설정 조회
    public Map<String, Object> getArticleDetail(QaScrp qaScrp) {
        Map<String, Object> rtnMap = new HashMap<>();

        QaScrpDTO qaScrpDTO = mapper.getArticleDetail(qaScrp);
        if(qaScrpDTO != null) {
            rtnMap.put("data", qaScrpDTO);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 문장 발화 조건 항목 조회
    public Map<String, Object> getCondList() {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaCondList> result = mapper.getCondList();
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 문장 발화 조건 상세 항목 조회
    public Map<String, Object> getCondDetail(QaCond qaCond) {
        Map<String, Object> rtnMap = new HashMap<>();

        List<QaCond> result = mapper.getCondDetail(qaCond);
        if(result != null && result.size() > 0) {
            rtnMap.put("data", result);
        }else {
            rtnMap.put("data", null);
        }

        return rtnMap;
    }

    // 점검기준 상세 설정 수정
    public Map<String, Object> updArticle(LoginUser loginUser, QaScrp qaScrp) {
        Map<String, Object> rtnMap = new HashMap<>();

        qaScrp.setUpdatorId(loginUser.getUserId());
        int result = mapper.updArticle(qaScrp);

        if(result > 0) {
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }else {
            rtnMap.put("status", DataAccessStatus.FAIL);
        }

        return rtnMap;
    }

}