package maum.biz.qa.service.manage;

import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.mapper.db.manage.ConsultAreaMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.entity.QaCuslKwd;
import maum.biz.qa.model.manage.entity.QaCuslSec;
import maum.biz.qa.model.manage.entity.QaDtcDtl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;
import java.util.stream.Collectors;

/**
 * ConsultAreaService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-27  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-27
 */
@Service
public class ConsultAreaService {

    final ConsultAreaMapper mapper;

    public ConsultAreaService(ConsultAreaMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * 상담구간 목록 조회
     * @param vo
     * @return
     */
    public Map<String, Object> getQaCuslSecList(QaCuslSec vo, CommonDBCursorVO commonDBCursorVO) {
        Map<String, Object> rtnMap = new HashMap();
        int cnt = mapper.getConsultAreaMainListCNT(vo);
        rtnMap.put("length", cnt);
        if(cnt > 0){
            return Optional.ofNullable(mapper.getConsultAreaMainList(vo, commonDBCursorVO))
                    .map(v -> {
                        rtnMap.put("status", DataAccessStatus.SUCCESS);
                        rtnMap.put("data", v);
                        return rtnMap;
                    }).orElseGet(() -> {
                        rtnMap.put("status", DataAccessStatus.FAIL);
                        return rtnMap;
                    });
        }else{
            rtnMap.put("status", DataAccessStatus.NOT_FOUND);
            rtnMap.put("data", new ArrayList<>());
            return rtnMap;
        }
    }

    /**
     * 문장 목록 조회
     * @param vo
     * @return
     */
    public Map<String, Object> getQaCuslKwdList(QaCuslKwd vo) {
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.ofNullable(mapper.getConsultAreaArticleList(vo))
                .map(v -> {
                    rtnMap.put("status", v.size() > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.NOT_FOUND);
                    rtnMap.put("data", v);
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put("status", DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 기 등록 상담코드 조회 (사용 등록)
     * @return
     */
    public Map<String, Object> getNewConsultTarget() {
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.ofNullable(mapper.getNewConsultTarget())
                .map(v -> {
                    rtnMap.put("status", v.size() > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.NOT_FOUND);
                    rtnMap.put("data", v);
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put("status", DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 상담코드 사용등록
     * @param loginUser
     * @param qaCuslSec
     * @return
     */
    public Map<String, Object> setConsultSection(LoginUser loginUser, QaCuslSec qaCuslSec){
        Map<String, Object> rtnMap = new HashMap<>();
        qaCuslSec.setCreatorId(loginUser.getUserId());
        rtnMap.put("status", mapper.setConsultSection(qaCuslSec) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
        return rtnMap;
    }

    /**
     * 상담코드 정보수정
     * @param loginUser
     * @param qaCuslSec
     * @return
     */
    public Map<String, Object> updConsultSection(LoginUser loginUser, QaCuslSec qaCuslSec){
        Map<String, Object> rtnMap = new HashMap<>();
        qaCuslSec.setUpdatorId(loginUser.getUserId());
        rtnMap.put("status", mapper.updConsultSection(qaCuslSec) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
        return rtnMap;
    }

    /**
     * 상담코드 정보삭제
     * @param cuslCd
     * @param cuslAppOrd
     * @return
     */
    public Map<String, Object> delConsultSection(String cuslCd, int cuslAppOrd){
        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("status", mapper.delConsultSection(cuslCd, cuslAppOrd) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
        return rtnMap;
    }

    /**
     * 문장 목록
     * @param val
     * @param commonDBCursorVO
     * @return
     */
    public Map<String, Object> getStmts(String val, CommonDBCursorVO commonDBCursorVO) {
        Map<String, Object> rtnMap = new HashMap<>();
        int cnt = mapper.getConsultAreaStmtListCNT(val, commonDBCursorVO);
        rtnMap.put("length", cnt);
        if(cnt > 0){
            return Optional.ofNullable(mapper.getConsultAreaStmtList(val, commonDBCursorVO))
                    .map(v -> {
                        rtnMap.put("status", v.size() > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.NOT_FOUND);
                        rtnMap.put("data", v);
                        return rtnMap;
                    }).orElseGet(() -> {
                        rtnMap.put("status", DataAccessStatus.FAIL);
                        return rtnMap;
                    });
        }else{
            rtnMap.put("status", DataAccessStatus.NOT_FOUND);
            rtnMap.put("data", new ArrayList<>());
            return rtnMap;
        }
    }

    /**
     * 상담구간 문장 등록
     * @param loginUser
     * @param list
     * @param cuslCd
     * @param cuslAppOrd
     * @return
     */
    public Map<String, Object> setConsultSectionSentences(LoginUser loginUser, List<QaCuslKwd> list, String cuslCd, int cuslAppOrd) {
        Map<String, Object> rtnMap = new HashMap<>();
        List<Map<String, Object>> result = list.stream()
                .map(v -> {
                    Map<String, Object> itemMap = new HashMap();
                    v.setCreatorId(loginUser.getUserId());
                    v.setCuslCd(cuslCd);
                    v.setCuslAppOrd(cuslAppOrd);
                    if(mapper.setConsultSentence(v) > 0){
                        itemMap.put("status", DataAccessStatus.SUCCESS);
                    }else{
                        itemMap.put("status", DataAccessStatus.FAIL);
                        itemMap.put("no", v.getDtctDtcNo());
                    }
                    return itemMap;
                }).collect(Collectors.toList());
        long failCount = result.stream().filter(v -> v.get("status").equals(DataAccessStatus.FAIL)).count();
        if(result.size() == failCount){
            rtnMap.put("status", DataAccessStatus.FAIL);
        }else if(failCount > 0){
            rtnMap.put("status", DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("datas", result.stream().filter(v -> v.get("no") != null).map(v -> v.get("no")).collect(Collectors.toList()));
        }else{
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get("status"))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

    /**
     * 상담구간 문장 삭제
     * @param list
     * @param cuslCd
     * @param cuslAppOrd
     * @return
     */
    public Map<String, Object> delConsultSectionSentences(List<QaCuslKwd> list, String cuslCd, int cuslAppOrd) {
        Map<String, Object> rtnMap = new HashMap<>();
        List<Map<String, Object>> result = list.stream()
                .map(v -> {
                    Map<String, Object> itemMap = new HashMap();
                    v.setCuslCd(cuslCd);
                    v.setCuslAppOrd(cuslAppOrd);
                    if(mapper.delConsultSentence(v) > 0){
                        itemMap.put("status", DataAccessStatus.SUCCESS);
                    }else{
                        itemMap.put("status", DataAccessStatus.FAIL);
                        itemMap.put("no", v.getDtctDtcNo());
                    }
                    return itemMap;
                }).collect(Collectors.toList());
        long failCount = result.stream().filter(v -> v.get("status").equals(DataAccessStatus.FAIL)).count();
        if(result.size() == failCount){
            rtnMap.put("status", DataAccessStatus.FAIL);
        }else if(failCount > 0){
            rtnMap.put("status", DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("datas", result.stream().filter(v -> v.get("no") != null).map(v -> v.get("no")).collect(Collectors.toList()));
        }else{
            rtnMap.put("status", DataAccessStatus.SUCCESS);
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get("status"))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

    // 탐지 사전 목록 조회
    public List<QaDtcDtl> getDtcDtlInfo(String val) {
        return mapper.getDtcDtlInfo(val);
    }

    // 탐지사전 등록
    public int insertDtcDtlInfo(QaDtcDtl vo) {
        return mapper.insertDtcDtlData(vo);
    }

    // 탐지사전 삭제
    public int deleteDtcDtlInfo(QaDtcDtl vo) {
        return mapper.deleteDtcDtl(vo);
    }

}