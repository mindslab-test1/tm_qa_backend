package maum.biz.qa.service.manage;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.mapper.db.manage.PuncStcMapper;
import maum.biz.qa.model.manage.vo.PuncStcVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PuncStcService {
    private PuncStcMapper puncStcMapper;

    @Autowired
    public PuncStcService(PuncStcMapper puncStcMapper) {
        this.puncStcMapper = puncStcMapper;
    }

    // 문장발화 메인 리스트 조회
    public List<CamelListMap> getMainList(PuncStcVO puncStcVO){
        return puncStcMapper.getMainList(puncStcVO);
    }

    // 문장발화 메인리스트 전체 갯수
    public int getMainListCnt(PuncStcVO puncStcVO) { return puncStcMapper.getMainListCnt(puncStcVO); }

    // 상품구분 조회
    public List<CamelListMap> getProdCat(){
        return puncStcMapper.getProdCat();
    }

    // 조건항목 등록 저장
    public int insertCondItem(PuncStcVO puncStcVO){
        return puncStcMapper.insertCondItem(puncStcVO);
    }

    // 문장발화 설정 상품구분
    public CamelMap getEditCondItemNm(PuncStcVO puncStcVO){
        return puncStcMapper.getEditCondItemNm(puncStcVO);
    }

    // 상품코드 조회
    public List<CamelListMap> getPuncStcCode(PuncStcVO puncStcVO){
        return puncStcMapper.getPuncStcCode(puncStcVO);
    }

    // 설정 등록 삭제
    public int delPuncStcData(PuncStcVO puncStcVO){
        return puncStcMapper.delPuncStcdata(puncStcVO);
    }

    // 코드번호 최대값 가져오기
    public CamelMap getCondItmCd(PuncStcVO puncStcVO){
        return puncStcMapper.getCondItmCd(puncStcVO);
    }

    // 항목 선택 정보 출력
    public CamelMap getPuncMainList(PuncStcVO puncStcVO){
        return puncStcMapper.getPuncMainList(puncStcVO);
    }

    // 조건항목 설정 저장
    public int insertPuncStc(PuncStcVO puncStcVO){
        return puncStcMapper.insertPuncStc(puncStcVO);
    }

    // 문장발화조건 설정 생성
    public List<CamelListMap> getPuncList(PuncStcVO puncStcVO){
        return puncStcMapper.getPuncList(puncStcVO);
    }

    // 문장발화조건 설정 카테고리 비교, 조건 조회
    public List<CamelListMap> getCompareCode(){
        return puncStcMapper.getCompareCode();
    }

    // 연산자 리스트 조회
    public List<CamelListMap> getOperatorCode(){
        return puncStcMapper.getOperatorCode();
    }

    // 특약코드 리스트 조회
    public List<CamelListMap> getDamboList(PuncStcVO puncStcVO){
        return puncStcMapper.getDamboList(puncStcVO);
    }

    // 특약 코드 추가
    public int putSpecialAgCd(PuncStcVO puncStcVO){
        return puncStcMapper.putSpecialAgCd(puncStcVO);
    }

    // 조건값 리스트 조회
    public List<CamelListMap> getConValCode(){
        return puncStcMapper.getConValCode();
    }

    // 논리연산자 리스트 조회
    public List<CamelListMap> getLogicCode(){
        return puncStcMapper.getLogicCode();
    }

    // 특약코드 리스트 아이템 삭제
    public int delSpItmData(PuncStcVO puncStcVO){
        return puncStcMapper.delSpItmData(puncStcVO);
    }

    // 특약코드 리스트 아이템 등록
    public int insertSpItmData(PuncStcVO puncStcVO){
        return puncStcMapper.insertSpItmData(puncStcVO);
    }

    // 테이블 condDataTb PK값 넣기
    public int putCondTbItem(PuncStcVO puncStcVO){
        return puncStcMapper.putCondTbItem(puncStcVO);
    }
}
