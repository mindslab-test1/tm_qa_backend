package maum.biz.qa.service.manage;

import com.google.common.collect.ImmutableMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.enums.tmqa.UserManageStatus;
import maum.biz.qa.mapper.db.manage.UserMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * UserService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-10  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-10
 */
@Service
public class UserService {

	final UserMapper userMapper;
	private String status = "status";

	public UserService(UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	/**
	 * 사용자 정보 조회
	 * @param param
	 * @return
	 */
	public CamelMap getUser(Map<String, Object> param){
		return userMapper.getUser(param);
	}

	/**
	 * 사용자 목록 조회
	 * @param param
	 * @return
	 */
	public Map<String, Object> getUsers(Map<String, Object> param) {
		Map<String, Object> rtnMap = new HashMap();
		int cnt = userMapper.getUsersCNT(param);
		rtnMap.put("length", cnt);
		if(cnt > 0){
			return Optional.ofNullable(userMapper.getUsers(param))
					.map(v -> {
						rtnMap.put(status, DataAccessStatus.SUCCESS);
						rtnMap.put("data", v);
						return rtnMap;
					}).orElseGet(() -> {
						rtnMap.put(status, DataAccessStatus.FAIL);
						return rtnMap;
					});
		}else{
			rtnMap.put(status, DataAccessStatus.NOT_FOUND);
			rtnMap.put("data", new ArrayList<>());
			return rtnMap;
		}
	}

	/**
	 * 권한 목록 조회
	 * @return
	 */
	public Map<String, Object> getAuths() {
		Map<String, Object> rtnMap = new HashMap<>();
		return Optional.ofNullable(userMapper.getAuths())
				.map(v -> {
					rtnMap.put(status, v.size() > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.NOT_FOUND);
					rtnMap.put("data", v);
					return rtnMap;
				}).orElseGet(() -> {
					rtnMap.put(status, DataAccessStatus.FAIL);
					return rtnMap;
				});
	}

	/**
	 * 사용자 추가
	 * @param loginUser
	 * @param param
	 * @return
	 */
	public Map<String, Object> setUser(LoginUser loginUser, Map<String, Object> param){
		Map<String, Object> rtnMap = new HashMap<>();
		param.put("loginId", loginUser.getUserId());
		param.put("enabled", "1");
		param.put("userPw", new MessageDigestPasswordEncoder("SHA-256").encode((String)param.get("userPw")));
		if(Pattern.matches("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$", (String) param.get("userPw"))){
			rtnMap.put(status, UserManageStatus.PASSWORD_VALIDATE_FAIL);
		}else{
			if(userMapper.chkUserId(param) > 0){
				rtnMap.put(status, DataAccessStatus.VIOLATION_OF_PRIMARY_KEY);
			}else{
				rtnMap.put(status, userMapper.setUser(param) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
			}
			if(rtnMap.get(status).equals(DataAccessStatus.SUCCESS)) {
				Map<String, Object> logMap = new HashMap<>();
				logMap.put("userId", param.get("userId"));
				logMap.put("logCd", "INST");
				logMap.put("userPw", param.get("userPw"));
				logMap.put("creatorId", loginUser.getUserId());
				userMapper.setLog(logMap);
			}
		}
		return rtnMap;
	}

	/**
	 * 사용자 수정
	 * @param loginUser
	 * @param param
	 * @return
	 */
	public Map<String, Object> updUser(LoginUser loginUser, Map<String, Object> param){
		Map<String, Object> rtnMap = new HashMap<>();
		String loginUserId = null;
		if(Optional.ofNullable(loginUser).isPresent()){
			loginUserId = loginUser.getUserId();
			param.put("loginId", loginUserId);
		}
		if(Optional.ofNullable(param.get("userPw")).isPresent()){
			String newPwd = (String)param.get("userPw");
			if(Pattern.matches("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$", (String) param.get("userPw"))){
				param.put("userPw", new MessageDigestPasswordEncoder("SHA-256").encode((String)param.get("userPw")));
			}else{
				rtnMap.put(status, UserManageStatus.PASSWORD_VALIDATE_FAIL);
			}
			if(userMapper.getPasswordPrevLog(param).stream().anyMatch(v -> new MessageDigestPasswordEncoder("SHA-256").matches(newPwd, v))){
				rtnMap.put(status, UserManageStatus.RESTRICT_USE_PREV_PWD);
				return rtnMap;
			}else if(newPwd.contains((String)param.get("userId"))){
				rtnMap.put(status, UserManageStatus.USE_USER_ID);
				return rtnMap;
			}
		}
		rtnMap.put(status, userMapper.updUser(param) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
		if(rtnMap.get(status).equals(DataAccessStatus.SUCCESS) && param.get("prevAutCd") != null && !param.get("prevAutCd").equals(param.get("autCd"))) {
			Map<String, Object> logMap = new HashMap<>();
			logMap.put("userId", param.get("userId"));
			logMap.put("updBefAutCd", param.get("prevAutCd"));
			logMap.put("updAftAutCd", param.get("autCd"));
			logMap.put("logCd", "ROLE");
			logMap.put("creatorId", loginUserId);
			userMapper.setLog(logMap);
		}else if (rtnMap.get("status").equals(DataAccessStatus.SUCCESS) && Optional.ofNullable(param.get("userPw")).isPresent()){
			Map<String, Object> logMap = new HashMap<>();
			logMap.put("userId", param.get("userId"));
			logMap.put("logCd", "PWD");
			logMap.put("userPw", param.get("userPw"));
			logMap.put("creatorId", loginUserId);
			userMapper.setLog(logMap);
		}else if (rtnMap.get(status).equals(DataAccessStatus.SUCCESS) && param.get("prevAutCd") != null && !param.get("prevAutCd").equals(param.get("autCd")) && Optional.ofNullable(param.get("userPw")).isPresent()){
			Map<String, Object> logMap = new HashMap<>();
			logMap.put("userId", param.get("userId"));
			logMap.put("updBefAutCd", param.get("prevAutCd"));
			logMap.put("updAftAutCd", param.get("autCd"));
			logMap.put("logCd", "PWD");
			logMap.put("userPw", param.get("userPw"));
			logMap.put("creatorId", loginUserId);
			userMapper.setLog(logMap);
		}
		return rtnMap;
	}

	/**
	 * 사용자 삭제
	 * @param loginUser
	 * @param userId
	 * @return
	 */
	public Map<String, Object> delUser(LoginUser loginUser, String userId){
		Map<String, Object> rtnMap = new HashMap<>();
		rtnMap.put(status, userMapper.delUser(userId) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
		if(rtnMap.get(status).equals(DataAccessStatus.SUCCESS)) {
			Map<String, Object> logMap = new HashMap<>();
			logMap.put("userId", userId);
			logMap.put("logCd", "DEL");
			logMap.put("creatorId", loginUser.getUserId());
			userMapper.setLog(logMap);
		}
		return rtnMap;
	}

	/**
	 * 로그인 실패횟수 수정
	 * @param userId 회원 아이디
	 * @return result map
	 */
	public Map<String, Object> updateLoginFailCount(String userId){
		return ImmutableMap.of(status, userMapper.updateLoginFailCount(userId) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL);
	}

}
