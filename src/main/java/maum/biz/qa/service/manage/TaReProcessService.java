package maum.biz.qa.service.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.manage.TaReProcessMapper;
import maum.biz.qa.model.manage.vo.TaReProcVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaReProcessService {
    private TaReProcessMapper taReProcessMapper;

    @Autowired
    public TaReProcessService(TaReProcessMapper taReProcessMapper) {
        this.taReProcessMapper = taReProcessMapper;
    }

    // 재처리 메인 목록 조회
    public List<CamelListMap> selectTaReProcessData(TaReProcVO taReProcVO){
        return taReProcessMapper.selectTaReProcessData(taReProcVO);
    }

    // 재처리 메인 목록 갯수
    public int getTareProcessDataCnt(TaReProcVO taReProcVO){
        return taReProcessMapper.getTareProcessDataCnt(taReProcVO);
    }

    // 재처리 전체 갯수
    public int selectTaReProcessTotalCnt(TaReProcVO taReProcVO){
        return taReProcessMapper.selectTaReProcessTotalCnt(taReProcVO);
    }

    // 재처리 그룹별 건 수 조회
    public List<CamelListMap> selectTaReProcessGroupCnt(TaReProcVO taReProcVO){
        return taReProcessMapper.selectTaReProcessGroupCnt(taReProcVO);
    }

    // 재처리 상태로 변경
    public int updateReProcess(TaReProcVO taReProcVO){
        return taReProcessMapper.updateReProcess(taReProcVO);
    }

    // 재처리 상태 변경 다중 선택
    public int updateReProcessSelect(TaReProcVO taReProcVO){
        return taReProcessMapper.updateReProcessSelect(taReProcVO);
    }

    // STT 재처리 목록 조회
    public List<CamelListMap> selectSttReProcessData(TaReProcVO taReProcVO){
        return taReProcessMapper.selectSttReProcessData(taReProcVO);
    }

    // STT 재처리 요청
    public int updateSttReProcess(TaReProcVO taReProcVO){
        return taReProcessMapper.updateSttReProcess(taReProcVO);
    }
}
