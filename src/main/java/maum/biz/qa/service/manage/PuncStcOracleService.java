package maum.biz.qa.service.manage;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.manage.DamboCdOracleTmMapper;
import maum.biz.qa.model.manage.vo.PuncStcVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class PuncStcOracleService {
    private DamboCdOracleTmMapper damboCdOracleTmMapper;

    @Autowired
    public PuncStcOracleService(DamboCdOracleTmMapper damboCdOracleTmMapper) {
        this.damboCdOracleTmMapper = damboCdOracleTmMapper;
    }

    public List<CamelListMap> getDamBoList(PuncStcVO puncStcVO){
        return damboCdOracleTmMapper.getDamboList(puncStcVO);
    }
}
