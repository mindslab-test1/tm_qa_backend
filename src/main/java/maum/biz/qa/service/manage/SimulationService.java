package maum.biz.qa.service.manage;

import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
@Service
public class SimulationService {

    /**
     * 형태소 분리 NLP 연동
     * @param analStr : NLP url
     * @param urlStr : 형태소 분리할 문장
     * @return
     * nlp_sent : 형태소 분리 문장
     * hmd_sample : 탐지사전(예시)
     */
    public String callSimulationNlp(String analStr, String urlStr) {
        HttpURLConnection conn = null;
        BufferedReader br  = null;
        StringBuffer sb = new StringBuffer();

        try{
            URL iUrl = new URL(urlStr);
            conn = (HttpURLConnection) iUrl.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("content-type", "application/json");

            JSONObject reqBody = new JSONObject();
            reqBody.put("uni_id","1");
            reqBody.put("target_str", analStr);
            OutputStreamWriter outStream = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            PrintWriter writer = new PrintWriter(outStream);
            writer.write(reqBody.toString());
            writer.flush();

            br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            String jsonData = "";
            while((jsonData = br.readLine()) != null){
                sb.append(jsonData);
            }
        }catch (Exception e){
            log.error(e.getMessage());
        }finally {
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
            if(conn != null){
                conn.disconnect();
            }
        }

        return sb.toString();
    }

    /**
     * 탐지 사전 HMD 연동
     * @param jsonStr
     * @param urlStr
     * @return
     */
    public String callSimulationHmd(String jsonStr, String urlStr){
        HttpURLConnection conn = null;
        BufferedReader br  = null;
        StringBuffer sb = new StringBuffer();

        try{
            URL iUrl = new URL(urlStr);
            conn = (HttpURLConnection) iUrl.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("content-type", "application/json");

            log.info("input >>>>> " + jsonStr);

            OutputStreamWriter outStream = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            PrintWriter writer = new PrintWriter(outStream);
            writer.write(jsonStr);
            writer.flush();

            br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            String jsonData = "";
            while((jsonData = br.readLine()) != null){
                sb.append(jsonData);
            }
        } catch (MalformedURLException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }finally {
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
            if(conn != null){
                conn.disconnect();
            }
        }

        return sb.toString();
    }
}
