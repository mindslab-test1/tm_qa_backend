package maum.biz.qa.service.manage;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.mapper.db.manage.AnsManageMapper;
import maum.biz.qa.model.manage.vo.QaAnsMnVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AnsManageService {
    private AnsManageMapper ansManageMapper;

    @Autowired
    public AnsManageService(AnsManageMapper ansManageMapper) {
        this.ansManageMapper = ansManageMapper;
    }

    public List<CamelListMap> getMainList(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.getMainList(qaAnsMnVO);
    }

    public int getMainListCnt(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.getMainListCnt(qaAnsMnVO);
    }

    public String getMaxAnsCd(){
        return ansManageMapper.getMaxAnsCd();
    }

    public int insertAnsMnList(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.insertAnsMnItem(qaAnsMnVO);
    }

    public int insertAnsCtItem(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.insertAnsCtItem(qaAnsMnVO);
    }

    public CamelMap getEditAnsList(QaAnsMnVO qaAnsMnVO){
        return  ansManageMapper.getEditAnsList(qaAnsMnVO);
    }

    public int insertAnsContItem(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.insertAnsContItem(qaAnsMnVO);
    }

    public int delAnsMainList(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.delAnsMainList(qaAnsMnVO);
    }

    public int delAnsContMainList(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.delAnsContMainList(qaAnsMnVO);
    }

    public int delEditContAnsList(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.delEditContAnsList(qaAnsMnVO);
    }

    public List<CamelListMap> getContAnsList(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.getContAnsList(qaAnsMnVO);
    }

    public int editContAns(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.editContAns(qaAnsMnVO);
    }

    public int editDellContAns(QaAnsMnVO qaAnsMnVO){
        return ansManageMapper.editDellContAns(qaAnsMnVO);
    }
}
