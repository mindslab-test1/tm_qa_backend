package maum.biz.qa.service.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.manage.StandardScriptMapper;
import maum.biz.qa.model.manage.vo.StandardScriptVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StandardScriptService {
    private StandardScriptMapper mapper;

    @Autowired
    public StandardScriptService(StandardScriptMapper mapper) {
        this.mapper = mapper;
    }

    //상품코드 조회
//    public List<QaProd> getMainList(QaProd vo) { return mapper.getMainList(vo); }
    // 보험 상품 조회
    public List<CamelListMap> getProductList(StandardScriptVO vo) { return mapper.getProductList(vo); }
    // 평가항목 조회
    public List<CamelListMap> getEvaluationItemList(StandardScriptVO vo) { return mapper.getEvaluationItemList(vo); }
    // 구간 선택 조회
    public List<CamelListMap> getIntervalChoiceList(StandardScriptVO vo) { return mapper.getIntervalChoiceList(vo); }
    // 스크립트 가이드문구 목록 조회
    public List<CamelListMap> getScriptSubList(StandardScriptVO vo) { return mapper.getScriptSubList(vo); }

    // 상품 등록 수정
    public int putProductData(StandardScriptVO vo) { return mapper.insertProductData(vo); }
    // 상품 삭제
    public int delProductData(StandardScriptVO vo) { return mapper.deleteProductData(vo); }
    // 상품 평가 항복 복제
    public int copyProdChkItm(StandardScriptVO vo) { return mapper.copyQaProdChkItm(vo); }
    // 상품 상담 복제
    public int copyCuslInfo(StandardScriptVO vo) { return mapper.copyQaCuslSecInfo(vo); }
    // 상품 스크립트 복제
    public int copyScrpInfo(StandardScriptVO vo) { return mapper.copyQaScrpSecInfo(vo); }

    // 구간 조회
    public List<CamelListMap> getScrpSecInfo(StandardScriptVO vo){
        return mapper.getScrpSecInfo(vo);
    }

    // 스크립트 목록 조회
    public List<CamelListMap> getStmtData(StandardScriptVO vo){
        return mapper.getStmtData(vo);
    }

    // 상담 조회
    public List<CamelListMap> getCuslSecInfo(StandardScriptVO vo){
        return mapper.getCuslSecInfo(vo);
    }

    // 키워드 목록 조회
    public List<CamelListMap> getKeywordData(StandardScriptVO vo){
        return mapper.getKeywordData(vo);
    }

    // 기선택 점검 아이템 조회
    public List<CamelListMap> getChoosedChkItm(StandardScriptVO vo){
        return mapper.getChoosedChkItm(vo);
    }

    // 평가 항목 등록
    public int insertCheckItem(StandardScriptVO vo){
        return mapper.insertCheckItem(vo);
    }

    // 평가 항목 삭제
    public int deleteCheckItem(StandardScriptVO vo){
        return mapper.deleteCheckItem(vo);
    }

    // 상담 정보 등록
    public int insertCuslSecInfo(StandardScriptVO vo){
        return mapper.insertCuslSecInfo(vo);
    }

    // 상담 정보 삭제
    public int deleteCuslSecInfo(StandardScriptVO vo){
        return mapper.deleteCuslSecInfo(vo);
    }

    // 스크립트 정보 등록
    public int insertScrpSecInfo(StandardScriptVO vo){
        return mapper.insertScrpSecInfo(vo);
    }

    // 스크립트 정보 삭제
    public int deleteScrpSecInfo(StandardScriptVO vo){
        return mapper.deleteScrpSecInfo(vo);
    }

    // 적용 차수 리스트 조회
    public List<CamelListMap> getApplyOrdList(StandardScriptVO vo){
        return mapper.getApplyOrdList(vo);
    }
}
