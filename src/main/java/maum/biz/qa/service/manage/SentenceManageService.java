package maum.biz.qa.service.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.mapper.db.manage.SentenceManageMapper;
import maum.biz.qa.model.manage.vo.QaStcMngVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SentenceManageService {
    private SentenceManageMapper sentenceManageMapper;

    @Autowired
    public SentenceManageService(SentenceManageMapper sentenceManageMapper) {
        this.sentenceManageMapper = sentenceManageMapper;
    }

    // 문장 관리 리스트 조회
    public List<CamelListMap> getSentenceManageList(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.getSentenceManageList(qaStcMngVO);
    }

    // 문장 관리 리스트 전체 갯수
    public int getSentenceListCnt(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.getSentenceListCnt(qaStcMngVO);
    }

    // 설정팝업 리스트 조회
    public CamelMap getSentenceManageDmSetList(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.getDetailManage(qaStcMngVO);
    }

    // 설정팝업 검색 리스트
    public List<CamelListMap> sentencePopList(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.sentencePopList(qaStcMngVO);
    }

    // 탐지사전 리스트 정보 조회
    public List<CamelListMap> getDtcDtlInfo(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.getDtcDtlInfo(qaStcMngVO);
    }

    // 상세설정 팝업 상단 설정 정보 입력(가이드문자, 문장내용, 고객답변 유무)
    public int putSentenceData(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.putSentenceData(qaStcMngVO);
    }

    // 등록 팝업 가이드, 스크립트 문장 데이터 저장
    public int insertSentenceRegi(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.insertSentenceRegi(qaStcMngVO);
    }

    public List<CamelListMap> getApplyAns(){
        return sentenceManageMapper.getApplyAns();
    }

    // 등록 팝업 가이트, 스크립트 문장 데이터 삭제
    public int delSentenceData(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.delSentenceData(qaStcMngVO);
    }

    // 탐지 사전 삭제
    public int deleteDtcDtlInfo(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.deleteDtcDtlInfo(qaStcMngVO);
    }

    // 탐지 사전 등록 혹은 수정
    public int insertDtcDtlInfo(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.insertDtcDtlInfo(qaStcMngVO);
    }

    // 문장관리 삭제를 통한 탐지사전 리스트 정보 삭제
    public int delDetectionDic(QaStcMngVO qaStcMngVO){
        return sentenceManageMapper.deleteDetectionDic(qaStcMngVO);
    }

    // 탐지사전 복사
    public Map<String, Object> updDtcDtl(Map<String, Object> paramMap) {
        Map<String, Object> rtnMap = new HashMap<>();
        AtomicInteger result = new AtomicInteger();
        QaStcMngVO selectQaStcMngVO = new QaStcMngVO();
        QaStcMngVO targetQaStcMngVO = new QaStcMngVO();

        if(paramMap == null || paramMap.get("selectDtctDtcNo") == null || paramMap.get("targetDtctDtcNo") == null) {
            rtnMap.put("isSuccess", false);
            return rtnMap;
        }

        selectQaStcMngVO.setDtctDtcNo((String)paramMap.get("selectDtctDtcNo"));
        targetQaStcMngVO.setDtctDtcNo((String)paramMap.get("targetDtctDtcNo"));
        AtomicInteger maxGrpNo = new AtomicInteger(sentenceManageMapper.getMaxDtctDtcGrpNo(targetQaStcMngVO));

        // 복사할 내용 조회
        List<CamelListMap> camelMapList = sentenceManageMapper.getDtcDtlInfo(selectQaStcMngVO);

        // DB 입력
        camelMapList.forEach(v -> {
            v.put("dtct_dtc_grp_no", maxGrpNo.intValue());
            result.addAndGet(sentenceManageMapper.insertDtcDtlInfo(targetQaStcMngVO.of(v, (String)paramMap.get("targetDtctDtcNo"))));
            if(v.get("dtctDtcGrpInNo").equals(v.get("dtctDtcEdNo"))) maxGrpNo.incrementAndGet();
        });

        rtnMap.put("isSuccess", true);

        return rtnMap;
    }
}
