package maum.biz.qa.service.manage;

import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.mapper.db.manage.EvaluationMapper;
import maum.biz.qa.model.manage.vo.EvaluationVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluationService {

    private EvaluationMapper mapper;

    @Autowired
    public EvaluationService(EvaluationMapper mapper) {
        this.mapper = mapper;
    }

    //상품코드 조회
    public List<CamelListMap> getProductCode(EvaluationVO evaluationVO) {
        return mapper.getProductCodeList(evaluationVO);
    }
    //평가 카테고리 조회
    public List<CamelListMap> getChkCateCode() {
        return mapper.getChkCtgCodeList();
    }
    //상품 차수 조회
    public List<CamelListMap> getApplyOrd(EvaluationVO evaluationVO) {
        return mapper.getProdOrd(evaluationVO);
    }
    //첫 평가 차수 조회
    public List<CamelListMap> getFirstMainList(EvaluationVO evaluationVO) {
        return mapper.getFirstMainList(evaluationVO);
    }
    // 쳣 평가 차수 갯수
    public int getFirstMainListCnt(){
        return mapper.getFirstMainListCnt();
    }

    // 평가 차수 조회
    public List<CamelListMap> getMainList(EvaluationVO evaluationVO) {
        return mapper.getMainList(evaluationVO);
    }
    // 평가 차수 갯수
    public int getMainListCnt(EvaluationVO evaluationVO){
        return mapper.getMainListCnt(evaluationVO);
    }

    // 평가 상세 조회
    public List<CamelListMap> getSubList(EvaluationVO evaluationVO) {
        return mapper.getSubList(evaluationVO);
    }
    // 상품 회차 등록
    public int putApplyOrd(EvaluationVO evaluationVO) {
        return mapper.insertApplyOrd(evaluationVO);
    }
    // 상품 회차 삭제
    public int delApplyOrd(EvaluationVO evaluationVO) {
        return mapper.deleteApplyOrd(evaluationVO);
    }
    // 상품코드 등록
    public int putProductCode(EvaluationVO evaluationVO) {
        return mapper.insertProductCode(evaluationVO);
    }
    // 상품코드 삭제
    public int delProductCode(EvaluationVO evaluationVO) {
        return mapper.deleteProductCode(evaluationVO);
    }
    // 점검 아이템 등록
    public int putChkItmData(EvaluationVO evaluationVO) {
        return mapper.insertChkItmData(evaluationVO);
    }
    // 점검 아이템 삭제
    public int delChkItmData(EvaluationVO evaluationVO) {
        return mapper.deleteChkItmData(evaluationVO);
    }
    // 조건 항목 조회
    public List<CamelListMap> getCondList(EvaluationVO evaluationVO){
        return mapper.getCondList(evaluationVO);
    }

    public List<CamelListMap> getEvalCategoryList(){
        return mapper.getEvalCategoryList();
    }
}
