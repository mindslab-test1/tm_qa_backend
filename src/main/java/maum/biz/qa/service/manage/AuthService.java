package maum.biz.qa.service.manage;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.mapper.db.manage.AuthMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;
import maum.biz.qa.model.manage.vo.AccessibleMenuVO;
import maum.biz.qa.model.manage.vo.AuthVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * AuthService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-12  / 전성진	 / 최초 생성
 * </pre>
 * @since 2021-07-12
 */
@Service
@RequiredArgsConstructor
public class AuthService {

	private final AuthMapper authMapper;


	/**
	 * 권한 목록 조회
	 * @param authVO
	 * @param commonDBCursorVO
	 * @return
	 */
	public Map<Object, Object> getAuthList(AuthVO authVO, CommonDBCursorVO commonDBCursorVO) {
		int cnt = authMapper.getAuthCnt(authVO);

		return cnt > 0 ?
				ImmutableMap.builder()
						.put("cnt", cnt)
						.put("data", authMapper.getAuthList(authVO, commonDBCursorVO))
						.build()
				:
				ImmutableMap.builder()
						.put("cnt", 0)
						.put("data", new ArrayList())
						.build();
	}

	/**
	 * 권한 등록
	 * @param loginUser
	 * @param authVO
	 * @return
	 */
	public Map<Object, Object> setAuth(LoginUser loginUser, AuthVO authVO) {
		authVO.setCreatorId(loginUser.getUserId());

		return Optional.ofNullable(authMapper.getAuth(authVO))
				.map(v -> ImmutableMap.builder().put("status", DataAccessStatus.CONFLICT).build())
				.orElseGet(() -> ImmutableMap.builder()
									.put("status", authMapper.setAuth(authVO) > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL)
									.build()
				);
	}

	/**
	 * 권한 조회
	 * @param authVO
	 * @return
	 */
	public Map<Object, Object> getAuth(AuthVO authVO) {
		return Optional.of(authMapper.getAuth(authVO))
				.map(v -> ImmutableMap.builder().put("data", v).build())
				.orElseGet(() -> ImmutableMap.builder().put("data", null).build());
	}

	/**
	 * 권한 수정
	 * @param authVO
	 * @return
	 */
	public Map<Object, Object> updAuth(LoginUser loginUser, AuthVO authVO) {
		authVO.setUpdatorId(loginUser.getUserId());

		return Optional.of(authMapper.updAuth(authVO))
				.map(v -> ImmutableMap.builder().put("status", v > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL).build())
				.orElseGet(() -> ImmutableMap.builder().put("status", DataAccessStatus.FAIL).build());
	}

	/**
	 * 권한 삭제
	 * @param authVO
	 * @return
	 */
	public Map<Object, Object> delAuth(AuthVO authVO) {
		return Optional.of(authMapper.delAuth(authVO))
				.map(v -> ImmutableMap.builder().put("status", v > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL).build())
				.orElseGet(() -> ImmutableMap.builder().put("status", DataAccessStatus.FAIL).build());
	}

	/**
	 * 접근가능 메뉴 목록 조회
	 * @param accessibleMenuVO
	 * @return
	 */
	public Map<Object, Object> getAccessibleMenuList(AccessibleMenuVO accessibleMenuVO) {
		List<AccessibleMenuVO> accessibleMenuVOList = authMapper.getAccessibleMenuList(accessibleMenuVO);

		return ImmutableMap.builder()
				.put("data", accessibleMenuVOList.size() > 0 ? accessibleMenuVOList : new ArrayList<>())
				.build();
	}


	/**
	 * 메뉴 전체 목록 조회
	 * @return
	 */
	public Map<Object, Object> getMenuList() {
		List<AccessibleMenuVO> accessibleMenuVOList = authMapper.getMenuList();
		return ImmutableMap.builder()
				.put("data", accessibleMenuVOList.size() > 0 ? accessibleMenuVOList : new ArrayList<>())
				.build();
	}

	/**
	 * 접근가능 메뉴 등록
	 * @param loginUser
	 * @param list
	 * @return
	 */
	@Transactional
	public Map<Object, Object> setAccessibleMenu(LoginUser loginUser, List<AccessibleMenuVO> list) throws SQLException {
		delAccessibleMenu(list.stream().findFirst().get().getAutCd());
		Optional.ofNullable(authMapper.setAccessibleMenu(loginUser, list))
				.orElseThrow(() -> new SQLException());
		return ImmutableMap.builder().put("status", DataAccessStatus.SUCCESS).build();
	}

	/**
	 * 접근가능 메뉴 삭제
	 * @param accessibleMenuVO
	 * @return
	 */
	public Map<Object, Object> delAccessibleMenu(String auth) {
		int result = authMapper.delAccessibleMenu(auth);

		return ImmutableMap.builder()
				.put("status", result > 0 ? DataAccessStatus.SUCCESS : DataAccessStatus.FAIL)
				.build();
	}

}
