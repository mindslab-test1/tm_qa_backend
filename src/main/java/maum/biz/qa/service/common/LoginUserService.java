package maum.biz.qa.service.common;

import com.google.common.collect.ImmutableMap;
import maum.biz.qa.mapper.db.common.LoginUserMapper;
import maum.biz.qa.mapper.db.manage.UserMapper;
import maum.biz.qa.model.common.vo.LoginUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.UUID;


/**
 *
 *
 * @author
 * @version 1.0
 */
@Service
public class LoginUserService {

    @Autowired
    private LoginUserMapper loginUserMapper;

    @Autowired
    private UserMapper userMapper;

    @Value("${env.user.password-change-check-days}")
    private int days;

    public LoginUserVO getLoginUserDetail(String userId) {
        userMapper.setLog(
                ImmutableMap.of(
                        "userId", userId,
                        "logCd", "LOGIN",
                        "loginDt", new Date(),
                        "creatorId", userId
                )
        );
        return loginUserMapper.getLoginUserDetail(userId);
    }

    /**
     * 로그아웃 로깅
     * @param userId 회원 아이디
     */
    public void setLogoutLog(String userId){
        userMapper.setLog(
                ImmutableMap.of(
                        "userId", userId,
                        "logCd", "LOGOT",
                        "loginDt", new Date(),
                        "creatorId", userId
                )
        );
    }

    /**
     * 마지막 로그인 일자 갱신
     * @param userId 회원 아이디
     * @return update result
     */
    public boolean updLastLoginDate(String userId) {
        return loginUserMapper.updLastLoginDate(userId) > 0;
    }

    /**
     * 메뉴 권한체크
     * @param auth
     * @param path
     * @return
     */
    public boolean accessibleMenuCheck(String auth, String path){
        return loginUserMapper.accessibleMenuCheck(auth, path) > 0;
    }

    /**
     * 최초 로그인 검증
     * @param userId 회원 아이디
     * @return 최초 로그인 여부
     */
    public boolean checkFirstLogin(String userId){
        return loginUserMapper.checkFirstLogin(userId) > 0;
    }

    /**
     * 기간으로 비밀번호 변경 이력 확인
     * @param userId
     * @return
     */
    public boolean getPasswordPrevLogByDate(String userId){
        return userMapper.getPasswordPrevLogByDate(ImmutableMap.of("userId", userId, "days", days)) > 0;
    }

    /**
     * 토큰 파싱 UUID 조회
     * @param userId 회원 아이디
     * @return UUID
     */
    public String getTokenParseUUID(String userId){
        return loginUserMapper.getTokenParseUUID(userId);
    }

    /**
     * 토큰 파싱 UUID 발급
     * @param userId 회원 아이디
     * @return UUID
     */
    public String setTokenParseUUID(String userId){
        String uId = java.util.UUID.randomUUID().toString();
        loginUserMapper.updateTokenParseUUID(userId, uId);
        return uId;
    }

    /**
     * 로그인 실패횟수 수정
     * @param userId 회원 아이디
     * @return result record count
     */
    public boolean updateLoginFailCount(String userId){
        return loginUserMapper.updateLoginFailCount(userId) > 0;
    }

    /**
     * 로그인 실패횟수 확인 (5회 이상일 경우 false)
     * @param userId 회원 아이디
     * @return loginFailCount
     */
    public boolean checkLoginFailCount(String userId){
        return loginUserMapper.getLoginFailCount(userId) < 5;
    }

}
