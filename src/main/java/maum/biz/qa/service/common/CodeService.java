package maum.biz.qa.service.common;

import maum.biz.qa.enums.common.DataAccessStatus;
import maum.biz.qa.mapper.db.common.CodeMapper;
import maum.biz.qa.model.common.resolvers.LoginUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.*;

/**
 * maum.biz.qa.service.common
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-11  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-11
 */
@Service
public class CodeService {

    final CodeMapper codeMapper;

    public CodeService(CodeMapper codeMapper) {
        this.codeMapper = codeMapper;
    }

    /**
     * 그룹 명으로 공통코드 목록 조회
     * @param cdGp
     * @param cdNm
     * @param cd
     * @param sortingColumn
     * @param sorting
     * @return
     */
    public Map<String, Object> getCodeListByGroupCode(String cdGp, String cdNm, String cd, String sortingColumn, String sorting){
        Map<String, Object> rtnMap = new HashMap<>();
        return Optional.of(codeMapper.getCodeListByGroupCode(cdGp, cdNm, cd, sortingColumn, sorting))
                .map(v -> {
                    rtnMap.put("data", v);
                    if(v.size() > 0){
                        rtnMap.put("status", DataAccessStatus.SUCCESS);
                    }else{
                        rtnMap.put("status", DataAccessStatus.NOT_FOUND);
                    }
                    return rtnMap;
                }).orElseGet(() -> {
                    rtnMap.put("status", DataAccessStatus.FAIL);
                    return rtnMap;
                });
    }

    /**
     * 코드 등록
     * @param loginUser
     * @param param
     * @return
     */
    public Map<String, Object> setCodeDetail(LoginUser loginUser, Map<String, Object> param){
        Map<String, Object> rtnMap = new HashMap<>();
        if(codeMapper.chkCode((String)param.get("cdGp"), (String)param.get("cd")) > 0){
            rtnMap.put("status", DataAccessStatus.VIOLATION_OF_PRIMARY_KEY);
            return rtnMap;
        }else{
            return Optional.ofNullable(codeMapper.setCodeDetail(loginUser, param))
                    .map(v -> {
                        if(v > 0){
                            rtnMap.put("status", DataAccessStatus.SUCCESS);
                        }else{
                            rtnMap.put("status", DataAccessStatus.FAIL);
                        }
                        return rtnMap;
                    })
                    .orElseGet(()->{
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                        rtnMap.put("status", DataAccessStatus.FAIL);
                        return rtnMap;
                    });
        }
    }

    /**
     * 코드 수정
     * @param loginUser
     * @param param
     * @return
     */
    public Map<String, Object> updCodes(LoginUser loginUser, List<Map<String, Object>> param){
        Map<String, Object> rtnMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        param.stream().forEach(map -> {
            if(codeMapper.updCodeDetail(loginUser, map) == 0){
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("cd", map.get("cd"));
                list.add(resultMap);
            }
        });
        if(list.size() > 0){
            rtnMap.put("result", list.size() == param.size() ? DataAccessStatus.FAIL : DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("data", list);
        }else{
            rtnMap.put("result", DataAccessStatus.SUCCESS);
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get("result"))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

    /**
     * 코드 삭제
     * @param param
     * @return
     */
    public Map<String, Object> delCodes(List<String> param){
        Map<String, Object> rtnMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        param.stream().forEach(v -> {
            if(codeMapper.delCodeDetail(v) == 0){
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("cd", v);
                list.add(resultMap);
            }
        });
        if(list.size() > 0){
            rtnMap.put("result", list.size() == param.size() ? DataAccessStatus.FAIL : DataAccessStatus.PARTIAL_SUCCESS);
            rtnMap.put("data", list);
        }else{
            rtnMap.put("result", DataAccessStatus.SUCCESS);
        }
        if(!DataAccessStatus.SUCCESS.equals(rtnMap.get("result"))){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return rtnMap;
    }

}
