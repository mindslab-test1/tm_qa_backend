package maum.biz.qa.service.common;

import lombok.extern.slf4j.Slf4j;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.common.utils.CamelMap;
import maum.biz.qa.mapper.db.common.DashboardMapper;
import maum.biz.qa.model.common.vo.DashboardVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DashboardService {

    private DashboardMapper dashboardMapper;

    @Autowired
    public DashboardService(DashboardMapper dashboardMapper) {
        this.dashboardMapper = dashboardMapper;
    }

    /**
     * Cs Stt pie chart
     */
    public CamelMap getCsSttChartInfo(){
        return dashboardMapper.selectCsSttChartInfo();
    }

    /**
     * Cs Ta pie chart
     */
    public CamelMap getCsTaChartInfo(){
        return dashboardMapper.selectCsTaChartInfo();
    }

    /**
     * Tm Stt pie chart
     */
    public CamelMap getTmSttChartInfo(){
        return dashboardMapper.selectTmSttChartInfo();
    }

    /**
     * Tm Qa pie chart
     */
    public CamelMap getTmQaChartInfo(){
        return dashboardMapper.selectTmQaChartInfo();
    }

    /**
     * 센터별 ta 점수
     */
    public List<CamelListMap> getCenterTaScoreInfo(DashboardVO dashboardVO){
        return dashboardMapper.selectTaScoreBarChart(dashboardVO);
    }

    /**
     * Ctr List 별 센터 목록
     */
    public List<CamelListMap> getCtrCntrList(DashboardVO dashboardVO){
        return dashboardMapper.selectCtrCntrList(dashboardVO);
    }

    /**
     * 센터별 TMR 배정 분포
     * @param dashboardVO
     * @return
     */
    public List<CamelListMap> getTmrCntrList(DashboardVO dashboardVO){
        return dashboardMapper.selectTmrCntrList(dashboardVO);
    }

    /**
     * 월별 TA 점수 누적
     * @param dashboardVO
     * @return
     */
    public List<CamelListMap> getMonthTaScoreList(DashboardVO dashboardVO){
        return dashboardMapper.selectMonthTaScoreList(dashboardVO);
    }
}
