package maum.biz.qa.service.common;

import maum.biz.qa.mapper.db.common.AdminUserMapper;
import maum.biz.qa.model.common.entity.AdminUser;
import maum.biz.qa.model.common.entity.UserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 * 관리사용자 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Service
public class AdminUserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(AdminUserService.class);

	@Autowired
	private AdminUserMapper adminUserMapper;

	public UserPrincipal findUserByLoginId(String loginId) {
		return adminUserMapper.findUserByLoginId(loginId);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserPrincipal userPrincipal = adminUserMapper.findUserByLoginId(username);
		logger.info("loadUserByUsername=====userPrincipal======> {}",userPrincipal);
		return userPrincipal;
	}

	public String getLoginId(String decEmpno) throws Exception{
		return adminUserMapper.getLoginId(decEmpno);
	}

	public AdminUser getLoginAdminUserDetail(String userId) throws Exception {

		AdminUser adminUser = new AdminUser();

		adminUser = adminUserMapper.getLoginAdminUserDetail(userId);
		logger.info("getLoginAdminUserDetail=====adminUser======> {}",adminUser);
		return adminUser;
	}

}
