package maum.biz.qa.enums.common;

/**
 * maum.biz.qa.enums.common
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-04-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-04-23
 */
public enum DataAccessStatus {

    UNAUTHORIZED,
    SUCCESS,
    FAIL,
    PARTIAL_SUCCESS,
    NOT_FOUND,
    PARTIAL_INQUIRY,
    VIOLATION_OF_PRIMARY_KEY,
    EXCEPTION,
    CONFLICT,       // ALREADY EXIST, post 요청을 보냈지만 해당 리소스가 이미 존재할 경우

}
