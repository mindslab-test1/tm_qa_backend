package maum.biz.qa.enums.tmqa;

/**
 * maum.biz.qa.enums.tmqa
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-11  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-11
 */
public enum AuditStatus {

    QA_TYPE_NOT_FOUND,
    TEMP_SAVE_WRONG_STATE,
    AUDIT_UPDATE_FAILED,
    QA_CHECK_RESULT_UPDATE_FAILED,
    QA_CHECK_RESULT_UPDATE_PARTIAL_SUCCEEDED,
    QA_CHECK_RESULT_UPDATE_SUCCEED,

}
