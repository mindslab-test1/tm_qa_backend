package maum.biz.qa.enums.tmqa;

/**
 * maum.biz.qa.enums.tmqa
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-04-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-04-23
 */
public enum AssignStatus {

    UPDATE_DATE_FAIL,
    ASSIGN_INSERT_FAIL,

}
