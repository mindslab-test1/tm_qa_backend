package maum.biz.qa.enums.tmqa;

/**
 * maum.biz.qa.enums.tmqa
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-11-25  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-11-25
 */
public enum UserManageStatus {

    PASSWORD_VALIDATE_FAIL,
    RESTRICT_USE_PREV_PWD,
    USE_USER_ID,

}
