package maum.biz.qa.model.manage.entity;

import lombok.Data;

@Data
public class QaScrpSec {
    private String scrpLctgCd;
    private String scrpMctgCd;
    private String scrpSctgCd;
    private int scrpAppOrd;
    private String scrpLctgNm;
    private String scrpMctgNm;
    private String scrpSctgNm;
    private String scrpAppStDt;
    private String scrpAppEdDt;
    private String usedYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
