package maum.biz.qa.model.manage.entity;

import lombok.Data;

@Data
public class QaScrpSecCd {
    private String scrpLctgCd;
    private String scrpMctgCd;
    private String scrpSctgCd;
    private String scrpLctgNm;
    private String scrpMctgNm;
    private String scrpSctgNm;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
    private String isChecked;
}
