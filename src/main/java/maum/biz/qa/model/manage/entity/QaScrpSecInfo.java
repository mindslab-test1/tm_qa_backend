package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

//삭제파일

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaScrpSecInfo {
    private String prodCat;
    private String prodCd;
    private String plnCd;
    private String chkItmCd;
    private String scrpLctgCd;
    private String scrpMctgCd;
    private String scrpSctgCd;
    private int scrpAppOrd;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
