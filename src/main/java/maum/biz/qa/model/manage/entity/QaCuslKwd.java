package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaCuslKwd {
    private String cuslCd;
    private int cuslAppOrd;
    private String dtctDtcNo;
    private String cuslKwd;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
