package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaScrp;

@Getter
@Setter
public class QaScrpDTO extends QaScrp {
    private String custAnsYn;
    private String ansCd;
    private String dtctDtcYn;
    private String gudStmt;
    private String scrp;
    private String damboNm;
    private String scrpLctgNm;
    private String scrpMctgNm;
    private String scrpSctgNm;
    private String condItmNm;
    private String isChecked;
}
