package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaCond {
    private String condItmCd;
    private String prodCat;
    private int condNo;
    private String condItmNm;
    private String condTpFir;
    private String condValFir;
    private String condOpFir;
    private String condTpSe;
    private String condValSe;
    private String condOpSe;
    private String condTpThr;
    private String condValThr;
    private String logicOp;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
