package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaScrpSecInfo;

//삭제파일

@Getter
@Setter
public class QaScrpSecInfoDTO extends QaScrpSecInfo {
    private String scrpSctgNm;
    private String targetProdCat;
    private String targetProdCd;
    private String targetPlnCd;
    private String state;
}
