package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaCuslSec {
    private String cuslCd;
    private int cuslAppOrd;
    private String cuslNm;
    private String cuslAppStDt;
    private String cuslAppEdDt;
    private String usedYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
