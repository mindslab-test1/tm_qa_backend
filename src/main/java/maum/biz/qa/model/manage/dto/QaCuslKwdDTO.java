package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaCuslKwd;

@Getter
@Setter
public class QaCuslKwdDTO extends QaCuslKwd {
    private String dtctDtcYn;
    private String gudStmt;
    private String scrp;
}
