package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaCondList {
    private String condItmCd;
    private String prodCat;
    private String condItmNm;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
