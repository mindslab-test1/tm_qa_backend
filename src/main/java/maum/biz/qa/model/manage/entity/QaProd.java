package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaProd {
    private String prodCat;
    private String prodCd;
    private String plnCd;
    private int chkAppOrd;
    private String saleStDt;
    private String saleEdDt;
    private String prodNm;
    private String scprNm;
    private String usedYn;
    private String bundleYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
