package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

//삭제파일

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaProdChkItm {
    private String prodCat;
    private String prodCd;
    private String plnCd;
    private int chkAppOrd;
    private String chkItmCd;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}