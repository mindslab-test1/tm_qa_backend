package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class PuncStcVO extends CommonDBCursorVO {
    private String elagNme;     // 특약명

    private String condItmCd; // 조건항목코드
    private String prodCat; // 보종구분
    private String condItmNm;   // 조건항목명
    private int condNo;     // 조건순서
    private String condTpFir;   // 조건타입1
    private String condValFir;  // 조건값1
    private String condOpFir;   // 조건연산자1
    private String condTpSe;    // 조건타입2
    private String condValSe;   // 조건값2
    private String condOpSe;    // 조건연산자2
    private String condTpThr;   // 조건타입3
    private String condValThr;  // 조건값3
    private String logicOp;     // 논리연산자

    private String damboCd;     // 담보코드
    private String damboNm;     // 담보명

    private String updatorId;   // 수정자 아이디
    private String updatedTm;   // 수정일자
    private String creatorId;   // 생성자 아이디
    private String createdtm;   // 생성일자
}
