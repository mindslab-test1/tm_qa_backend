package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationVO  extends CommonDBCursorVO {
    private String cdGp;    // 코드그룹
    private String cd;      // 코드
    private String cdNm;    // 코드명
    private String cdDesc;  // 코드설명

    private String prodCat; // 보종구분
    private int chkAppOrd;  // 항목적용차수
    private String chkAppStDt;  // 항목적용 시작일자
    private String chkAppEdDt;  // 항목적용 종료일자
    private String usedYn;  // 사용여부

    private String chkItmCd;    // 평가항목코드
    private String chkItmNm;    // 평가항목설명
    private String chkItmDesc;  // 평가항목설명
    private int chkNo;  // 항목순서
    private String partInspYn;  // 부분검수여부
    private String autoInspYn;  // 자동검수여부
    private String chkCtgCd;    // 평가구분코드
    private String chkCtgNm;    // 평가구분명
    private String norScr;  // 정상점수
    private String supScr;  // 보완점수
    private String rtnScr;  // 반송점수
    private String dtctCtgCd;   //  탐지구분코드
    private String condItmCd;   // 조건항목코드

    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;

    private String mode;    // 첫조회 판별 or 삭제/수정 판별

    @Builder
    public EvaluationVO(int startRow, int rowsPerPage, String cdGp, String cd, String cdNm, String cdDesc,
                        String prodCat, int chkAppOrd, String chkAppStDt, String chkAppEdDt, String usedYn,
                        String chkItmCd, String chkItmNm, String chkItmDesc, int chkNo, String partInspYn,
                        String autoInspYn, String chkCtgCd, String chkCtgNm, String norScr, String supScr,
                        String rtnScr, String dtctCtgCd, String condItmCd, String updatorId, String updatedTm,
                        String creatorId, String createdTm, String mode) {
        super(startRow, rowsPerPage);
        this.cdGp = cdGp;
        this.cd = cd;
        this.cdNm = cdNm;
        this.cdDesc = cdDesc;
        this.prodCat = prodCat;
        this.chkAppOrd = chkAppOrd;
        this.chkAppStDt = chkAppStDt;
        this.chkAppEdDt = chkAppEdDt;
        this.usedYn = usedYn;
        this.chkItmCd = chkItmCd;
        this.chkItmNm = chkItmNm;
        this.chkItmDesc = chkItmDesc;
        this.chkNo = chkNo;
        this.partInspYn = partInspYn;
        this.autoInspYn = autoInspYn;
        this.chkCtgCd = chkCtgCd;
        this.chkCtgNm = chkCtgNm;
        this.norScr = norScr;
        this.supScr = supScr;
        this.rtnScr = rtnScr;
        this.dtctCtgCd = dtctCtgCd;
        this.condItmCd = condItmCd;
        this.updatorId = updatorId;
        this.updatedTm = updatedTm;
        this.creatorId = creatorId;
        this.createdTm = createdTm;
        this.mode = mode;
    }
}
