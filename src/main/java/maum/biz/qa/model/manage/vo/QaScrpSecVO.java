package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QaScrpSecVO extends CommonDBCursorVO {
    private String scrpLctgCd;  // 스크립트 대분류코드
    private String scrpMctgCd;  // 스크립트 중분류코드
    private String scrpSctgCd;  // 스크립트 소분류코드
    private String scrpLctgNm;  // 스크립트 대분류명
    private String scrpMctgNm;  // 스크립트 중분류명
    private String scrpSctgNm;  // 스크립트 소분류명

    private String scrpAppOrd;  // 스크립트 적용차수
    private String scrpAppStDt; // 스크립트 적용시작일자
    private String scrpAppEdDt; // 스크립트 적용종료일자
    private String usedYn;      // 사용여부
}
