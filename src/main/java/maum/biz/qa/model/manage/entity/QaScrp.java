package maum.biz.qa.model.manage.entity;

import lombok.Data;

@Data
public class QaScrp {
    private String scrpLctgCd;
    private String scrpMctgCd;
    private String scrpSctgCd;
    private int scrpAppOrd;
    private int stmtSeq;
    private String dtctDtcNo;
    private String damboCd;
    private String condItmCd;
    private String stScrpYn;
    private String estyScrpYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
