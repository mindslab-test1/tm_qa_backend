package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaDtcDtl;

@Getter
@Setter
public class QaDtcDtlDTO extends QaDtcDtl {
    private String state;
}
