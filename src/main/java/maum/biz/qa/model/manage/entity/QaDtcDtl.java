package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaDtcDtl {
    private String dtctDtcNo;
    private String dtctDtcGrpNo;
    private String dtctDtcGrpInNo;
    private String dtctDtcEdNo;
    private String dtctDtcCon;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
