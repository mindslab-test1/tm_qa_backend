package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaProdChkItm;

//삭제파일

@Getter
@Setter
public class QaProdChkItmDTO extends QaProdChkItm {
    private String chkItmNm;
    private String chkCtgCd;
    private String state;
    private String targetProdCat;
    private String targetProdCd;
    private String targetPlnCd;
}
