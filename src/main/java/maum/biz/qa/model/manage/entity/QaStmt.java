package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaStmt {
    private String dtctDtcNo;
    private String gudStmt;
    private String scrp;
    private String ansCd;
    private String custAnsYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
