package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaChkItm {
    private String prodCat;
    private int chkAppOrd;
    private String chkItmCd;
    private String chkItmNm;
    private String chkItmDesc;
    private int chkNo;
    private String partInspYn;
    private String usedYn;
    private String autoInspYn;
    private String chkCtgCd;
    private String chkCtgNm;
    private String norScr;
    private String supScr;
    private String rtnScr;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}