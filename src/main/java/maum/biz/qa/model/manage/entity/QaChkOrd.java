package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaChkOrd {
    private String prodCat;
    private int chkAppOrd;
    private String chkAppStDt;
    private String chkAppEdDt;
    private String usedYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
