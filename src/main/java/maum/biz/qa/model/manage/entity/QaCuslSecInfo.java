package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaCuslSecInfo {
    private String prodCat;
    private String prodCd;
    private String plnCd;
    private String chkItmCd;
    private String cuslCd;
    private int cuslAppOrd;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
