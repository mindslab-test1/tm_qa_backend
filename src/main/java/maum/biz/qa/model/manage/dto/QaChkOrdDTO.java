package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaChkOrd;

@Getter
@Setter
public class QaChkOrdDTO extends QaChkOrd {
    private String cdNm;
}
