package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccessibleMenuVO {
    private String autCd;
    private String pageCd;
    private String creatorId;
    private String createdTm;
    private String updatorId;
    private String updatedTm;
    private String pageNm;
    private String pageLoc;
}
