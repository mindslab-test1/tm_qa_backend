package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaAns {
    private String ansCd;
    private String ansNm;
    private String ansDesc;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
