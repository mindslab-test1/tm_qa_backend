package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthVO {
    private String autCd;
    private String autNm;
    private String creatorId;
    private String createdTm;
    private String updatorId;
    private String updatedTm;
}
