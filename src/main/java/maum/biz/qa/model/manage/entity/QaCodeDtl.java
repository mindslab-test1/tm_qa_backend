package maum.biz.qa.model.manage.entity;

import lombok.Data;

@Data
public class QaCodeDtl {
    private String cdGp;
    private String cd;
    private String cdNm;
    private String cdDesc;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
