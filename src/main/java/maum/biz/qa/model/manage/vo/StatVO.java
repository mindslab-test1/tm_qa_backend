package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatVO extends CommonDBCursorVO {
    private String fromDate;
    private String toDate;

    private String qaId;    // 담당 QA 사번
    private String prodNm;  // 상품명
    private String aptnCd;  // 청약상태
    private String cntrCd;  // 모집지점코드
    private String partCd;  // 실코드
    private String empNo;   // 상담원사번

}
