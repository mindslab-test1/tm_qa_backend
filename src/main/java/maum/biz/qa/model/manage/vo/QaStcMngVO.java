package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.common.utils.CamelListMap;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QaStcMngVO extends CommonDBCursorVO {
    private String dtctDtcNo;   // 탐지사전번호
    private String gudStmt;     // 가이드 문장
    private String scrp;        // 스크립트
    private String damboCd;     // 담보코드
    private String ansCd;       // 답변코드
    private String custAnsYn;   // 고객답변여부
    private String updatorId;   // 수정자아이디
    private String updatedTm;   // 수정일시
    private String creatorId;   // 생성자 아이디
    private String createdTm;   // 생성일시

    private String damboNm; // 담보명
    private String ansNm;   // 답변명

    private String dtctDtcGrpNo;    // 탐지사전그룹번호
    private String dtctDtcGrpInNo;  // 탐지사전그룹내순서
    private String dtctDtcEdNo;     // 탐지사전끝번호
    private String dtctDtcCon;      // 탐지사전내용

    QaStcMngVO(String dtctDtcNo) {
        this.dtctDtcNo = dtctDtcNo;
    }

    public static QaStcMngVO of(CamelListMap camelListMap, String dtctDtcNo){
        return QaStcMngVO.builder()
                .dtctDtcNo(dtctDtcNo)
                .dtctDtcGrpNo((String)camelListMap.get("dtctDtcGrpNo"))
                .dtctDtcGrpInNo((String)camelListMap.get("dtctDtcGrpInNo"))
                .dtctDtcEdNo((String)camelListMap.get("dtctDtcEdNo"))
                .dtctDtcCon((String)camelListMap.get("dtctDtcCon"))
                .creatorId((String)camelListMap.get("creatorId"))
                .build();
    }

}
