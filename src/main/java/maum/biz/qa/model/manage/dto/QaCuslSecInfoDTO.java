package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaCuslSecInfo;

@Getter
@Setter
public class QaCuslSecInfoDTO extends QaCuslSecInfo {
    private String cuslNm;
    private String targetProdCat;
    private String targetProdCd;
    private String targetPlnCd;
    private String state;
}
