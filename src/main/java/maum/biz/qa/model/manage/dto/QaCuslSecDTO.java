package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaCuslSec;

@Getter
@Setter
public class QaCuslSecDTO extends QaCuslSec {
    private String mode;
}
