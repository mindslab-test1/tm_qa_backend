package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QaAnsMnVO extends CommonDBCursorVO {
    private String ansCd;
    private String ansNm;
    private String ansDesc;
    private int ansSeq;
    private int ansSeqSub;
    private int iner;
    private String ansCont;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
}
