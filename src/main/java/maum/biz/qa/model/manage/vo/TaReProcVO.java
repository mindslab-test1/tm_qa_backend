package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaReProcVO extends CommonDBCursorVO {
    private String nsplNo;      // 증권번호
    private String taTmsInfo;   // TA 회차정보
    private String recKey;      // 녹취키
    private String recId;       // 녹취 아이디
    private String recFileNm;   // 녹취파일명

    private String fromDate;    // QA 등록 시작일자
    private String toDate;      // QA 등록 종료일자

    private String creatorId;

}
