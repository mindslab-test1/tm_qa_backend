package maum.biz.qa.model.manage.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class StandardScriptVO extends CommonDBCursorVO {
    private String prodCat;
    private String prodCd;
    private String plnCd;
    private int chkAppOrd;
    private String saleStDt;
    private String saleEdDt;
    private String prodNm;
    private String scprNm;
    private String usedYn;
    private String bundleYn;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;


    //평가항목 추가
    private String chkItmCd;

    //구간선택 추가
    private String scrpLctgCd;
    private String scrpMctgCd;
    private String scrpSctgCd;
    private int scrpAppOrd;


    //스크립트 가이드문구
    private int stmtSeq;
    private String dtctDtcNo;
    private String damboCd;
    private String condItmCd;
    private String stScrpYn;
    private String estyScrpYn;

    private String gudStmt;
    private String scrp;
    private String ansCd;
    private String custAnsYn;

    //상품 등록,수정,삭제 구분값
    private String mode;

    //상품 복사
    private String chkItmNm;
    private String chkCtgCd;
    private String state;
    private String targetProdCat;
    private String targetProdCd;
    private String targetPlnCd;
    private String cuslNm;
    private String cuslCd;
    private int cuslAppOrd;
    private String scrpSctgNm;


    private String cd;
    private String cdNm;

    @Builder
    public StandardScriptVO(int startRow, int rowsPerPage, String prodCat, String prodCd, String plnCd,
                            int chkAppOrd, String saleStDt, String saleEdDt, String prodNm, String scprNm,
                            String usedYn, String bundleYn, String updatorId, String updatedTm, String creatorId,
                            String createdTm, String chkItmCd, String scrpLctgCd, String scrpMctgCd,
                            String scrpSctgCd, int scrpAppOrd, int stmtSeq, String dtctDtcNo, String damboCd,
                            String condItmCd, String stScrpYn, String estyScrpYn, String gudStmt, String scrp,
                            String ansCd, String custAnsYn, String mode, String chkItmNm, String chkCtgCd,
                            String state, String targetProdCat, String targetProdCd, String targetPlnCd,
                            String cuslNm, String cuslCd, int cuslAppOrd, String scrpSctgNm, String cd,
                            String cdNm) {
        super(startRow, rowsPerPage);
        this.prodCat = prodCat;
        this.prodCd = prodCd;
        this.plnCd = plnCd;
        this.chkAppOrd = chkAppOrd;
        this.saleStDt = saleStDt;
        this.saleEdDt = saleEdDt;
        this.prodNm = prodNm;
        this.scprNm = scprNm;
        this.usedYn = usedYn;
        this.bundleYn = bundleYn;
        this.updatorId = updatorId;
        this.updatedTm = updatedTm;
        this.creatorId = creatorId;
        this.createdTm = createdTm;
        this.chkItmCd = chkItmCd;
        this.scrpLctgCd = scrpLctgCd;
        this.scrpMctgCd = scrpMctgCd;
        this.scrpSctgCd = scrpSctgCd;
        this.scrpAppOrd = scrpAppOrd;
        this.stmtSeq = stmtSeq;
        this.dtctDtcNo = dtctDtcNo;
        this.damboCd = damboCd;
        this.condItmCd = condItmCd;
        this.stScrpYn = stScrpYn;
        this.estyScrpYn = estyScrpYn;
        this.gudStmt = gudStmt;
        this.scrp = scrp;
        this.ansCd = ansCd;
        this.custAnsYn = custAnsYn;
        this.mode = mode;
        this.chkItmNm = chkItmNm;
        this.chkCtgCd = chkCtgCd;
        this.state = state;
        this.targetProdCat = targetProdCat;
        this.targetProdCd = targetProdCd;
        this.targetPlnCd = targetPlnCd;
        this.cuslNm = cuslNm;
        this.cuslCd = cuslCd;
        this.cuslAppOrd = cuslAppOrd;
        this.scrpSctgNm = scrpSctgNm;
        this.cd = cd;
        this.cdNm = cdNm;
    }
}
