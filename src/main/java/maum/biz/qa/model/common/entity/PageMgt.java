package maum.biz.qa.model.common.entity;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PageMgt extends CommonVo {
	// DB Table Mapping: QA_PAGE_MGT_TB Entity

	private String pageCd;	// 페이지코드
	private String pageNm;	// 페이지명
	private String pageLoc;	// 페이지위치
}