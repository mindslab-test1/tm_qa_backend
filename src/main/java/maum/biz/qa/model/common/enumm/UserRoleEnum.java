package maum.biz.qa.model.common.enumm;

public enum UserRoleEnum {
    ROLE_ADMIN,
    ROLE_QA_REVIEWER,
    ROLE_QA_ADMIN,
    ROLE_UW_REVIEWER,
    ROLE_UW_ADMIN,
    ROLE_TM_CENTER,
}
