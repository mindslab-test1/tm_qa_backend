package maum.biz.qa.model.common.form;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AuthorityForm extends CommonForm {

	private String autCd;	// 권한코드
	private String autNm;	// 권한명
	private String pageCd;	// 페이지코드
	private String pageNm;	// 페이지명
	private String pageLoc;	// 페이지 uri
	private String funcCd;	// 기능코드
	private String funcNm;	// 기능명

	private String originPageCd;	// 변경전 pageCd
	private String changedPageCd;	// 변경된 pageCd
	private String originFuncCd;	// 변경전 funcCd
	private String changedFuncCd;	// 변경된 funcCd


	
	// TODO: 삭제필요
	private String btnCd;	// 버튼코드
	private String btnNm;	// 버튼명


}
