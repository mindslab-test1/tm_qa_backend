package maum.biz.qa.model.common.entity;

import lombok.Data;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 데이터베이스에서 사용하는 공통 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
public class CommonVo {
	private Integer num;
	private String updatorId = SecurityContextHolder.getContext().getAuthentication().getName();
	private String updatedTm;
	private String creatorId = SecurityContextHolder.getContext().getAuthentication().getName();
	private String createdTm;
}
