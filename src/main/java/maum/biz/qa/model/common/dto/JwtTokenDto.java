package maum.biz.qa.model.common.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
public class JwtTokenDto {

    private String accessToken;
    private String tokenType = "Bearer";
    private Collection<? extends GrantedAuthority> roles;

    public JwtTokenDto(String accessToken, Collection<? extends GrantedAuthority> roles)
    {
        this.accessToken = accessToken;
        this.roles = roles;
    }
}
