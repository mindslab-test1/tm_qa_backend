package maum.biz.qa.model.common.resolvers;

import lombok.*;
import maum.biz.qa.model.common.enumm.UserRoleEnum;
import maum.biz.qa.model.common.vo.LoginUserVO;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginUser {

    private String userId;
    private String userNm;
    private UserRoleEnum userRole;
    private String accessToken;
    private long expiredTime;
    private String tokenParseUuid;
    private String lastLoginDate;

    public static LoginUser of(LoginUserVO loginUserVO) {
        return LoginUser.builder()
                .userId(loginUserVO.getUserId())
                .userNm(loginUserVO.getUserNm())
                .userRole(UserRoleEnum.valueOf(loginUserVO.getAutCd()))
                .lastLoginDate(loginUserVO.getLastLoginDate())
                .build();
    }
}
