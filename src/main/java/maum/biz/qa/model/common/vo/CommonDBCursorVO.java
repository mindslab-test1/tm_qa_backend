package maum.biz.qa.model.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonDBCursorVO implements Serializable {

  private static final long serialVersionUID = -2724417989225482013L;

  private int startRow;

  @ApiModelProperty(hidden = true)
  private int endRow;

  private String sorting;

  private String sortingColumn;

  @ApiModelProperty(hidden = true)
  private String queryType;

  private int rowsPerPage;

  @ApiModelProperty(hidden = true)
  private int draw;

  @ApiModelProperty(hidden = true)
  private int start;

  @ApiModelProperty(hidden = true)
  private int length;

  @ApiModelProperty(hidden = true)
  private Map<SearchCriterias, String> search;

  public enum SearchCriterias {
    value,
    regex
  }
  private List<Map<OrderCriterias, String>> order;
  public enum OrderCriterias {
    column,
    dir
  }
  private List<Map<ColumnCriterias, String>> columns;
  public enum ColumnCriterias {
    data,
    name,
    searchable,
    orderable,
    searchValue,
    searchRegex
  }

  public CommonDBCursorVO(int startRow, int rowsPerPage) {
    this.startRow = startRow;
    this.sortingColumn = sortingColumn;
    this.rowsPerPage = rowsPerPage;
  }
}
