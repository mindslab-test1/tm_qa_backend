package maum.biz.qa.model.common.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.entity.Role;


@Getter
@Setter
public class AuthorityDto extends Role {

//	private String result;
	private String pageCd;
	private String pageNm;
	private String pageLoc;
	private String funcCd;
	private String funcNm;

	// TODO: 아래 변수 삭제필요
	private String btnCd;
	private String btnNm;
	private String btnLoc;
}
