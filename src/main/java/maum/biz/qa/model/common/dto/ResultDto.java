package maum.biz.qa.model.common.dto;

import lombok.Data;

/**
 * 결과 값을 리턴하기 위한 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */
@Data
public class ResultDto {

	private String state = "FAIL";	
	private String msg	= "";
	private String menuName	= "";
	private String viewType	= "";
	private String subMenuName	= "";
	private String selectedStr	= "";
	private int selectedInt;
	private Object data = "";

}
