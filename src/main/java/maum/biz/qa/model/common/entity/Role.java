package maum.biz.qa.model.common.entity;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Role extends CommonVo {
	// DB Table Mapping: QA_AUT_MGT_TB Entity

	private String autCd;	// 권한코드
	private String autNm;	// 권한명
}