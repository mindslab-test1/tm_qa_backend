package maum.biz.qa.model.common.vo;

import lombok.*;
import maum.biz.qa.model.common.entity.UserPrincipal;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginUserVO {
    private String userId;
    private String userNm;
    private String empNo;
    private String autCd;
    private Integer enabled;
    private String authority;
    private String tokenParseUuid;
    private String lastLoginDate;

    public static LoginUserVO of(UserPrincipal userPrincipal){
        return LoginUserVO.builder()
                .userId(userPrincipal.getId())
                .userNm(userPrincipal.getUsername())
                .build();
    }

}
