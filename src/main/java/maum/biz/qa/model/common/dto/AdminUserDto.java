package maum.biz.qa.model.common.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.entity.AdminUser;


@Getter
@Setter
public class AdminUserDto extends AdminUser {

//	private String result;
	private String autNm;	// 권한명

}
