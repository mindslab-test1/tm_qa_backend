package maum.biz.qa.model.common.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter
@NoArgsConstructor
public class CommonPageVO {
  @JsonIgnore
  private int pageIndex;
  @JsonIgnore
  private int pageLimit;
  @JsonIgnore
  private int startRow;
  @JsonIgnore
  private int endRow;
  @JsonIgnore
  private String sorting;
  @JsonIgnore
  private String sortingColumn;

  private int draw;
  private int recordsTotal;
  private int recordsFiltered;

  private List<?> data;

  // startRow, endRow를 계산한다.
  public void handlePaging(int page, int limit, String sortingColumn ,String sorting) {
    this.pageIndex = page < 1 ? 1 : page;
    this.pageLimit = limit;
    this.startRow = ((page - 1) * limit + 1);
    this.endRow = ((page - 1) * limit) + limit;
    this.sorting = sorting;
    this.sortingColumn = sortingColumn;
  }

  // 페이징 처리된 결과값을 담는 메서드
  public void handlePagingList(List<?> list) {
    this.data = list;
  }
}
