package maum.biz.qa.model.common.resolvers;

import maum.biz.qa.common.config.JwtTokenProvider;
import maum.biz.qa.service.common.LoginUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Date;
import java.util.Optional;

@Component
public class LoginUserParamResolver implements HandlerMethodArgumentResolver {

    @Autowired
    private JwtTokenProvider tokenProvider;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private LoginUserService loginUserService;

    @Value("${spring.jwt.accessExpiredTime}")
    private long accessExpiredTime;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(LoginUserParam.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        String token = webRequest.getHeader("Authorization");

        if(token == null) {
            return null;
        }else {
            token = token.startsWith("Bearer ") ? token.substring(7) : token;
            if(!tokenProvider.validateToken(token)) {
                return null;
            }else {
                UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                String uuid = loginUserService.getTokenParseUUID(userDetails.getUsername());
                String tokenUuid = tokenProvider.getTokenParseUuid(token);
                if(Optional.ofNullable(tokenUuid).isPresent() && tokenUuid.equals(uuid)){
                    String key = "loginUser:" + userDetails.getUsername();
                    Optional<LoginUser> opt = Optional.ofNullable((LoginUser)redisTemplate.opsForValue().get(key));
                    return opt.orElseGet(() -> {
                        LoginUser loginUser = LoginUser.of(loginUserService.getLoginUserDetail(userDetails.getUsername()));
                        redisTemplate.opsForValue().set(key, loginUser);
                        redisTemplate.expireAt(key, new Date(new Date().getTime() + accessExpiredTime));
                        return loginUser;
                    });
                }else{
                    return null;
                }
            }
        }
    }
}
