package maum.biz.qa.model.common.entity;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class FuncAuthority extends CommonVo {
	// DB Table Mapping: QA_FUNC_MGT_TB Entity

	private String pageCd;	// 페이지코드
	private String funcCd;	// 기능코드
	private String funcNm;	// 기능명

}