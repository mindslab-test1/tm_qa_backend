package maum.biz.qa.model.common.dto;

import lombok.Data;

@Data
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class PagingResultDto {

    /* Data Object */
    Object data;    // main data

    // Pagination variable
    private Integer draw;   // current page number
    private Integer recordsFiltered;    // total data count (필터링 전 총 레코드 - DB 총 레코드 수)
    private Integer recordsTotal;   // total data count (필터링 후 총 레코드 수 - 필터링이 적용된 총 레코드 수)





}
