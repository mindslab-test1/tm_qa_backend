package maum.biz.qa.model.common.entity;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AdminUser extends CommonVo {
	// DB Table Mapping: QA_USR_MGT_TB Entity

	private String userId;	// 사용자ID
	private String userNm;	// 사용자명
	private String userPw;	// 패스워드
	private String empNo;	// 사번
	private String autCd;	// 권한코드
	private Integer enabled;	// 활성화
	private String authority;	// 권한

//	private String initialPage;	// 초기 화면
//	private String logNo;	// 로그pk

}