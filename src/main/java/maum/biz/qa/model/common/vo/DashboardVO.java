package maum.biz.qa.model.common.vo;

import lombok.Data;

@Data
public class DashboardVO {

    private String regDy;   // 등록일
    private String fromDate;    // 시작 등록일
    private String toDate;  // 끝 등록일

    private String cntrCd;
    private String cntrNmSrch;
    private String qaFmDt;
    private String qaEdDt;
    private String partSrch;
    private String saesEmnoSrch;
    private String saesType;
    private String saesEmnmSrch;
    private String csmrAdmn;
    private String pnm;
    private String targetYnSrch;
    private String prodSndYnSrch;
    private String qaUserNm;
    private String qaUserId;
    private String evltRsltStat;

}
