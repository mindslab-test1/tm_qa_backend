package maum.biz.qa.model.common.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Map;

/**
 * 화면에서 사용하는 공통 Form 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class CommonForm {

	// Pagination variable
	private Integer draw;	// current page number
	private Integer start;	// page offset
	private Integer length;		// page limit
	private Map<SearchCriterias, String> search;
	public enum SearchCriterias {
		value,
		regex
	}
	private List<Map<OrderCriterias, String>> order;
	public enum OrderCriterias {
		column,
		dir
	}
	private List<Map<ColumnCriterias, String>> columns;
	public enum ColumnCriterias {
		data,
		name,
		searchable,
		orderable,
		searchValue,
		searchRegex
	}

	private String viewType;

	private String updatorId = SecurityContextHolder.getContext().getAuthentication().getName();
	private String updatedTm;
	private String creatorId = SecurityContextHolder.getContext().getAuthentication().getName();
	private String createdTm;
}
