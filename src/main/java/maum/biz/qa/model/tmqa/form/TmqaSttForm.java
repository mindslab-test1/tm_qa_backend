package maum.biz.qa.model.tmqa.form;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

/**
 * 화면 TmqaSttForm 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class TmqaSttForm extends CommonDBCursorVO {
	private String fromDate;
	private String toDate;
	private String wrapId;  			// 청약서ID
	private String tmsInfo;  			// 회차정보
	private String cntrNmSrch;  		// 센터
	private String saesType;  		    // 상담원
	private String saesEmnmSrch;		// 상담원 이름
	private String saesEmnoSrch;  		// 사번
	private String targetYnSrch;    	// 대상
	private String targetContSrch;    	// 대상내용
	private String taScoreSrch1;    	// TA점수1
	private String taScoreSrch2;    	// TA점수2
	private String nsplNoSrch;    		// 증권번호
	private String prodNmeSrch;    		// 상품명
	private String bundleYnSrch;    	// 번들유무
	private String aptnStatSrch;    	// 청약상태
	private String prodSndYnSrch;    	// 상품발송안내

	private String qaFmDt;
	private String qaEdDt;
	private String partSrch;
	private String csmrAdmn;
	private String pnm;
	private String qaUserNm;
	private String qaUserId;
	private String evltRsltStat;
	private String queryType;

	private String nsplNo;
	private String qaTmsInfo;
}
