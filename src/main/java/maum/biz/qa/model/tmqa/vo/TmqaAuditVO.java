package maum.biz.qa.model.tmqa.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * maum.biz.qa.model.tmqa.vo
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-05-06  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-05-06
 */
@Getter
@Setter
public class TmqaAuditVO {

    // MASTER
    private String nsplNo; // 증권번호
    private String regDy; // 청약일자
    private String prodNme; // 상품명
    private String csmrAdmn; // 계약자명
    private String pnm; // 피보험자명
    private String cntrNm; // 센터명
    private String prtNme; // 실명
    private String saesEmnm; // 이름
    private String cmpgnNm; // 캠페인명
    private int insFee; // 보험료

    // 녹취목록
    private String calType; // 상담유형
    private String callDt; // 상담일시
    private String recDuTm; // 통화시간
    private String recKey; // 콜키
    private String progStatCdNm; // STT상태
    private String recFileNm;

    // 회차목록
    private String tmsInfo; // 회차
    private String auditDt; // 심사일시
    private String qaUserNm; // 심사자
    private String totScr; // 점수
    private String ctrCalTotTm; // 녹취시간
    private String qaProcTm; // 심사시간
    private String evltRsltStatNm; // QA결과
    private String evltRsltStat; // QA결과
    private String splmCtnt; // 심사의견
    private String tmpSavYn; // 임시저장여부

    // 평가항목
    private String prodCat; // 보종구분
    private String chkAppOrd; // 항목적용차수
    private String chkItmCd; // 평가항목코드
    private String psnJugCat; // 인심사구분
    private String chkCtgCd; // 평가구분코드
    private String jugRst; // 심사결과
    private String scrRst; // 점수결과

    private String updatorId; // 수정자
    private String updatedTm; // 수정시간

    private String auditTimeCounter; // 심사시간 누적값

    private int qaCnslScr;
    private int qaScrpScr;

    private List<TmqaAuditVO> evaluationItems; // 평가항목목록

}
