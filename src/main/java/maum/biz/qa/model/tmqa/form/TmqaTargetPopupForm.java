package maum.biz.qa.model.tmqa.form;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;


@Getter
@Setter
public class TmqaTargetPopupForm extends CommonDBCursorVO {
    // 대상설정 팝업: TMR 지정
    private String cntrCd;
    private String partCd;
    private String empNo;
    private String empNm;
    private String cntrNm;
    private String partNm;
    private String creatorId;

    // 대상설정 팝업: TA 점수
    private String upperTa;
    private String lowerTa;

    // 대상설정 팝업: TA 부분/미탐지 구간
    private String chkCtgCd;
    private String chkItmCd;
    private String chkCtgNm;
    private String chkItmNm;

    // 대상설정 팝업: 상품명
    private String prodCat;
    private String prodCd;

    // 대상설정 팝업: 건수
    private String tmrCnt;
    private String scrCnt;
    private String dtctCnt;
    private String prodCnt;
}
