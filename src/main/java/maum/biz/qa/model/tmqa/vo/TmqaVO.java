package maum.biz.qa.model.tmqa.vo;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

@Getter
@Setter
public class TmqaVO extends CommonDBCursorVO {
    private String fromDate;
    private String toDate;
    private String wrapId;  			// 청약서ID
    private String tmsInfo;  			// 회차정보
    private String cntrNmSrch;  		// 센터
    private String saesType;  		    // 상담원
    private String saesEmnoSrch;  		// 사번
    private String targetYnSrch;    	// 대상
    private String targetContSrch;    	// 대상내용
    private String taScoreSrch1;    	// TA점수1
    private String taScoreSrch2;    	// TA점수2
    private String nsplNoSrch;    		// 증권번호
    private String prodNmeSrch;    		// 상품명
    private String bundleYnSrch;    	// 번들유무
    private String aptnStatSrch;    	// 청약상태
    private String prodSndYnSrch;    	// 상품발송안내

    private String nsplNo;
    private String resultNm;
    private String tpNm;
    private String prgstNm;
    private String regDy;
    private String improveYn;

    private String qaLimitDt;

    private String userId;
    private String userNm;
    private String ntcmDy;
    private String extrDy;
    private String userAutrGrd;
    private String updatorId;
    private String updatedTm;
    private String creatorId;
    private String createdTm;
    private String offMgt;

    private String annoYn;
    private String aptnStat;
    private String bundleCnt;
    private String bundleYn;
    private String cnslSpkSpd;
    private String cntrNm;
    private String csmrAdmn;
    private String csmrAdmnNo;
    private String ctrCalTotTm;
    private String ctrInsAg;
    private String evltRsltStat;
    private String impItmSpkSpd;
    private String insProdCd;
    private String insProdCtgy;
    private String overSxtfvYn;
    private String pbInsAg;
    private String pnm;
    private String prodNme;
    private String prtNme;
    private String qaAllcDtme;
    private String qaEdTm;
    private String qaNameId;
    private String qaReqTm;
    private String qaScr;
    private String qaTmsInfo;
    private String qaTrgConCd;
    private String qaTrgTransTm;
    private String qaUserNm;
    private String saesCntrCd;
    private String saesEmnm;
    private String saesEmno;
    private String saesPrtCd;
    private String scrpSpkSpd;
    private String smpRecYn;
    private String taScr;
    private String taTmsInfo;
    private String targetYn;
    private String tmpSavYn;
    private String ucid;

    private String partSrch;
    private String saesEmnmSrch;

    private String saesNmSrch;

    private String qaFmDt;
    private String qaEdDt;
    private String qaUserId;
    private String queryType;

    private String userRole;
}
