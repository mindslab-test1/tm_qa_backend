package maum.biz.qa.model.tmqa.form;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

/**
 * 화면 TmqaSttPopupForm 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class TmqaSttPopupForm extends CommonDBCursorVO {
	private String wrapId;  			// 청약서ID
	private String nsplNo;	    		// 증권번호
	private String recKey;	    		// 녹취키
	private String taTmsInfo;  			// TA 회차정보
	private String qaTmsInfo;  			// QA 회차정보
	private String prodCat;  			// 보종구분
	private String chkAppOrd;  			// 항목적용차수
	private String chkItmCd;  			// 평가항목코드
	private String chkCtgCd;  			// 평가항목구분코드 스크립트/상담(S/C)
	private String ucid;   			    // 콜키
	private String stmtNo;				// 문장 번호
	private String sttText;				// 원문장
	private String updateStmt;			// 수정 문장
	private String updatorId;			// 수정자 아이디
}