package maum.biz.qa.model.cs.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 화면 CsSttPopupForm 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class CsSttPopupForm {
	private String csPk;  			// 고객번호
	private String recgIdCtt;  		// 녹취번호
	private String recId;  			// 녹취아이디
	private String recFileNm;  		// 녹취파일명
	private String updateStmt;  	// 수정문장
	private String updateStmtYn;  	// 수정문장여부
	private String stmtNo;  		// 문장번호
}
