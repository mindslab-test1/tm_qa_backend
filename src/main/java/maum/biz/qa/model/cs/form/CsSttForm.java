package maum.biz.qa.model.cs.form;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

/**
 * 화면 CsSttForm 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class CsSttForm extends CommonDBCursorVO {
	private String fromDate;
	private String toDate;
	private String csTlnoCttSrch;  		// 고객 전화번호
	private String csPkSrch;  			// 고객번호
	private String insLonCtrNumSrch;  	// 증권번호
	private String cnslEmpnoSrch;    	// 사번
	private String maxSilenceSrch;    	// 무음길이
}
