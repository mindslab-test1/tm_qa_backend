package maum.biz.qa.model.tm.form;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.vo.CommonDBCursorVO;

/**
 * 화면 TmSttForm 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class TmSttForm extends CommonDBCursorVO {
	private String fromDate;
	private String toDate;
	private String cnslId;  			// 상담ID
	private String cntrCdSrch;  		// 센터코드
	private String cntrNmSrch;  		// 센터이름
	private String prtCdSrch;  			// 실코드
	private String prtNmSrch;  			// 실이름
	private String saesTypeSrch;  		// 상담원 유형코드
	private String saesTypeNmSrch;  	// 상담원명
	private String culeIdSrch;    		// 사번
	private String csNmSrch;    		// 고객명
	private String csPkSrch;    		// 고객번호
	private String progStatCdSrch;    	// stt상태
	private String ucidSrch;    		// 콜키
}
