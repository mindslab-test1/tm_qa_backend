package maum.biz.qa.model.tm.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 화면 TmSttPopupForm 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class TmSttPopupForm {
	private String cnslId;  		// 상담ID
	private String ucid;    		// 콜키
	private String recId;  			// 녹취아이디
	private String recFileNm;  		// 녹취파일명
	private String updateStmt;  	// 수정문장
	private String updateStmtYn;  	// 수정문장여부
	private String stmtNo;  		// 문장번호
}
