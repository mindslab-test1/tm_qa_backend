package maum.biz.qa.eai.common.enums;

/**
 * maum.biz.qa.eai.common.enums
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
public enum Eai {

    GET_AUDIT_QUESTION("XXBSMANSJJ001"),
    GET_NSPL_INFO("XXBSMANSJJ002"),
    GET_USER_INFO("XXBSMANSJJ003");

    private final String id;

    Eai(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
