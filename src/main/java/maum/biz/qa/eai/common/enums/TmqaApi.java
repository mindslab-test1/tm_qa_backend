package maum.biz.qa.eai.common.enums;

/**
 * maum.biz.qa.eai.common.enums
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
public enum TmqaApi {

    AUDIT_STATISTICS("/api/eai/statistics/audit");

    private final String path;

    TmqaApi(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}
