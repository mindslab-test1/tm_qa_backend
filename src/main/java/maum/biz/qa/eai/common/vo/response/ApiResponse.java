package maum.biz.qa.eai.common.vo.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * maum.biz.qa.eai.common.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse {

    private boolean success = true;
    private String message = "성공";

}
