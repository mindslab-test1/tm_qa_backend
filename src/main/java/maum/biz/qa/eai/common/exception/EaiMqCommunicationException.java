package maum.biz.qa.eai.common.exception;

import maum.biz.qa.eai.common.enums.Eai;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * maum.biz.qa.eai.common.exception
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
@Getter
public class EaiMqCommunicationException extends Exception implements Serializable {

    private static final long serialVersionUID = -6200144591966883826L;
    private final Eai eai;
    private final HttpStatus httpStatus;
    private final String errorCode;
    private final String errorMessage;



    public EaiMqCommunicationException(Eai eai, HttpStatus httpStatus, String errorCode, String errorMessage) {
        super(httpStatus.getReasonPhrase());
        this.eai = eai;
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

}
