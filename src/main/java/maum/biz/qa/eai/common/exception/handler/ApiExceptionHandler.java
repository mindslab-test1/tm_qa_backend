package maum.biz.qa.eai.common.exception.handler;

import maum.biz.qa.eai.common.exception.EaiMqCommunicationException;
import maum.biz.qa.eai.common.exception.TmqaApiCommunicationException;
import maum.biz.qa.eai.common.vo.response.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * maum.biz.qa.eai.common.exception.handler
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { EaiMqCommunicationException.class })
    protected ResponseEntity<Object> handleConflict(EaiMqCommunicationException ex, WebRequest request) {
        HttpStatus statusCode = ex.getHttpStatus();
        ErrorResponse response = ErrorResponse.builder()
                .requestApi(ex.getEai().name()) // FIXME name or 명세로 변경 필요
                .httpStatus(ex.getHttpStatus().value())
                .errorCode(ex.getErrorCode())
                .errorMessage(ex.getErrorMessage()).build();
        response.setSuccess(false);
        response.setMessage(String.format("EAI 인터페이스 - %s 통신 중 오류(%s:%s)가 발생했습니다.", ex.getEai().name(), statusCode.value(), statusCode.name()));
        return handleExceptionInternal(ex, response, new HttpHeaders(), ex.getHttpStatus(), request);
    }

    @ExceptionHandler(value = { TmqaApiCommunicationException.class })
    protected ResponseEntity<Object> handleConflict(TmqaApiCommunicationException ex, WebRequest request) {
        HttpStatus statusCode = ex.getHttpStatus();
        ErrorResponse response = ErrorResponse.builder()
                .requestApi(ex.getTmqaApi().name()) // FIXME name or 명세로 변경 필요
                .httpStatus(ex.getHttpStatus().value())
                .errorCode(ex.getErrorCode())
                .errorMessage(ex.getErrorMessage()).build();
        response.setSuccess(false);
        response.setMessage(String.format("TMQA API - %s 통신 중 오류(%s:%s)가 발생했습니다.", ex.getTmqaApi().name(), statusCode.value(), statusCode.name()));
        return handleExceptionInternal(ex, response, new HttpHeaders(), ex.getHttpStatus(), request);
    }

}
