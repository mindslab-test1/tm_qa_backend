package maum.biz.qa.eai.common.exception;

import maum.biz.qa.eai.common.enums.TmqaApi;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * maum.biz.qa.eai.common.exception
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
@Getter
public class TmqaApiCommunicationException extends Exception implements Serializable {

    private static final long serialVersionUID = -6595348592002863866L;

    private final TmqaApi tmqaApi;
    private final HttpStatus httpStatus;
    private final String errorCode;
    private final String errorMessage;

    public TmqaApiCommunicationException(TmqaApi tmqaApi, HttpStatus httpStatus, String errorCode, String errorMessage) {
        super(httpStatus.getReasonPhrase());
        this.tmqaApi = tmqaApi;
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

}
