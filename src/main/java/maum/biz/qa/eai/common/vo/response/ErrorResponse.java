package maum.biz.qa.eai.common.vo.response;

import lombok.*;

/**
 * maum.biz.qa.eai.common.vo.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-01
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResponse extends ApiResponse {

    private String requestApi;
    private int httpStatus;
    private String errorCode;
    private String errorMessage;

}
